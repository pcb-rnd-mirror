-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: pcb
Binary: pcb, pcb-common, pcb-gtk, pcb-lesstif
Architecture: any all
Version: 20110918-9
Maintainer: Debian Electronics Team <pkg-electronics-devel@lists.alioth.debian.org>
Uploaders: Hamish Moffatt <hamish@debian.org>, Wesley J. Landaker <wjl@icecavern.net>, Ramakrishnan Muthukrishnan <rkrishnan@debian.org>, أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@sabily.org>
Homepage: http://pcb.gpleda.org/
Standards-Version: 3.9.4
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-electronics/pcb.git
Vcs-Git: git://anonscm.debian.org/pkg-electronics/pcb.git
Build-Depends: debhelper (>= 9), bison, flex, libgtkglext1-dev, tk8.5, libgd-dev, libdbus-1-dev, libmotif-dev, libxmu-dev, libxml-parser-perl, intltool, imagemagick, gerbv
Package-List: 
 pcb deb electronics optional
 pcb-common deb electronics optional
 pcb-gtk deb electronics optional
 pcb-lesstif deb electronics optional
Checksums-Sha1: 
 53ca27797d4db65a068b56f157e3ea6c5c29051f 4015128 pcb_20110918.orig.tar.gz
 1295917d424a5d158dcec1dddf7c7a7295d085a5 15163 pcb_20110918-9.debian.tar.gz
Checksums-Sha256: 
 6da47c4f98491c8a9ed010503d4ec1d03473bfc5bb00ea9b7176cb3848a09f1b 4015128 pcb_20110918.orig.tar.gz
 6b4da9f663a0c34d3dda52f7967aa13e026c0513dbc81c16a729c846e4452b94 15163 pcb_20110918-9.debian.tar.gz
Files: 
 54bbc997eeb22b85cf21fed54cb8e181 4015128 pcb_20110918.orig.tar.gz
 b04dda418ee1f0737d62fd40048c6502 15163 pcb_20110918-9.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQEcBAEBCAAGBQJR0nB/AAoJELwZapTt3aG3GkQH/i6UQ8aX5Sc0td/RJ7U3axZf
fBBWBcJgD3t4qCqhAxphuR0w0kJP6eY7u09rnKRjQH1O+qYbUhyHrgTfRyZPcEuy
JFk+eK29RcSSCsPu/KuV/iMfAckcwkykKhtmycnyRyVSvoj4ECc+nUJajD0/mv5y
Q1IYm4CTsUj/OUIGnQyMTAN8y5tFOhP6jYgxMkX0cVoB0G5cltjjkLnR3V9/4wuJ
dyPAjwtRRPXCmbEGS6ep4M24+uUrCA8FpIBw/YeFCNNJYqdoT+JOdDlyrTK8KrNY
Sp9kpljwd4exmL/JiRIyUNLZzfXDzsaQulhkOv0KxBAiKrJzjOk5QFdHj7dbmes=
=z5qJ
-----END PGP SIGNATURE-----
