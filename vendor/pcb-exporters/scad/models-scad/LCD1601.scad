
include <proto/lcd_module.scad>

lcd_module(
		W=80,
		H=36,
		t=1.76,
		BW=71.2,
		BH=26.2,
		DW=66,
		DH=16,
		th=8.9,
		dh=4.8,
		cpd=5,
		chd=2.5,
		cx=75,
		cy=31,
		px=8,
		py=2,
		ppd=2,
		phd=1,
		pdt=2.54,
		n=16
);
