
include <proto/soic.scad>

soic(
	N=16,
	L=6.0,
	P=1.27,
	W=0.45,
	H=1.73,
	T=0.7,
	A=3.9,
	B=9.9,
	K=0.1
);