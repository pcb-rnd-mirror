/*

DIP 28, 0.300mil

*/

include <proto/dip.scad>

translate ([3.81,-16.51,0]) dip(
	N=28,
	P=2.54,
	TC=7.62,
	W=0.45,
	T=0.32,
	A=6.5,
	B=35.5,
	H=5.33,
	K=0.38,
	h=3.55
);