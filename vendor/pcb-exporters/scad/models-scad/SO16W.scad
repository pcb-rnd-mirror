
include <proto/soic.scad>

soic(
	N=16,
	L=10.32,
	P=1.27,
	W=0.42,
	H=2.65,
	T=1.0,
	A=7.5,
	B=10.3,
	K=0.1
);