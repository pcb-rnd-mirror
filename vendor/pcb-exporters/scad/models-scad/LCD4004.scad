
include <proto/lcd_module.scad>

lcd_module(
		W=190,
		H=54,
		t=1.6,
		BW=166.2,
		BH=42,
		DW=147,
		DH=29.5,
		th=13.6,
		dh=9.0,
		cpd=5,
		chd=3.5,
		cx=183,
		cy=47,
		px=10,
		py=2.5,
		ppd=2,
		phd=1,
		pdt=2.54,
		n=0
);
