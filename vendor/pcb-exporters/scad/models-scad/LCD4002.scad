
include <proto/lcd_module.scad>

lcd_module(
		W=182,
		H=33.5,
		t=1.6,
		BW=161.4,
		BH=28.9,
		DW=154.4,
		DH=16.5,
		th=13.1,
		dh=9.0,
		cpd=5.5,
		chd=3.5,
		cx=175,
		cy=26.5,
		px=10,
		py=2.5,
		ppd=2,
		phd=1,
		pdt=2.54,
		n=0
);
