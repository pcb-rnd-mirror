/*

DIP 22, 0.400mil

*/

include <proto/dip.scad>

translate ([5.08,-12.7,0]) dip(
	N=22,
	P=2.54,
	TC=10.16,
	W=0.45,
	T=0.32,
	A=9.14,
	B=27.7,
	H=6.35,
	K=0.38,
	h=5.08
);