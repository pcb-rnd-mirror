/*

DIP 14, 0.300mil

*/

include <proto/dip.scad>

translate ([3.81,-7.62,0]) dip(
	N=14,
	P=2.54,
	TC=7.62,
	W=0.45,
	T=0.32,
	A=6.5,
	B=19,
	H=5.33,
	K=0.38,
	h=3.55
);