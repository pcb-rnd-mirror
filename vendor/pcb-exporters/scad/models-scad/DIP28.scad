/*

DIP 28, 0.600mil

*/

include <proto/dip.scad>

translate ([7.62,-16.51,0]) dip(
	N=28,
	P=2.54,
	TC=15.24,
	W=0.45,
	T=0.32,
	A=13.525,
	B=37.4,
	H=6.35,
	K=0.38,
	h=5.08
);