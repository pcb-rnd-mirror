/*

DIP 24, 0.400mil

*/

include <proto/dip.scad>

translate ([5.08,-13.97,0]) dip(
	N=24,
	P=2.54,
	TC=10.16,
	W=0.45,
	T=0.32,
	A=9.14,
	B=30,
	H=5.33,
	K=0.38,
	h=3.55
);