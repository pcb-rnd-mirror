
include <proto/soic.scad>

soic(
	N=14,
	L=10.32,
	P=1.27,
	W=0.39,
	H=1.73,
	T=1.0,
	A=7.5,
	B=9.0,
	K=0.1
);