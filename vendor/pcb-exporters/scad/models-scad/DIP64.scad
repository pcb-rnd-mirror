/*

DIP 64, 0.900mil

*/

include <proto/dip.scad>

translate ([11.43,-39.37,0]) dip(
	N=64,
	P=2.54,
	TC=22.86,
	W=0.45,
	T=0.32,
	A=21.7,
	B=81.0,
	H=4.83,
	K=0.51,
	h=4.45
);