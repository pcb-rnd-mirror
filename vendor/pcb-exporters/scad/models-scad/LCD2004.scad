
include <proto/lcd_module.scad>

lcd_module(
		W=98,
		H=60,
		t=1.6,
		BW=95.2,
		BH=38,
		DW=77,
		DH=25.2,
		th=13.1,
		dh=9.0,
		cpd=5,
		chd=2.5,
		cx=93,
		cy=55,
		px=10,
		py=2.5,
		ppd=2,
		phd=1,
		pdt=2.54,
		n=16
);
