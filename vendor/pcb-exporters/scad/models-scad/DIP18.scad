/*

DIP 18, 0.300mil

*/

include <proto/dip.scad>

translate ([3.81,-10.16,0]) dip(
	N=18,
	P=2.54,
	TC=7.62,
	W=0.45,
	T=0.32,
	A=6.5,
	B=22,
	H=5.33,
	K=0.38,
	h=3.55
);