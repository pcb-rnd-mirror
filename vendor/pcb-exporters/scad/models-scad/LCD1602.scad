
include <proto/lcd_module.scad>

lcd_module(
		W=80,
		H=36,
		t=1.6,
		BW=71.2,
		BH=25.2,
		DW=66,
		DH=16,
		th=12.7,
		dh=8.6,
		cpd=5,
		chd=2.5,
		cx=75,
		cy=31,
		px=8,
		py=2,
		ppd=1.8,
		phd=1,
		pdt=2.54,
		n=16
);
