
include <proto/soic.scad>

soic(
	N=14,
	L=6.0,
	P=1.27,
	W=0.45,
	H=1.73,
	T=0.7,
	A=3.92,
	B=8.6,
	K=0.1
);