/*

DIP 40, 0.600mil

*/

include <proto/dip.scad>

translate ([7.62,-24.13,0]) dip(
	N=40,
	P=2.54,
	TC=15.24,
	W=0.45,
	T=0.32,
	A=14.5,
	B=50.3,
	H=4.83,
	K=0.51,
	h=4.45
);