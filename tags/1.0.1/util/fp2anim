#!/bin/sh

photo=0
grid_unit="mil"

while test $# -gt 0
do
	case $1 in
		--photo|p) photo=1;;
		--grid-unit|--grid_unit|g) grid_unit=$2;;
		--mm) grid_unit="mm";;
		--diamond) diamond=1;;
	esac
	shift 1
done



awk -v "photo=$photo" -v "grid_unit=$grid_unit" -v "diamond=$diamond" '

BEGIN {
	q="\""

	ds = 100*20

	if (photo) {
		clr_board[-1] = "#003300"
		clr_board[0]  = "#006600"
		clr_board[1]  = "#22AA22"

		clr_plated[-1] = "#555555"
		clr_plated[0]  = "#777777"
		clr_plated[1]  = "#999999"

		clr_silk[-1] = "#AAAAAA"
		clr_silk[0] = "#CCCCCC"
		clr_silk[1] = "#FFFFFF"

		clr_solderstop[0] = "#00BB00"


		clr_hole[0] = "black"
		clr_annotation[0] = "red"
		clr_grid[0] = "blue"
	}
	else {
		clr_board[0] = "darkgreen"
		clr_plated[0] = "#777777"
		clr_hole[0] = "white"
		clr_annotation[0] = "red"
		clr_silk[0] = "black"
		clr_solderstop[0] = "white"
		clr_grid[0] = "blue"
	}

	marg=100000
}


function edges(x, y)
{
	if ((xmin == "") || (x < xmin))
		xmin = x
	if ((ymin == "") || (y < ymin))
		ymin = y
	if ((xmax == "") || (x > xmax))
		xmax = x
	if ((ymax == "") || (y > ymax))
		ymax = y
}

# draw a line with rounded ends
# do not draw end circles if omit_ends is non-zero
# extend both ends by length_bloat
function rline(x1, y1, x2, y2, width    ,omit_ends , length_bloat    ,nx,ny,vx,vy, len)
{

	nx = y2-y1
	ny = -(x2-x1)

	len=sqrt(nx*nx+ny*ny)
	if (len != 0) {
		nx /= len
		ny /= len
		if (length_bloat != 0) {
			vx = -ny
			vy = nx
			x1 -= length_bloat * vx
			x2 += length_bloat * vx
			y1 -= length_bloat * vy
			y2 += length_bloat * vy
		}
		print "poly", x1+nx*width/2, y1+ny*width/2, x2+nx*width/2, y2+ny*width/2, x2-nx*width/2, y2-ny*width/2, x1-nx*width/2, y1-ny*width/2
	}
	if (!omit_ends) {
		print "fillcircle", x1, y1, width/2, width/10
		print "fillcircle", x2, y2, width/2, width/10
	}
	edges(x1, y1)
	edges(x2, y2)
}

function deg2rad(d)
{
	return d/180*3.141592654
}

function rarc(cx, cy, rx, ry, a_start, a_delta, width   ,a1, a2)
{
	if (a_delta < 0) {
		a_start = 180-a_start
		a_delta = -a_delta
	}
	step = a_delta * 100 * 2 / (((rx > ry) ? rx : ry) + 1)
	for(a1 = a_start; a1 < a_start + a_delta - step; a1 += step) {
		a2 = a1+step
		rline( cx - rx * cos(deg2rad(a1)), cy + ry * sin(deg2rad(a1)),
		       cx - rx * cos(deg2rad(a2)), cy + ry * sin(deg2rad(a2)),
		       width, 0)
	}

	a1 = a2;
	a2 = a_start+a_delta
	rline( cx - rx * cos(deg2rad(a1)), cy + ry * sin(deg2rad(a1)),
	       cx - rx * cos(deg2rad(a2)), cy + ry * sin(deg2rad(a2)),
	       width, 0)

}

function rsquare(x1, y1, x2, y2, r   ,tmp)
{
	if (x1 < x2) {
		tmp=x1
		x1 = x2
		x2 = tmp
	}
	if (y1 < y2) {
		tmp=y1
		y1 = y2
		y2 = tmp
	}

	print "poly", x1, y1-r,x1-r, y1,  x2+r,y1, x2, y1-r,    x2, y2+r,   x2+r,y2,     x1-r, y2,  x1, y2+r
	print "fillcircle", x1-r, y1-r, r, r/10
	print "fillcircle", x2+r, y2+r, r, r/10
	print "fillcircle", x2+r, y1-r, r, r/10
	print "fillcircle", x1-r, y2+r, r, r/10
}

function flash(x, y, dia, square, roundr)
{
	if (square) {
		if (roundr != "")
			rsquare(x-dia/2, y-dia/2, x+dia/2, y+dia/2, roundr)
		else
			print "fillrect", x-dia/2, y-dia/2, dia, dia
	}
	else
		print "fillcircle", x, y, dia/2, dia/10
}

function hole(x, y, ring, clr, mask, drill, name, square)
{
	if (photo) {
		print "macro push mask"
		flash(x, y, (ring+clr), square, (ring+clr)/10)
		print "endmacro"
	}

	print "macro push copper"
	flash(x, y, ring, square)
	print "endmacro"

	has_hole=1
	print "macro push hole"
	flash(x, y, drill, 0)
	print "endmacro"

	print "macro push annotation"
	print "text", x, y, name
	print "endmacro"

	edges(x-(ring+clr)/2, y-(ring+clr)/2)
	edges(x+(ring+clr)/2, y+(ring+clr)/2)
}

function pad(x1, y1, x2, y2, thickness, clr, mask, name, square)
{
	if (photo) {
		print "macro push mask"
		rline(x1, y1, x2, y2, thickness+clr, square, clr/2)
		print "endmacro"
	}


	print "macro push copper"
	rline(x1, y1, x2, y2, thickness, square)
	print "endmacro"

	print "macro push annotation"
	print "text", (x2+x1)/2, (y2+y1)/2, name
	print "endmacro"

}

/^[ \t]*#/ { next }

/ElementLine[[]/ {
	sub(".*ElementLine[[]", "", $0)
	sub("]$", "", $0)

	print "macro push silk"
	rline($1, $2, $3, $4, $5)
	print "endmacro"

	next
}

/ElementLine[(]/ {
	sub(".*ElementLine[(]", "", $0)
	sub(")$", "", $0)

	print "macro push silk"
	rline($1*100, $2*100, $3*100, $4*100, $5*100)
	print "endmacro"

	next
}



/ElementArc[[]/ {
	sub(".*ElementArc[[]", "", $0)
	sub("]$", "", $0)

	print "macro push silk"
	rarc($1, $2,  $3, $4,  $5, $6,  $7)
	print "endmacro"

	next
}

#   ElementArc(0 0 59 59    45  90 10)
/ElementArc[(]/ {
	sub(".*ElementArc[(]", "", $0)
	sub(")$", "", $0)

	print "macro push silk"
	rarc($1*100, $2*100,  $3*100, $4*100,  $5, $6,  $7*100)
	print "endmacro"

	next
}


#	Pin[0 0 8000 5000 8600 3937 "" "1" "square"]
/Pin[[]/ {
	sub(".*Pin[[]", "", $0)
	sub("]$", "", $0)
	hole($1, $2, $3, $4, $5, $6, $8, ($9 ~ "square"))
}

#	Pin(300 800 90 60 "3" 0x01)
/Pin[(]/ {
	sub(".*Pin[(]", "", $0)
	sub("]$", "", $0)
	hole($1*100, $2*100, $3*100, $4*100, 3000, 2000, $5, ($6 ~ "0x10"))
}

#	Pad[ 0 0 0 0 0 5000 8600 "" "4" ""]
/Pad[[]/ {
	sub(".*Pad[[]", "", $0)
	sub("]$", "", $0)

	pad($1, $2, $3, $4, $5, $6, $7, $9,   1)
}


function layer_3d(name, color,  offs)
{
	if ((offs == "") || (offs == 1))
		offs = 150
	else if (offs == -1)
		offs = -150

	if (1 in color) {
		print "push"
		print "shift", (-1*offs), (-1*offs)
		print "color", color[1]
		print "invoke ", name
		print "pop"
	}

	if (-1 in color) {
		print "push"
		print "shift", (offs), (offs)
		print "color", color[-1]
		print "invoke ", name
		print "pop"
	}

	if (0 in color) {
		print "color", color[0]
		print "invoke ", name
	}
}

function draw_grid()
{
	if (grid_unit == "mil") {
		gstep = 10000
		gmul = 1/100
	}
	else {
		gstep = 3937
		gmul = 1/3937
	}

	while ((size/gstep) > 10)
		gstep=gstep*2

	for(x = 0 - int(marg/gstep)*gstep; x < size+marg; x+=gstep) {
		print "line", x, ymin-marg, x, ymin+size+marg
		print "line", xmin-marg, x, xmin+size+marg, x
		print "text", x, ymax+gstep/2, q x*gmul "\\n" grid_unit q
		print "text", xmax+gstep/2, x, q x*gmul "\\n" grid_unit q
	}

}

END {
	print "frame"

	sx = xmax-xmin
	sy = ymax-ymin

	size = sx > sy ? sx : sy

	print "scale 1 -1"
	print "viewport", xmin, ymin, "-", xmin+size, ymin+size
	print "bg", clr_solderstop[0]
	print "shift", (size-sx)/2, (size-sy)/2
	print "scale 0.85"


	print "dash 0xaaaa"
	print "color", clr_grid[0]

	if (grid_unit != "")
		draw_grid()

	print "dash 0xffff"

	if (diamond) {
		print "macro push annotation"
		print "lines", -ds,0,   0,ds,   ds,0,   0,-ds,   -ds,0
		print "endmacro"
	}


	if (photo) {
		layer_3d("mask", clr_board, -1)
	}


	print "color", clr_plated[0]
	print "invoke copper"
	layer_3d("copper", clr_plated, 1)
	if (has_hole) {
		layer_3d("hole", clr_hole, -1)
	}


	layer_3d("silk", clr_silk)

	print "color", clr_annotation[0]
	print "invoke annotation"

	print "flush"
}

'
