#ifndef PCB_LREALPATH_H
#define PCB_LREALPATH_H

/* A well-defined realpath () that is always compiled in.  */
char *pcb_lrealpath(const char *);

#endif /* PCB_LREALPATH_H */
