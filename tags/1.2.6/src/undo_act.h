/* Publish actions - these may be useful for other actions */
int pcb_act_Undo(int argc, const char **argv, pcb_coord_t x, pcb_coord_t y);
int pcb_act_Redo(int argc, const char **argv, pcb_coord_t x, pcb_coord_t y);
int pcb_act_Atomic(int argc, const char **argv, pcb_coord_t x, pcb_coord_t y);
