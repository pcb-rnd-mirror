#include <gtk/gtk.h>

GtkWidget *ghid_scrolled_text_view(GtkWidget * box, GtkWidget ** scr, GtkPolicyType h_policy, GtkPolicyType v_policy);

void ghid_text_view_append(GtkWidget * view, const gchar * string);
