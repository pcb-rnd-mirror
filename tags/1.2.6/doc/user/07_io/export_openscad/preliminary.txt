The openscad exporter generates the 3D model of the board in form of 
an openscad script.

The main entry point of the script is pcb_board(). All coordinates are in
mm. The in x and y directions the 0;0 point of the board is the 0;0 point on
screen in pcb-rnd, to make it easy to wrap the board in a translate() after
measuring the coords of a point in pcb-rnd. The vertical origin is the middle
of the board's FR4.

Supported features:
	- arbitrary shaped outline
	- top/bottom copper (optional)
	- top/bottom mask (optional)
	- top/bottom silk (optional)
	- holes (optional)
	- element models (optional)

Element models are loaded from external files referenced from the element
in the pcb-rnd board file. Model files are searched recursively under the
footprint library search path.

The element (footprint instance) can have the following openscad related
attributes, all optional:

 - openscad=filename                  file name of the .scad script (3d element model)
 - openscad-transformation=script     apply the specified transformation on the model instance
 - openscad-param=p1,p2,p3...         parameters to pass on to the model

If the openscad attribute is not present, no 3d model is inserted.

If openscad-transformation is specified, it is inserted before the model
call; it typically contains a transform() and/or a rotate() to modify the
placement of the model. The 0;0;0 point of the placement is the origin of
the element in pcb-rnd, on the surface of the board.

If openscad-param is specified, the module is called with this parameter
list, else the model is called with empty parameter list.

A model file shall contain a single module, with "module" and the arbitrary
module name in the same line, { in the following line. One instance of
each model is copied into the output file so that the resulting openscad
script has no external reference and is self-contained.


Known bugs:
	- with pcb elements it's impossible to tell the rotation; this will
	  be fully solved only with the subcircuit transition. Until that you
	  will need to manually specify an openscd-transformation for each element
