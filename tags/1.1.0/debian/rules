#!/usr/bin/make -f
#                                          -*- makefile -*-
# debian/rules file for the Debian/GNU Linux pcb package
# Copyright 1997-99 by Hartmut Koptein <koptein@debian.org>

package = pcb-rnd

CONFIGURE_OPTS=--disable-doc --disable-gl --disable-rpath --disable-dbus --disable-update-desktop-database --disable-update-mime-database --enable-dependency-tracking --disable-coord64 LDFLAGS="$(LDFLAGS) -Wl,--as-needed"

%:
	dh $@

override_dh_auto_configure:
	./configure --prefix=/usr

override_dh_auto_build:
	make
#	dh_auto_build --builddirectory build_gtk
#	dh_auto_build --builddirectory build_lesstif

override_dh_auto_test:
	dh_auto_test --builddirectory build_gtk
#	dh_auto_test --builddirectory build_lesstif

override_dh_auto_install:
	make install install_root=`pwd`/debian/tmp

override_dh_auto_clean:
	dh_auto_clean --builddirectory build_gtk
#	dh_auto_clean --builddirectory build_lesstif
	-make distclean

override_dh_install:
	# Remove needlessly installed static library and header file before
	# installing common files:
	rm -rf $(CURDIR)/debian/tmp/usr/lib
	rm -rf $(CURDIR)/debian/tmp/usr/include
	dh_install -Xusr/bin -Xusr/share/pcb-rnd- -Xusr/share/doc -Xexamples -Xtutorial -Xusr/share/info

	# Install pcb-gtk binary:
	install debian/tmp/usr/bin/pcb-rnd debian/$(package)-gtk/usr/bin/pcb-rnd-gtk


	# Install pcb-lesstif binary:
#	install build_lesstif/src/pcb-rnd debian/$(package)-lesstif/usr/bin/pcb-rnd-lesstif

	# Install common binaries:
	mkdir debian/$(package)-common/usr/bin
	install debian/tmp/usr/bin/gsch2pcb-rnd debian/$(package)-common/usr/bin/gsch2pcb-rnd

	# Set executable bit for pcb tools:
	-[ ! -d debian/$(package)-common ] || chmod a+x debian/$(package)-common/usr/share/pcb-rnd/tools/MergePCBPS
	-[ ! -d debian/$(package)-common ] || chmod a+x debian/$(package)-common/usr/share/pcb-rnd/tools/Merge_dimPCBPS
	-[ ! -d debian/$(package)-common ] || chmod a+x debian/$(package)-common/usr/share/pcb-rnd/tools/tgo2pcb.tcl
	-[ ! -d debian/$(package)-common ] || chmod a+x debian/$(package)-common/usr/share/pcb-rnd/tools/PCB2HPGL

	# Remove empty dirs:
	[ ! -d debian/$(package)-common ] || find debian/$(package)-common -type d -empty -delete

override_dh_fixperms:
	dh_fixperms
	# Fix permissions of a couple of example files:
#	-[ ! -d debian/$(package)-common ] || chmod -x debian/$(package)-common/usr/share/doc/$(package)-common/examples/LED.pcb
#	-[ ! -d debian/$(package)-common ] || chmod -x debian/$(package)-common/usr/share/doc/$(package)-common/examples/LED.net

override_dh_installexamples:
	dh_installexamples -XMakefile

override_dh_installchangelogs:
	dh_installchangelogs -p$(package)-common

override_dh_installdocs:
	# Only install docs in $(package)-common & link other packages' docs to
	# $(package)-common:
	dh_installdocs --link-doc=$(package)-common

override_dh_compress:
	# exclude example files from compression
	dh_compress -X.pcb -XLED
