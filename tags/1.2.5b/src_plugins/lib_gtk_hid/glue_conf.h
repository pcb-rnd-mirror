#include "conf.h"

void ghid_confchg_line_refraction(conf_native_t *cfg);
void ghid_confchg_all_direction_lines(conf_native_t *cfg);
void ghid_confchg_fullscreen(conf_native_t *cfg);
void ghid_confchg_checkbox(conf_native_t *cfg);
void ghid_confchg_flip(conf_native_t *cfg);
void ghid_confchg_grid(conf_native_t *cfg);

void ghid_conf_regs(const char *cookie);
