/*
 *                            COPYRIGHT
 *
 *  pcb-rnd, interactive printed circuit board design
 *  Copyright (C) 2016 Tibor 'Igor2' Palinkas
 *
 *  This module, layer_ui.c, was written and is Copyright (C) 2016 by
 *  Tibor 'Igor2' Palinkas.
 *  this module is also subject to the GNU GPL as described below
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: email to pcb-rnd (at) igor2.repo.hu
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* Virtual layers for UI and debug */
#include "config.h"
#include "layer.h"
#include "event.h"
#define GVT_DONT_UNDEF
#include "layer_ui.h"
#include <genvector/genvector_impl.c>

vtlayer_t pcb_uilayer;

pcb_layer_t *pcb_uilayer_alloc(const char *cookie, const char *name, const char *color)
{
	int n;
	pcb_layer_t *l;

	if (cookie == NULL)
		return NULL;

	for(n = 0; n < vtlayer_len(&pcb_uilayer); n++) {
		l = &pcb_uilayer.array[n];
		if (l->meta.real.cookie == NULL) {
			l->meta.real.cookie = cookie;
			goto found;
		}
	}

	l = vtlayer_alloc_append(&pcb_uilayer, 1);
found:;
	l->meta.real.cookie = cookie;
	l->meta.real.color = color;
	l->name = name;
	l->meta.real.vis = 1;
	pcb_event(PCB_EVENT_LAYERS_CHANGED, NULL);
	return l;
}

static void pcb_uilayer_free_(pcb_layer_t *l)
{
	pcb_layer_free(l);
	l->meta.real.cookie = NULL;
	l->meta.real.color = l->name = NULL;
	l->meta.real.vis = 0;
}

void pcb_uilayer_free(pcb_layer_t *ly)
{
	int n;
	for(n = 0; n < vtlayer_len(&pcb_uilayer); n++) {
		pcb_layer_t *l = &pcb_uilayer.array[n];
		if (l == ly) {
			pcb_uilayer_free_(l);
			break;
		}
	}
	pcb_event(PCB_EVENT_LAYERS_CHANGED, NULL);
}

void pcb_uilayer_free_all_cookie(const char *cookie)
{
	int n;
	for(n = 0; n < vtlayer_len(&pcb_uilayer); n++) {
		pcb_layer_t *l = &pcb_uilayer.array[n];
		if (l->meta.real.cookie == cookie)
			pcb_uilayer_free_(l);
	}
	pcb_event(PCB_EVENT_LAYERS_CHANGED, NULL);
}

