<title>fp2anim</title>
<sect>1</sect>
<date>2016-12-27</date>

<name> fp2anim - convert a gEDA/PCB footprint to animator script </name>
<syn> <call>fp2anim</call> [<arg>options</arg>] [<arg>inputfile</arg>] </syn>

<description>
<call>fp2anim</call> converts a footprint to a script that can be run through
animator(1). It's main use is to have a automated, light-weight footprint
preview generator on hosts where installing pcb-rnd would be too expensive
(e.g. on web servers).
<p>
If <arg>inputfile</arg> is not specified, <call>fp2anim</call> reads its
STDIN for the footprint.
</description>

<options>
<kvlist>

	<item>
		<key> --photo </key>
		<value> draw the footprint in "photo realistic mode" </value>
	</item>
	<item>
		<key> -p </key>
		<value> same as --photo
	</item>

	<item>
		<key> --grid unit </key>
		<value> set grid unit e.g. to mm or mil </value>
	</item>
	<item>
		<key> -g unit </key>
		<value> same as --grid
	</item>

	<item>
		<key> --mm </key>
		<value> use an mm grid instead of a mil grid; shorthand for -g mm</value>
	</item>

	<item>
		<key> --diamond </key>
		<value> draw a small diamond to mark at footprint origin</value>
	</item>

	<item>
		<key> --annotation types </key>
		<value> turn on annotation types; types is a string that may consist
		        "pins" for pin numbers, "dimname" and "dimvalue" for dimension
		        names and values and "background" for drawing a background under
		        annotation text </value>
	</item>

</kvlist>
</options>
