GPMI system-wide installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The installation process is somewhat manual at this early stage. Please 
follow the steps below.

First, you'll need libgpmi - it's a general purpose script glue interface.

A/1. svn checkout svn://repo.hu/gpmi/trunk gpmi/trunk
A/2. look at doc/languages.txt there; if you have preferred languages, 
please install the -dev packages of those. My favorite is mawk, but I 
think most of the example pcb scripts would be in lua. Versions should 
match; it may be that gpmi works with a newer minor version out of the 
box, tho.
A/3. run ./configure
A/4. check if it found the language(s) you wanted; if not, or anything 
else breaks, please send me your scconfig/config.log
A/5. make
A/6. make install (as root, but see below)

There are options to step 6:

- if you want to check what it'd do, you could run this:

   install_root=/tmp/foo make install

   it's like DESTDIR for autotools: it will do everything but will use
   /tmp/foo instead of / so you see what it'd do without using root or
   messing with /usr

- you can use make linstall instead of make install; this creates symlinks 
(pointing to the binaries in your user checkout) instead of copying the 
files. If gpmi changes and you need to update and recompile, you won't 
need to reinstall (until the number of files change).

- there's a make uninstall, but it's more or less untested (failed once 
and worked once for me)

Second, you'll need pcb-rnd:

B/1. svn checkout svn://repo.hu/pcb-rnd/trunk pcb-rnd/trunk
B/2. ./configure
B/3. check the output; if anything went wrong, please send me 
scconfig/config.log
B/4. make
B/5. you don't need to install it for testing: cd src; ./pcb-rnd


There are options in step 2: by default, if gpmi is found, it's compiled
in "buildin" mode, which means script support is compiled into the pcb-rnd
executable. An alternative is to have it as a plugin (dynamic linkable
object). This can be done using the --plugin-gpmi argument for ./configure.


GPMI local installation
~~~~~~~~~~~~~~~~~~~~~~~
GPMI can be installed without root, into a local user directory (typically
under home). To achieve this, follow the above steps with the following
modifications (assuming you want to install gpmi in ~/usr):

A/3. ./configure --prefix=~/usr
B/2. ./configure --gpmi-prefix=~/usr

NOTE: you don't need to use install_root; gpmi will install under ~/usr as
if it was /usr. 
