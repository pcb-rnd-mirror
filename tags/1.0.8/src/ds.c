#include <string.h>
#include <ds.h>

/* ---------------------------------------------------------------------------
 * reallocates memory for a dynamic length string if necessary
 */
static void DSRealloc(DynamicStringTypePtr Ptr, size_t Length)
{
	int input_null = (Ptr->Data == NULL);
	if (input_null || Length >= Ptr->MaxLength) {
		Ptr->MaxLength = Length + 512;
		Ptr->Data = (char *) realloc(Ptr->Data, Ptr->MaxLength);
		if (input_null)
			Ptr->Data[0] = '\0';
	}
}

/* ---------------------------------------------------------------------------
 * adds one character to a dynamic string
 */
void DSAddCharacter(DynamicStringTypePtr Ptr, char Char)
{
	size_t position = Ptr->Data ? strlen(Ptr->Data) : 0;

	DSRealloc(Ptr, position + 1);
	Ptr->Data[position++] = Char;
	Ptr->Data[position] = '\0';
}

/* ---------------------------------------------------------------------------
 * add a string to a dynamic string
 */
void DSAddString(DynamicStringTypePtr Ptr, const char *S)
{
	size_t position = Ptr->Data ? strlen(Ptr->Data) : 0;

	if (S && *S) {
		DSRealloc(Ptr, position + 1 + strlen(S));
		strcat(&Ptr->Data[position], S);
	}
}

/* ----------------------------------------------------------------------
 * clears a dynamic string
 */
void DSClearString(DynamicStringTypePtr Ptr)
{
	if (Ptr->Data)
		Ptr->Data[0] = '\0';
}
