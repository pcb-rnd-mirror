#!/usr/bin/python
"""
  Generate sample hyperlynx file 
  Copyright 2017 Koen De Vleeschauwer.
 
  This file is part of pcb-rnd.
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from math import pi, sin, cos

print "{VERSION=2.0}"
print "{UNITS=METRIC LENGTH}"
print "{BOARD"
print "  (PERIMETER_SEGMENT X1=1.0 Y1=0.0 X2=19.0 Y2=0.0)"
print "  (PERIMETER_SEGMENT X1=20.0 Y1=1.0 X2=20.0 Y2=9.0)"
print "  (PERIMETER_SEGMENT X1=19.0 Y1=10.0 X2=1.0 Y2=10.0)"
print "  (PERIMETER_SEGMENT X1=0.0 Y1=9.0 X2=0.0 Y2=1.0)"
print "  (PERIMETER_ARC X1=0.0 Y1=1.0 X2=1.0 Y2=0.0 XC=1.0 YC=1.0 R=1.0)"
print "  (PERIMETER_ARC X1=1.0 Y1=10.0 X2=0.0 Y2=9.0 XC=1.0 YC=9.0 R=1.0)"
print "  (PERIMETER_ARC X1=20.0 Y1=9.0 X2=19.0 Y2=10.0 XC=19.0 YC=9.0 R=1.0)"
print "  (PERIMETER_ARC X1=19.0 Y1=0.0 X2=20.0 Y2=1.0 XC=19.0 YC=1.0 R=1.0)"
print ""
print "  (PERIMETER_SEGMENT X1=2.0 Y1=1.0 X2=3.0 Y2=1.0)"
print "  (PERIMETER_SEGMENT X1=3.0 Y1=9.0 X2=2.0 Y2=9.0)"
print "  (PERIMETER_ARC X1=2.0 Y1=9.0 X2=1.0 Y2=8.0 XC=2.0 YC=8.0 R=1.0)"
print "  (PERIMETER_SEGMENT X1=1.0 Y1=8.0 X2=1.0 Y2=2.0)"
print "  (PERIMETER_ARC X1=1.0 Y1=2.0 X2=2.0 Y2=1.0 XC=2.0 YC=2.0 R=1.0)"
print "  (PERIMETER_SEGMENT X1=4.0 Y1=2.0 X2=4.0 Y2=8.0)"
print "  (PERIMETER_ARC X1=3.0 Y1=1.0 X2=4.0 Y2=2.0 XC=3.0 YC=2.0 R=1.0)"
print "  (PERIMETER_ARC X1=4.0 Y1=8.0 X2=3.0 Y2=9.0 XC=3.0 YC=8.0 R=1.0)"

print "  (PERIMETER_ARC X1=5.5 Y1=5.0 X2=5.5 Y2=5.0 XC=5.0 YC=5.0 R=0.5)"

print "  (PERIMETER_ARC X1=5.5 Y1=6.0 X2=4.5 Y2=6.0 XC=5.0 YC=6.0 R=0.5)"
print "  (PERIMETER_SEGMENT X1=4.5 Y1=6.0 X2=5.5 Y2=6.0)"

print "  (PERIMETER_ARC X1=4.5 Y1=7.5 X2=5.5 Y2=7.5 XC=5.0 YC=7.5 R=0.5)"
print "  (PERIMETER_SEGMENT X1=4.5 Y1=7.5 X2=5.5 Y2=7.5)"

print "  (PERIMETER_ARC X1=5.5 Y1=8.5 X2=5.0 Y2=8.0 XC=5.0 YC=8.5 R=0.5)"
print "  (PERIMETER_SEGMENT X1=5.0 Y1=8.0 X2=5.5 Y2=8.5)"

print "}"
print "{STACKUP"
print "  (SIGNAL T=0.000700 L=component)"
print "  (DIELECTRIC T=0.002000 C=4.000000)"
print "  (PLANE  T=0.000700 L=solder PS=0.001000)"
print "}"
print "{DEVICES"
print "}"

#
# PADSTACK. MDEF is shorthand for "all copper layers"
#

print "{PADSTACK=roundpad, 0.3"
print "  (MDEF, 0, 0.5, 0.5, 0)"
print "}"
print "{PADSTACK=squarepad, 0.3"
print "  (MDEF, 1, 0.5, 0.5, 0)"
print "}"
print "{PADSTACK=oblongpad, 0.3"
print "  (MDEF, 2, 0.5, 0.5, 0)"
print "}"
print "{PADSTACK=nodrill,"
print "  (MDEF, 0, 0.5, 0.5, 0)"
print "}"

#
# SEG line segments
#

x0 = 6
y0 = 3
r1 = 0.5
r2 = 2
max = 12

print "{NET=segtst"

for x in range (0, max):
  alpha = x * 2 * pi / max
  x1 = x0 + r1 * cos (alpha)
  y1 = y0 + r1 * sin (alpha)
  r = r1 + (r2 - r1) * (x + 1) / max
  x2 = x0 + r * cos (alpha)
  y2 = y0 + r * sin (alpha)
  w = (x + 1) * 0.1 / max
  print "  (SEG X1=%f Y1=%f X2=%f Y2=%f W=%f L=component)" % ( x1, y1, x2, y2, w)
  
print "}"

#
# ARC arc segments
#

max = 8
x0 = 9
y0 = 1
r = 0.1
w = r / 4

print "{NET=arctst"

for a in range (0, max):
  for b in range (0, max):
    alpha = 2 * pi * a / max
    beta = 2 * pi * b / max
    xc = x0 + 4  * r * a 
    yc = y0 + 4  * r * b
    x1 = xc + r * cos (alpha)
    y1 = yc + r * sin (alpha)
    x2 = xc + r * cos (beta)
    y2 = yc + r * sin (beta)
    print "  (ARC X1=%f Y1=%f X2=%f Y2=%f XC=%f YC=%f R=%f W=%f L=component)" % ( x1, y1, x2, y2, xc, yc, r, w)
  
print "}"

#
# VIA
#

print "{NET=viatst"
print "  (VIA X=13 Y=1 P=roundpad)"
print "  (VIA X=13 Y=2 P=squarepad)"
print "  (VIA X=13 Y=3 P=oblongpad)"
print "  (VIA X=13 Y=4 P=nodrill)"
print "}"

print "{END}"
