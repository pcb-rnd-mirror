#include <gdk/gdk.h>

const gchar *ghid_get_color_name(GdkRGBA * color);
void ghid_map_color_string(const char *color_string, GdkRGBA * color);
