#include <gtk/gtk.h>
extern const char pcb_gtk_acts_print[];
extern const char pcb_gtk_acth_print[];
int pcb_gtk_act_print(GtkWidget *top_window, int argc, const char **argv, pcb_coord_t x, pcb_coord_t y);

extern const char pcb_gtk_acts_printcalibrate[];
extern const char pcb_gtk_acth_printcalibrate[];
int pcb_gtk_act_printcalibrate(int argc, const char **argv, pcb_coord_t x, pcb_coord_t y);

