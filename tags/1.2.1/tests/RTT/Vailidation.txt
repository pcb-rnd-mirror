 Validation state of references
#
# Each line is a file from ref/, in the following format:
#  The first word is the file name, relative to ref/
#  The second word is either "good" or "bad"
#  The third word is the revision number the output file had at the moment of
#   check (the "Last Changed Revision line in 'svn info ref/filename')
#  The rest of the line is a real short summary on whaty's broken, if the
#   second column was "bad".
#
# There's no particular order of lines. If you recheck a file, edit the exiting
# line, each ref file should have only one line in this file. If you are doing
# checks, please insert new lines somewhere in the list, preferrably not at the
# end, so we can avoid svn conflicts in concurrent edits.
#
# Test method: open the .pcb file with pcb-rnd, open the ref/ file with
# the appropriate viewer, look at them side by side and decide if the reference
# output fully matches the input, in all relevant details. What relevent
# details are can be file format specific.
#

Proto.bom good r5452
coord_rounding.bom good r5652
elem_pads.bom good r5652
elem_pads_ds.bom good r5652
elem_pins.bom good r5652
elem_sides_smd.bom good r5652
elem_sides_trh.bom good r5652
arc_f_clear.bom good r6630
arc_normal.bom good r6630
arc_offpage.bom good r6630
arc_sizes.bom good r6630
arc_angles.bom good r6630
arc_sizes.svg broken r6023 ellipticals not handled
arc_angles.png good r6630
arc_f_clear.png bad r6630
arc_normal.png good r6630
arc_offpage.png good r6630
arc_sizes.png good r6630
coord_rounding.png good r6630
clearance.png bad r6630 missing pcb & text file?!
elem_pads_ds.png good r6630
elem_pads.png good r6630
elem_pins.png good r6630
elem_sides_smd.png good r6630
elem_sides_trh.png good r6630
layer_copper.png good r6630
layer_outline.png good r6630
layer_spc.png good r6630
line_f_clear.png good r6630
line_normal.png good r6630
line_offpage.png good r6630
line_overlap1.png good r6630
line_overlap2.png good r6630
line_overlap3.png good r6630
line_overlap4.png good r6630
line_zerolen.png good r6630
netlist.png good r6630
netlist_ba.png good r6630
poly_hole.png good r6630
poly_rect.png good r6630
poly_triangle.png good r6630
Proto.png good r6731
rat.png good r6731
text_rot.png good r6731
text_scale.png good r6731
text_sides.png good r6731
thermal_last.png good r6731
thermal_layer.png good r6731


