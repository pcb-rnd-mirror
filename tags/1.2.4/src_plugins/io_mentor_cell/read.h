int io_mentor_cell_read_pcb(pcb_plug_io_t *ctx, pcb_board_t *pcb, const char *fn, conf_role_t settings_dest);
int io_mentor_cell_test_parse_pcb(pcb_plug_io_t *ctx, pcb_board_t *Ptr, const char *Filename, FILE *f);
