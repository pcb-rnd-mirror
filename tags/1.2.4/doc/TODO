- Lihata persistent save:
	- flag compatibility: save unknown, string-flags as loaded by io_pcb
	- ind: FONTS: second font indents incorrectly
	- ind: indentation bug in inline struct therm: closing } is preceded by indentation whitespace for some reason
	- keep numeric format: test all
	- keep unknown subtrees

- replace settings with lihata (conf_*)
	- TEST:
		- vendor drill mapping (all functionality, including regex based refdes ignore)
		- route style in gtk and lesstif, save, load, 0, 1 or a lot of styles
		- route style vs. autorouter

- query & advanced search
	- search expr wizard should pick up the expression from the button when clicked
	- field accessors:
		- pad area
		- pin area
		- line (and pad) length and length^2
	- make a run on a large board, speed test, with -O0 and -O3:
		- iteration speed (a simple @)
		- eval speed (a simple @ with a lot of constants in an expr)
		- geo speed
		- regex and string cmp match speed vs. select by name


- polygon helper functions to flag non rectangular clipped polygons being exported to formats which only support rectangles, i.e. write.c in io_autotrax, +/- export a hatched version with outline 

For the next release:
- BUGS:
	- make distclean doesn't remove everything from tests/RTT [report: Alain]
	- lihata board: move generates much more diff than reasonable: does it renumber IDs?
	- Rotate a line and then undo with rubber band mode is enabled. Connected lines become corrupted. [report:Ade]
	+ io_pcb font parser succeeds(?) but clears the font id as if it had failed. Causes a duplicate font with id #0 [report:Ade]

LATER RELEASES:
- feature: fp_wget cache dir confiurable [report: agaran]
- BUG: exporters should export empty layers if there are pin/via rings on them; see PATCH.LAYER_GRP_EMPTY
- feature req:
	- if the library path starts with a "?", the path is optional; so we don't get warnings for ./footprints [report: celem]
	- add some sort of $(project_path) and/or $(design_path) so we don't need to use ./ and we won't depend on CWD [report: celem]
- feature req: show grid in both mm and mil in the status bar [report:smilie]
- BUG: when DRC sizes are saved in user conf, they are overridden from the default.pcb [report: Alain]
	Reason: pcb_board_t Bloat, Shrink, minWid, minSlk, minDrill, minRing -> remove them, use values from the config system instead
- update /usr/share/pcb-rnd/default.pcb to default lihata 
	- after switch, update doc/conf and doc/user references to conf settings from .pcb to .lht as needed 
- HID: option for using a fixed background color for the preview widget renders; using the user configured one goes wrong when it's set to dark [report: Alain]
- composite layer draw API cleanup [Ade]:
	- composite silk layer highlight bug: add a second, sub silk layer; place an element; select the element -> element silk is not cyan; reason: real composite layer is drawn with mask, mask loses color on blotting (affect gtk only)
	- (the same happens on paste, probably on mask too)
- BUG: cycledrag doesn't work on arc endpoint + line endpoint when rubber band is enabled [report:Ade]
- Rubberband Arcs:
	- Moving arc endpoints in rubberband mode doesn't move connected lines correctly so they become detached. [Ade]
	- Rubberband move line endpoints ignores connected arc endpoints. [Ade]
- bug: csect can't make a layer outline
- fontsel:
	- replace font in slot (even in default)
	- ttf import?
- library window: allow long tags and long location text to split
- undo on new layer from right-click menu in gtk layersel doesn't work
- GTK layers [report: Ade]:
	- move the layer+selected color change from the preferences menu to the layer popup (but this should probably live in gui-config.h)
- gtk layersel feature requests:
	- consider to provide a config setting for making open-group-name horizontal [report: istankovic]
- layer rewrite:
	- catch the layer change signal from polygons and reclip; Evan's test case:
	  draw a line on a layer and a poly on another and change whether they are in
	  the same layer group
- GTK Preferences->User PoV->Layers : Hit 'v' to "zoom fit" the preview [report: Alain]
		- allow GTK to handle keys in preview
		- use the zoompan engine for the zooming. Modifiy API if necessary
- check gpmi when pcb-rnd is fully linstalled - broken symlinks?
- multi-key: display state on the gui
- cleanup/rewrite resources:
	- load/swap menus (TODO#1)
	- gpmi:
		- hid: hid_destroy (pair of hid_create)
		- cleanup on unload: remove menus
- TODO#3: works only on US keyboard
- next_gui: keep them open, hide
- look for #warnings
- conf:
	- remove CFN_INCREMENTS, test if saving increments from preferences works
	- config pov: change prio
	- gtk preferences: check what takes 131 megs of VIRT ram (on top); destroy the dialog structures when closed
	- gtk preferences: poly isle area widget missing from the sizes tab
	- debug why default layer names from the user config are ignored
	- fp_fs_search: kill anything that gets ':' separated string as path list, get a config list instead
	- switch the lesstif HID over from attribs to conf
	- remove redundancy: don't store values in the PCB struct that are in the design conf
	- increments are not really implemented
- mark bug - ortho shouldn't use it, it should use last point (crosshair.X2); make sure nothing else abuses it [James]
- bug: search for move.dst_layer - don't compare pointer to -1!
- unravel the undo code
	- grid size change should be undoable? [report:Evan]
	- maybe other conf settings too?
- pcb_act_Attributes -> this could be handled by the property editor maybe? but what about layout attributes and the lesstif hid?
- layer group rewrite:
	- remove PCB_MAX_LAYERGRP and PCB_MAX_LAYER and make them dynamic
	- make the color config dynamic too; invent color for new layers
- layer groups from config (e.g. CLI) is ignored
- layer rewrite: central menu buttons
	- lesstif needs to remember the widgets via lb->w in menu.c
	- rewrite the central code for callbacks?
- inconsistencies in UI/terminology:
	- allow clearance to be set to 0 in the route style [report: miloh]
	- similar thing on hole size [report: miloh]
	- report dialog should use the same terminology for clearances as elements and route style dialog [report: miloh]
- io_pcb: new, optional "layer tag" field in mainline's file format
- extend the DRC window to be a generic location list
	- buttons to select one or all items or all items including locked
	- io infra for save incompatibility note with DRC list
	- import sch. shouldn't select elements to be removed but put them on a drc list
	- display number of items at top of drc window
	- export to file 
- biggish revamp of the export menu. The whole GUI is clearly designed for the case of only a very few exporters and we now have a lot. 
	- unify how output files are accessed/checked: confirmation on overwrite
- fp_board: extend the API so that the element can be presented without saving to a file
- cleanup: remove mnemonic from the menu system
	- check the central code and struct fields
	- remove "m=" fields from the menu lihata files
- SUBC missing features:
	- manual layer binding (reuse csect?)
	- ctrl+e propedig on subc
- parametric footprint gtk attribute box for editing: also mention the default value in the tooltip [report: agaran]
- silent failure on parser error in io_pcb: e.g. missing or non-integer fields of Via[] [report: Erich]
- core lib splitup:
	- gsch2pcb: generalize plugin/buildin loading for external tools, check if gsch2pcb can work from plugins
- lihata board/fp format:
	- pads should be polygons (0 long line can't be rotated!)
	- footprint orientation with a vector (just a second point)
- res:
	- search for vendor in the hid plugins, there should be no trace of it (vendor should register its in submenus with anchors)
	- re-add dynamic menus after a gui change:
		- either remember dynamic menus in a separate list so they can be rebuilt
		- or provide hooks and let plugins deal with it
	- gpmi: auto-remove menus by cookie (TODO#2)
	- load new res on the fly (replace the menu system):
		- low level reload code (re-add the dynamic menus too!)
		- action to reload if file name is known
		- gui load dialog with tags listed
- decide about exporter policy:
	- png exports selection (even in not-as-shown), others don't
	- extend png hid attribs with a flag for this, maybe do the same for others
	- document the decision in "hacking"
- reduce
		- export_bom: rewrite the string list code using htsp and/or lists
		- hash in hid_attrib.c?
		- nelma and gcode both invent .png name locally
		- get rid of gcode/lists.h,  and vector.[ch] (autorouter)
		- vendordrill
			- search for /units, replace it with pcb-printf something
			- add round down
			- replace ignore_refdes, ignore_value and ignore_descr with genvector
	- mods:
		- gpmi (and other buildins/plugins) not showing up in the about box
- self contained
	- dialog box to print where the actual files are coming from, including if the font came from embedded/internal (gui version of --show-paths)
	- project specific menus from extra lihata files - maybe from project.lht
- main.c:
	- SIGPIPE - detect it (needed for popen)
- dir rename trunk/pcblib/tru-hole should handle make correctly and not walk on existing web services or user installs
- BUG: subc ctl-i 'report' doesn't work on subc: hover over object in subc, press ctl-i msg warning: 'Nothing found to report on" issued, [report: Miloh]
- BUG: in both gtk guis, clicking Subcircuits layer control box 'off' effectively locks all subc, but gui lock tool doesn't work on subc. [report: Miloh]

GTK3:
	- overlapping objects (e.g. 2 fat lines crossing on silk) are darker on the overlap; we need uniform color
	- mouse scroll wheel doesn't work in cross section
	- zoom hangs: press 'z' on the drawing area -> hang
	- xor draw fails after 'i' library footprint placement; in fail mode there's no xor outline of buffer; drawing a line fixes it

CLEANUP #7: the big object split
- remove pcb_obj_type_t from const.h - use obj_any.h instead


FEATURES
- BUILD: menuconfig and a config file for scconfig
- menu item for export of multiple selected layout elements to discrete .fp files

Low prio:
- advanced grid settings:
	- keep two lists of preferred grid sizes, one in mil, one in mm
	- generate the grid selection menu from those lists
	- action to step up and down among those predefined grid sizes
	- make sure it is possible to edit the lists in preferences
- libstroke: zoom
- improve locking:
	- consider a move-only lock
	- when thermal and some other operations are applied on a locked element, indicate it
- Erich's gtk lag report: press 's' a lot then move the mouse - 's' ends up in the new loc!
- the TAB bug: (over ssh -X) press tab for a good 15 seconds; release; it won't work again for a fraction of a second
- bug: rubberband_orig: draw a 90 deg arc, a 45 deg line that ends in the endpoints of the arc; enable rubber band, move the arc: only one endpoint of the line is moved [fixing:Ade]
- bug: redrawbug1: grap c1, the diagonal trace gets drawn over U1's silk - it's redrawn because of the overlapping bounding box with C1, while U1 has no overlap [report:Evan]
- bug: draw overlapping lines, a short fully under a long; click on the short, it gets selected but it's not visible because the long hides it [report:Evan]
- bug: layer menu: the bottom silk screen icon color changes from gray-black to brown when any layer is selected. [report:Evan]
- GTK "save as" dialog: there should be an "auto" or "guess by extension" option in the save dialog format
- insert drag&drop strangeness (mainline too):
	insert should just insert a new point and stop there, not starting a drag&drop
	move; the new point should be marked somehow (e.g. green-find one half of the
	object, like arc split does) lines can be inserted mostly only in all-dir-line
	which is strange
- missing rotate polygon (mainline too)
- zoom in too deep onto board edge and the implicit outline rectangle behaves strangely [report:Evan]
- scconfig: check if it picks up settings from env vars (for a gentoo build)
- replace mkdtemp() with something safer
- display net names on pins, vias (and maybe tracks?) when zoomed in enough
- DRC should warn for thin poly hair
- rotate shaped vias don't rotate the shape (is via rotated at all?)
- new examples
	- blinking led with parametric footprints
	- example of nonetlist: 1206 jumpers and logos
- decide what to do with old doc - texi doesn't seem to work at all
- rethink/rewrite the action/change infrastructure - too many void *ptr1
  pointers, too many code duplication
- double sided board, poly on both layers; grab existing lines on one layer and
  move then around. If all layers are visible, redraw of the far side layer
  is slow and causes flickering elements from that layer until the front is
  redrawn. Maybe we should have less flushes?
- gpmi:
	- dialog: attribute dialog: mini parser from text
	- fix debug draw in the gtk hid
	- ACTE_action(): coords should not be int but Coord
	- get timers to work
- dmalloc: #include from the bottom from each file. Is this a good idea?!
- win32 port {large}
	- clean up central Makefile.in rules:
		- remove cat, sed, grep where possible, use scconfig instead
- arc bug: draw an arc with one end out of the drawing area; it will be jumpy and can not be placed properly
	-> AttachForCopy() calls SetCrosshairRange() that then sets crosshair max* which
	limits the arc to move freely. Problem: this is not arc-specific, this happens with any buffer copy! going for no-design-limit is probably better
- while drawing a line, the temporary outline should reflect the layer color
- push&shove
	- keep 45-deg knee when grabbing a point for drag&drop in non-all-dir
- --dump-actions shows gpmi paths ( is this related? it looks unexpected to me)
- unify gui invocation options: pcb-rnd --help gtk_[gdk|gl] and pcb-rnd --help lesstif both show and use different input methods [report:miloh]
- consider allowing negative Delta to be specified in CTRL-E property inspector
- consider a simple all in one function to create a default layer stackup for static/predictable layer formats being imported, to simplify importer coding
- trace length calculator:
	- click a line or arc or via
	- start a search in two directions mapping lines, arcs and vias connected (green-highlight them, marking them found)
	- stop at the first junction (anywhere where more than 2 objects are connected)
	- stop at polygons
	- display the number of vias and net trace length along the found objects
