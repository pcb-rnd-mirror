(Created by G-code exporter)
(600 dpi)
(Unit: inch)
(Board size: 0.50x0.50 inches)#100=0.002000  (safe Z)
#101=-0.000050  (cutting depth)
(---------------------------------)
G17 G20 G90 G64 P0.003 M3 S3000 M7 F1
G0 Z#100
(polygon 1)
G0 X0.221667 Y0.335000    (start point)
G1 Z#101
G1 X0.213333 Y0.326667
G1 X0.213333 Y0.246667
G1 X0.221667 Y0.238333
G1 X0.226667 Y0.238333
G1 X0.235000 Y0.246667
G1 X0.235000 Y0.326667
G1 X0.226667 Y0.335000
G1 X0.221667 Y0.335000
G0 Z#100
(polygon end, distance 0.22)
(end, total distance 5.52mm = 0.22in)
M5 M9 M2
