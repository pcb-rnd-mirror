extern const char pcb_acts_Zoom[];
extern const char pcb_acth_Zoom[];
fgw_error_t pcb_act_Zoom(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_Pan[];
extern const char pcb_acth_Pan[];
fgw_error_t pcb_act_Pan(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_Center[];
extern const char pcb_acth_Center[];
fgw_error_t pcb_act_Center(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_Scroll[];
extern const char pcb_acth_Scroll[];
fgw_error_t pcb_act_Scroll(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_SwapSides[];
extern const char pcb_acth_SwapSides[];
fgw_error_t pcb_act_SwapSides(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_Command[];
extern const char pcb_acth_Command[];
fgw_error_t pcb_act_Command(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char pcb_acts_Popup[];
extern const char pcb_acth_Popup[];
fgw_error_t pcb_act_Popup(fgw_arg_t *res, int argc, fgw_arg_t *argv);
