pcb-rnd 1.0.3
~~~~~~~~~~~~~
	[gpmi]
		-Add: GPMI plugin/buildin for scripiting (import of project pcb-gpmi)
	
	[tostyle]
		-Add: new feature to adjust the sizes of object(s) to the values of the current routing style
	
	[pcb-fp]
		-Add: support for parsing and caching footprint tags (file elements)
		-Add: the gtk HID displays tags
		-Add: the gtk HID can filter for tags
	
	[pcblib-param]
		-Add: bga()
		-Add: generic qf() generator
		-Add: qfn() - based on qf()
		-Add: qfp() - based on qf()
		-Add: plcc() - based on qf()
		-Add: qsop() - based on so()
		-Add: silkmark: accept multiple values
		-Add: silkmark: can be external
		-Add: silkmark: new styles exteranl45, dot, circle, and arc
		-Add: connector() has a sequence option to change pin numbering
		-Add: some tunings for connector sequence=zigzag to do something usable with etrunc=1
	
	[hid]
		-Add: dynamic menus: create_menu(), menu paths, runtime insertion of menus in core
		-Add: dynamic menus in gtk
		-Add: dynamic menus in lesstif
		-Fix: more const correctness in dialog box code
	
	[scconfig]
		-Add: ./configure --help
		-Add: print configuration summary at the end
		-Add: autodetect HOST (use sys/sysid)
		-Add: src/3rd for central hosting of 3rd party software libs
		-Add: support "buildins": plugins built into the code
		-Change: move genht to src/3rd; policy: prefer genht over glib hash
		-Fix: tests try to run pcb-rnd, not pcb
		-Fix: central LDFLAGS and CFLAGS should contain the ldflags/cflags detected for generic use
	
	[pcb-mincut]
		-Merge: pcb-mincut is imported in pcb-rnd, the extern repo will be removed
	
	[core]
		-Add: event subsystem
		-Add: gui_init event
		-Add: generic plugin support: track plugins loaded, have a menu for managing them
		-Add: more accessors to query unit details - for the gpmi scripts
		-Add: pcb-printf %mI prints coordinate in internal coord format
		-Add: calls for removing actions
		-Add: calls for removing a hid (useful for removing exporter hids registered by scripts)
		-Add: path resolution: support ~ in paths
		-Add: real regex search and real string list search (in search by name actions)
		-Change: switch over actions from bsearch() to a genht hash - simpler code, potentially also faster
		-Fix: don't allow the registration of an action with a name that's already registered
	
	
	[fp2anim]
		-Add: optional dimension lines
		-Add: more fine grained annotation control
		-Change: switch over to vector fonts for better scaling
		-Fix: draw rounded pads (new pad syntax only)
		-Fix: make sure to generate sane arcs
	
	[doc-rnd]
		-Add: official central key list

pcb-rnd 1.0.2
~~~~~~~~~~~~~
	[pcblib-param]
		-Fix: central bool handling (connector() etrunc), values: on/off, true/false, 1/0
		-Fix: typo in so() parameter descriptions
		-Fix: connector() typo in error()
		-Add: more elaborate help syntax: easier to read with scripts

	[fp2anim]
		-Fix: read multiline directives properly - a newline after each directive is still needed, tho
		-Fix: allow whitepsace between directive name and opening bracket
		-Fix: create all layers in advance - this fixes the case when the fp doesn't draw on some layers (the macro still exists)
		-Fix: rline() is extended by width/2 if there are no rounding; it seems this how pcb defines lines
		-Fix: leave extra margin in photo mode for the 3d edges
		-Add: support for old-style pad()
		-Add: support for Mark() (relocate the diamond)
		-Add: options to turn off the grid and annotation - useful for thumbnails

	[scconfig]
		-Fix: always have config.h, don't ifdef it
		-Fix: glib is a core dependency that should be added even if the gtk hid is not used
		-Fix: make clean removes pcb-rnd, not pcb (executable file name typo)
		-Add: make clean in util/
		-Add: options to disable gd and/or jpeg or png or gif
		-Add: be able to detect and configure lesstif 
		-Add: --disable-gtk and --disable-lesstif; --disable-xinerama and --disable-xrender for lesstif
		-Add: use detected -rdynamic so that plugins can link against the executable
		-Change: detect both gtk and lesstif as optional dependencies
		-Cleanup: generate hidlist.h from tmpasm instead of shell to remove shell dependency
		-Del: a bunch of obsolete #defines inherited from auto*

	[core]
		-Add: --gui explicitly selects a gui hid
		-Add: don't leave preferred order of GUIs to the chance, make it explicit


pcb-rnd 1.0.1
~~~~~~~~~~~~~
	[core]
		-Fix: don't read beyond the common part of the struct in IsPointInPad (since it's called for lines too, and those have much less fields)
		-Fix: where stdarg is avaialble, also print messages to stderr - useful if things go wrong before a GUI is working

	[gtk]
		-Fix: don't crash but write error message and exit if gpcb-menu.res is empty 

	[square]
		-Fix: 90 deg rotation rotates shape style
		-Add: action.c and change.c code to get shaped vias
		-Fix: don't change pin shape for square and octagon in rotation

	[mincut]
		-Add: command line setting --enable-mincut (0 or 1) as mincut can be slow
		      it is a global disbale setting and make a local, per pcb setting
		      for enabling mincut; also add a per pcb setting/flag
		-Fix: disable debug draw by default
		-Fix: fall back to the old short warn method when mincut fails
		-Fix: avoid segfaults by detecting broken graph early
		-Workaround: if mincut sees a graph with multiple unconnected components, doesn't try to solve but falls back to randomly highlight something

	[intconn]
		-Workaround: find intconn pads only on the same layer

	[nonetlist]
		-Workaround: ignore nonetlist pads even if the flag is in the element name

	[scconfig]
		-Add: scconfig/ - switch over from autotools to scconfig

	[pcblib]
		-Cleanup: new, trimmed back pcblib/ with essential footprints only

 [pcblib-param]
		-Add: new parametric footprints - no more m4-hardwiring, use your
		      preferred language!
		-Add: acy(), alf(), rcy(), connector(), dip()
		-Add: so(), tssop(), msop(), ssop()

	[pcb-fp]
		-Add: central lib for footprint search and load in pcb and gsch2pcb

	[util]
		-Add: gsch2pcb fork to support [nonetlist] and [pcblib-param]

	[fp2anim]
		-Add: fp to animator script for fast preview

	[polygrid]
		-Add: ps output: draw grid in polys instead of fill (doesn't fully work)
		-Fix: set proper max value so the control is enabled


	[debian]
		-Update: build the package with scconfig and footprint changes


pcb-rnd 1.0.0
~~~~~~~~~~~~~
	[square] -Add: initial implementation
	[intconn] -Add: initial implementation
	[nonetlist] -Add: initial implementation
	[flagcomp] -Add: initial implementation
	[mincut] -Add: initial implementation
