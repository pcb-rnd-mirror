#include "data.h"
#include "unit.h"

/* et_swap_sides: toggle elements/text solder side flag */
void pcb_flip_data(pcb_data_t *data, pcb_bool flip_x, pcb_bool flip_y, pcb_coord_t xo, pcb_coord_t yo, pcb_bool et_swap_sides);

