#ifndef WT_ACCEL_LABEL_H
#define WT_ACCEL_LABEL_H

/** Helper: creates a new menu item with label and accel label set up. */
GtkWidget *pcb_gtk_menu_item_new(const char *label, const char *accel_label);

#endif /* WT_ACCEL_LABEL_H */
