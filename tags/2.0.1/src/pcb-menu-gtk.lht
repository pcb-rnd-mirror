# Old, OBSOLETE, somewhat unmaintained menu file for users who got used
# to the old gtk menus and have not yet switched to the new, unified default
# menu file, pcb-menu-default.lht
ha:{
	li:mouse {
		li:left {
			li:press            = { Mode(Notify) }
			li:press-shift      = { Mode(Notify) }
			li:press-ctrl       = { Mode(Save); Mode(None); Mode(Restore); Mode(Notify) }
			li:press-shift-ctrl = { Mode(Save); Mode(Remove); Mode(Notify); Mode(Restore) }
			li:release          = { Mode(Release) }
			li:release-shift    = { Mode(Release) }
			li:release-ctrl     = { Mode(Release) }
			li:release-shift-ctrl = { Mode(Release) }
		}
		li:right {
			li:press         = { Pan(1) }
			li:release       = { Pan(0) }
			li:press-shift   = { Popup(popup1) }
			li:press-ctrl    = { Display(CycleCrosshair) }
		}
		li:middle {
			li:press               = { Mode(Stroke) }
			li:release             = { Mode(Release) }
			li:press-ctrl          = { Mode(Save); Mode(Copy); Mode(Notify) }
			li:release-ctrl        = { Mode(Notify); Mode(Restore); }
			li:press-shift-ctrl    = { conf(toggle, editor/rubber_band_mode, design); Mode(Save); Mode(Move); Mode(Notify) }
			li:release-shift-ctrl  = { Mode(Notify); Mode(Restore); conf(toggle, editor/rubber_band_mode, design) }
		}
		li:scroll-up {
			li:press        = { Zoom(0.8) }
			li:press-shift  = { Scroll(up) }
			li:press-ctrl   = { Scroll(left) }
		}
		li:scroll-down {
			li:press       = { Zoom(1.25) }
			li:press-shift = { Scroll(down) }
			li:press-ctrl  = { Scroll(right) }
		}
# If you want zoom to center, do this instead.
		#ha:scroll-up = { li:{} = {Zoom(0.8); Center()} }
		#ha:scroll-down = { li:{} = {Zoom(1.25); Center()} }
	}
	
	li:main_menu {
		### File Menu
		ha:File {
			m=F
			li:submenu {
				ha:Start New Layout                  = { a=Ctrl<Key>n; action=New() }
				ha:Revert                            = {                             action=Load(Revert,none); tip=Revert to the layout stored on disk }
				-
				ha:Load layout                       = {                             action=Load(Layout); tip=Load a layout from a file }
				-
				ha:Save Layout                       = { m=S; a={Ctrl<Key>s};        action=Save(Layout); tip=Saves current layout }
				ha:Save Layout As...                 = { m=A; a={Shift Ctrl<Key>s};  action=Save(LayoutAs); tip=Saves current layout into a new file }
				-
				ha:Import {
					m=I
					li:submenu {
						ha:Import gschem schematics          = {                             action=Import() }
						ha:Import tEDAx schematics           = {                             action=LoadTedaxFrom(netlist) }
						ha:Import KiCad/eeschema schematics  = {                             action=LoadEeschemaFrom() }
						ha:Import mentor graphics schematics = {                             action=LoadMentorFrom() }
						ha:Load subcircuit data to paste-buffer = {                          li:action={PasteBuffer(Clear); Load(ElementTobuffer)} }
						ha:Load layout data to paste-buffer  = {                             li:action={PasteBuffer(Clear); Load(LayoutTobuffer)} }
						ha:Load netlist file                 = {                             action=Load(Netlist) }
						ha:Load vendor resource file         = {                             action=LoadVendorFrom() }
						ha:Load routed dsn or ses file       = {                             action=LoadDsnFrom() }
						ha:Load MUCS pl file                 = {                             action=LoadMucsFrom() }
						ha:Load TinyCAD .net file            = {                             action=LoadTinyCADFrom() }
						ha:Load LTSpice .net and .asc files  = {                             action=LoadLTSpiceFrom() }
						ha:Load HP-GL plot to paste-buffer   = {                             action=LoadHPGLFrom() }
						ha:Load Hyperlynx .hyp file          = {                             action=LoadHypFrom() }
					}
				}
				-
				ha:Save connection data of {
					li:submenu {
						ha:a single element                  = {                  action=Save(ElementConnections)}
						ha:all elements                      = {                  action=Save(AllConnections) }
						ha:unused pins                       = {                  action=Save(AllUnusedPins) }
						ha:netlist patch for back annotation = {a=Alt Ctrl<Key>b; action=SavePatch() }
					}
				}
				ha:Print layout...      = {               action=Print()}
				ha:Export layout...     = {               action=ExportGUI()}
				-
				ha:Preferences...       = {               action=DoWindows(Preferences)}
				ha:Maintenance {
					li:submenu {
						ha:Calibrate Printer...           = { action=PrintCalibrate() }
						ha:Re-scan the footprint library  = { action=fp_rehash() }
						-
						ha:Edit current font              = { action=FontEdit() }
						ha:Edit on-disk font              = { action={ LoadFontFrom("", 126); conf(set, design/text_font_id, 126, design); FontEdit() } }
						ha:Save font from font editor     = { action=FontSave() }
					}
				}
				-
				ha:Quit Program         = { a=Ctrl<Key>q; action=Quit() }
			}
		}

		ha:Edit {
			m=E
			li:submenu {
				ha:Undo last operation          = { a=<Key>u;           action=Undo() }
				ha:Redo last undone operation   = { a=Shift<Key>r;      action=Redo() }
				ha:Clear undo-buffer            = { a=Shift Ctrl<Key>u; action=Undo(ClearList) }
				-
				ha:Cut selection to buffer      = { a=Ctrl<Key>x;       li:action={ GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); RemoveSelected(); Mode(PasteBuffer) } }
				ha:Copy selection to buffer     = { a=Ctrl<Key>c;       li:action={ GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); Unselect(All); Mode(PasteBuffer) } }
				ha:Paste buffer to layout       = { a=Ctrl<Key>v;       action=Mode(PasteBuffer) }
				-
				ha:Unselect all                 = { a=Shift Alt<Key>a;  action=Unselect(All) }
				ha:Select all visible           = { a=Alt<Key>a;        action=Select(All) }
				-
				ha:Change font on layout        = { a=Alt Ctrl<Key>f;   action=FontSel(Object) }
				ha:Edit name of {
					li:submenu {
						ha:text on layout             = { a=<Key>n;           action=ChangeName(Object) }
						ha:pin on layout              = { a=Shift Ctrl<Key>n; action=ChangeName(Object, Number) }
						ha:layout                     = {                     action=ChangeName(Layout) }
						ha:active layer               = {                     action=ChangeName(Layer) }
					}
				}
				ha:Edit attributes of {
					li:submenu {
						ha:Layout                     = { action=Attributes(Layout) }
						ha:CurrentLayer               = { action=Attributes(Layer) }
						ha:Subcircuit                 = { action=Attributes(Subc) }
					}
				}
				ha:Route Styles {
					li:submenu {
						@routestyles
						-
						ha:Edit...                    = { action=AdjustStyle(0) }
					}
				}
				ha:Subcircuit {
					li:submenu {
						ha:Layer bindings...          = { action=LayerBinding() }
						ha:External editor...         = { li:action={ GetXY(Click on the subcircuit to edit); extedit(object) } }
					}
				}
				ha:Object flags...                = { action=FlagEdit() }
			}
		} # Edit

		ha:View {
			m=V
			li:submenu {
				ha:Enable visible grid        = { checked=editor/draw_grid; action=conf(toggle, editor/draw_grid, design) }
				ha:Enable local grid          = { checked=plugins/hid_gtk/local_grid/enable; action=conf(toggle, plugins/hid_gtk/local_grid/enable, design) }
				ha:Grid units {
					li:submenu {
						ha:mil                    = { checked=ChkGridUnits(mil); action=SetUnits(mil); update_on={editor/grid_unit} }
						ha:mm                     = { checked=ChkGridUnits(mm); action=SetUnits(mm); update_on={editor/grid_unit} }
					}
				}
				ha:Grid size = {
					li:submenu {
						ha:No Grid                = { checked=ChkGridSize(none); action=SetValue(Grid,1); update_on={editor/grid} }
						-
						ha:0.1 mil                = { checked=ChkGridSize(0.1mil); li:action={SetUnits(mil); SetValue(Grid,0.1mil)}; update_on={editor/grid}}
						ha:1 mil                  = { checked=ChkGridSize(1mil); li:action={SetUnits(mil); SetValue(Grid,1mil)}; update_on={editor/grid} }
						ha:5 mil                  = { checked=ChkGridSize(5mil); li:action={SetUnits(mil); SetValue(Grid,5mil)}; update_on={editor/grid} }
						ha:10 mil                 = { checked=ChkGridSize(10mil); li:action={SetUnits(mil); SetValue(Grid,10mil)}; update_on={editor/grid} }
						ha:25 mil                 = { checked=ChkGridSize(25mil); li:action={SetUnits(mil); SetValue(Grid,25mil)}; update_on={editor/grid} }
						ha:50 mil                 = { checked=ChkGridSize(50mil); li:action={SetUnits(mil); SetValue(Grid,50mil)}; update_on={editor/grid} }
						ha:100 mil                = { checked=ChkGridSize(100mil); li:action={SetUnits(mil); SetValue(Grid,100mil)}; update_on={editor/grid} }
						-
						ha:0.01 mm                = { checked=ChkGridSize(0.01mm); li:action={SetUnits(mm); SetValue(Grid,0.01mm)}; update_on={editor/grid} }
						ha:0.05 mm                = { checked=ChkGridSize(0.05mm); li:action={SetUnits(mm); SetValue(Grid,0.05mm)}; update_on={editor/grid} }
						ha:0.1 mm                 = { checked=ChkGridSize(0.10mm); li:action={SetUnits(mm); SetValue(Grid,0.1mm)}; update_on={editor/grid} }
						ha:0.25 mm                = { checked=ChkGridSize(0.25mm); li:action={SetUnits(mm); SetValue(Grid,0.25mm)}; update_on={editor/grid} }
						ha:0.5 mm                 = { checked=ChkGridSize(0.50mm); li:action={SetUnits(mm); SetValue(Grid,0.5mm)}; update_on={editor/grid} }
						ha:1 mm                   = { checked=ChkGridSize(1mm); li:action={SetUnits(mm); SetValue(Grid,1mm)}; update_on={editor/grid} }
						-
						ha:Grid -5mil             = { a=Shift<Key>g; action=SetValue(Grid,-5,mil) }
						ha:Grid +5mil             = { a=<Key>g;  action=SetValue(Grid,+5,mil) }
						ha:Grid -0.05mm           = { a=Shift Ctrl<Key>g; action=SetValue(Grid,-0.05,mm) }
						ha:Grid +0.05mm           = { a=Ctrl<Key>g; action=SetValue(Grid,+0.05,mm) }
					}
				}
				ha:Grid properties = {
					li:submenu {
						ha:Enable local grid          = { checked=plugins/hid_gtk/local_grid/enable; action=conf(toggle, plugins/hid_gtk/local_grid/enable, design) }
						-
						ha:local grid radius 4   =  { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 4)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 4, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) }; update_on={} }
						ha:local grid radius 8    = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 8)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 8, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) }; update_on={} }
						ha:local grid radius 16   = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 16)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 16, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) }; update_on={} }
						ha:local grid radius 32   = { checked={conf(iseq, plugins/hid_gtk/local_grid/radius, 32)}; li:action={conf(set, plugins/hid_gtk/local_grid/radius, 32, design); conf(set, plugins/hid_gtk/local_grid/enable, 1, design) }; update_on={} }
						-
						ha:sparse global grid     = { checked=plugins/hid_gtk/global_grid/sparse; action=conf(toggle, plugins/hid_gtk/global_grid/sparse, design); update_on={} }
						ha:global grid density 4  = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 4)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 4, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) }; update_on={} }
						ha:global grid density 8  = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 8)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 8, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) }; update_on={} }
						ha:global grid density 16 = { checked={conf(iseq, plugins/hid_gtk/global_grid/min_dist_px, 16)}; li:action={conf(set, plugins/hid_gtk/global_grid/min_dist_px, 16, design); conf(set, plugins/hid_gtk/local_grid/enable, 0, design) }; update_on={} }
					}
				}
				ha:Realign grid               = { action={ GetXY(Click to set the grid origin); Display(ToggleGrid) } }
				-
				ha:Displayed subcircuit ID {
					li:submenu {
						ha:refdes                 = { checked=ChkSubcID(); action=Display(SubcID); update_on={editor/name_on_pcb} }
						ha:footprint              = { checked=ChkSubcID(%a.footprint%); action=Display(SubcID,"%a.footprint%"); update_on={editor/subc_id} }
						ha:value                  = { checked=ChkSubcID(%a.value%); action=Display(SubcID,"%a.value%"); update_on={editor/subc_id} }
						ha:refdes+value           = { checked=ChkSubcID(%a.refdes%\\n%a.value%); action=Display(SubcID,"%a.refdes%\\\\n%a.value%"); update_on={editor/subc_id} }
					}
				}
				ha:Pins/Via show Name/Number  = { a=<Key>d; action=Display(PinOrPadName) }
				ha:Zoom In 20%                = { m=Z; a=<Key>z; action=Zoom(-1.2) }
				ha:Zoom Out 20%               = { m=O; a=Shift<Key>z; action=Zoom(+1.2) }
				ha:More zooms and view changes {
					li:submenu {
						ha:Zoom Max               = { m=M; a=<Key>v; action=Zoom() }
						ha:Zoom In 2X             = { action=Zoom(-2) }
						ha:Zoom Out 2X            = { action=Zoom(+2) }
						ha:Zoom to 0.1mil/px      = { action={Zoom(=0.1mil)} }
						ha:Zoom to 0.01mm/px      = { action={Zoom(=0.01mm)} }
						ha:Zoom to 1mil/px        = { action={Zoom(=1mil)} }
						ha:Zoom to 0.05mm/px      = { action={Zoom(=0.05mm)} }
						ha:Zoom to 2.5mil/px      = { action={Zoom(=2.5mil)} }
						ha:Zoom to 0.1mm/px       = { action={Zoom(=0.1mm)} }
						ha:Zoom to 10mil/px       = { action={Zoom(=10mil)} }
						ha:Zoom In 20% and center = { m=Z; li:action={Zoom(-1.2); Center()} }
						ha:Zoom Out 20% and center= { m=O; li:action={Zoom(+1.2); Center()} }
						ha:Flip up/down           = { checked=editor/view/flip_y; a=<Key>Tab; action=SwapSides(V) }
						ha:Flip left/right        = { checked=editor/view/flip_x; a=Shift<Key>Tab; action=SwapSides(H) }
						ha:Spin 180 degrees       = { a=Ctrl<Key>Tab; action=SwapSides(R) }
						ha:Swap Sides             = { a=Ctrl Shift<Key>Tab; action=SwapSides() }
						ha:Center cursor          = { a=<Key>c; action=Center() }
					}
				}
				-
				ha:Shown Layers {
					li:submenu {
						@layerview
						-
						ha:Edit Layer Groups      = { action=DoWindows(Preferences, 1, "User PoV/Layers") }
					}
				}
				ha:Current Layer {
					li:submenu {
						anon2=@layerpick
						-
						ha:Delete current layer   = { action=MoveLayer(c,-1) }
						ha:Add new layer          = { action=MoveLayer(-1,c) }
						ha:Move current layer up  = { action=MoveLayer(c,step-) }
						ha:Move current layer down= { action=MoveLayer(c,step+) }
					}
				}
				ha:Full screen                = { checked=editor/fullscreen; a=<char>\\;  action=fullscreen(toggle) }
			}
		} # View

		ha:Settings = {
			m=S
			li:submenu {
				ha:'All-direction' lines            = { checked=editor/all_direction_lines; a=<char>.; action=conf(toggle, editor/all_direction_lines, design) }
				ha:Auto swap line start angle       = { checked=editor/swap_start_direction; action=conf(toggle, editor/swap_start_direction, design) }
				ha:Orthogonal moves                 = { checked=editor/orthogonal_moves; action=conf(toggle, editor/orthogonal_moves, design) }
				ha:Crosshair snaps to pins and pads = { checked=editor/snap_pin; action=conf(toggle, editor/snap_pin, design) }
				ha:Crosshair snaps to off-grid points on lines = { checked=editor/snap_offgrid_line; action=conf(toggle, editor/snap_offgrid_line, design) }
				ha:Crosshair shows DRC clearance    = { checked=editor/show_drc; action=conf(toggle, editor/show_drc, design) }
				ha:Auto enforce DRC clearance       = { checked=editor/auto_drc; action=conf(toggle, editor/auto_drc, design) }
				ha:Lock floaters                    = { checked=editor/lock_names; action=conf(toggle, editor/lock_names, design) }
				ha:Only floaters                    = { checked=editor/only_names; action=conf(toggle, editor/only_names, design) }
				ha:Hide floaters                    = { checked=editor/hide_names; action=conf(toggle, editor/hide_names, design) }
				ha:Mincut on shorts                 = { checked=plugins/mincut/enable; action=conf(toggle, plugins/mincut/enable, design) }
				ha:Libstroke gestures on middle button = { checked=editor/enable_stroke; action=conf(toggle, editor/enable_stroke, design) }
				-
				ha:Rubber band mode                 = { checked=editor/rubber_band_mode; action=conf(toggle, editor/rubber_band_mode, design) }
				ha:Rubber band keeps middle line dir= { checked=editor/rubber_band_keep_midlinedir; action=conf(toggle, editor/rubber_band_keep_midlinedir, design) }
				ha:Auto-zero delta measurements     = { checked=editor/local_ref; action=conf(toggle, editor/local_ref, design) }
				ha:New lines, arcs clear polygons   = { checked=editor/clear_line; action=conf(toggle, editor/clear_line, design) }
				ha:New polygons clear polygons      = { checked=editor/clear_polypoly; action=conf(toggle, editor/clear_polypoly, design) }
				ha:New polygons are full ones       = { checked=editor/full_poly; action=conf(toggle, editor/full_poly, design) }
				ha:Show autorouter trials           = { checked=editor/live_routing; action=conf(toggle, editor/live_routing, design) }
				ha:Highlighting on line, arc points = { checked=editor/highlight_on_point; action=conf(toggle, editor/highlight_on_point, design) }
				ha:Wireframe draw                   = { checked=editor/wireframe_draw; a=Ctrl<Key>w;  action=conf(toggle, editor/wireframe_draw, design) }
				ha:Thin draw                        = { checked=editor/thin_draw; a=<char>|;  action=conf(toggle, editor/thin_draw, design) }
				ha:Thin draw poly                   = { checked=editor/thin_draw_poly; a=Ctrl Shift<Key>p; action=conf(toggle, editor/thin_draw_poly, design) }
				ha:Check polygons                   = { checked=editor/check_planes; action=conf(toggle, editor/check_planes, design) }
				ha:Brave mode                       = { action=brave() }
				-
				ha:Vendor drill mapping             = { checked=plugins/vendor/enable; action=conf(toggle, plugins/vendor/enable, design) }
				ha:Import New Elements at = {
					m=I
					li:submenu {
						ha:Center          = { m=C; action=Import(setnewpoint,center) }
						ha:Mark            = { m=M; action=Import(setnewpoint,mark) }
						ha:Crosshair       = { m=h; action=Import(setnewpoint) }
						-
						ha:Set Dispersion  = { m=D; action=Import(setdisperse) }
					}
				}
			}
		} #Settings

		ha:Select {
			m=l
			li:submenu {
				ha:Select all visible objects      = { action=Select(All) }
				ha:Select all connected objects    = { action=Select(Connection) }
				-
				ha:Unselect all objects            = { action=Unselect(All) }
				ha:unselect all connected objects  = { action=Unselect(Connection) }
				-
#TODO: this is gtk-specific
				ha:Advanced search and select= { action=DoWindows(search) }
				-
				ha:Auto-place selected subcircuits       = { a=Ctrl<Key>p; action=AutoPlaceSelected() }
				ha:Disperse all subcircuits              = { action=DisperseElements(All) }
				ha:Disperse selected subcircuits         = { action=DisperseElements(Selected) }
				-
				ha:Move selected subcircuits to other side= { a=Shift<Key>b; action=Flip(SelectedElements) }
				ha:Move selected to current layer        = { a=Shift<Key>m; action=MoveToCurrentLayer(Selected) }
				ha:Remove selected objects               = { a=Shift<Key>Delete; action=RemoveSelected() }
				ha:Convert selection to subcircuit       = { action=Select(ConvertSubc) }
				ha:Convert selection to padstack         = { li:action={PadstackConvert(selected); Mode(PasteBuffer);} }
				-
				ha:Optimize selected rats                = { li:action={DeleteRats(SelectedRats); AddRats(SelectedRats) } }
				ha:Auto-route selected rats              = { a=Alt<Key>r; action=AutoRoute(SelectedRats) }
				ha:Rip up selected auto-routed tracks    = { action=RipUp(Selected) }
				-
				ha:Edit properties of selected...        = { a=Ctrl<Key>e; action=PropEdit(Selected) }
				ha:Change size of selected objects {
					li:submenu {
						ha:Lines -10 mil = { li:action={ChangeSize(SelectedLines,-10,mil); ChangeSize(SelectedArcs,-10,mil)} }
						ha:Lines +10 mil = { li:action={ChangeSize(SelectedLines,+10,mil); ChangeSize(SelectedArcs,+10,mil)} }
						ha:Pads -10 mil  = { action=ChangeSize(SelectedPads,-10,mil) }
						ha:Pads +10 mil  = { action=ChangeSize(SelectedPads,+10,mil) }
						ha:Pins -10 mil  = { action=ChangeSize(SelectedPins,-10,mil) }
						ha:Pins +10 mil  = { action=ChangeSize(SelectedPins,+10,mil) }
						ha:Texts -10 mil = { action=ChangeSize(SelectedTexts,-10,mil) }
						ha:Texts +10 mil = { action=ChangeSize(SelectedTexts,+10,mil) }
						ha:Vias -10 mil  = { action=ChangeSize(SelectedVias,-10,mil) }
						ha:Vias +10 mil  = { action=ChangeSize(SelectedVias,+10,mil) }
					}
				}
				-
				ha:Change drilling hole of selected padstacks {
					li:submenu {
						ha:Vias -10 mil = { action=ChangeDrillSize(SelectedPadstacks,-10,mil) }
						ha:Vias +10 mil = { action=ChangeDrillSize(SelectedPadstacks,+10,mil) }
					}
				}
				-
				ha:Cycle object being dragged = { a=<Key>x; action=CycleDrag() }
			}
		} # Select

		ha:Buffer {
			m=B
			li:submenu {
				ha:Cut selection to buffer = { li:action={GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); RemoveSelected(); Mode(PasteBuffer)} }
				ha:Paste buffer to layout  = { action=Mode(PasteBuffer) }
				-
				ha:Rotate buffer 90 deg CCW   = { a=Shift<Key>F7; li:action={Mode(PasteBuffer); PasteBuffer(Rotate,1)} }
				ha:Rotate buffer 90 deg CW    = { li:action={Mode(PasteBuffer); PasteBuffer(Rotate,3)} }
				ha:Arbitrarily Rotate Buffer  = { li:action={Mode(PasteBuffer); FreeRotateBuffer()} }
				ha:Mirror buffer (up/down)    = { li:action={Mode(PasteBuffer); PasteBuffer(Mirror)} }
				ha:Mirror buffer (left/right) = { li:action={Mode(PasteBuffer); PasteBuffer(Rotate,1); PasteBuffer(Mirror); PasteBuffer(Rotate,3)} }
				-
				ha:Clear buffer                    = { action=PasteBuffer(Clear) }
				ha:Convert buffer to subcircuit    = { action=PasteBuffer(ConvertSubc) }
				ha:Convert buffer to padstack      = { li:action={PadstackConvert(buffer); Mode(PasteBuffer);} }
				ha:Break buffer subcircuits to pieces = { action=PasteBuffer(Restore) }
				ha:Save buffer subcircuits to file = { action=Save(PasteBuffer) }
				ha:Layer bindings...               = { action=LayerBinding(buffer) }
				-
				ha:Select Buffer \#1 = { checked=ChkBuffer(1); m=1; a=Shift<Key>1; action=PasteBuffer(1); update_on={editor/buffer_number} }
				ha:Select Buffer \#2 = { checked=ChkBuffer(2); m=2; a=Shift<Key>2; action=PasteBuffer(2); update_on={editor/buffer_number} }
				ha:Select Buffer \#3 = { checked=ChkBuffer(3); m=3; a=Shift<Key>3; action=PasteBuffer(3); update_on={editor/buffer_number} }
				ha:Select Buffer \#4 = { checked=ChkBuffer(4); m=4; a=Shift<Key>4; action=PasteBuffer(4); update_on={editor/buffer_number} }
				ha:Select Buffer \#5 = { checked=ChkBuffer(5); m=5; a=Shift<Key>5; action=PasteBuffer(5); update_on={editor/buffer_number} }
			}
		} # Buffer

		ha:Connects = {
			m=C
			li:submenu {
				ha:Lookup connection to object   = { a=Ctrl<Key>f; li:action={GetXY(Click on the object); Connection(Find)} }
				ha:Reset scanned pads/pins/vias  = { li:action={Connection(ResetPinsViasAndPads); Display(Redraw)} }
				ha:Reset scanned lines/polygons  = { li:action={Connection(ResetLinesAndPolygons); Display(Redraw)} }
				ha:Reset all connections         = { a=Shift<Key>f; li:action={Connection(Reset); Display(Redraw)} }
				-
				ha:Optimize rats nest            = { a=<Key>o; li:action={Atomic(Save); DeleteRats(AllRats); Atomic(Restore); AddRats(AllRats); Atomic(Block)} }
				ha:Erase rats nest               = { a=<Key>e; action=DeleteRats(AllRats) }
				ha:Erase selected rats           = { a=Shift<Key>e; action=DeleteRats(SelectedRats) }
				-
				ha:Auto-route selected rats      = { action=AutoRoute(Selected) }
				ha:Auto-route all rats           = { action=AutoRoute(AllRats) }
				ha:Rip up all auto-routed tracks = { action=RipUp(All) }
				-
				ha:Optimize routed tracks {
					li:submenu {
						ha:Auto-Optimize        = { a={Ctrl<char>=}; action=djopt(auto) }
						ha:Debumpify            = { action=djopt(debumpify) }
						ha:Unjaggy              = { action=djopt(unjaggy) }
						ha:Vianudge             = { action=djopt(vianudge) }
						ha:Viatrim              = { action=djopt(viatrim) }
						ha:Ortho pull           = { action=djopt(orthopull) }
						ha:Simple optimization  = { a={<char>=}; action=djopt(simple) }
						ha:Miter                = { action=djopt(miter) }
						ha:Puller               = { a=<Key>y; action=Puller() }
						ha:Global Puller {
							li:submenu {
								ha:Selected  = { action=GlobalPuller(selected) }
								ha:Found     = { action=GlobalPuller(found) }
								ha:All       = { action=GlobalPuller() }
							}
						}
						-
						ha:Only autorouted nets = { checked=plugins/djopt/auto_only; action=conf(toggle, plugins/djopt/auto_only, design) }
					}
				}
				-
				ha:Design Rule Checker = { action=DRC() }
				-
				ha:Apply vendor drill mapping = { action=ApplyVendor() }
				-
				ha:Design changes (back annotation) {
					li:submenu {
						ha:Swap nets on two selected pins  = { a=Shift<Key>x; action=net(swap) }
						ha:Replace footprint               = { a=Alt Shift<Key>f; action=ReplaceFootprint() }
					}
				}
			}
		} # Connects
	
		ha:Plugins {
			m=P
			li:submenu {
				ha:Manage plugins... = { a=Alt<Key>p; action=ManagePlugins() }
			}
		} # Plugins

		ha:Info = {
			m=I
			li:submenu {
				ha:Generate object report  = { a=Ctrl<Key>r; action=ReportObject() }
				ha:Generate drill summary  = { action=Report(DrillReport) }
				ha:Report found pins\/pads = { action=Report(FoundPins) }
				ha:Key Bindings {
					li:submenu {
						ha:Remove                       = { a=<Key>Delete; li:action={Mode(Save); Mode(Remove); Mode(Notify); Mode(Restore)} }
						ha:Remove Selected              = { a=<Key>BackSpace; action=RemoveSelected() }
						ha:Remove Connected             = { a=Shift<Key>BackSpace; li:action={Atomic(Save); Connection(Reset); Atomic(Restore); Unselect(All); Atomic(Restore); Connection(Find); Atomic(Restore); Select(Connection); Atomic(Restore); RemoveSelected(); Atomic(Restore); Connection(Reset); Atomic(Restore); Unselect(All); Atomic(Block)} }
						ha:Remove Connected             = { li:action={Atomic(Save); Connection(Reset); Atomic(Restore); Unselect(All); Atomic(Restore); Connection(Find); Atomic(Restore); Select(Connection); Atomic(Restore); RemoveSelected(); Atomic(Restore); Connection(Reset); Atomic(Restore); Unselect(All); Atomic(Block)} }
						ha:Set Same                     = { a=<Key>a; action=SetSame() }
						ha:Flip Object                  = { a=<Key>b; action=Flip(Object) }
						ha:Find Connections             = { a=<Key>f; li:action={Connection(Reset); Connection(Find)} }
						ha:ChangeJoin Object            = { a=<Key>j; action=ChangeJoin(Object) }
						ha:ChangeJoin SelectedObject    = { a=Shift<Key>j; action=ChangeJoin(SelectedObjects) }
						ha:Clear Object +2 mil          = { a=<Key>k; action=ChangeClearSize(Object,+2,mil) }
						ha:Clear Object -2 mil          = { a=Shift<Key>k; action=ChangeClearSize(Object,-2,mil) }
						ha:Clear Selected +2 mil        = { a=Ctrl<Key>k; action=ChangeClearSize(SelectedObjects,+2,mil) }
						ha:Clear Selected -2 mil        = { a=Shift Ctrl<Key>k; action=ChangeClearSize(SelectedObjects,-2,mil) }
						ha:Line Tool size +5 mil        = { a=<Key>l; action=SetValue(LineSize,+5,mil) }
						ha:Line Tool size -5 mil        = { a=Shift<Key>l; action=SetValue(LineSize,-5,mil) }
						ha:Move Object to current layer = { a=<Key>m; action=MoveToCurrentLayer(Object) }
						ha:MarkCrosshair                = { a=Ctrl<Key>m; action=MarkCrosshair() }
						ha:Select shortest rat          = { a=Shift<Key>n; action=AddRats(Close) }
						ha:AddRats to selected pins     = { a=Shift<Key>o; li:action={Atomic(Save); DeleteRats(AllRats); Atomic(Restore); AddRats(SelectedRats); Atomic(Block)} }
						ha:Polygon PreviousPoint        = { a=<Key>p; action=Polygon(PreviousPoint) }
						ha:Polygon Close                = { a=Shift<Key>p; action=Polygon(Close) }
						ha:ChangeSizes to Route style   = { a=Shift<Key>y; action=ChangeSizes(Object,style,mil) }
						ha:ChangeSize +5 mil            = { a=<Key>s; action=ChangeSize(Object,+5,mil) }
						ha:ChangeSize -5 mil            = { a=Shift<Key>s; action=ChangeSize(Object,-5,mil) }
						ha:ChangeDrill +5 mil           = { a=Alt<Key>s; action=ChangeDrillSize(Object,+5,mil) }
						ha:ChangeDrill -5 mil           = { a=Alt Shift<Key>s; action=ChangeDrillSize(Object,-5,mil) }
						ha:Text Tool scale +10 mil      = { a=<Key>t; action=SetValue(TextScale,+10,mil) }
						ha:Text Tool scale -10 mil      = { a=Shift<Key>t; action=SetValue(TextScale,-10,mil) }
						ha:Route radius +0.5            = { a=Alt<Key>c; action=conf(delta, editor/route_radius, +0.5, design); }
						ha:Route radius -0.5            = { a=Alt Shift<Key>c; action=conf(delta, editor/route_radius, -0.5, design); }
						ha:AddRats Selected             = { a=Shift<Key>w; action=AddRats(SelectedRats) }
						ha:Add All Rats                 = { a=<Key>w; action=AddRats(AllRats) }
						ha:Cycle Clip                   = { a=<char>/; action=Display(CycleClip) }
						ha:Arrow Mode                   = { a=<Key>space; checked=ChkMode(arrow); action=Mode(Arrow); update_on={editor/mode} }
						ha:Temp Arrow ON                = { a=<char>[; li:action={Mode(Save); Mode(Arrow); Mode(Notify)} }
						ha:Temp Arrow OFF               = { a=<char>]; li:action={Mode(Release); Mode(Restore)} }
						-
						ha:Step Up                      = { a=<Key>Up; action=Cursor(Warp,0,1,grid) }
						ha:Step Down                    = { a=<Key>Down; action=Cursor(Warp,0,-1,grid) }
						ha:Step Left                    = { a=<Key>Left; action=Cursor(Warp,-1,0,grid) }
						ha:Step Right                   = { a=<Key>Right; action=Cursor(Warp,1,0,grid) }
						ha:Step +Up                     = { a=Shift<Key>Up; action=Cursor(Pan,0,50,view) }
						ha:Step +Down                   = { a=Shift<Key>Down; action=Cursor(Pan,0,-50,view) }
						ha:Step +Left                   = { a=Shift<Key>Left; action=Cursor(Pan,-50,0,view) }
						ha:Step +Right                  = { a=Shift<Key>Right; action=Cursor(Pan,50,0,view) }
						ha:Click                        = { a=<Key>Enter; li:action={Mode(Notify); Mode(Release)} }
						-
						ha:layer keys {
							li:submenu {
								ha:Select Layer 1           = { a=<Key>1; action=SelectLayer(1) }
								ha:Select Layer 2           = { a=<Key>2; action=SelectLayer(2) }
								ha:Select Layer 3           = { a=<Key>3; action=SelectLayer(3) }
								ha:Select Layer 4           = { a=<Key>4; action=SelectLayer(4) }
								ha:Select Layer 5           = { a=<Key>5; action=SelectLayer(5) }
								ha:Select Layer 6           = { a=<Key>6; action=SelectLayer(6) }
								ha:Select Layer 7           = { a=<Key>7; action=SelectLayer(7) }
								ha:Select Layer 8           = { a=<Key>8; action=SelectLayer(8) }
								ha:Select Layer 9           = { a=<Key>9; action=SelectLayer(9) }
								ha:Select Layer 10          = { a=<Key>0; action=SelectLayer(10) }
								ha:Select Layer 11          = { a=Alt<Key>1; action=SelectLayer(11) }
								ha:Select Layer 12          = { a=Alt<Key>2; action=SelectLayer(12) }
								ha:Select Layer 13          = { a=Alt<Key>3; action=SelectLayer(13) }
								ha:Select Layer 14          = { a=Alt<Key>4; action=SelectLayer(14) }
								ha:Select Layer 15          = { a=Alt<Key>5; action=SelectLayer(15) }
								ha:Select Layer 16          = { a=Alt<Key>6; action=SelectLayer(16) }
								ha:Select Layer 17          = { a=Alt<Key>7; action=SelectLayer(17) }
								ha:Select Layer 18          = { a=Alt<Key>8; action=SelectLayer(18) }
								ha:Select Layer 19          = { a=Alt<Key>9; action=SelectLayer(19) }
								ha:Select Layer 20          = { a=Alt<Key>0; action=SelectLayer(20) }
								-
								ha:Toggle Layer 1           = { a=Ctrl<Key>1; action=ToggleView(1) }
								ha:Toggle Layer 2           = { a=Ctrl<Key>2; action=ToggleView(2) }
								ha:Toggle Layer 3           = { a=Ctrl<Key>3; action=ToggleView(3) }
								ha:Toggle Layer 4           = { a=Ctrl<Key>4; action=ToggleView(4) }
								ha:Toggle Layer 5           = { a=Ctrl<Key>5; action=ToggleView(5) }
								ha:Toggle Layer 6           = { a=Ctrl<Key>6; action=ToggleView(6) }
								ha:Toggle Layer 7           = { a=Ctrl<Key>7; action=ToggleView(7) }
								ha:Toggle Layer 8           = { a=Ctrl<Key>8; action=ToggleView(8) }
								ha:Toggle Layer 9           = { a=Ctrl<Key>9; action=ToggleView(9) }
								ha:Toggle Layer 10          = { a=Ctrl<Key>0; action=ToggleView(10) }
								ha:Toggle Layer 11          = { a=Ctrl-Alt<Key>1; action=ToggleView(11) }
								ha:Toggle Layer 12          = { a=Ctrl-Alt<Key>2; action=ToggleView(12) }
								ha:Toggle Layer 13          = { a=Ctrl-Alt<Key>3; action=ToggleView(13) }
								ha:Toggle Layer 14          = { a=Ctrl-Alt<Key>4; action=ToggleView(14) }
								ha:Toggle Layer 15          = { a=Ctrl-Alt<Key>5; action=ToggleView(15) }
								ha:Toggle Layer 16          = { a=Ctrl-Alt<Key>6; action=ToggleView(16) }
								ha:Toggle Layer 17          = { a=Ctrl-Alt<Key>7; action=ToggleView(17) }
								ha:Toggle Layer 18          = { a=Ctrl-Alt<Key>8; action=ToggleView(18) }
								ha:Toggle Layer 19          = { a=Ctrl-Alt<Key>9; action=ToggleView(19) }
								ha:Toggle Layer 20          = { a=Ctrl-Alt<Key>0; action=ToggleView(20) }
							}
						} # layer keys
					}
				}
			}
		} # Info

		ha:Window {
			m=W
			li:submenu {
				ha:Library        = { a=<Key>i; action=DoWindows(Library) }
				ha:Message Log    = { action=DoWindows(Log) }
				ha:DRC Check      = { action=DoWindows(DRC) }
				ha:Netlist        = { action=DoWindows(Netlist) }
				ha:Command Entry  = { a={<char>:}; action=Command() }
				ha:Pinout         = { a=Shift<Key>d; action=Display(Pinout) }
				ha:Font selector  = { a=Ctrl Shift<Key>f; action=FontSel() }
				-
				ha:About...       = { action=About() }
			}
		} # Window
	} # main menu

	li:popups {

		ha:popup1 {
			li:submenu {
				ha:Operations on selections {
					li:submenu {
						ha:Unselect all objects               = { action=Unselect(All) }
						ha:Remove selected objects            = { action=RemoveSelected() }
						ha:Copy selection to buffer           = { li:action={GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); Mode(PasteBuffer)} }
						ha:Cut selection to buffer            = { li:action={GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); RemoveSelected(); Mode(PasteBuffer)} }
						ha:Convert selection to subcircuit    = { action=Select(ConvertSubc) }
						ha:Auto place selected subcircuits    = { action=AutoPlaceSelected() }
						ha:Autoroute selected subcircuits     = { action=AutoRoute(SelectedRats) }
						ha:Rip up selected auto-routed tracks = { action=RipUp(Selected) }
						ha:Rotate selected                    = { action={GetXY(Click to set the snap point for this buffer); PasteBuffer(Clear); PasteBuffer(AddSelected); RemoveSelected(); Mode(PasteBuffer) FreeRotateBuffer(); Mode(PasteBuffer); PasteBuffer(Tolayout, crosshair); } }
					}
				}
				ha:Operations on this location {
					li:submenu {
						ha:Generate object report = { li:action={GetXY(Click on the object); Report(Object)} }
					}
				}
				-
				ha:Undo last operation          = { action=Undo() }
				ha:Redo last undone operation   = { action=Redo() }
				-
				ha:Tools {
					li:submenu {
						ha:None           = { checked=ChkMode(none); action=Mode(None); update_on={editor/mode} }
						ha:Via            = { checked=ChkMode(via); a=<Key>F1; action=Mode(Via); update_on={editor/mode} }
						ha:Line           = { checked=ChkMode(line); a=<Key>F2; action=Mode(Line); update_on={editor/mode} }
						ha:Arc            = { checked=ChkMode(arc); a=<Key>F3; action=Mode(Arc); update_on={editor/mode} }
						ha:Text           = { checked=ChkMode(text); a=<Key>F4; action=Mode(Text); update_on={editor/mode} }
						ha:Rectangle      = { checked=ChkMode(rectangle); a=<Key>F5; action=Mode(Rectangle); update_on={editor/mode} }
						ha:Polygon        = { checked=ChkMode(polygon); a=<Key>F6; action=Mode(Polygon); update_on={editor/mode} }
						ha:Polygon Hole   = { checked=ChkMode(polygonhole); action=Mode(PolygonHole); update_on={editor/mode} }
						ha:Buffer         = { checked=ChkMode(pastebuffer); a=<Key>F7; action=Mode(PasteBuffer); update_on={editor/mode} }
						ha:Remove         = { checked=ChkMode(remove); a=<Key>F8; action=Mode(Remove); update_on={editor/mode} }
						ha:Rotate         = { checked=ChkMode(rotate); a=<Key>F9; action=Mode(Rotate); update_on={editor/mode} }
						ha:Thermal        = { checked=ChkMode(thermal); a=<Key>F10; action=Mode(Thermal); update_on={editor/mode} }
						ha:Arrow          = { checked=ChkMode(arrow); a=<Key>F11; action=Mode(Arrow); update_on={editor/mode} }
						ha:Insert Point   = { checked=ChkMode(insertpoint); a=<Key>Insert; action=Mode(InsertPoint); update_on={editor/mode} }
						ha:Move           = { checked=ChkMode(move); action=Mode(Move); update_on={editor/mode} }
						ha:Copy           = { checked=ChkMode(copy); action=Mode(Copy); update_on={editor/mode} }
						ha:Lock           = { checked=ChkMode(lock); a=<Key>F12; action=Mode(Lock); update_on={editor/mode} }
						ha:Cancel         = { a=<Key>Escape; action=Mode(Escape) }
					}
				}
			}
		} # popup1

		ha:layer {
			li:submenu {
				ha:Layer properties...              = { action=EditLayer() }
				ha:Layer attributes...              = { action=Attributes(Layer) }
				ha:Grouping... (layer preferences)  = { action=DoWindows(Preferences, 1, "User PoV/Layers") }
				-
				ha:Move up (within group)           = { action=MoveLayer(c, step-) }
				ha:Move down (within group)         = { action=MoveLayer(c, step+) }
				-
				ha:Remove                           = { action=MoveLayer(c, -1) }
				ha:Insert new, before this layer    = { action={ MoveLayer(-1, c); EditLayer() } }
				ha:Insert new, after this layer     = { action={ MoveLayer(-1, c); MoveLayer(c, step+); EditLayer() } }
				-
				ha:all layers on                    = { action=ToggleView(all, vis, set) }
				ha:all layers off                   = { action=ToggleView(all, vis, clear) }
			}
		} # layer

		ha:group {
			li:submenu {
				ha:Group properties...              = { action=EditGroup(@) }
				ha:Grouping... (layer preferences)  = { action=DoWindows(Preferences, 1, "User PoV/Layers") }
				-
				ha:Insert new layer                 = { action={ MoveLayer(-1, gi); EditLayer() } }
				ha:Append new layer                 = { action={ MoveLayer(-1, ga); EditLayer() } }
				-
				ha:expand groups                    = { action=ToggleView(all, open, set) }
				ha:collapse groups                  = { action=ToggleView(all, open, clear) }
				ha:all layers on                    = { action=ToggleView(all, vis, set) }
				ha:all layers off                   = { action=ToggleView(all, vis, clear) }
			}
		} # group

	} # popups
} # root
