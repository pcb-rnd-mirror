extern pcb_tool_t pcb_tool_arrow;

void pcb_tool_arrow_uninit(void);
void pcb_tool_arrow_notify_mode(void);
void pcb_tool_arrow_release_mode(void);
void pcb_tool_arrow_adjust_attached_objects(void);
