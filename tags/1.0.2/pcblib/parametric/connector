#!/bin/sh

#@@example connector(2, 3, silkmark=external)

#@@purpose Generate pin-array connectors (e.g. headers).

#@@desc Generate thru-hole connectors that consits of an array of
#@@desc evenly spaced pins, a plain frame and some optional pin marks
#@@desc on the silk layer.

#@@params nx, ny, spacing, silkmark, eshift, etrunc

#@@param:nx number of pins in the X direction
#@@param:ny number of pins in the Y direction
#@@param:spacing spacing between the pins
#@@dim:spacing
#@@default:spacing 100 mil

#@@param:silkmark how to mark pin 1 on the silk layer
#@@enum:silkmark:square a rectangle around pin 1
#@@enum:silkmark:external a little trinagle placed outside of the box
#@@enum:silkmark:angled an angled line in the corner
#@@enum:silkmark:none no mark
#@@optional:silkmark
#@@default:silkmark square
#@@preview_args:silkmark 2,3


#@@param:eshift shift even rows or columns by half spacing (optional; default: don't shift)
#@@enum:eshift:x shift columns
#@@enum:eshift:y shift rows
#@@enum:eshift:none do not shift anything
#@@default:eshift none
#@@optional:eshift
#@@preview_args:eshift 2,3

#@@param:etrunc truncate the last pin of a shifted row or column
#@@bool:etrunc
#@@default:etrunc false
#@@optional:etrunc
#@@preview_args:etrunc 2,3,eshift=x


#@@include common.awk

awk -f `dirname $0`/common.awk -f `dirname $0`/connector.awk -v "args=$*" -v gen=`basename $0`

