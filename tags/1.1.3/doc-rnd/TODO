- BUGS
	? all route styles deleted when using 'File>load layout data to paste buffer' 
	+ in the 45-deg line mode, while DRC enforce settings toggled 'on' can cause isolated trace crossing, violating DRC
	- save as: change format, save as again: it sets back the format to default but the file name is wrong [report:Erich]

- Lihata persistent save:
	- flag compatibility: save unknown, string-flags as loaded by io_pcb
	- ind: space in ha:layer name
	- ind: symbol height is not in-line on the output
	- ind: indentation bug in via and netlists
	- ind: indentation bug in inline struct therm: closing } is preceded by indentation whitespace for some reason
	- keep numeric format: test all
	- keep unknown subtrees

- replace settings with lihata (conf_*)
	- TEST:
		- vendor drill mapping (all functionality, including regex based refdes ignore)
		- route style in gtk and lesstif, save, load, 0, 1 or a lot of styles
		- route style vs. autorouter

- query & advanced search
	- search expr wizard should pick up the expression from the button when clicked
	- field accessors:
		- pad area
		- pin area
		- line (and pad) length and length^2
	- make a run on alarge board, speed test, with -O0 and -O3:
		- iteration speed (a simple @)
		- eval speed (a simple @ with a lot of constants in an expr)
		- geo speed
		- regex and string cmp match speed vs. select by name

CLEANUP #5
- save/load bugs:
	- gtk load dialog extension filter should reflect known formats
	- when changing format in the save dialog, the file extension is not modified
	- there should be an "auto" or "guess by extension" option in the save dialog format
- convert PCBChanged into an event (and provide a core action for compatibility)
- rework the gpmi plugin to make it more standard in regard of the build system and to remove a few directories
- dir rename trunk/pcblib/tru-hole should handle make correctly and not walk on existing web services or user installs
- grid status line label doesn't update when using setvalue(grid,delta) add another hook to update this value [miloh]
- Erich's select undo bug: place an element, select, save & load, unselect, undo 2 times: bad serial
- Erich's gtk lag report: press 's' a lot then move the mouse - 's' ends up in the new loc!
- conf: throw an error if user config can not be written
- the TAB bug: (over ssh -X) press tab for a good 15 seconds; release; it won't work again for a fraction of a second
- mark bug - ortho shouldn't use it, it should use last point (crosshair.X2); make sure nothing else abuses it [James]
- conf:
	- remove CFN_INCREMENTS, test if saving increments from preferences works
	- config pov: change prio
	- gtk preferences: check what takes 131 megs of VIRT ram (on top); destroy the dialog structures when closed
	- gtk preferences: poly isle area widget missing from the sizes tab
	- debug why default layer names from the user config are ignored
	- fp_fs_search: kill anything that gets ':' separated string as path list, get a config list instead
	- switch the lesstif HID over from attribs to conf
	- remove redundancy: don't store values in the PCB struct that are in the design conf
	- increments are not really implemented
	- if font file is not found:
		- embedded version?
		- warn when try to write text on pcb?
- action bug: gui_act.c shouldn't reference Crosshair.X directly; check d-fix.patch about how to fix it
- next_gui: keep them open, hide
- look for #warnings
- fix librarychanged: disable update of gedasymbols on-start, make it an explicit refresh button
- libstroke: zoom
- cleanup/rewrite resources:
	- load/swap menus (TODO#1)
	- gpmi:
		- hid: hid_destroy (pair of hid_create)
		- cleanup on unload: remove menus
- check whether local copy of gts is needed, bisect when toporouter broke 
- check gpmi when pcb-rnd is fully linstalled - broken symlinks?
- multi-key: display state on the gui
- implement loglevels and log style tags in the lesstif HID
- TODO#3: works only on US keyboard
- gsch2pcb: generalize plugin/buildin loading for external tools, check if gsch2pcb can work from plugins

CLEANUP #6:
- lihata board format:
	- pads should be polygons (0 long line can't be rotated!)
- layer groups from config (e.g. CLI) is ignored
- res:
	- search for vendor in the hid plugins, there should be no trace of it (vendor should register its in submenus with anchors)
	- re-add dynamic menus after a gui change:
		- either remember dynamic menus in a separate list so they can be rebuilt
		- or provide hooks and let plugins deal with it
	- gpmi: auto-remove menus by cookie (TODO#2)
	- load new res on the fly (replace the menu system):
		- low level reload code (re-add the dynamic menus too!)
		- action to reload if file name is known
		- gui load dialog with tags listed
- decide about exporter policy:
	- png exports selection (even in not-as-shown), others don't
	- extend png hid attribs with a flag for this, maybe do the same for others
	- document the decision in "hacking"
- reduce
		- export_bom: rewrite the string list code using htsp and/or lists
		- hash in hid_attrib.c?
		- nelma and gcode both invent .png name locally
		- get rid of gcode/lists.h,  and vector.[ch] (autorouter)
		- vendordrill
			- search for /units, replace it with pcb-printf something
			- add round down
			- replace ignore_refdes, ignore_value and ignore_descr with genvector
	- mods:
		- gpmi (and other buildins/plugins) not showing up in the about box
- self contained
	- files
		- default font
	- action (--show-paths!) and dialog box to print where the actual files are coming from
	- project specific menus from extra lihata files - maybe from project.lht
- main.c:
	- SIGPIPE - detect it (needed for popen)

CLEANUP #7: the big object split
- remove pcb_obj_type_t from const.h - use obj_any.h instead




FEATURES
- BUILD: menuconfig and a config file for scconfig

Low prio:
- scconfig: check if it picks up settings from env vars (for a gentoo build)
- replace mkdtemp() with something safer
- display net names on pins, vias (and maybe tracks?) when zoomed in enough
- DRC should warn for thin poly hair
- rotate shaped vias don't rotate the shape (is via rotated at all?)
- new examples
	- blinking led with parametric footprints
	- example of nonetlist: 1206 jumpers and logos
- decide what to do with old doc - texi doesn't seem to work at all
- rethink/rewrite the action/change infrastructure - too many void *ptr1
  pointers, too many code duplication
- double sided board, poly on both layers; grab existing lines on one layer and
  move then around. If all layers are visible, redraw of the far side layer
  is slow and causes flickering elements from that layer until the front is
  redrawn. Maybe we should have less flushes?
- gpmi:
	- dialog: attribute dialog: mini parser from text
	- fix debug draw in the gtk hid
	- ACTE_action(): coords should not be int but Coord
	- get timers to work
- dmalloc: #include from the bottom from each file. Is this a good idea?!
- win32 port {large}
	- clean up central Makefile.in rules:
		- remove cat, sed, grep where possible, use scconfig instead
- arc bug: draw an arc with one end out of the drawing area; it will be jumpy and can not be placed properly
	-> AttachForCopy() calls SetCrosshairRange() that then sets corsshair max* which
	limits the arc to move freely. Problem: this is not arc-specific, this happens with any buffer copy! going for no-design-limit is probably better
- while drawing a line, the temporary outline should reflect the layer color
- push&shove
	- keep 45-deg knee when grabbing a point for drag&drop in non-all-dir
- examine current handling of long options [miloh]
