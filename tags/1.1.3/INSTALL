1. Installation

Run ./configure.

Run make.

Run make install.

For compile-time options run ./configure --help

2. Running from source

cd src && ./pcb-rnd

(Note: it is important to cd to src to run pcb-rnd from source; src/pcb-rnd
won't work unless pcb-rnd is installed).

If this doesn't work, please refer to doc-rnd/UNIX.txt or doc-rnd/mac.txt

--

PCB is organized into:
  src/            a core program that deals with all of the internal
                  database procedures
  src_plugins/    a collection of plugins
  src_3rd/        third-party minilibs (dependencies, not integral parts)
  pcblib/         a basic footprint library
  tests/          automated tests to check whether pcb-rnd works properly
  util/           utility programs like gsch2pcb-rnd


./configure will try to static link most plugins and disable ones that
have missing dependencies. This process can be controlled using configure
command line switches, see ./configure --help.

After running ./configure with your selected options, run

  make

to build pcb-rnd.  You can try out the program by running

  cd src
  ./pcb-rnd

prior to installation (CWD _must_ be src/).

To install PCB after it has been built run:

  make install

from the top level directory. An alternative installation method
is the link-install, which places symlinks instead of copying files so
no subsequent make install is needed after a recompilation if no new
files appeared (useful for developers):

  make linstall

-------- Summary of dependencies --------------------
For users:
 - C compiler
 - make
 - optional: glib and gtk if you are using the gtk frontend
 - motif or lesstif if you are using the lesstif frontend
 - gdlib if you are using the png HID

For developers:
 - flex
 - bison

