<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title> pcb-rnd user manual </title>
	<meta http-equiv="Content-Type" content="text/html;charset=us-ascii">
	<link rel="stylesheet" type="text/css" href="../default.css">
</head>
<body>
<h1> pcb-rnd - user manual </h1>

<h2> 2. Model of the world  </h2>
<p>
Pcb-rnd is designed to handle geometric data of a PCB. This section describes
how pcb-rnd represents reality (e.g. copper shapes) in memory.

<h3> 2.1. board </h3>
<p>
Each design pcb-rnd handles is a <i>board</i>. The board has global properties and hosts
layers. Most drawing primitives (objects) are on layers. This section describes
the most important global properties.
<p>
<b>Board size</b> is given as a width and a height. For rectangular boards
this can be the real board size, but more commonly it is used to simply
determine the on-screen drawing area and the final board dimensions are
specified using the outline layer. If the board is not rectangular, the
contour must be specified on the outline layer and the board size must be
large enough that the outline fits in it.
<p>
<b>Netlist</b> is the list of logical connections to be realized in copper.
A netlist is a list of named <i>nets</i>. Each net consists of a list of
<i>terminals</i> (pins or pads) to connect. A terminal is given as
elementname-pinname, e.g. U4-7 means "pin number 7 in element called U4".
<p>
<b>Font</b>, which is always embedded in the design file to guarantee
that the file can be ported and will look the same on different hosts.
<p>
Misc editor settings, such as grid size and offset.

<h3> 2.2. layers </h3>
<p>
Unlike a <i>physical layer</i>, a pcb-rnd layer has no thickness. It is a
2 dimensional, logical canvas, similar to layers in image manipulation software
like gimp. There are <i>explicit</i> and <i>virtual</i> layers. An explicit
layer contains drawing primitives (objects) placed by the user. The user has
full control over an explicit layer: objects can be added or removed or
changed any time. A virtual layer has no such flexibility: pcb-rnd computes
its content from explicit layers and there's no way to change the result
directly. A layer is always part of a <i>layer group</i>.

<h3> 2.3. layer groups </h3>
<p>
One or more explicit layers form a <i>layer group</i>. All pcb-rnd layers
of a layer group will end up on the same physical layer. The visibility of
layers in a layer group are toggled together. Having more than one layer
in a group may be useful to:
<ul>
	<li> exploit that layers have different drawing color on screen: e.g. there
	     can be a signal and a gnd layers with different color in the same
	     layer group, on the same physical layer.
	<li> on composite-groups (mask, paste, [TODO]), layers are combined using
	     their properties: some layers may draw, others may clear; some layers
	     may contain auto-generated objects. The order of layers does matter
	     in draw-clear combinations.
<ul>
<p>
Since <i>layer groups</i> donate the physical layers, a board stack-up
is built of layer groups. Substrates are layer groups without drawable
layers in them. The mask, paste, silk layer groups behave the same and
can host zero or more logical layers.
<p>
Each laeyr group has a type. Types are:
<ul>
	<li> copper for signal layers
	<li> silk for silkscreen layers
	<li> outline for the contour of the board and slots and cutouts
	<li> mask for solder masks
	<li> paste for solder paste
</ul>
<p>
Each layer group has a location. Locations are:
<ul>
	<li> top (used to be the "component side" in the age of thru-hole components)
	<li> bottom (used to be the "solder side" in the age of thru-hole components)
	<li> intern (sandwiched between other layers)
	<li> global (affects all physical layers, thus has no specific location)
</ul>
<p>
Not all combination of type and location are supported: e.g. for an outline
layer group the location is always global. The table below lists whether a
combination is supported or not.
<p>
<table border=1>
	<tr><th> &nbsp; <th> top <th> bottom <th> intern <th> global <th> composite
	<tr><th> copper <td> yes <td> yes    <td> yes    <td> no     <td> no
	<tr><th> silk   <td> yes <td> yes    <td> no     <td> no     <td> TODO
	<tr><th> outline<td> no  <td> no     <td> no     <td> yes    <td> no
	<tr><th> mask   <td> yes <td> yes    <td> no     <td> no     <td> yes
	<tr><th> paste  <td> yes <td> yes    <td> no     <td> no     <td> yes
</table>

<h3> 2.4. Basic drawing objects </h3>
<p>
Pcb-rnd supports a small number of basic drawing objects, from which complex
objects can be build. The following figure demonstrates all basic objects:
<p>
<img src="objects_basic.png" alt="[Arc, Line, Polygon, Pin, Via]">
<p>
Objects have flags that control their behavior. The following flags are common
to all objects:
<p>
<table>
	<tr><th>name        <th> description
	<tr><td>selected    <td> selected by the user ("cyan")
	<tr><td>found       <td> found as a galvanic connection in the last connection lookup ("green")
	<tr><td>warn        <td> offending object e.g. in a short circuit ("orange")
	<tr><td>lock        <td> locked by the user: can't be selected, moved or changed
</table>


<h4> 2.4.1. Line objects </h4>
<p>
Lines are round ended straight line segments with a width and
a clearance. The above image shows 3 lines connected. Lines are mainly
used to construct traces. A line is always on a specific layer. The user
interface allows drawing lines aligned to 90 or 45 degree axes or
lines with random angle.
<p>
A line is specified by its two endpoints, width and clearance:
<p>
<img src="obj_line.png" alt="[Line interacting with a polygon]">
<p>
A <i>clearance</i> is the gap between a line and the surrounding polygon
in the same layer group. The gap is made only if the surrounding polygon has
the "clearpoly" flag set and the line has the "clearline" flag set. If either
of these flags is not set, no gap is made - or in pcb-rnd terminology,
the line is joined to the polygon.
<p>
Extra object flags:
<p>
<table>
	<tr><th>name        <th> description
	<tr><td>clearline   <td> clears polygons with the "clearpoly" flag in the same layer group
</table>

<h4> 2.4.2. Arc objects </h4>
<p>
Arcs are round ended circular arcs with trace width and clearance. They
behave like lines in all respects.
<p>
<img src="obj_arc.png" alt="[Arc interacting with a polygon]">
<p>
Although the arc is described with it's center, radius, start and end
angles, the user interface may offer drawing arcs by endpoints.
<p>
Extra object flags:
<p>
<table>
	<tr><th>name        <th> description
	<tr><td>clearline   <td> clears polygons with the "clearpoly" flag in the same layer group
</table>

<h4> 2.4.3. Polygon objects </h4>
<p>
Polygons are solid, filled copper areas with optional holes in them. Polygon
contour consists of lines - when they look curvy, it's really high resolution
line approximation. There are two type of holes in a polygon: <i>explicit</i>,
user drawn holes and <i>clearance</i> cutouts. User drawn holes are "negative"
polygons drawn manually. To keep polygons simple, if an user drawn hole
touches the contour of a polygon, the hole is removed and the contour is
modified; if two holes touch, they are merged into one hole.
<p>
If the polygon has the "clearpoly" flag set (default), clearance cutouts are
automatically inserted around objects on the same layer group:
<ul>
	<li> lines and arcs, if they have the "clearline" flag set (default)
	<li> vias and pins, if they are not connected to the polygon by thermals
	<li> pads
</ul>
<p>
Overlapping or touching polygons are not automatically merged. An object
with the "clearline" flag set will clear all "clearpolys" it is over -
if there are multiple such polygons overlapping under the objects (on
the same layer group), all such polygons get the clearance cutout.
<p>
If a polygon is cut into multiple islands, the behaviour depends on the
"fullpoly" flag of the polygon. If it is not set (default), only the largest
island is kept, else all islands are kept. In the "fullpoly" mode islands
will have no galvanic connection (unless the user adds vias and connect them
on another layer), still the program will handle all islands as a single
polygon. This is risky: the program will indicate connection between polygon
islands that are not really connected, only because they are part of the same
polygon!
<p>
Extra object flags:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>clearpoly   <td> should have clearance around objects, if the objects have the appropriate flags too
	<tr><td>fullpoly    <td> keep all islands, not only the largest
</table>

<h4> 2.4.4. text objects </h4>
<p>
A text object is string and a series of symbols (pcb-rnd's terminology for
glyph). Symbols are built of lines and are stored in the font. Each board
can have its own font, but there can be only one font per board. When
the string of the text is edited, the object is rendered again so that the
new string appears.
<p>
Text objects can be placed on copper and silk layers. Text can be rotated
only in 90 degree steps. Each text object has a scale parameter that
determines its size in percentage. A scale of 100% means symbols are
rendered in 1:1 size.
<p>
The clearance around text is rendered as a round corner rectangular cutout.
Bug: copper text can not participate in short circuits, the galvanic connection
checker code skips texts.

<h4> 2.4.5. via objects </h4>
<p>
A via is a galvanized hole that connects copper rings on multiple layers.
Pcb-rnd currently has two limitations: there are no blind or buried vias -
a hole always punches all layers; there's no pad stack support, the copper
ring around the hole on each layer have the same sizes.
<p>
A copper ring also has a per layer property whether or how it connects to
the surrounding polygon on the given layer; this is called the thermal style
of the via. The following options are available:
<table border=1>
	<tr><th>thermal style <th> appearance
	<tr><td>no connection <td> <img src=TODO alt="TODO">
	<tr><td>solid        <td> <img src=TODO alt="TODO">
	<tr><td>round x 90   <td> <img src=TODO alt="TODO">
	<tr><td>round x 45   <td> <img src=TODO alt="TODO">
	<tr><td>sharp x 90   <td> <img src=TODO alt="TODO">
	<tr><td>sharp x 45   <td> <img src=TODO alt="TODO">
</table>
<p>
The shape of the copper annulus (the ring) is also selectable from a predefined
set of symmetrical and asymmetrical set:
<table border=1>
	<tr><th>shape          <th> appearance
	<tr><td>ring (default) <td> <img src=TODO alt="TODO">
	<tr><td>square         <td> <img src=TODO alt="TODO">
	<tr><td>octagon        <td> <img src=TODO alt="TODO">
	<tr><td>asymmetrical   <td> <img src=TODO alt="TODO">
</table>

<h4> 2.4.6. element objects and footprints </h4>

<p>
An element is an instance of a footprint that is already placed on the
board or loaded into a paste buffer.

<p>
In the footprint form the construct is small and flexible. It describes
all the physical parts, like pins, pads, silk lines. In the same time a
footprint leaves many details blank, e.g. it doesn't specify exact layers,
it doesn't have font and the refdes is random.

<p>
When the footprint is loaded, it becomes an element. The element inherits all
the physical properties and the blank details are filled in with the data taken
from the current board: the layer binding is done, all parts of the element
lands on a specific board layer; the refdes is rendered using the font in
the current board.

<p>
The footprint -&gt; element instantiation is also a copy. Once the element
is created from a footprint, the element is a self-containing object and
does not have any direct reference to the footprint it was once derived from.
Changes to the original footprint will <b>not</b> affect the elements.

<p>
In other words, a footprint is an abstract recipe, part of a
library, while an element is a land pattern already embedded in a
specific design and describes actual copper and silk.

<p>
Currently an element or footprint can contain the following objects:
<ul>
	<li> pins - same as vias, but also have a pin number and pin name within the element; they affect all layers
	<li> pads - short lines, usually with rectangle end cap; pads also have number and name; they affect the top or bottom copper layer only
	<li> silk lines - on top or bottom silk layer
	<li> silk arcs - on top or bottom silk layer
	<li> silk text - limited: only one of refdes, element name and element value is displayed at the time
</ul>
<p>
An element has the following properties:
<p>
<table border=1>
	<tr><th>element property   <th> description
	<tr><td> name: refdes      <td> unique identifier, e.g. "C42"
	<tr><td> name: value       <td> informal value, e.g. "15 pF"
	<tr><td> name: description <td> informal element or footprint description, e.g. "1206"
</table>
<p>
Extra object flags:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>ONSOLDER    <td> when set, the element is on the bottom side, else it's on the top side
	<tr><td>NONETLIST   <td> when set, the element is not intended to be on the netlist; useful for elements that are not present on the schematics and are placed during the layout design
	<tr><td>HIDENAME    <td> when set the name of the element is hidden
	<tr><td>DISPLAYNAME <td> when set the names of pins are shown
</table>

<h4> 2.4.7. pins of elements </h4>

A pin of an element is really a via, plus some metadata and capabilities:
<ul>
	<li> pin name
	<li> pin number
	<li> the capability to act as a netlist terminal
</ul>

Each element has its own list of pins. Pin rings can overlap (which will
make galvanic connection). There may be duplicate pin numbers and pin names.
Pin numbers are in the same namespace as pad numbers.

A pin has the following properties:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>name        <td> pin name, e.g. "base"
	<tr><td>number      <td> pin name, e.g. 2
	<tr><td>intconn     <td> internal element connections (see section TODO)
</table>

Extra object flags:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>via flags   <td> (extra flags listed for vias are applicable to pins too)
	<tr><td>WARN        <td> the pin contributes to a short circuit ("orange mark")
</table>

<h4> 2.4.8. pads of elements </h4>
<p>
A pad is an smd pad of an element. It is modelled as a line segment, usually
with square cap - this makes the pad look like a rectangle. A pad has
the same metadata and capabilities as pins. Overlapping pads are supported.
A pad is always on either the top or the bottom copper layer group.
<p>
A pad has the following properties:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>name        <td> pin name, e.g. "base"
	<tr><td>number      <td> pin name, e.g. 2
	<tr><td>intconn     <td> internal element connections (see section TODO)
</table>

Extra object flags:
<p>
<table border=1>
	<tr><th>name        <th> description
	<tr><td>via flags   <td> (extra flags listed for vias are applicable to pins too)
	<tr><td>WARN        <td> the pin contributes to a short circuit ("orange mark")
	<tr><td>EDGE2       <td> indicates that the second point is closer to the edge.  For pins, indicates that the pin is closer to a horizontal edge and thus pinout text should be vertical. (Padr.Point2 is closer to outside edge also pinout text for pins is vertical)
</table>

<h4> 2.4.9. rat line objects </h4>
<p>
A rat line represents a logical connection that is not yet realized in copper.
It connects two drawing primitives.

<h4> 2.4.10. netlists </h4>
<p>
A netlist is a list of named logical networks. Each network is a list of
netlist terminals that should be connected. A netlist terminal is a pair
of element-refdes and pin-number (or pad-number). Thus a typical netlist
looks like the following:
<ul>
	<li> net: GND
		<ul>
			<li> U1-2
			<li> U2-7
			<li> U3-7
			<li> C1-1
		</ul>
	<li> net: Vcc
		<ul>
			<li> U1-3
			<li> U2-14
			<li> U3-14
			<li> C1-2
		</ul>
	<li> net: strobe
		<ul>
			<li> U2-2
			<li> U3-5
		</ul>
</ul>
<p>
The netlist assumes element refdes are unique. If an element has multiple
instances of the same pin (or pad) number, the engine picks one randomly and
assumes there's an invisible, internal connection within the element.
<p>
Rat lines can be regenerated from the current netlist for missing connections.
Connections that are realized in copper but not present on the netlist, pcb-rnd
gives a "short circuit" warning. Both happens when the net is "optimized"
(upon user request).
<p>
The netlist is typically derived from a schematics by external tools
(such as gnetlist). The netlist can be imported (updated) any time. This
process is called "forward annotation".
<p>
It is also possible to make changes to the netlist from within pcb-rnd:
pins can be swapped, element packages replaced using <i>back annotation
actions</i>. Such actions will keep a list of intended netlist and element
changes, called the netlist patch. Pcb-rnd will keep these changes even
if a new version of the netlist is imported. It is possible to export the
netlist patch that can be imported in the schematics editor to change the
schematics - this process is called "back annotation". A new forward
annotation from the schematics editor to pcb-rnd will then cancel
the netlist/element changes as the new netlist import netlist matches
the intended (changed) netlist.

<h3> 2.5. comparison of terms </h3>
<p>
<table border=1>
	<tr><th> physical board term     <th> pcb-rnd term
	<tr><td> layer                   <td> layer group
	<tr><td> copper layer            <td> layer group with copper layers
	<tr><td> substrate               <td> N/A
	<tr><td> contour of the board    <td> outline layer
	<tr><td> outline routing path    <td> outline layer
	<tr><td> polygon pour            <td> polygon
	<tr><td> plane                   <td> polygon
</table>
