/* $Id$ */

#ifndef PCB_HID_GTK_GTKHID_H
#define PCB_HID_GTK_GTKHID_H

void ghid_notify_gui_is_up(void);

void gtkhid_begin(void);
void gtkhid_end(void);

#endif /* PCB_HID_GTK_GTKHID_H */
