#ifndef PCB_HID_COMMON_HIDNOGUI_H
#define PCB_HID_COMMON_HIDNOGUI_H

void pcb_hid_nogui_init(pcb_hid_t * hid);
pcb_hid_t *pcb_hid_nogui_get_hid(void);

#endif
