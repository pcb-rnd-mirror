extern pcb_tool_t pcb_tool_buffer;

void pcb_tool_buffer_notify_mode(void);
void pcb_tool_buffer_release_mode(void);
void pcb_tool_buffer_draw_attached(void);
