#include "data.h"

int tedax_fp_save(pcb_data_t *data, const char *fn);
int tedax_fp_load(pcb_data_t *data, const char *fn, int multi);
