/* $Id$ */

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define YYDEBUG 0
#define YYERROR_VERBOSE 1

typedef struct {
	int anons, inhash;
	int actions;
	char act_str[1024];
	char *parent_s;
} state_t;
state_t sstack[32];
#define ST(x) (sstack[level].x)

int reserror(char *);
int reslex();

char *parent_s;
int level = 0;

static void indd(int delta)
{
	int n;
	for(n = 0; n < level+delta; n++)
		putchar(' ');
}

static void ind()
{
	indd(0);
}

static void lopen()
{
	level++;
	memset(&sstack[level], 0, sizeof(sstack[level]));
	ST(parent_s) = parent_s;
}

static void lclose()
{
	level--;
}


void d1() {}

%}

%name-prefix "res"
%start top_res

%union {
  int ival;
  char *sval;
  void *rval;
};

%token <sval> STRING INCLUDE
%type <rval> res

%%

top_res:
	res_item_zm
	;

res:
	'{' { lopen(); } res_item_zm '}' { lclose(); }
	;

res_item_zm : res_item res_item_zm | ;
res_item:
		STRING {
			
			if (strcmp(ST(parent_s), "a") == 0) {
				switch(ST(anons)) {
					case 0: /* printf("name22=%s\n", $1); */ break;
					default: indd(-1); printf("a=%s\n", $1); break;
				}
			}
			else {
				char *e;
				switch(ST(anons)) {
					case 0:  indd(-1); printf("ha:%s = {\n", $1); ST(inhash)=1; break;
					default:
						e = ST(act_str)+strlen(ST(act_str));
						sprintf(e, "%s%s", (ST(actions) > 0 ? "; " : ""), $1);
						ST(actions)++;
						break;
				}
			}
			ST(anons)++;
		}
	|	STRING '=' STRING {
			ind();
			printf("%s=%s\n", $1, $3);
		}
	|	INCLUDE {
			ind();
			printf("anon2=%s\n", $1);
		}
	|	{ parent_s = strdup(""); } res {
d1();
		if (ST(inhash)) {
			level++; /* pretend we are still in */
			if (ST(actions) > 1) {
				ind();
				printf("li:action={%s}\n", ST(act_str));
			}
			else if (ST(actions) > 0) {
				ind();
				printf("action=%s\n", ST(act_str));
			}
			ind();
			printf("}\n");
			level--;
		}
		}
	|	STRING '=' { parent_s=strdup($1); } res
	|	error
	;

%%

static const char *res_filename = 0;
static FILE *res_file = 0;
static const char **res_strings = 0;
static int res_string_idx = 0;
int res_lineno;

int
res_parse_getchars(char *buf, int max_size)
{
  if (res_file)
    {
      int i = fgetc(res_file);
      buf[0] = i;
      if (i == EOF)
	return 0;
    }
  else
    {
      if (res_strings[0] == 0)
	return 0;
      buf[0] = res_strings[0][res_string_idx];
      res_string_idx ++;
      if (buf[0] == 0)
	{
	  res_strings ++;
	  res_string_idx = 0;
	  buf[0] = '\n';
	  return 1;
	}
    }
  return 1;
}

int resource_parse(FILE *f)
{
	res_file = f;
#if YYDEBUG
  yydebug = 1;
#endif
  return resparse();
}


int reserror(char *str)
{
	fprintf(stderr, "Error around line %d: %s\n", res_lineno, str);
	return 0;
}

