/* $Id$ */

%{

#include <string.h>
#include "res_parse.tab.h"

#define YY_NO_INPUT
#define YY_INPUT(buf,result,max_size) { result = res_parse_getchars(buf, max_size); }
 
extern int res_lineno;
extern int res_parse_getchars(char *buf, int max_size);

%}

%option prefix="res"
%option outfile="lex.yy.c"
%option yylineno
%option noyywrap

PARENSTR	([^ (){}=\"\'\t\r\n]|\([^\)]*\))+
INCSTR		@[a-z0-9A-Z_]+
COMMENT		#[^\n]*

%%

\"[^"]*\"	{ reslval.sval = strdup(yytext+1);
		  reslval.sval[strlen(reslval.sval) - 1] = 0;
		  return STRING; }

\'[^']*\'	{ reslval.sval = strdup(yytext+1);
		  reslval.sval[strlen(reslval.sval) - 1] = 0;
		  return STRING; }

{COMMENT}\n	{ res_lineno++; }
[ \t\r\n]	{ if (yytext[0] == '\n') res_lineno++; }

{INCSTR}	{ reslval.sval = strdup(yytext);
		  	  return INCLUDE; }

{PARENSTR}	{ reslval.sval = strdup(yytext);
		  	  return STRING; }

.		{ return yytext[0]; }

%%

/* ' */
