#include <stdio.h>
#include <stdarg.h>
#include <math.h>

int pcb_fprintf(FILE * f, const char *fmt, ...) { return 0; }
int pcb_vfprintf(FILE * f, const char *fmt, va_list args) { return 0; }
int pcb_vsnprintf(char *string, size_t len, const char *fmt, va_list args) { return 0; }

double pcb_distance(double x1, double y1, double x2, double y2)
{
	double delta_x = (x2 - x1);
	double delta_y = (y2 - y1);
	return sqrt(delta_x * delta_x + delta_y * delta_y);
}
