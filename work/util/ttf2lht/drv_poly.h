#include "config.h"
#include "src/pcb_bool.h"
#include "src/rtree.h"
#include "src/polyarea.h"

typedef struct {
	pcb_ttf_stroke_t s;

	/* polylines used while loading the outline */
	pcb_pline_t *curr;
	pcb_pline_t *pos, *neg;

	/* the resulting polyarea */
	int numpa;
	pcb_polyarea_t **pa;

	int spid;
	double maxx, maxy;
} poly_pcb_ttf_stroke_t;

extern poly_pcb_ttf_stroke_t poly_stroke;


