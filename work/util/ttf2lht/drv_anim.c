#include "ttf2lht.h"
#include "drv_anim.h"
#include "../../../trunk/src_plugins/import_ttf/str_approx.h"

static int stroke_anim_move_to(const FT_Vector *to, void *s_)
{
	anim_pcb_ttf_stroke_t *s = (anim_pcb_ttf_stroke_t *)s_;
	s->s.x = to->x;
	s->s.y = to->y;
	return 0;
}

static int stroke_anim_line_to(const FT_Vector *to, void *s_)
{
	anim_pcb_ttf_stroke_t *s = (anim_pcb_ttf_stroke_t *)s_;
	printf("line %f %f %ld %ld\n", s->s.x, s->s.y, to->x, to->y);
	s->s.x = to->x;
	s->s.y = to->y;
	return 0;
}

void stroke_anim_init(pcb_ttf_stroke_t *s)
{
	printf("frame\n");
}

void stroke_anim_start(pcb_ttf_stroke_t *s, int chr)
{
}

void stroke_anim_finish(pcb_ttf_stroke_t *s)
{
	printf("flush\n");
}

void stroke_anim_uninit(pcb_ttf_stroke_t *s)
{
}

anim_pcb_ttf_stroke_t anim_stroke = {
	{
		{
			stroke_anim_move_to,
			stroke_anim_line_to,
			stroke_approx_conic_to,
			stroke_approx_cubic_to,
			0, 0
		},
		stroke_anim_init,
		stroke_anim_start,
		stroke_anim_finish,
		stroke_anim_uninit,
	}
};
