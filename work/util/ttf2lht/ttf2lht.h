#include <freetype/ftoutln.h>

#include "../../../trunk/src_plugins/import_ttf/ttf_load.h"

#define OTX(s, x)  (((x) + (s)->dx) * (s)->scale_x)
#define OTY(s, y)  (((y) + (s)->dy) * (s)->scale_y)
