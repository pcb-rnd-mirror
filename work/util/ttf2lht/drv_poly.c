#include "ttf2lht.h"
#include "drv_poly.h"
#include "../../../trunk/src_plugins/import_ttf/str_approx.h"
#include "temp_copy.c"
#include "../../../trunk/src/box.h"

#define APPEND(list, pl) \
do { \
	pl->next = list; \
	list = pl; \
} while(0)

static void pline_close(poly_pcb_ttf_stroke_t *s)
{
	if (s->curr == NULL)
		return;

	pcb_poly_contour_pre(s->curr, pcb_true);

	if (s->curr->Flags.orient) {
		pcb_poly_contour_inv(s->curr);
		APPEND(s->neg, s->curr);
	}
	else {
		pcb_poly_contour_inv(s->curr);
		APPEND(s->pos, s->curr);
	}

	s->curr = NULL;
}

static int stroke_poly_move_to(const FT_Vector *to, void *s_)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)s_;
	pcb_vector_t v;

	pline_close(s);
	v[0] = to->x;
	v[1] = to->y;
	s->curr = pcb_poly_contour_new(v);
	s->s.x = to->x;
	s->s.y = to->y;
	return 0;
}

static int stroke_poly_line_to(const FT_Vector *to, void *s_)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)s_;
	pcb_vnode_t *tmp;
	pcb_vector_t v;

	v[0] = to->x;
	v[1] = to->y;

	tmp = pcb_poly_node_create(v);
	pcb_poly_vertex_include(s->curr->head.prev, tmp);

	s->s.x = to->x;
	s->s.y = to->y;
	return 0;
}

void stroke_poly_init(pcb_ttf_stroke_t *s_)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)s_;

	str_approx_comment = NULL;
	printf("li:pcb-rnd-font-v1 {\n");
	printf(" ha:geda_pcb {\n");
	printf("  id = 0\n");
	printf("  ha:symbols {\n");

	s->maxx = s->maxy = 0;
}

static void emit(pcb_pline_t *pl, void *user_data)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)user_data;
	pcb_coord_t n, i;
	pcb_vnode_t *v;

	printf("     li:simplepoly.%d {\n", s->spid++);
	n = pl->Count;
	for(i = 0, v = &pl->head; i < n; v = v->next, i++) {
		double x = OTX(&s->s, v->point[0]), y = 100.0 - OTY(&s->s, v->point[1]);
		if (x > s->maxx)
			s->maxx = x;
		if (y > s->maxy)
			s->maxy = y;
		printf("       %fmil; %fmil\n", x, y);
	}
	printf("     }\n");
}

void stroke_poly_start(pcb_ttf_stroke_t *s_, int chr)
{
	if ((chr <= 32) || (chr > 126) || (chr == '&') || (chr == '#') || (chr == '{') || (chr == '}') || (chr == '/') || (chr == ':') || (chr == ';') || (chr == '=') || (chr == '\\') || (chr == ':'))
		printf("   ha:&%02x {\n", chr);
	else
		printf("   ha:%c {\n", chr);
}

void stroke_poly_finish(pcb_ttf_stroke_t *s_)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)s_;
	pcb_pline_t *pl, *next;
	int err = 0, n, valids;
	pcb_box_t bbox;

	pline_close(s);

	s->numpa = 0;
	for(pl = s->pos; pl != NULL; pl = pl->next)
		s->numpa++;

	s->pa = malloc(sizeof(pcb_polyarea_t *) * s->numpa);
	for(pl = s->pos, n = 0; pl != NULL; pl = next, n++) {
		s->pa[n] = malloc(sizeof(pcb_polyarea_t));
		pcb_polyarea_init(s->pa[n]);
		next = pl->next;
		pl->next = NULL;
		if (!pcb_polyarea_contour_include(s->pa[n], pl)) {
			fprintf(stderr, "Failed to include positive outline\n");
			err++;
		}
	}

	/* subtract all holes from all positive pa's; error happens if none of the
	   subtraction worked */
	for(pl = s->neg; pl != NULL; pl = next) {
		int worked = 0;
		next = pl->next;
		pl->next = NULL;
		for(n = 0; n < s->numpa; n++) {
			pcb_box_t *pab = (pcb_box_t *)&s->pa[n]->contour_tree->bbox;
			pcb_box_t *plb = (pcb_box_t *)&pl->tree->bbox;
			if ((pcb_box_intersect(pab, plb)) && (pcb_polyarea_contour_include(s->pa[n], pl)))
				worked++;
		}

		if (!worked) {
			err++;
			fprintf(stderr, "Failed to include negative outline\n");
		}
	}

	memcpy(&bbox, &s->pa[0]->contour_tree->bbox, sizeof(pcb_box_t));
	valids = pcb_poly_valid(s->pa[0]);
	for(n = 1; n < s->numpa; n++) {
		if (pcb_poly_valid(s->pa[n])) {
			pcb_box_bump_box(&bbox, (pcb_box_t *)&s->pa[n]->contour_tree->bbox);
			valids++;
		}
	}

	if ((err == 0) && (valids > 0)) {
		s->spid = 0;
		printf("    width=%fmil; delta=12.0mil;\n", OTX(&s->s, bbox.X2 - bbox.X1));
		printf("    li:objects {\n");
		for(n = 0; n < s->numpa; n++)
			r_NoHolesPolygonDicer(s->pa[n], emit, s);
		printf("    }\n");
	}
	else {
		fprintf(stderr, "Invalid outlines err=%d valids=%d numpa=%d\n", err, valids, s->numpa);
		for(n = 0; n < s->numpa; n++)
			pcb_polyarea_free(&(s->pa[n]));
	}

	printf("   }\n");
	s->neg = s->pos = s->curr = NULL;
	free(s->pa);
	s->numpa = 0;
}

void stroke_poly_uninit(pcb_ttf_stroke_t *s_)
{
	poly_pcb_ttf_stroke_t *s = (poly_pcb_ttf_stroke_t *)s_;

	printf("  }\n");
	printf("  cell_width = %fmil\n", s->maxx);
	printf("  cell_height = %fmil\n", s->maxy);
	printf(" }\n");
	printf("}\n");
#warning TODO: free fields
}


poly_pcb_ttf_stroke_t poly_stroke = {
	{
		{
			stroke_poly_move_to,
			stroke_poly_line_to,
			stroke_approx_conic_to,
			stroke_approx_cubic_to,
			0, 0
		},
		stroke_poly_init,
		stroke_poly_start,
		stroke_poly_finish,
		stroke_poly_uninit,
	}
};
