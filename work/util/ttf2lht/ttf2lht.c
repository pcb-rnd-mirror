#include <stdio.h>
#include "ttf2lht.h"

#include "drv_anim.h"
#include "drv_poly.h"

#include <freetype/freetype.h>
#include <freetype/ftmodapi.h>
#include <freetype/ftglyph.h>
#include <freetype/ftoutln.h>

#include "../../../trunk/src_plugins/import_ttf/ttf_load.h"

int main(int argc, char *argv[])
{
	pcb_ttf_t ctx;
	char *fontname = "FreeMono.ttf";
	const char *errmsg;
	FT_Error errnum;
	FT_ULong chr;
	FT_UInt idx;
	pcb_ttf_stroke_t *stroker = (pcb_ttf_stroke_t *)&poly_stroke;
	FT_ULong t, tbl[256];

	errnum = pcb_ttf_load(&ctx, fontname);
	if (errnum != 0) {
		fprintf(stderr, "%s\n", pcb_ttf_errmsg(errnum));
		return 1;
	}

	for(t = 0; t < 256; t++)
		tbl[t] = t;

	fprintf(stderr, "Family: %s\n", ctx.face->family_name);
	fprintf(stderr, "Height=%d ascender=%d descender=%d\n", ctx.face->height, ctx.face->ascender, ctx.face->descender);

	stroker->scale_x = stroker->scale_y = (100.0 / ctx.face->height) / 1000.0;
	stroker->dx = 0;
	stroker->dy = -ctx.face->descender;
	stroker->init(stroker);

	for(t = 33; t < 126; t++) {
		errnum = pcb_ttf_trace(&ctx, tbl[t], t, stroker, 1000);
		if (errnum != 0) {
			fprintf(stderr, "%s\n", pcb_ttf_errmsg(errnum));
		}
	}

	stroker->uninit(stroker);
	pcb_ttf_unload(&ctx);
	return 0;
}
