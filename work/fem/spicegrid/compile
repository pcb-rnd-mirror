#!/bin/sh

awk '
	BEGIN {
		board_sy = 0
		stderr = "/dev/stderr"
	}

	function param(s)
	{
		sub("^.*=", "", s)
		return s
	}

	function load_cell(x, y, val)
	{
		if ((val == ".") || (val == "=") || (val == "S") || (val == "T")) BOT[x,y] = 1
		if ((val == "'\''") || (val == "=") || (val == "S") || (val == "T")) TOP[x,y] = 1

		if (val == "S") {
			if (Sx != "") {
				print "Multiple S ports in board is not supported" > stderr
				exit 1
			}
			Sx = x;
			Sy = y;
			NODE_TOP[x, y] = "St"
			NODE_BOT[x, y] = "GND"
		}
		else if (val == "T") {
			if (Tx != "") {
				print "Multiple T ports in board is not supported" > stderr
				exit 1
			}
			Tx = x;
			Ty = y;
			NODE_TOP[x, y] = "Tt"
			NODE_BOT[x, y] = "Tb"
		}
		else {
			NODE_TOP[x, y] = "nt_" x "_" y
			NODE_BOT[x, y] = "nb_" x "_" y
		}
	}

	function load_board(s    ,A,x)
	{
		v = split(s, A, "")
		for(x = 1; x <= v; x++)
			load_cell(x-1, board_sy, A[x])
		if (v-1 > board_sx)
			board_sx = v-1;
		board_sy++
	}

	function refdes(typ)
	{
		REFDES[typ]++
		return typ REFDES[typ]
	}

	function gen(c1val, c1name, c2val, c2name, R, L, C)
	{
		if ((c1val != 1) || (c2val != 1)) return
		if (R != "") print refdes("R"), c1name, c2name, R
		if (L != "") print refdes("L"), c1name, c2name, L
		if (C != "") print refdes("C"), c1name, c2name, C
	}

	/^#/ { next }

	(board) { load_board($0); next; }

	/^Rx=/ { Rx=param($0); next; }
	/^Ry=/ { Ry=param($0); next; }
	/^Lx=/ { Lx=param($0); next; }
	/^Ly=/ { Ly=param($0); next; }
	/^Rz=/ { Rz=param($0); next; }
	/^Cz=/ { Cz=param($0); next; }
	/^board/ { board=1; next; }

	END {
		print ".title Spicegrid compiler output"
		print "* size:", board_sx, board_sy, "S:", Sx ";" Sy, "T:", Tx ";" Ty
		if ((Sx == "") || (Tx == "")) {
			print "Need both S and T" > stderr
			exit 1
		}

		for(y = 0; y < board_sy; y++) {
			for(x = 0; x < board_sx; x++) {
				# generate top and bottom planes
				gen(TOP[x, y], NODE_TOP[x, y], TOP[x+1, y], NODE_TOP[x+1, y], Rx, Lx, "")
				gen(TOP[x, y], NODE_TOP[x, y], TOP[x, y+1], NODE_TOP[x, y+1], Ry, Ly, "")
				gen(BOT[x, y], NODE_BOT[x, y], BOT[x+1, y], NODE_BOT[x+1, y], Rx, Lx, "")
				gen(BOT[x, y], NODE_BOT[x, y], BOT[x, y+1], NODE_BOT[x, y+1], Ry, Ly, "")

				# generate cross section
				gen(TOP[x, y], NODE_TOP[x, y], BOT[x, y], NODE_BOT[x, y], Rz, "", Cz)
			}
		}

	}
'

echo '
Vsrc Str GND DC 0V AC 1V
Rsrc Str St 50
Rload Tt Tb 50

.control
*op
*print V(Tt)-V(Tb)

ac dec 10 1k 50G

settype decibel V(Tt)
plot Vdb(Tt) xlimit 1k 50G
print Vdb(Tt) > LOG

.endc
.end
'
