
the way we setup gaussexcite right now isn't wrong but it's one of two common
ways. we should let the user select between them at the exporter menu. this
method also happens to be the one you use for simulating pcb filters.

there are other forms of excitation besides gauss but I am not going to get
into them too deeply in our first release.

