%%% Board mesh, part 1
unit = 1.0e-3;
f_max = 7e9;
FDTD = InitFDTD();
FDTD = SetGaussExcite(FDTD, f_max/2, f_max/2);
BC = {'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8'};
FDTD = SetBoundaryCond(FDTD, BC);
physical_constants;
CSX = InitCSX();

run bug_geo.m

Sim_Path = 'openems'; % a path is required. to use the current directory just leave this field null
Sim_CSX = 'csxcad.xml'; % the file name is manditory, sadly the other files it will dump out are not under our control
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );

