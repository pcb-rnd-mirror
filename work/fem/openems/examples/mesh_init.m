
% Copyright (C) 2018 Evan Foss
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%

% this is how the scale is set for the units. In this case mm
unit = 1.0e-3;

% as the user for a positive integer for this one
f_max = 7e9; % maximum frequency of interest

% just printf these line verbatim
FDTD = InitFDTD();

% the excitation is something that the user should probably just be given a blank text field for for now.
% if you want to see why I prefer an empty text window the doc for excitation is here http://openems.de/index.php/Excitation
FDTD = SetGaussExcite( FDTD, f_max/2, f_max/2 );

% Boundary condition - user selects the values of 6 variables that can be any of the following
% the options for these are 
% abbreviation - Meaning
%    PEC - Perfect electric conductor (default BC)
%    PMC - Perfect magnetic conductor
%    MUR - A simple absorbing boundary condition (ABC)
%    PML_x - Perfectly Matched Layer absorbing boundary condition, using x number of cells
%
% The fields are labeled xmin (left side of the board), xmax (right side of the board), ymin, ymax, zmin (under the board), zmax (over the board)
% BC = [xmin xmax ymin ymax zmin zmax];
% like this...
BC   = {'PML_8' 'PML_8' 'MUR' 'MUR' 'PEC' 'MUR'};
% 

% just printf these lines verbatim
FDTD = SetBoundaryCond( FDTD, BC );
physical_constants;
CSX = InitCSX();


