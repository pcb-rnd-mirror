
% Start of mesh stuff
% So this is what x mesh from -50 to 50 by 1 looks like
mesh.x = [-50:0,1:50];
% this is what it looks like if you feel like doing it the fast way
mesh.y = -50:50;
% this is what it looks like if you feel like doing it with just a stack of weird numbers
mesh.z = [0.00000, 0.40000, 0.80000, 1.20000, 1.60000, 2.03636, 2.47273, 2.90909, 3.34545, 3.78182, 4.21818, 4.65455, 5.09091, 5.52727, 5.96364, 6.40000, 6.83636, 7.2727, 7.70909, 8.14545, 8.58182, 9.01818, 9.45455, 9.89091, 10.32727, 10.76364, 11.20000, 11.63636, 12.07273, 12.50909, 12.94545, 13.38182, 13.81818, 14.25455, 14.69091, 15.12727, 15.56364, 16.00000 ];

mesh.x = mesh.x .+ offset.x;
mesh.y = mesh.y .+ offset.y;
mesh.z = mesh.z .+ offset.z;

CSX = DefineRectGrid( CSX, unit, mesh );
% End of meshing stuff


