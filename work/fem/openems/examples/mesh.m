
% Copyright (C) 2017 Evan Foss
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
%
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%

unit = 1.0e-3; % all length in mm

% this stuff is needed top initialize things for csxcad
% the idea is that the user would export from pcb-rnd once and then play 
% with this, port configuration (to change the type of simulation), then 
% optionally also the meshing focus.
f_max = 7e9; % maximum frequency of interest
FDTD = InitFDTD();
FDTD = SetGaussExcite( FDTD, f_max/2, f_max/2 );
BC   = {'PML_8' 'PML_8' 'MUR' 'MUR' 'PEC' 'MUR'};
FDTD = SetBoundaryCond( FDTD, BC );
physical_constants;


% Start of meshing stuff (ignore for now)
CSX = InitCSX();
substrate_thick = 1.6;
substrate_center_z = 2;
MSL_length = 50;
MSL_width = 6;
stub_length = 12;
resolution = c0/(f_max*sqrt(3.66))/unit /50; % resolution of lambda/50



% Start of mesh stuff
% So this is what x mesh from -50 to 50 by 1 looks like
mesh.x = [-50:0,1:50];
% this is what it looks like if you feel like doing it the fast way
mesh.y = -50:50;
% this is what it looks like if you feel like doing it with just a stack of weird numbers
mesh.z = [0.00000, 0.40000, 0.80000, 1.20000, 1.60000, 2.03636, 2.47273, 2.90909, 3.34545, 3.78182, 4.21818, 4.65455, 5.09091, 5.52727, 5.96364, 6.40000, 6.83636, 7.2727, 7.70909, 8.14545, 8.58182, 9.01818, 9.45455, 9.89091, 10.32727, 10.76364, 11.20000, 11.63636, 12.07273, 12.50909, 12.94545, 13.38182, 13.81818, 14.25455, 14.69091, 15.12727, 15.56364, 16.00000 ];
CSX = DefineRectGrid( CSX, unit, mesh );
% End of meshing stuff


%run square_geo.m

% copied from http://openems.de/index.php/Tutorial:_Microstrip_Notch_Filter
% this stuff enables the xml export and graphing to work
 Sim_Path = 'tmp';
 Sim_CSX = 'msl.xml';
 [status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous directory
 [status, message, messageid] = mkdir( Sim_Path ); % create empty simulation folder
 WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );
 CSXGeomPlot( [Sim_Path '/' Sim_CSX] );
%RunOpenEMS( Sim_Path, Sim_CSX );

