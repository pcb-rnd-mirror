%%% Board mesh, part 1
unit = 1.0e-3;
f_max = 7e9;
FDTD = InitFDTD();
FDTD = SetGaussExcite(FDTD, f_max/2, f_max/2);
BC = {'(null)' '(null)' '(null)' '(null)' '(null)' '(null)'};
FDTD = SetBoundaryCond(FDTD, BC);
physical_constants;
CSX = InitCSX();

%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
mesh.y=[];
mesh.x=[];
mesh.z=[];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = mesh.z .+ offset.z;
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.0000;
layer_types(1).conductivity = 56*10^6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 0.0000;
layer_types(2).epsilon = 3.66;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'Intern';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.0000;
layer_types(3).conductivity = 56*10^6;

layers(4).number = 4;
layers(4).name = 'grp_6';
layers(4).clearn = 0;
layer_types(4).name = 'SUBSTRATE_4';
layer_types(4).subtype = 3;
layer_types(4).thickness = 0.0000;
layer_types(4).epsilon = 3.66;
layer_types(4).mue = 0;
layer_types(4).kappa = 0;
layer_types(4).sigma = 0;

layers(5).number = 5;
layers(5).name = 'Intern';
layers(5).clearn = 0;
layer_types(5).name = 'COPPER_5';
layer_types(5).subtype = 2;
layer_types(5).thickness = 0.0000;
layer_types(5).conductivity = 56*10^6;

layers(6).number = 6;
layers(6).name = 'grp_8';
layers(6).clearn = 0;
layer_types(6).name = 'SUBSTRATE_6';
layer_types(6).subtype = 3;
layer_types(6).thickness = 0.0000;
layer_types(6).epsilon = 3.66;
layer_types(6).mue = 0;
layer_types(6).kappa = 0;
layer_types(6).sigma = 0;

layers(7).number = 7;
layers(7).name = 'bottom_copper';
layers(7).clearn = 0;
layer_types(7).name = 'COPPER_7';
layer_types(7).subtype = 2;
layer_types(7).thickness = 0.0000;
layer_types(7).conductivity = 56*10^6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 1.9050; outline_xy(2, 1) = -1.9050;
outline_xy(1, 2) = 1.9050; outline_xy(2, 2) = -1.9050;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);
CSX = AddPcbrndPoly(CSX, PCBRND, 4, outline_xy, 1);
CSX = AddPcbrndPoly(CSX, PCBRND, 6, outline_xy, 1);

%%% Copper objects
points0(1, 1) = 3.1750; points0(2, 1) = -7.6200;
points0(1, 2) = 3.1750; points0(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 3, points0, 0.2000, 0);
points1(1, 1) = 3.1750; points1(2, 1) = -7.6200;
points1(1, 2) = 3.1750; points1(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 5, points1, 0.2000, 0);
poly2_xy(1, 1) = 5.2750; poly2_xy(2, 1) = -7.3700;
poly2_xy(1, 2) = 17.4250; poly2_xy(2, 2) = -7.3700;
poly2_xy(1, 3) = 17.4250; poly2_xy(2, 3) = -8.5700;
poly2_xy(1, 4) = 5.2750; poly2_xy(2, 4) = -8.5700;
poly2_xy(1, 5) = 5.2750; poly2_xy(2, 5) = -8.4700;
poly2_xy(1, 6) = 5.5250; poly2_xy(2, 6) = -8.4700;
poly2_xy(1, 7) = 5.5250; poly2_xy(2, 7) = -7.9700;
poly2_xy(1, 8) = 5.2750; poly2_xy(2, 8) = -7.9700;
CSX = AddPcbrndPoly(CSX, PCBRND, 7, poly2_xy, 1);
poly3_xy(1, 1) = 2.2250; poly3_xy(2, 1) = -7.3700;
poly3_xy(1, 2) = 5.2750; poly3_xy(2, 2) = -7.3700;
poly3_xy(1, 3) = 5.2750; poly3_xy(2, 3) = -7.9700;
poly3_xy(1, 4) = 5.0250; poly3_xy(2, 4) = -7.9700;
poly3_xy(1, 5) = 5.0250; poly3_xy(2, 5) = -8.4700;
poly3_xy(1, 6) = 5.2750; poly3_xy(2, 6) = -8.4700;
poly3_xy(1, 7) = 5.2750; poly3_xy(2, 7) = -8.5700;
poly3_xy(1, 8) = 2.2250; poly3_xy(2, 8) = -8.5700;
CSX = AddPcbrndPoly(CSX, PCBRND, 7, poly3_xy, 1);
points4(1, 1) = 3.1750; points4(2, 1) = -7.6200;
points4(1, 2) = 3.1750; points4(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 7, points4, 0.2500, 0);
poly5_xy(1, 1) = 5.0250; poly5_xy(2, 1) = -7.9700;
poly5_xy(1, 2) = 5.5250; poly5_xy(2, 2) = -7.9700;
poly5_xy(1, 3) = 5.5250; poly5_xy(2, 3) = -8.4700;
poly5_xy(1, 4) = 5.0250; poly5_xy(2, 4) = -8.4700;
CSX = AddPcbrndPoly(CSX, PCBRND, 7, poly5_xy, 1);
points6(1, 1) = 3.1750; points6(2, 1) = -7.6200;
points6(1, 2) = 3.1750; points6(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 7, points6, 0.2000, 0);
poly7_xy(1, 1) = 2.7250; poly7_xy(2, 1) = -2.9700;
poly7_xy(1, 2) = 2.7250; poly7_xy(2, 2) = -2.4700;
poly7_xy(1, 3) = 7.7250; poly7_xy(2, 3) = -2.4700;
poly7_xy(1, 4) = 7.7250; poly7_xy(2, 4) = -5.1100;
poly7_xy(1, 5) = 9.7250; poly7_xy(2, 5) = -5.1100;
poly7_xy(1, 6) = 9.7250; poly7_xy(2, 6) = -2.4700;
poly7_xy(1, 7) = 12.4250; poly7_xy(2, 7) = -2.4700;
poly7_xy(1, 8) = 12.4250; poly7_xy(2, 8) = -5.1100;
poly7_xy(1, 9) = 14.4250; poly7_xy(2, 9) = -5.1100;
poly7_xy(1, 10) = 14.4250; poly7_xy(2, 10) = -2.4700;
poly7_xy(1, 11) = 17.1250; poly7_xy(2, 11) = -2.4700;
poly7_xy(1, 12) = 17.1250; poly7_xy(2, 12) = -5.9100;
poly7_xy(1, 13) = 16.6250; poly7_xy(2, 13) = -5.9100;
poly7_xy(1, 14) = 16.6250; poly7_xy(2, 14) = -2.9700;
poly7_xy(1, 15) = 14.9250; poly7_xy(2, 15) = -2.9700;
poly7_xy(1, 16) = 14.9250; poly7_xy(2, 16) = -5.6100;
poly7_xy(1, 17) = 11.9250; poly7_xy(2, 17) = -5.6100;
poly7_xy(1, 18) = 11.9250; poly7_xy(2, 18) = -2.9700;
poly7_xy(1, 19) = 10.2250; poly7_xy(2, 19) = -2.9700;
poly7_xy(1, 20) = 10.2250; poly7_xy(2, 20) = -5.6100;
poly7_xy(1, 21) = 7.2250; poly7_xy(2, 21) = -5.6100;
poly7_xy(1, 22) = 7.2250; poly7_xy(2, 22) = -2.9700;
poly7_xy(1, 23) = 5.5250; poly7_xy(2, 23) = -2.9700;
poly7_xy(1, 24) = 5.5250; poly7_xy(2, 24) = -7.8700;
poly7_xy(1, 25) = 5.0250; poly7_xy(2, 25) = -7.8700;
poly7_xy(1, 26) = 5.0250; poly7_xy(2, 26) = -2.9700;
poly7_xy(1, 27) = 3.6250; poly7_xy(2, 27) = -2.9700;
poly7_xy(1, 28) = 3.6250; poly7_xy(2, 28) = -7.8700;
poly7_xy(1, 29) = 2.7250; poly7_xy(2, 29) = -7.8700;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
points8(1, 1) = 3.1750; points8(2, 1) = -7.6200;
points8(1, 2) = 3.1750; points8(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points8, 0.2500, 0);
poly9_xy(1, 1) = 5.0250; poly9_xy(2, 1) = -7.3700;
poly9_xy(1, 2) = 5.5250; poly9_xy(2, 2) = -7.3700;
poly9_xy(1, 3) = 5.5250; poly9_xy(2, 3) = -7.8700;
poly9_xy(1, 4) = 5.0250; poly9_xy(2, 4) = -7.8700;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly9_xy, 1);
points10(1, 1) = 3.1750; points10(2, 1) = -7.6200;
points10(1, 2) = 3.1750; points10(2, 2) = -7.6200;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points10, 0.2000, 0);
%%% Testpoints on terminals

% 3.175mm, 7.62mm

point(1, 1) = 3.175; % x
point(2, 1) = -7.62; % y
startstop(1, 1) = 1; %this is the layer it starts on
startstop(2, 1) = 7; %this is the layer it stops on
port_number = 1;
port1_resistance = 50;
port_priority = 999;
[port1_start, port1_stop] = CalcPcbrnd2PortV(PCBRND, point, startstop);
% the true at the end of the port definition line sets this port as active. the [0 0 -1] is the direction the port is facing.
[CSX, port{port_number}] = AddLumpedPort( CSX, port_priority, port_number, port1_resistance, port1_start, port1_stop, [0 0 -1], true);


