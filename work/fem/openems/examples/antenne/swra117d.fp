li:pcb-rnd-subcircuit-v6 {
 ha:subc.554 {
  ha:attributes {
   refdes = ANT1
  }
  ha:data {
   li:padstack_prototypes {
    ha:ps_proto_v6.0 {
     htop = 0
     hdia = 0.2mm
     li:shape {
      ha:ps_shape_v4 {
       clearance = 40.0mil
       ha:ps_circ {
        x = 0.0
        y = 0.0
        dia = 0.25mm
       }
       ha:layer_mask {
        copper = 1
        top = 1
       }
       ha:combining {
       }
      }
      ha:ps_shape_v4 {
       clearance = 40.0mil
       ha:ps_circ {
        x = 0.0
        y = 0.0
        dia = 0.25mm
       }
       ha:layer_mask {
        bottom = 1
        copper = 1
       }
       ha:combining {
       }
      }
     }
     hbottom = 0
     hplated = 1
    }
    ha:ps_proto_v6.1 {
     htop = 0
     hdia = 0.0
     li:shape {
      ha:ps_shape_v4 {
       clearance = 40.0mil
       li:ps_poly {
        -0.25mm
        -0.25mm
        0.25mm
        -0.25mm
        0.25mm
        0.25mm
        -0.25mm
        0.25mm
       }
       ha:layer_mask {
        copper = 1
        top = 1
       }
       ha:combining {
       }
      }
     }
     hbottom = 0
     hplated = 0
    }
    ha:ps_proto_v6.2 {
     htop = 0
     hdia = 0.0
     li:shape {
      ha:ps_shape_v4 {
       clearance = 40.0mil
       li:ps_poly {
        -0.25mm
        -0.25mm
        0.25mm
        -0.25mm
        0.25mm
        0.25mm
        -0.25mm
        0.25mm
       }
       ha:layer_mask {
        bottom = 1
        copper = 1
       }
       ha:combining {
       }
      }
     }
     hbottom = 0
     hplated = 0
    }
   }
   li:objects {
    ha:padstack_ref.553 {
     smirror = 0
     ha:attributes {
     }
     proto = 0
     xmirror = 0
     x = 431.1023622mil
     rot = 0.000000
     y = 14.65mm
     li:thermal {
      li:0 {
       on
       solid
       noshape
      }
      li:1 {
       on
       solid
       noshape
      }
      li:5 {
       on
       solid
       noshape
      }
     }
     ha:flags {
      clearline = 1
     }
     clearance = 0.1mm
    }
    ha:padstack_ref.565 {
     smirror = 0
     ha:attributes {
      term = 1
     }
     proto = 1
     xmirror = 0
     x = 13.05mm
     rot = 0.000000
     y = 14.65mm
     li:thermal {
     }
     ha:flags {
      clearline = 1
     }
     clearance = 20.0mil
    }
    ha:padstack_ref.567 {
     smirror = 0
     ha:attributes {
      term = 2
     }
     proto = 2
     xmirror = 0
     x = 13.05mm
     rot = 0.000000
     y = 15.25mm
     li:thermal {
      li:5 {
       on
       diag
       round
       noshape
      }
     }
     ha:flags {
      clearline = 1
     }
     clearance = 20.0mil
    }
   }
   li:layers {
    ha:top-sig {
     lid = 0
     ha:type {
      copper = 1
      top = 1
     }
     li:objects {
      ha:polygon.513 {
       ha:attributes {
       }
       li:geometry {
        ta:contour {
         { 10.5mm; 10.0mm }
         { 10.5mm; 9.5mm }
         { 15.5mm; 9.5mm }
         { 15.5mm; 12.14mm }
         { 17.5mm; 12.14mm }
         { 17.5mm; 9.5mm }
         { 20.2mm; 9.5mm }
         { 20.2mm; 12.14mm }
         { 22.2mm; 12.14mm }
         { 22.2mm; 9.5mm }
         { 24.9mm; 9.5mm }
         { 24.9mm; 12.94mm }
         { 24.4mm; 12.94mm }
         { 24.4mm; 10.0mm }
         { 893.7007874mil; 10.0mm }
         { 893.7007874mil; 12.64mm }
         { 19.7mm; 12.64mm }
         { 19.7mm; 10.0mm }
         { 18.0mm; 10.0mm }
         { 18.0mm; 12.64mm }
         { 15.0mm; 12.64mm }
         { 15.0mm; 10.0mm }
         { 13.3mm; 10.0mm }
         { 13.3mm; 14.9mm }
         { 12.8mm; 14.9mm }
         { 12.8mm; 10.0mm }
         { 11.4mm; 10.0mm }
         { 11.4mm; 14.9mm }
         { 10.5mm; 14.9mm }
        }
       }
       ha:flags {
       }
       clearance = 40.0mil
      }
     }
     ha:combining {
     }
    }
    ha:bottom-gnd {
     lid = 1
     ha:type {
      bottom = 1
      copper = 1
     }
     li:objects {
      ha:polygon.543 {
       ha:attributes {
       }
       li:geometry {
        ta:contour {
         { 10.0mm; 14.4mm }
         { 25.2mm; 14.4mm }
         { 25.2mm; 15.6mm }
         { 10.0mm; 15.6mm }
        }
       }
       ha:flags {
        clearpoly = 1
       }
      }
     }
     ha:combining {
     }
    }
    ha:top-mask {
     lid = 2
     ha:type {
      top = 1
      mask = 1
     }
     li:objects {
      ha:polygon.548 {
       ha:attributes {
       }
       li:geometry {
        ta:contour {
         { 10.0mm; 362.20472441mil }
         { 25.2mm; 362.20472441mil }
         { 25.2mm; 14.9mm }
         { 10.0mm; 14.9mm }
        }
       }
       ha:flags {
        clearpoly = 1
       }
       clearance = 40.0mil
      }
     }
     ha:combining {
      sub = 1
      auto = 1
     }
    }
    ha:subc-aux {
     lid = 3
     ha:type {
      top = 1
      misc = 1
      virtual = 1
     }
     li:objects {
      ha:line.555 {
       clearance = 0.0
       y2 = 14.65mm
       thickness = 0.1mm
       ha:attributes {
        subc-role = origin
       }
       x1 = 431.1023622mil
       x2 = 431.1023622mil
       ha:flags {
       }
       y1 = 14.65mm
      }
      ha:line.558 {
       clearance = 0.0
       y2 = 14.65mm
       thickness = 0.1mm
       ha:attributes {
        subc-role = x
       }
       x1 = 431.1023622mil
       x2 = 11.95mm
       ha:flags {
       }
       y1 = 14.65mm
      }
      ha:line.561 {
       clearance = 0.0
       y2 = 15.65mm
       thickness = 0.1mm
       ha:attributes {
        subc-role = y
       }
       x1 = 431.1023622mil
       x2 = 431.1023622mil
       ha:flags {
       }
       y1 = 14.65mm
      }
     }
     ha:combining {
     }
    }
    ha:top-silk {
     lid = 4
     ha:type {
      silk = 1
      top = 1
     }
     li:objects {
      ha:text.564 {
       scale = 100
       ha:attributes {
       }
       x = 10.2mm
       y = 7.4mm
       rot = 0.000000
       string = %a.parent.refdes%
       fid = 0
       ha:flags {
        dyntext = 1
        floater = 1
       }
      }
     }
     ha:combining {
     }
    }
   }
  }
  uid = TpE6wLnjK7lchmJOVAEAAAAB
  ha:flags {
  }
 }
}
