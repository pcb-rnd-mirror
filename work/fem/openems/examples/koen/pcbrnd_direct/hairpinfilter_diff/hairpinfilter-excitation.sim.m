%%% Board mesh, part 1
unit = 1.0e-3;
FDTD = InitFDTD();
% Excitation begin
FDTD = SetGaussExcite(FDTD, 10000.000000 Hz, 11000.000000 Hz);
% Excitation end
BC = {'(null)' '(null)' '(null)' '(null)' '(null)' '(null)'};
FDTD = SetBoundaryCond(FDTD, BC);
physical_constants;
CSX = InitCSX();

run /home/fosse/Documents/build/pcb-rnd/work/fem/examples/koen/pcbrnd_direct/hairpinfilter_diff/hairpinfilter-excitation.m

Sim_Path = '.';
Sim_CSX = 'csxcad.xml';
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );
