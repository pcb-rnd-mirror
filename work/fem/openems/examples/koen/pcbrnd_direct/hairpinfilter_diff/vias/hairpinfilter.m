%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=1.5000
mesh.y=[0.0000 0.6528 1.3057 1.9585 2.6113 3.2642 3.9170 4.5698 5.3198 5.3198 5.9165 6.5132 7.1098 7.1098 7.3598 7.8598 7.8598 8.1098 8.1098 8.8492 9.5886 10.3280 11.0674 11.8068 12.5462 13.2856 14.0250 14.7644 15.5038 16.2432 16.9826 17.7219 18.4613 19.2007 19.9401 20.6795 21.4189 22.1583 22.8977 23.6371 24.3765 25.1159 25.8553 26.5947 27.3341 28.0735 28.8128 29.5522 30.2916 31.0310 31.7704 32.5098 32.7598 32.7598 33.2598 33.5098 33.5098 33.5098 34.1065 34.7032 35.2998 36.0498 37.0000 37.8398 38.5898 38.5898 39.2456 39.9013 40.5571 41.2128 41.8685 42.5243 43.1800];
mesh.x=[0.0000 0.6786 1.3572 2.0358 2.7144 3.3930 4.0716 4.7502 5.4288 6.1074 6.7860 7.5360 8.5500 9.5780 10.3280 10.3280 11.0087 11.6893 12.3700 13.1200 14.1520 14.9020 14.9020 15.5820 16.2620 16.9420 17.6920 18.7240 19.4740 19.4740 20.1540 20.8340 21.5140 22.2640 23.2960 24.0460 24.0460 24.7260 25.4060 26.0860 26.8360 27.8680 28.6180 28.6180 29.2980 29.9780 30.6580 31.4080 32.4400 33.1900 33.1900 33.8707 34.5513 35.2320 35.9820 37.0000 38.0240 38.7740 38.7740 39.4610 40.1480 40.8349 41.5219 42.2089 42.8959 43.5829 44.2698 44.9568 45.6438];
mesh.z=[0.0000 0.3750 0.7500 1.1250 1.5000 -3.0000 -2.4000 -1.8000 -1.2000 -0.6000 1.5000 2.1000 2.7000 3.3000 3.9000];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
mesh = AddPML(mesh, 8);
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.07/1000;
layer_types(1).conductivity = 56*10^6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 1.5;
layer_types(2).epsilon = 4.8;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.07/1000;
layer_types(3).conductivity = 56*10^6;

% This is an additional layer type created just for the copper that has to be 
% 3D in the via.
layer_types(4).name = 'COPPER_via';
layer_types(4).subtype = 3;
layer_types(4).conductivity = 56*10^6;



%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.6278; poly0_xy(2, 1) = -42.1538;
poly0_xy(1, 2) = 1.0160; poly0_xy(2, 2) = -42.1538;
poly0_xy(1, 3) = 1.0160; poly0_xy(2, 3) = -1.0160;
poly0_xy(1, 4) = 44.6278; poly0_xy(2, 4) = -1.0160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
poly1_xy(1, 1) = 35.4800; poly1_xy(2, 1) = -33.0098;
poly1_xy(1, 2) = 32.9400; poly1_xy(2, 2) = -33.0098;
poly1_xy(1, 3) = 32.9400; poly1_xy(2, 3) = -7.6098;
poly1_xy(1, 4) = 30.9080; poly1_xy(2, 4) = -7.6098;
poly1_xy(1, 5) = 30.9080; poly1_xy(2, 5) = -33.0098;
poly1_xy(1, 6) = 28.3680; poly1_xy(2, 6) = -33.0098;
poly1_xy(1, 7) = 28.3680; poly1_xy(2, 7) = -7.6098;
poly1_xy(1, 8) = 30.9080; poly1_xy(2, 8) = -5.0698;
poly1_xy(1, 9) = 32.9400; poly1_xy(2, 9) = -5.0698;
poly1_xy(1, 10) = 35.4800; poly1_xy(2, 10) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly1_xy, 1);
poly2_xy(1, 1) = 19.2240; poly2_xy(2, 1) = -7.6098;
poly2_xy(1, 2) = 21.7640; poly2_xy(2, 2) = -7.6098;
poly2_xy(1, 3) = 21.7640; poly2_xy(2, 3) = -33.0098;
poly2_xy(1, 4) = 23.7960; poly2_xy(2, 4) = -33.0098;
poly2_xy(1, 5) = 23.7960; poly2_xy(2, 5) = -7.6098;
poly2_xy(1, 6) = 26.3360; poly2_xy(2, 6) = -7.6098;
poly2_xy(1, 7) = 26.3360; poly2_xy(2, 7) = -33.0098;
poly2_xy(1, 8) = 23.7960; poly2_xy(2, 8) = -35.5498;
poly2_xy(1, 9) = 21.7640; poly2_xy(2, 9) = -35.5498;
poly2_xy(1, 10) = 19.2240; poly2_xy(2, 10) = -33.0098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly2_xy, 1);
poly3_xy(1, 1) = 17.1920; poly3_xy(2, 1) = -33.0098;
poly3_xy(1, 2) = 14.6520; poly3_xy(2, 2) = -33.0098;
poly3_xy(1, 3) = 14.6520; poly3_xy(2, 3) = -7.6098;
poly3_xy(1, 4) = 12.6200; poly3_xy(2, 4) = -7.6098;
poly3_xy(1, 5) = 12.6200; poly3_xy(2, 5) = -33.0098;
poly3_xy(1, 6) = 10.0800; poly3_xy(2, 6) = -33.0098;
poly3_xy(1, 7) = 10.0800; poly3_xy(2, 7) = -7.6098;
poly3_xy(1, 8) = 12.6200; poly3_xy(2, 8) = -5.0698;
poly3_xy(1, 9) = 14.6520; poly3_xy(2, 9) = -5.0698;
poly3_xy(1, 10) = 17.1920; poly3_xy(2, 10) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 9.8260; poly4_xy(2, 1) = -38.0898;
poly4_xy(1, 2) = 7.2860; poly4_xy(2, 2) = -38.0898;
poly4_xy(1, 3) = 7.2860; poly4_xy(2, 3) = -7.6098;
poly4_xy(1, 4) = 9.8260; poly4_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 38.2740; poly5_xy(2, 1) = -38.0898;
poly5_xy(1, 2) = 35.7340; poly5_xy(2, 2) = -38.0898;
poly5_xy(1, 3) = 35.7340; poly5_xy(2, 3) = -7.6098;
poly5_xy(1, 4) = 38.2740; poly5_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 8.0500; poly6_xy(2, 1) = -36.5000;
poly6_xy(1, 2) = 9.0500; poly6_xy(2, 2) = -36.5000;
poly6_xy(1, 3) = 9.0500; poly6_xy(2, 3) = -37.5000;
poly6_xy(1, 4) = 8.0500; poly6_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 36.5000; poly7_xy(2, 1) = -36.5000;
poly7_xy(1, 2) = 37.5000; poly7_xy(2, 2) = -36.5000;
poly7_xy(1, 3) = 37.5000; poly7_xy(2, 3) = -37.5000;
poly7_xy(1, 4) = 36.5000; poly7_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);

% the following 2 lines are the only ones that matter
% x and y set just like for polygons and everything else
% this via is at x=20.5, y=9; it starts at layer 1 and stops on layer 2; the copper plated radius is 0.8mm and the air inside the copper has a radius of 0.75mm
% the matterial for the copper is layer_types(4).name and the matterial for the air inside is PCBRND.void.name these are just variable names
point3(1, 1) = 20.5;point3(2, 1) = -9;
CSX = AddPcbrndVia(CSX, PCBRND, 1, 2, point3, 0.75, 0.8, PCBRND.void, layer_types(4));

%%% Port(s) on terminals

point1(1, 1) = 8.5500; point1(2, 1) = -37.0000;
[start1, stop1] = CalcPcbrnd2PortV(PCBRND, point1, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, start1, stop1, [0 0 -1], true);

point2(1, 1) = 37.0000; point2(2, 1) = -37.0000;
[start2, stop2] = CalcPcbrnd2PortV(PCBRND, point2, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 2, 50.000000, start2, stop2, [0 0 -1]);
