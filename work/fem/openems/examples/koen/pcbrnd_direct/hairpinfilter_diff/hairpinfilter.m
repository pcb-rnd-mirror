%%% Board mesh, part 1
unit = 1.0e-3;
FDTD = InitFDTD();
fc= 1.1e9;   % center frequency
f0= 1.0e9;   % estimated 20db frequency
FDTD = SetGaussExcite( FDTD, f0, fc );
BC = {'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8'};
FDTD = SetBoundaryCond(FDTD, BC);
physical_constants;
CSX = InitCSX();

%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=1.5000
mesh.y=[0.0000 0.4570 0.9140 1.3710 1.8279 2.2849 2.7419 3.1989 3.6559 4.1129 4.5698 5.3198 5.3198 5.9165 6.5132 7.1098 7.1098 7.3598 7.8598 7.8598 8.1098 8.1098 8.6098 32.0098 31.2598 9.3598 10.0898 10.8198 11.5498 12.2798 13.0098 13.7398 14.4698 15.1998 15.9298 16.6598 17.3898 18.1198 18.8498 19.5798 20.3098 21.0398 21.7698 22.4998 23.2298 23.9598 24.6898 25.4198 26.1498 26.8798 27.6098 28.3398 29.0698 29.7998 30.5298 32.5098 32.7598 32.7598 33.2598 33.5098 33.5098 33.5098 34.1065 34.7032 35.2998 36.0498 37.0000 37.8398 38.5898 38.5898 39.0489 39.5079 39.9669 40.4259 40.8849 41.3439 41.8030 42.2620 42.7210 43.1800];
mesh.x=[0.0000 0.4786 0.9572 1.4358 1.9144 2.3929 2.8715 3.3501 3.8287 4.3073 4.7859 5.2645 5.7431 6.2216 6.7002 7.1788 7.6574 8.1360 8.8860 10.0000 10.9280 11.6780 11.6780 12.3587 13.0393 13.7200 14.4700 15.5020 16.2520 16.2520 16.9320 17.6120 18.2920 19.0420 19.0420 19.3860 19.7300 20.0740 20.8240 20.8240 21.5040 22.1840 22.8640 23.6140 24.6460 25.3960 25.3960 26.0760 26.7560 27.4360 28.1860 28.1860 28.5300 28.8740 29.2180 29.9680 29.9680 30.6480 31.3280 32.0080 32.7580 33.7900 34.5400 34.5400 35.2207 35.9013 36.5820 37.3320 38.0000 39.3740 40.1240 40.1240 40.5840 41.0440 41.5039 41.9639 42.4239 42.8839 43.3439 43.8039 44.2638 44.7238 45.1838 45.6438];
mesh.z=[0.0000 0.7500 1.5000 -4.0000 -3.3333 -2.6667 -2.0000 -1.3333 -0.6667 1.5000 2.1667 2.8333 3.5000 4.1667 4.8333];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
mesh = AddPML(mesh, 8);
mesh
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.0500;
layer_types(1).conductivity = 56*10^6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 1.5000;
layer_types(2).epsilon = 4.8;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.0500;
layer_types(3).conductivity = 56*10^6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.6278; poly0_xy(2, 1) = -1.0160;
poly0_xy(1, 2) = 44.6278; poly0_xy(2, 2) = -42.1538;
poly0_xy(1, 3) = 1.0160; poly0_xy(2, 3) = -42.1538;
poly0_xy(1, 4) = 1.0160; poly0_xy(2, 4) = -1.0160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
poly1_xy(1, 1) = 36.8300; poly1_xy(2, 1) = -7.6098;
poly1_xy(1, 2) = 36.8300; poly1_xy(2, 2) = -33.0098;
poly1_xy(1, 3) = 34.2900; poly1_xy(2, 3) = -33.0098;
poly1_xy(1, 4) = 34.2900; poly1_xy(2, 4) = -7.6098;
poly1_xy(1, 5) = 32.2580; poly1_xy(2, 5) = -7.6098;
poly1_xy(1, 6) = 32.2580; poly1_xy(2, 6) = -33.0098;
poly1_xy(1, 7) = 29.7180; poly1_xy(2, 7) = -33.0098;
poly1_xy(1, 8) = 29.7180; poly1_xy(2, 8) = -7.6098;
poly1_xy(1, 9) = 32.2580; poly1_xy(2, 9) = -5.0698;
poly1_xy(1, 10) = 34.2900; poly1_xy(2, 10) = -5.0698;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly1_xy, 1);
poly2_xy(1, 1) = 20.5740; poly2_xy(2, 1) = -33.0098;
poly2_xy(1, 2) = 20.5740; poly2_xy(2, 2) = -7.6098;
poly2_xy(1, 3) = 23.1140; poly2_xy(2, 3) = -7.6098;
poly2_xy(1, 4) = 23.1140; poly2_xy(2, 4) = -33.0098;
poly2_xy(1, 5) = 25.1460; poly2_xy(2, 5) = -33.0098;
poly2_xy(1, 6) = 25.1460; poly2_xy(2, 6) = -7.6098;
poly2_xy(1, 7) = 27.6860; poly2_xy(2, 7) = -7.6098;
poly2_xy(1, 8) = 27.6860; poly2_xy(2, 8) = -33.0098;
poly2_xy(1, 9) = 25.1460; poly2_xy(2, 9) = -35.5498;
poly2_xy(1, 10) = 23.1140; poly2_xy(2, 10) = -35.5498;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly2_xy, 1);
poly3_xy(1, 1) = 18.5420; poly3_xy(2, 1) = -7.6098;
poly3_xy(1, 2) = 18.5420; poly3_xy(2, 2) = -33.0098;
poly3_xy(1, 3) = 16.0020; poly3_xy(2, 3) = -33.0098;
poly3_xy(1, 4) = 16.0020; poly3_xy(2, 4) = -7.6098;
poly3_xy(1, 5) = 13.9700; poly3_xy(2, 5) = -7.6098;
poly3_xy(1, 6) = 13.9700; poly3_xy(2, 6) = -33.0098;
poly3_xy(1, 7) = 11.4300; poly3_xy(2, 7) = -33.0098;
poly3_xy(1, 8) = 11.4300; poly3_xy(2, 8) = -7.6098;
poly3_xy(1, 9) = 13.9700; poly3_xy(2, 9) = -5.0698;
poly3_xy(1, 10) = 16.0020; poly3_xy(2, 10) = -5.0698;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 11.1760; poly4_xy(2, 1) = -7.6098;
poly4_xy(1, 2) = 11.1760; poly4_xy(2, 2) = -38.0898;
poly4_xy(1, 3) = 8.6360; poly4_xy(2, 3) = -38.0898;
poly4_xy(1, 4) = 8.6360; poly4_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 39.6240; poly5_xy(2, 1) = -7.6098;
poly5_xy(1, 2) = 39.6240; poly5_xy(2, 2) = -38.0898;
poly5_xy(1, 3) = 37.0840; poly5_xy(2, 3) = -38.0898;
poly5_xy(1, 4) = 37.0840; poly5_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 9.5000; poly6_xy(2, 1) = -36.5000;
poly6_xy(1, 2) = 10.5000; poly6_xy(2, 2) = -36.5000;
poly6_xy(1, 3) = 10.5000; poly6_xy(2, 3) = -37.5000;
poly6_xy(1, 4) = 9.5000; poly6_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 37.5000; poly7_xy(2, 1) = -36.5000;
poly7_xy(1, 2) = 38.5000; poly7_xy(2, 2) = -36.5000;
poly7_xy(1, 3) = 38.5000; poly7_xy(2, 3) = -37.5000;
poly7_xy(1, 4) = 37.5000; poly7_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
%%% Port(s) on terminals

a1_point(1, 1) = 10.0000; a1_point(2, 1) = -37.0000;
[a1_start, a1_stop] = CalcPcbrnd2PortV(PCBRND, a1_point, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, a1_start, a1_stop, [0 0 -1], true);

a2_point(1, 1) = 38.0000; a2_point(2, 1) = -37.0000;
[a2_start, a2_stop] = CalcPcbrnd2PortV(PCBRND, a2_point, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 2, 50.000000, a2_start, a2_stop, [0 0 -1]);


Sim_Path = 'tmp';
Sim_CSX = 'msl.xml';

[status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous directory
[status, message, messageid] = mkdir( Sim_Path ); % create empty simulation folder

disp([ 'Estimated simulation runtime: 25000 timesteps' ]); % inform user this may take a while...
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );
CSXGeomPlot( [Sim_Path '/' Sim_CSX] );
RunOpenEMS( Sim_Path, Sim_CSX );

% post-processing
close all
f = linspace( 1e6, 2e9, 1601 );
port = calcPort( port, Sim_Path, f, 'RefImpedance', 50);

s11 = port{1}.uf.ref./ port{1}.uf.inc;
s21 = port{2}.uf.ref./ port{1}.uf.inc;

plot(f/1e9,20*log10(abs(s11)),'k-','LineWidth',2);
hold on;
grid on;
plot(f/1e9,20*log10(abs(s21)),'r--','LineWidth',2);
legend('S_{11}','S_{21}');
ylabel('S-Parameter (dB)','FontSize',12);
xlabel('frequency (GHz) \rightarrow','FontSize',12);
ylim([-60 2]);
print ('hairpinfilter_simulation.png', '-dpng');



