%%% Board mesh, part 1
unit = 1.0e-3;
f_max = 7e9;
FDTD = InitFDTD();
FDTD = SetGaussExcite(FDTD, f_max/2, f_max/2);
BC = {'(null)' '(null)' '(null)' '(null)' '(null)' '(null)'};
FDTD = SetBoundaryCond(FDTD, BC);
physical_constants;
CSX = InitCSX();

%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=0.0000
mesh.y=[];
mesh.x=[];
mesh.z=[];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.0500;
layer_types(1).conductivity = 56*10^6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 0.0500;
layer_types(2).epsilon = 3.66;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.0500;
layer_types(3).conductivity = 56*10^6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.6278; poly0_xy(2, 1) = -1.0160;
poly0_xy(1, 2) = 44.6278; poly0_xy(2, 2) = -42.1538;
poly0_xy(1, 3) = 1.0160; poly0_xy(2, 3) = -42.1538;
poly0_xy(1, 4) = 1.0160; poly0_xy(2, 4) = -1.0160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
poly1_xy(1, 1) = 36.8300; poly1_xy(2, 1) = -7.6098;
poly1_xy(1, 2) = 36.8300; poly1_xy(2, 2) = -33.0098;
poly1_xy(1, 3) = 34.2900; poly1_xy(2, 3) = -33.0098;
poly1_xy(1, 4) = 34.2900; poly1_xy(2, 4) = -7.6098;
poly1_xy(1, 5) = 32.2580; poly1_xy(2, 5) = -7.6098;
poly1_xy(1, 6) = 32.2580; poly1_xy(2, 6) = -33.0098;
poly1_xy(1, 7) = 29.7180; poly1_xy(2, 7) = -33.0098;
poly1_xy(1, 8) = 29.7180; poly1_xy(2, 8) = -7.6098;
poly1_xy(1, 9) = 32.2580; poly1_xy(2, 9) = -5.0698;
poly1_xy(1, 10) = 34.2900; poly1_xy(2, 10) = -5.0698;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly1_xy, 1);
poly2_xy(1, 1) = 20.5740; poly2_xy(2, 1) = -33.0098;
poly2_xy(1, 2) = 20.5740; poly2_xy(2, 2) = -7.6098;
poly2_xy(1, 3) = 23.1140; poly2_xy(2, 3) = -7.6098;
poly2_xy(1, 4) = 23.1140; poly2_xy(2, 4) = -33.0098;
poly2_xy(1, 5) = 25.1460; poly2_xy(2, 5) = -33.0098;
poly2_xy(1, 6) = 25.1460; poly2_xy(2, 6) = -7.6098;
poly2_xy(1, 7) = 27.6860; poly2_xy(2, 7) = -7.6098;
poly2_xy(1, 8) = 27.6860; poly2_xy(2, 8) = -33.0098;
poly2_xy(1, 9) = 25.1460; poly2_xy(2, 9) = -35.5498;
poly2_xy(1, 10) = 23.1140; poly2_xy(2, 10) = -35.5498;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly2_xy, 1);
poly3_xy(1, 1) = 18.5420; poly3_xy(2, 1) = -7.6098;
poly3_xy(1, 2) = 18.5420; poly3_xy(2, 2) = -33.0098;
poly3_xy(1, 3) = 16.0020; poly3_xy(2, 3) = -33.0098;
poly3_xy(1, 4) = 16.0020; poly3_xy(2, 4) = -7.6098;
poly3_xy(1, 5) = 13.9700; poly3_xy(2, 5) = -7.6098;
poly3_xy(1, 6) = 13.9700; poly3_xy(2, 6) = -33.0098;
poly3_xy(1, 7) = 11.4300; poly3_xy(2, 7) = -33.0098;
poly3_xy(1, 8) = 11.4300; poly3_xy(2, 8) = -7.6098;
poly3_xy(1, 9) = 13.9700; poly3_xy(2, 9) = -5.0698;
poly3_xy(1, 10) = 16.0020; poly3_xy(2, 10) = -5.0698;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 11.1760; poly4_xy(2, 1) = -7.6098;
poly4_xy(1, 2) = 11.1760; poly4_xy(2, 2) = -38.0898;
poly4_xy(1, 3) = 8.6360; poly4_xy(2, 3) = -38.0898;
poly4_xy(1, 4) = 8.6360; poly4_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 39.6240; poly5_xy(2, 1) = -7.6098;
poly5_xy(1, 2) = 39.6240; poly5_xy(2, 2) = -38.0898;
poly5_xy(1, 3) = 37.0840; poly5_xy(2, 3) = -38.0898;
poly5_xy(1, 4) = 37.0840; poly5_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 9.5000; poly6_xy(2, 1) = -36.5000;
poly6_xy(1, 2) = 10.5000; poly6_xy(2, 2) = -36.5000;
poly6_xy(1, 3) = 10.5000; poly6_xy(2, 3) = -37.5000;
poly6_xy(1, 4) = 9.5000; poly6_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 37.5000; poly7_xy(2, 1) = -36.5000;
poly7_xy(1, 2) = 38.5000; poly7_xy(2, 2) = -36.5000;
poly7_xy(1, 3) = 38.5000; poly7_xy(2, 3) = -37.5000;
poly7_xy(1, 4) = 37.5000; poly7_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
%%% Port(s) on terminals

1_point(1, 1) = 10.0000; 1_point(2, 1) = -37.0000;
[1_start, 1_stop] = CalcPcbrnd2PortV(PCBRND, 1_point, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, 1_start, 1_stop, [0 0 -1], true);

2_point(1, 1) = 38.0000; 2_point(2, 1) = -37.0000;
[2_start, 2_stop] = CalcPcbrnd2PortV(PCBRND, 2_point, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 2, 50.000000, 2_start, 2_stop, [0 0 -1]);

Sim_Path = 'tmp'; % tmp should be a user selected directory to put the openems data into
Sim_CSX = 'msl.xml'; % msl.xml is the xml file that gets the csxcad geometry

[status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous directory
[status, message, messageid] = mkdir( Sim_Path ); % create empty simulation folder

WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );
RunOpenEMS( Sim_Path, Sim_CSX );

close all
f = linspace( 1e6, 2e9, 1601 );
port = calcPort( port, Sim_Path, f, 'RefImpedance', 50);

s11 = port{1}.uf.ref./ port{1}.uf.inc;
s21 = port{2}.uf.ref./ port{1}.uf.inc;

plot(f/1e9,20*log10(abs(s11)),'k-','LineWidth',2);
hold on;
grid on;
plot(f/1e9,20*log10(abs(s21)),'r--','LineWidth',2);
legend('S_{11}','S_{21}');
ylabel('S-Parameter (dB)','FontSize',12);
xlabel('frequency (GHz) \rightarrow','FontSize',12);
ylim([-60 2]);
print ('hairpinfilter_simulation.png', '-dpng');

