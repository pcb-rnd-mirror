
close all
clear
clc

% initialize
physical_constants;
unit = 1*10^-3; % this pcb has units in meters.
fc= 1.1e9;   % center frequency
f0= 1.0e9;   % estimated 20db frequency
substrate_epr = 4.8;
resolution = c0 / (f0+fc) / sqrt(substrate_epr) / unit / 25;

AirBox = c0/(f0+fc)/unit/25;

% set up FTDT
FDTD  = InitFDTD();
FDTD = SetGaussExcite( FDTD, f0, fc );
BC   = {'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8' 'PML_8'};
FDTD = SetBoundaryCond( FDTD, BC );

% create 3d model
CSX = InitCSX();

CSX = AddMaterial(CSX, 'SUBSTRATE_2');
CSX = SetMaterialProperty( CSX, 'SUBSTRATE_2', 'Epsilon', substrate_epr );

CSX = AddConductingSheet(CSX, 'COPPER_1', 56e6, 70e-6);
CSX = AddConductingSheet(CSX, 'COPPER_3', 56e6, 70e-6);


mat1(1,1) = 0.00000;mat1(2,1) = 0.00000;
mat1(1,2) = 45.64380;mat1(2,2) = 0.00000;
mat1(1,3) = 45.64380;mat1(2,3) = -43.18000;
mat1(1,4) = 0.00000;mat1(2,4) = -43.18000;

% 0.00000   45.64380   45.64380    0.00000
% 0.00000    0.00000  -43.18000  -43.18000

CSX = AddLinPoly( CSX,'SUBSTRATE_2',  0, 2, 0, mat1,  1.5000, 'CoordSystem',0); 

% 44.6278   44.6278    1.0160    1.0160
% -1.0160  -42.1538  -42.1538   -1.0160

mat2(1,1) = 44.6278;mat2(2,1) = -1.0160;
mat2(1,2) = 44.6278;mat2(2,2) = -42.1538;
mat2(1,3) = 1.0160;mat2(2,3) = -42.1538;
mat2(1,4) = 1.0160;mat2(2,4) = -1.0160;

CSX = AddPolygon( CSX,'COPPER_3', 1, 2, 0, mat2, 'CoordSystem',0); 

mat3(1,1) = 36.8300;mat3(2,1) = -7.6098;
mat3(1,2) = 36.8300;mat3(2,2) = -33.0098;
mat3(1,3) = 34.2900;mat3(2,3) = -33.0098;
mat3(1,4) = 34.2900;mat3(2,4) = -7.6098;
mat3(1,5) = 32.2580;mat3(2,5) = -7.6098;
mat3(1,6) = 32.2580;mat3(2,6) = -33.0098;
mat3(1,7) = 29.7180;mat3(2,7) = -33.0098;
mat3(1,8) = 29.7180;mat3(2,8) = -7.6098;
mat3(1,9) = 32.2580;mat3(2,9) = -5.0698;
mat3(1,10) = 34.2900;mat3(2,10) = -5.0698;

% 36.8300   36.8300   34.2900   34.2900   32.2580   32.2580   29.7180   29.7180   32.2580   34.2900
% -7.6098  -33.0098  -33.0098   -7.6098   -7.6098  -33.0098  -33.0098   -7.6098   -5.0698   -5.0698

CSX = AddPolygon( CSX,'COPPER_1', 1, 2,  1.5000, mat3, 'CoordSystem',0); 

mat4(1,1) = 20.5740;mat4(2,1) = -33.0098;
mat4(1,2) = 20.5740;mat4(2,2) = -7.6098;
mat4(1,3) = 23.1140;mat4(2,3) = -7.6098;
mat4(1,4) = 23.1140;mat4(2,4) = -33.0098;
mat4(1,5) = 25.1460;mat4(2,5) = -33.0098;
mat4(1,6) = 25.1460;mat4(2,6) = -7.6098;
mat4(1,7) = 27.6860;mat4(2,7) = -7.6098;
mat4(1,8) = 27.6860;mat4(2,8) = -33.0098;
mat4(1,9) = 25.1460;mat4(2,9) = -35.5498;
mat4(1,10) = 23.1140;mat4(2,10) = -35.5498;

%   20.5740   20.5740   23.1140   23.1140   25.1460   25.1460   27.6860   27.6860   25.1460   23.1140
%  -33.0098   -7.6098   -7.6098  -33.0098  -33.0098   -7.6098   -7.6098  -33.0098  -35.5498  -35.5498

CSX = AddPolygon( CSX, 'COPPER_1', 1, 2,  1.5000, mat4, 'CoordSystem',0); 

mat5(1,1) = 18.5420;mat5(2,1) = -7.6098;
mat5(1,2) = 18.5420;mat5(2,2) = -33.0098;
mat5(1,3) = 16.0020;mat5(2,3) = -33.0098;
mat5(1,4) = 16.0020;mat5(2,4) = -7.6098;
mat5(1,5) = 13.9700;mat5(2,5) = -7.6098;
mat5(1,6) = 13.9700;mat5(2,6) = -33.0098;
mat5(1,7) = 11.4300;mat5(2,7) = -33.0098;
mat5(1,8) = 11.4300;mat5(2,8) = -7.6098;
mat5(1,9) = 13.9700;mat5(2,9) = -5.0698;
mat5(1,10) = 16.0020;mat5(2,10) = -5.0698;

%   18.5420   18.5420   16.0020   16.0020   13.9700   13.9700   11.4300   11.4300   13.9700   16.0020
%   -7.6098  -33.0098  -33.0098   -7.6098   -7.6098  -33.0098  -33.0098   -7.6098   -5.0698   -5.0698

CSX = AddPolygon( CSX, 'COPPER_1', 1, 2,  1.5000, mat5, 'CoordSystem',0); 

mat6(1,1) = 11.1760;mat6(2,1) = -7.6098;
mat6(1,2) = 11.1760;mat6(2,2) = -38.0898;
mat6(1,3) = 8.6360;mat6(2,3) = -38.0898;
mat6(1,4) = 8.6360;mat6(2,4) = -7.6098;

%  11.1760   11.1760    8.6360    8.6360
%  -7.6098  -38.0898  -38.0898   -7.6098

CSX = AddPolygon( CSX,'COPPER_1',  1, 2,  1.5000, mat6, 'CoordSystem',0);

mat7(1,1) = 39.6240;mat7(2,1) = -7.6098;
mat7(1,2) = 39.6240;mat7(2,2) = -38.0898;
mat7(1,3) = 37.0840;mat7(2,3) = -38.0898;
mat7(1,4) = 37.0840;mat7(2,4) = -7.6098;

%  39.6240   39.6240   37.0840   37.0840   
%  -7.6098  -38.0898  -38.0898   -7.6098

CSX = AddPolygon( CSX,'COPPER_1',  1, 2,  1.5000, mat7, 'CoordSystem',0); 

mesh = DetectEdges(CSX);

port1_start = [ 10.0000 -36.3000 1.5000 ];

port2_start = [ 38.5000 -36.3000 1.5000 ];

port1_stop = port1_start;
port1_stop(3) = 0;

port2_stop = port2_start;
port2_stop(3) = 0;

[CSX, port{1}] = AddLumpedPort( CSX, 999, 1, 50 , port1_start, port1_stop, [0 0 -1], true);
[CSX, port{2}] = AddLumpedPort( CSX, 999, 2, 50, port2_start, port2_stop, [0 0 -1]);

mesh = DetectEdges(CSX);

%% add air-box around the imported structure
mesh.x = [min(mesh.x)-AirBox max(mesh.x)+AirBox mesh.x];
mesh.y = [min(mesh.y)-AirBox max(mesh.y)+AirBox mesh.y];
mesh.z = [min(mesh.z)-AirBox max(mesh.z)+2*AirBox mesh.z];
mesh = SmoothMesh(mesh, resolution);
mesh = AddPML(mesh, 8);
CSX = DefineRectGrid(CSX, (unit), mesh);


Sim_Path = 'tmp';
Sim_CSX = 'msl.xml';
  
[status, message, messageid] = rmdir( Sim_Path, 's' ); % clear previous directory
[status, message, messageid] = mkdir( Sim_Path ); % create empty simulation folder

disp([ 'Estimated simulation runtime: 25000 timesteps' ]); % inform user this may take a while... 
WriteOpenEMS( [Sim_Path '/' Sim_CSX], FDTD, CSX );
CSXGeomPlot( [Sim_Path '/' Sim_CSX] );
RunOpenEMS( Sim_Path, Sim_CSX );

%% post-processing
close all
f = linspace( 1e6, 2e9, 1601 );
port = calcPort( port, Sim_Path, f, 'RefImpedance', 50);

s11 = port{1}.uf.ref./ port{1}.uf.inc;
s21 = port{2}.uf.ref./ port{1}.uf.inc;

plot(f/1e9,20*log10(abs(s11)),'k-','LineWidth',2);
hold on;
grid on;
plot(f/1e9,20*log10(abs(s21)),'r--','LineWidth',2);
legend('S_{11}','S_{21}');
ylabel('S-Parameter (dB)','FontSize',12);
xlabel('frequency (GHz) \rightarrow','FontSize',12);
ylim([-60 2]);
print ('hairpinfilter_simulation.png', '-dpng');



