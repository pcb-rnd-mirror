%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=1.5000
mesh.y=[4.2365 3.4865 0.0000 0.6973 1.3946 2.0919 2.7892 4.7365 5.2365 5.2365 5.6445 6.0525 6.4605 6.8685 7.2765 7.2765 7.4432 7.7765 7.7765 7.9432 7.9432 8.4395 8.9358 9.4322 9.9285 10.4248 10.9212 11.4175 11.9138 12.4102 12.9065 13.4028 13.8992 14.3955 14.8918 15.3882 15.8845 16.3808 16.8772 17.3735 17.8698 18.3662 18.8625 19.3588 19.8552 20.3515 20.8478 21.3442 21.8405 22.3368 22.8332 23.3295 23.8258 24.3222 24.8185 25.3148 25.8112 26.3075 26.8038 27.3002 27.7965 28.2928 28.7892 29.2855 29.7818 30.2782 30.7745 31.2708 31.7672 32.2635 32.7598 32.8432 33.0098 33.3432 33.3432 33.3432 33.8012 34.2592 34.7172 35.1752 35.6332 35.6332 36.0888 36.5444 37.0000 37.9232 38.4232 38.4232 38.9232 39.6732 40.3745 41.0759 41.7773 42.4786 43.1800];
mesh.x=[8.0527 7.3027 0.0000 0.7303 1.4605 2.1908 2.9211 3.6513 4.3816 5.1119 5.8421 6.5724 8.5527 8.5527 9.0018 9.4509 9.9000 9.9000 10.2843 10.6687 11.0530 11.5530 11.5530 11.9697 12.3864 12.8032 13.2199 13.6366 14.0533 14.0533 14.5197 14.9860 15.4523 15.9187 15.9187 16.3698 16.8209 17.2720 17.7231 18.1742 18.6253 18.6253 19.2471 19.8689 20.4907 20.4907 20.9418 21.3929 21.8440 22.2951 22.7462 23.1973 23.1973 23.6637 24.1300 24.5963 25.0627 25.0627 25.5138 25.9649 26.4160 26.8671 27.3182 27.7693 27.7693 28.3911 29.0129 29.6347 29.6347 30.0858 30.5369 30.9880 31.4391 31.8902 32.3413 32.3413 32.8077 33.2740 33.7403 34.2067 34.2067 34.6234 35.0401 35.4568 35.8736 36.2903 36.7070 37.2070 37.2070 37.5880 37.9690 38.3500 38.3500 38.8024 39.2549 39.7073 39.7073 40.2073 40.9573 41.6268 42.2963 42.9658 43.6353 44.3048 44.9743 45.6438];
mesh.z=[0.0000 0.3750 0.7500 1.1250 1.5000 -4.0000 -3.3333 -2.6667 -2.0000 -1.3333 -0.6667 1.5000 2.1667 2.8333 3.5000 4.1667 4.8333];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
mesh = AddPML(mesh, 8);
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.0500;
layer_types(1).conductivity = 56*10^6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 1.5000;
layer_types(2).epsilon = 4.8;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.0500;
layer_types(3).conductivity = 56*10^6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.6278; poly0_xy(2, 1) = -42.1538;
poly0_xy(1, 2) = 1.0160; poly0_xy(2, 2) = -42.1538;
poly0_xy(1, 3) = 1.0160; poly0_xy(2, 3) = -1.0160;
poly0_xy(1, 4) = 44.6278; poly0_xy(2, 4) = -1.0160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
poly1_xy(1, 1) = 36.8300; poly1_xy(2, 1) = -33.0098;
poly1_xy(1, 2) = 34.2900; poly1_xy(2, 2) = -33.0098;
poly1_xy(1, 3) = 34.2900; poly1_xy(2, 3) = -7.6098;
poly1_xy(1, 4) = 32.2580; poly1_xy(2, 4) = -7.6098;
poly1_xy(1, 5) = 32.2580; poly1_xy(2, 5) = -33.0098;
poly1_xy(1, 6) = 29.7180; poly1_xy(2, 6) = -33.0098;
poly1_xy(1, 7) = 29.7180; poly1_xy(2, 7) = -7.6098;
poly1_xy(1, 8) = 32.2580; poly1_xy(2, 8) = -5.0698;
poly1_xy(1, 9) = 34.2900; poly1_xy(2, 9) = -5.0698;
poly1_xy(1, 10) = 36.8300; poly1_xy(2, 10) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly1_xy, 1);
poly2_xy(1, 1) = 20.5740; poly2_xy(2, 1) = -7.6098;
poly2_xy(1, 2) = 23.1140; poly2_xy(2, 2) = -7.6098;
poly2_xy(1, 3) = 23.1140; poly2_xy(2, 3) = -33.0098;
poly2_xy(1, 4) = 25.1460; poly2_xy(2, 4) = -33.0098;
poly2_xy(1, 5) = 25.1460; poly2_xy(2, 5) = -7.6098;
poly2_xy(1, 6) = 27.6860; poly2_xy(2, 6) = -7.6098;
poly2_xy(1, 7) = 27.6860; poly2_xy(2, 7) = -33.0098;
poly2_xy(1, 8) = 25.1460; poly2_xy(2, 8) = -35.5498;
poly2_xy(1, 9) = 23.1140; poly2_xy(2, 9) = -35.5498;
poly2_xy(1, 10) = 20.5740; poly2_xy(2, 10) = -33.0098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly2_xy, 1);
poly3_xy(1, 1) = 18.5420; poly3_xy(2, 1) = -33.0098;
poly3_xy(1, 2) = 16.0020; poly3_xy(2, 2) = -33.0098;
poly3_xy(1, 3) = 16.0020; poly3_xy(2, 3) = -7.6098;
poly3_xy(1, 4) = 13.9700; poly3_xy(2, 4) = -7.6098;
poly3_xy(1, 5) = 13.9700; poly3_xy(2, 5) = -33.0098;
poly3_xy(1, 6) = 11.4300; poly3_xy(2, 6) = -33.0098;
poly3_xy(1, 7) = 11.4300; poly3_xy(2, 7) = -7.6098;
poly3_xy(1, 8) = 13.9700; poly3_xy(2, 8) = -5.0698;
poly3_xy(1, 9) = 16.0020; poly3_xy(2, 9) = -5.0698;
poly3_xy(1, 10) = 18.5420; poly3_xy(2, 10) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 11.1760; poly4_xy(2, 1) = -38.0898;
poly4_xy(1, 2) = 8.6360; poly4_xy(2, 2) = -38.0898;
poly4_xy(1, 3) = 8.6360; poly4_xy(2, 3) = -7.6098;
poly4_xy(1, 4) = 11.1760; poly4_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 39.6240; poly5_xy(2, 1) = -38.0898;
poly5_xy(1, 2) = 37.0840; poly5_xy(2, 2) = -38.0898;
poly5_xy(1, 3) = 37.0840; poly5_xy(2, 3) = -7.6098;
poly5_xy(1, 4) = 39.6240; poly5_xy(2, 4) = -7.6098;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 9.4000; poly6_xy(2, 1) = -36.5000;
poly6_xy(1, 2) = 10.4000; poly6_xy(2, 2) = -36.5000;
poly6_xy(1, 3) = 10.4000; poly6_xy(2, 3) = -37.5000;
poly6_xy(1, 4) = 9.4000; poly6_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 37.8500; poly7_xy(2, 1) = -36.5000;
poly7_xy(1, 2) = 38.8500; poly7_xy(2, 2) = -36.5000;
poly7_xy(1, 3) = 38.8500; poly7_xy(2, 3) = -37.5000;
poly7_xy(1, 4) = 37.8500; poly7_xy(2, 4) = -37.5000;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
points8(1, 1) = 9.8000; points8(2, 1) = -37.0000;
points8(1, 2) = 10.0000; points8(2, 2) = -37.0000;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points8, 0.0000, 0);
points9(1, 1) = 9.9000; points9(2, 1) = -36.9000;
points9(1, 2) = 9.9000; points9(2, 2) = -37.1000;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points9, 0.0000, 0);
points10(1, 1) = 38.2500; points10(2, 1) = -37.0000;
points10(1, 2) = 38.4500; points10(2, 2) = -37.0000;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points10, 0.0000, 0);
points11(1, 1) = 38.3500; points11(2, 1) = -36.9000;
points11(1, 2) = 38.3500; points11(2, 2) = -37.1000;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points11, 0.0000, 0);
%%% Port(s) on terminals

point1(1, 1) = 9.9000; point1(2, 1) = -37.0000;
[start1, stop1] = CalcPcbrnd2PortV(PCBRND, point1, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, start1, stop1, [0 0 -1], true);

point2(1, 1) = 38.3500; point2(2, 1) = -37.0000;
[start2, stop2] = CalcPcbrnd2PortV(PCBRND, point2, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 2, 50.000000, start2, stop2, [0 0 -1]);
