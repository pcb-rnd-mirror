%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=1.5000
mesh.y=[0.0000 0.7147 1.4295 2.1442 2.8590 3.5737 4.2885 5.0033 5.7180 6.4328 7.1475 7.8623 8.5770 9.2918 10.0065 10.7212 11.4360 11.4360 11.5257 11.6153 11.7050 11.7947 11.8843 11.9740 11.9740 12.0640 12.2365 12.4915 12.8290 13.2490 13.7515 14.3365 15.0040 15.7540 16.4952 17.2365 17.9777 18.7190 19.4602 20.2015 20.9427 21.6839 22.4252 23.1664 23.9077 24.6489 25.3902 26.1314 26.8726 27.6139 28.3551 29.0964 29.8376 30.5789 31.3201 32.0613 32.8026 33.5438 34.2851 35.0263 35.7676 36.5088 37.2500 37.9913 38.7325 39.4738 40.2150 40.9563 41.6975 42.4387 43.1800];
mesh.x=[0.0000 0.7361 1.4723 2.2084 2.9446 3.6807 4.4169 5.1530 5.8891 6.6253 7.3614 8.0976 8.8337 9.5698 10.3060 10.3060 10.3957 10.4854 10.5750 10.6647 10.7544 10.8441 10.9338 11.0234 11.1131 11.2028 11.2925 11.3822 11.4719 11.5615 11.6512 11.7409 11.8306 11.9203 12.0099 12.0996 12.1893 12.2790 12.3687 12.4583 12.5480 12.6377 12.7274 12.8171 12.9067 12.9964 13.0861 13.1758 13.2655 13.3552 13.4448 13.5345 13.6242 13.7139 13.8036 13.8932 13.9829 14.0726 14.1623 14.2520 14.3416 14.4313 14.5210 14.6107 14.7004 14.7901 14.8797 14.9694 15.0591 15.1488 15.2385 15.3281 15.4178 15.5075 15.5972 15.6869 15.7765 15.8662 15.9559 16.0456 16.1353 16.2249 16.3146 16.4043 16.4940 16.5837 16.6734 16.7630 16.8527 16.9424 17.0321 17.1218 17.2114 17.3011 17.3908 17.4805 17.5702 17.6598 17.7495 17.8392 17.9289 18.0186 18.1082 18.1979 18.2876 18.3773 18.4670 18.5567 18.6463 18.7360 18.8257 18.9154 19.0051 19.0947 19.1844 19.2741 19.3638 19.4535 19.5431 19.6328 19.7225 19.8122 19.9019 19.9915 20.0812 20.1709 20.2606 20.3503 20.4400 20.5296 20.6193 20.7090 20.7987 20.8884 20.9780 21.0677 21.1574 21.2471 21.3368 21.4264 21.5161 21.6058 21.6955 21.7852 21.8748 21.9645 22.0542 22.1439 22.2336 22.3233 22.4129 22.5026 22.5923 22.6820 22.7717 22.8613 22.9510 23.0407 23.1304 23.2201 23.3097 23.3994 23.4891 23.5788 23.6685 23.7582 23.8478 23.9375 24.0272 24.1169 24.2066 24.2962 24.3859 24.4756 24.5653 24.6550 24.7446 24.8343 24.9240 25.0137 25.1034 25.1930 25.2827 25.3724 25.4621 25.5518 25.6415 25.7311 25.8208 25.9105 26.0002 26.0899 26.1795 26.2692 26.3589 26.4486 26.5383 26.6279 26.7176 26.8073 26.8970 26.9867 27.0763 27.1660 27.2557 27.3454 27.4351 27.5248 27.6144 27.7041 27.7938 27.8835 27.9732 28.0628 28.1525 28.2422 28.3319 28.4216 28.5112 28.6009 28.6906 28.7803 28.8700 28.9596 29.0493 29.1390 29.2287 29.3184 29.4081 29.4977 29.5874 29.6771 29.7668 29.8565 29.9461 30.0358 30.1255 30.2152 30.3049 30.3945 30.4842 30.5740 30.5740 31.2916 32.0092 32.7268 33.4444 34.1620 34.8797 35.5973 36.3149 37.0325 37.7501 38.4677 39.1853 39.9029 40.6205 41.3381 42.0557 42.7734 43.4910 44.2086 44.9262 45.6438];
mesh.z=[-3.0000 -2.4000 -1.8000 -1.2000 -0.6000 0.0000 0.3750 0.7500 1.1250 1.5000 1.5000 2.1000 2.7000 3.3000 3.9000];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
mesh = AddPML(mesh, 8);
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.07/1000;
layer_types(1).conductivity = 56e6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 1.5;
layer_types(2).epsilon = 3.66;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.07/1000;
layer_types(3).conductivity = 56e6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.8778; poly0_xy(2, 1) = -42.5538;
poly0_xy(1, 2) = 1.2660; poly0_xy(2, 2) = -42.5538;
poly0_xy(1, 3) = 1.2660; poly0_xy(2, 3) = -1.4160;
poly0_xy(1, 4) = 44.8778; poly0_xy(2, 4) = -1.4160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
points1(1, 1) = 10.5750; points1(2, 1) = -11.7050;
points1(1, 2) = 30.3050; points1(2, 2) = -11.7050;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points1, 0.5080, 0);
poly2_xy(1, 1) = 3.7170; poly2_xy(2, 1) = -7.6410;
poly2_xy(1, 2) = 5.8760; poly2_xy(2, 2) = -7.6410;
poly2_xy(1, 3) = 5.8760; poly2_xy(2, 3) = -8.1490;
poly2_xy(1, 4) = 3.7170; poly2_xy(2, 4) = -8.1490;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly2_xy, 1);
poly3_xy(1, 1) = 3.7170; poly3_xy(2, 1) = -8.9110;
poly3_xy(1, 2) = 5.8760; poly3_xy(2, 2) = -8.9110;
poly3_xy(1, 3) = 5.8760; poly3_xy(2, 3) = -9.4190;
poly3_xy(1, 4) = 3.7170; poly3_xy(2, 4) = -9.4190;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 3.7170; poly4_xy(2, 1) = -10.1810;
poly4_xy(1, 2) = 5.8760; poly4_xy(2, 2) = -10.1810;
poly4_xy(1, 3) = 5.8760; poly4_xy(2, 3) = -10.6890;
poly4_xy(1, 4) = 3.7170; poly4_xy(2, 4) = -10.6890;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 3.7170; poly5_xy(2, 1) = -11.4510;
poly5_xy(1, 2) = 5.8760; poly5_xy(2, 2) = -11.4510;
poly5_xy(1, 3) = 5.8760; poly5_xy(2, 3) = -11.9590;
poly5_xy(1, 4) = 3.7170; poly5_xy(2, 4) = -11.9590;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 11.0830; poly6_xy(2, 1) = -8.1490;
poly6_xy(1, 2) = 8.9240; poly6_xy(2, 2) = -8.1490;
poly6_xy(1, 3) = 8.9240; poly6_xy(2, 3) = -7.6410;
poly6_xy(1, 4) = 11.0830; poly6_xy(2, 4) = -7.6410;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 11.0830; poly7_xy(2, 1) = -9.4190;
poly7_xy(1, 2) = 8.9240; poly7_xy(2, 2) = -9.4190;
poly7_xy(1, 3) = 8.9240; poly7_xy(2, 3) = -8.9110;
poly7_xy(1, 4) = 11.0830; poly7_xy(2, 4) = -8.9110;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
poly8_xy(1, 1) = 11.0830; poly8_xy(2, 1) = -10.6890;
poly8_xy(1, 2) = 8.9240; poly8_xy(2, 2) = -10.6890;
poly8_xy(1, 3) = 8.9240; poly8_xy(2, 3) = -10.1810;
poly8_xy(1, 4) = 11.0830; poly8_xy(2, 4) = -10.1810;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly8_xy, 1);
poly9_xy(1, 1) = 11.0830; poly9_xy(2, 1) = -11.9590;
poly9_xy(1, 2) = 8.9240; poly9_xy(2, 2) = -11.9590;
poly9_xy(1, 3) = 8.9240; poly9_xy(2, 3) = -11.4510;
poly9_xy(1, 4) = 11.0830; poly9_xy(2, 4) = -11.4510;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly9_xy, 1);
poly10_xy(1, 1) = 29.4170; poly10_xy(2, 1) = -7.6410;
poly10_xy(1, 2) = 31.5760; poly10_xy(2, 2) = -7.6410;
poly10_xy(1, 3) = 31.5760; poly10_xy(2, 3) = -8.1490;
poly10_xy(1, 4) = 29.4170; poly10_xy(2, 4) = -8.1490;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly10_xy, 1);
poly11_xy(1, 1) = 36.7830; poly11_xy(2, 1) = -8.1490;
poly11_xy(1, 2) = 34.6240; poly11_xy(2, 2) = -8.1490;
poly11_xy(1, 3) = 34.6240; poly11_xy(2, 3) = -7.6410;
poly11_xy(1, 4) = 36.7830; poly11_xy(2, 4) = -7.6410;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly11_xy, 1);
poly12_xy(1, 1) = 36.7830; poly12_xy(2, 1) = -9.4190;
poly12_xy(1, 2) = 34.6240; poly12_xy(2, 2) = -9.4190;
poly12_xy(1, 3) = 34.6240; poly12_xy(2, 3) = -8.9110;
poly12_xy(1, 4) = 36.7830; poly12_xy(2, 4) = -8.9110;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly12_xy, 1);
poly13_xy(1, 1) = 36.7830; poly13_xy(2, 1) = -10.6890;
poly13_xy(1, 2) = 34.6240; poly13_xy(2, 2) = -10.6890;
poly13_xy(1, 3) = 34.6240; poly13_xy(2, 3) = -10.1810;
poly13_xy(1, 4) = 36.7830; poly13_xy(2, 4) = -10.1810;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly13_xy, 1);
poly14_xy(1, 1) = 36.7830; poly14_xy(2, 1) = -11.9590;
poly14_xy(1, 2) = 34.6240; poly14_xy(2, 2) = -11.9590;
poly14_xy(1, 3) = 34.6240; poly14_xy(2, 3) = -11.4510;
poly14_xy(1, 4) = 36.7830; poly14_xy(2, 4) = -11.4510;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly14_xy, 1);
poly15_xy(1, 1) = 29.4170; poly15_xy(2, 1) = -8.9110;
poly15_xy(1, 2) = 31.5760; poly15_xy(2, 2) = -8.9110;
poly15_xy(1, 3) = 31.5760; poly15_xy(2, 3) = -9.4190;
poly15_xy(1, 4) = 29.4170; poly15_xy(2, 4) = -9.4190;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly15_xy, 1);
poly16_xy(1, 1) = 29.4170; poly16_xy(2, 1) = -10.1810;
poly16_xy(1, 2) = 31.5760; poly16_xy(2, 2) = -10.1810;
poly16_xy(1, 3) = 31.5760; poly16_xy(2, 3) = -10.6890;
poly16_xy(1, 4) = 29.4170; poly16_xy(2, 4) = -10.6890;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly16_xy, 1);
poly17_xy(1, 1) = 29.4170; poly17_xy(2, 1) = -11.4510;
poly17_xy(1, 2) = 31.5760; poly17_xy(2, 2) = -11.4510;
poly17_xy(1, 3) = 31.5760; poly17_xy(2, 3) = -11.9590;
poly17_xy(1, 4) = 29.4170; poly17_xy(2, 4) = -11.9590;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly17_xy, 1);
%%% Port(s) on terminals

point1(1, 1) = 10.5750; point1(2, 1) = -11.7050;
[start1, stop1] = CalcPcbrnd2PortV(PCBRND, point1, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, start1, stop1, [0 0 -1], true);

point2(1, 1) = 29.9250; point2(2, 1) = -11.7050;
[start2, stop2] = CalcPcbrnd2PortV(PCBRND, point2, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 2, 50.000000, start2, stop2, [0 0 -1]);
