%%% User tunables

%% base_priority and offset: chassis for the board to sit in.
% base priority: if the board displaces the model of the chassis or the other way around.
base_priority=0;

% offset on the whole layout to locate it relative to the simulation origin
offset.x = 0.0000;
offset.y = 0.0000;
offset.z = 0;

% void is the material used for: fill holes, cutouts in substrate, etc
void.name = 'AIR';
void.epsilon = 1.000000;
void.mue = 1.000000;
% void.kappa = kappa;
% void.sigma = sigma;

% how many points should be used to describe the round end of traces.
kludge.segments = 10;


%%% Board mesh, part 2
z_bottom_copper=1.5000
mesh.y=[0.0000 0.7261 1.4523 2.1784 2.9046 3.6307 4.3569 5.0830 5.8091 6.5353 7.2614 7.9876 8.7137 9.4398 10.1660 10.1660 10.2557 10.3453 10.4350 10.5247 10.6143 10.7040 11.4360 11.4360 11.5257 11.6153 11.7050 11.7947 11.8843 11.9740 11.9740 12.0640 12.2365 12.4915 12.8290 13.2490 13.7515 14.3365 15.0040 15.7540 16.4952 17.2365 17.9777 18.7190 19.4602 20.2015 20.9427 21.6839 22.4252 23.1664 23.9077 24.6489 25.3902 26.1314 26.8726 27.6139 28.3551 29.0964 29.8376 30.5789 31.3201 32.0613 32.8026 33.5438 34.2851 35.0263 35.7676 36.5088 37.2500 37.9913 38.7325 39.4738 40.2150 40.9563 41.6975 42.4387 43.1800];
mesh.x=[0.0000 0.7329 1.4659 2.1988 2.9317 3.6646 4.3976 5.1305 5.8634 6.5964 7.3293 8.0622 8.7951 9.5281 10.2610 10.3510 10.3510 10.4408 10.5306 10.6204 10.7101 10.7999 10.8897 10.9795 11.0693 11.1591 11.2489 11.3387 11.4284 11.5182 11.6080 11.6978 11.7876 11.8774 11.9672 12.0570 12.1467 12.2365 12.3263 12.4161 12.5059 12.5957 12.6855 12.7752 12.8650 12.9548 13.0446 13.1344 13.2242 13.3140 13.4038 13.4935 13.5833 13.6731 13.7629 13.8527 13.9425 14.0323 14.1221 14.2118 14.3016 14.3914 14.4812 14.5710 14.6608 14.7506 14.8404 14.9301 15.0199 15.1097 15.1995 15.2893 15.3791 15.4689 15.5586 15.6484 15.7382 15.8280 15.9178 16.0076 16.0974 16.1872 16.2769 16.3667 16.4565 16.5463 16.6361 16.7259 16.8157 16.9055 16.9952 17.0850 17.1748 17.2646 17.3544 17.4442 17.5340 17.6237 17.7135 17.8033 17.8931 17.9829 18.0727 18.1625 18.2523 18.3420 18.4318 18.5216 18.6114 18.7012 18.7910 18.8808 18.9706 19.0603 19.1501 19.2399 19.3297 19.4195 19.5093 19.5991 19.6888 19.7786 19.8684 19.9582 20.0480 20.1378 20.2276 20.3174 20.4071 20.4969 20.5867 20.6765 20.7663 20.8561 20.9459 21.0357 21.1254 21.2152 21.3050 21.3948 21.4846 21.5744 21.6642 21.7539 21.8437 21.9335 22.0233 22.1131 22.2029 22.2927 22.3825 22.4722 22.5620 22.6518 22.7416 22.8314 22.9212 23.0110 23.1008 23.1905 23.2803 23.3701 23.4599 23.5497 23.6395 23.7293 23.8191 23.9088 23.9986 24.0884 24.1782 24.2680 24.3578 24.4476 24.5373 24.6271 24.7169 24.8067 24.8965 24.9863 25.0761 25.1659 25.2556 25.3454 25.4352 25.5250 25.6148 25.7046 25.7944 25.8842 25.9739 26.0637 26.1535 26.2433 26.3331 26.4229 26.5127 26.6024 26.6922 26.7820 26.8718 26.9616 27.0514 27.1412 27.2310 27.3207 27.4105 27.5003 27.5901 27.6799 27.7697 27.8595 27.9493 28.0390 28.1288 28.2186 28.3084 28.3982 28.4880 28.5778 28.6675 28.7573 28.8471 28.9369 29.0267 29.1165 29.2063 29.2961 29.3858 29.4756 29.5654 29.6552 29.7450 29.8348 29.9246 30.0144 30.1041 30.1940 30.1940 30.2700 30.3460 30.4220 30.4980 30.5740 30.5740 31.2916 32.0092 32.7268 33.4444 34.1620 34.8797 35.5973 36.3149 37.0325 37.7501 38.4677 39.1853 39.9029 40.6205 41.3381 42.0557 42.7734 43.4910 44.2086 44.9262 45.6438];
mesh.z=[-3.0000 -2.4000 -1.8000 -1.2000 -0.6000 0.0000 0.3750 0.7500 1.1250 1.5000 1.5000 2.1000 2.7000 3.3000 3.9000];
mesh.x = mesh.x .+ offset.x;
mesh.y = offset.y .- mesh.y;
mesh.z = z_bottom_copper .- mesh.z .+ offset.z;
mesh = AddPML(mesh, 8);
CSX = DefineRectGrid(CSX, unit, mesh);

%%% Layer mapping
layers(1).number = 1;
layers(1).name = 'top_copper';
layers(1).clearn = 0;
layer_types(1).name = 'COPPER_1';
layer_types(1).subtype = 2;
layer_types(1).thickness = 0.07/1000;
layer_types(1).conductivity = 56e6;

layers(2).number = 2;
layers(2).name = 'grp_4';
layers(2).clearn = 0;
layer_types(2).name = 'SUBSTRATE_2';
layer_types(2).subtype = 3;
layer_types(2).thickness = 1.5;
layer_types(2).epsilon = 3.66;
layer_types(2).mue = 0;
layer_types(2).kappa = 0;
layer_types(2).sigma = 0;

layers(3).number = 3;
layers(3).name = 'bottom_copper';
layers(3).clearn = 0;
layer_types(3).name = 'COPPER_3';
layer_types(3).subtype = 2;
layer_types(3).thickness = 0.07/1000;
layer_types(3).conductivity = 56e6;


%%% Initialize pcb2csx
PCBRND = InitPCBRND(layers, layer_types, void, base_priority, offset, kludge);
CSX = InitPcbrndLayers(CSX, PCBRND);

%%% Board outline
outline_xy(1, 1) = 0; outline_xy(2, 1) = 0;
outline_xy(1, 2) = 45.6438; outline_xy(2, 2) = 0;
outline_xy(1, 3) = 45.6438; outline_xy(2, 3) = -43.1800;
outline_xy(1, 4) = 0; outline_xy(2, 4) = -43.1800;
CSX = AddPcbrndPoly(CSX, PCBRND, 2, outline_xy, 1);

%%% Copper objects
poly0_xy(1, 1) = 44.8778; poly0_xy(2, 1) = -42.5538;
poly0_xy(1, 2) = 1.2660; poly0_xy(2, 2) = -42.5538;
poly0_xy(1, 3) = 1.2660; poly0_xy(2, 3) = -1.4160;
poly0_xy(1, 4) = 44.8778; poly0_xy(2, 4) = -1.4160;
CSX = AddPcbrndPoly(CSX, PCBRND, 3, poly0_xy, 1);
points1(1, 1) = 10.5750; points1(2, 1) = -11.7050;
points1(1, 2) = 30.3050; points1(2, 2) = -11.7050;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points1, 0.5080, 0);
points2(1, 1) = 10.5750; points2(2, 1) = -10.4350;
points2(1, 2) = 29.9250; points2(2, 2) = -10.4350;
CSX = AddPcbrndTrace(CSX, PCBRND, 1, points2, 0.5080, 0);
poly3_xy(1, 1) = 3.7170; poly3_xy(2, 1) = -7.6410;
poly3_xy(1, 2) = 5.8760; poly3_xy(2, 2) = -7.6410;
poly3_xy(1, 3) = 5.8760; poly3_xy(2, 3) = -8.1490;
poly3_xy(1, 4) = 3.7170; poly3_xy(2, 4) = -8.1490;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly3_xy, 1);
poly4_xy(1, 1) = 3.7170; poly4_xy(2, 1) = -8.9110;
poly4_xy(1, 2) = 5.8760; poly4_xy(2, 2) = -8.9110;
poly4_xy(1, 3) = 5.8760; poly4_xy(2, 3) = -9.4190;
poly4_xy(1, 4) = 3.7170; poly4_xy(2, 4) = -9.4190;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly4_xy, 1);
poly5_xy(1, 1) = 3.7170; poly5_xy(2, 1) = -10.1810;
poly5_xy(1, 2) = 5.8760; poly5_xy(2, 2) = -10.1810;
poly5_xy(1, 3) = 5.8760; poly5_xy(2, 3) = -10.6890;
poly5_xy(1, 4) = 3.7170; poly5_xy(2, 4) = -10.6890;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly5_xy, 1);
poly6_xy(1, 1) = 3.7170; poly6_xy(2, 1) = -11.4510;
poly6_xy(1, 2) = 5.8760; poly6_xy(2, 2) = -11.4510;
poly6_xy(1, 3) = 5.8760; poly6_xy(2, 3) = -11.9590;
poly6_xy(1, 4) = 3.7170; poly6_xy(2, 4) = -11.9590;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly6_xy, 1);
poly7_xy(1, 1) = 11.0830; poly7_xy(2, 1) = -8.1490;
poly7_xy(1, 2) = 8.9240; poly7_xy(2, 2) = -8.1490;
poly7_xy(1, 3) = 8.9240; poly7_xy(2, 3) = -7.6410;
poly7_xy(1, 4) = 11.0830; poly7_xy(2, 4) = -7.6410;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly7_xy, 1);
poly8_xy(1, 1) = 11.0830; poly8_xy(2, 1) = -9.4190;
poly8_xy(1, 2) = 8.9240; poly8_xy(2, 2) = -9.4190;
poly8_xy(1, 3) = 8.9240; poly8_xy(2, 3) = -8.9110;
poly8_xy(1, 4) = 11.0830; poly8_xy(2, 4) = -8.9110;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly8_xy, 1);
poly9_xy(1, 1) = 11.0830; poly9_xy(2, 1) = -10.6890;
poly9_xy(1, 2) = 8.9240; poly9_xy(2, 2) = -10.6890;
poly9_xy(1, 3) = 8.9240; poly9_xy(2, 3) = -10.1810;
poly9_xy(1, 4) = 11.0830; poly9_xy(2, 4) = -10.1810;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly9_xy, 1);
poly10_xy(1, 1) = 11.0830; poly10_xy(2, 1) = -11.9590;
poly10_xy(1, 2) = 8.9240; poly10_xy(2, 2) = -11.9590;
poly10_xy(1, 3) = 8.9240; poly10_xy(2, 3) = -11.4510;
poly10_xy(1, 4) = 11.0830; poly10_xy(2, 4) = -11.4510;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly10_xy, 1);
poly11_xy(1, 1) = 29.4170; poly11_xy(2, 1) = -7.6410;
poly11_xy(1, 2) = 31.5760; poly11_xy(2, 2) = -7.6410;
poly11_xy(1, 3) = 31.5760; poly11_xy(2, 3) = -8.1490;
poly11_xy(1, 4) = 29.4170; poly11_xy(2, 4) = -8.1490;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly11_xy, 1);
poly12_xy(1, 1) = 36.7830; poly12_xy(2, 1) = -8.1490;
poly12_xy(1, 2) = 34.6240; poly12_xy(2, 2) = -8.1490;
poly12_xy(1, 3) = 34.6240; poly12_xy(2, 3) = -7.6410;
poly12_xy(1, 4) = 36.7830; poly12_xy(2, 4) = -7.6410;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly12_xy, 1);
poly13_xy(1, 1) = 36.7830; poly13_xy(2, 1) = -9.4190;
poly13_xy(1, 2) = 34.6240; poly13_xy(2, 2) = -9.4190;
poly13_xy(1, 3) = 34.6240; poly13_xy(2, 3) = -8.9110;
poly13_xy(1, 4) = 36.7830; poly13_xy(2, 4) = -8.9110;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly13_xy, 1);
poly14_xy(1, 1) = 36.7830; poly14_xy(2, 1) = -10.6890;
poly14_xy(1, 2) = 34.6240; poly14_xy(2, 2) = -10.6890;
poly14_xy(1, 3) = 34.6240; poly14_xy(2, 3) = -10.1810;
poly14_xy(1, 4) = 36.7830; poly14_xy(2, 4) = -10.1810;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly14_xy, 1);
poly15_xy(1, 1) = 36.7830; poly15_xy(2, 1) = -11.9590;
poly15_xy(1, 2) = 34.6240; poly15_xy(2, 2) = -11.9590;
poly15_xy(1, 3) = 34.6240; poly15_xy(2, 3) = -11.4510;
poly15_xy(1, 4) = 36.7830; poly15_xy(2, 4) = -11.4510;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly15_xy, 1);
poly16_xy(1, 1) = 29.4170; poly16_xy(2, 1) = -8.9110;
poly16_xy(1, 2) = 31.5760; poly16_xy(2, 2) = -8.9110;
poly16_xy(1, 3) = 31.5760; poly16_xy(2, 3) = -9.4190;
poly16_xy(1, 4) = 29.4170; poly16_xy(2, 4) = -9.4190;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly16_xy, 1);
poly17_xy(1, 1) = 29.4170; poly17_xy(2, 1) = -10.1810;
poly17_xy(1, 2) = 31.5760; poly17_xy(2, 2) = -10.1810;
poly17_xy(1, 3) = 31.5760; poly17_xy(2, 3) = -10.6890;
poly17_xy(1, 4) = 29.4170; poly17_xy(2, 4) = -10.6890;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly17_xy, 1);
poly18_xy(1, 1) = 29.4170; poly18_xy(2, 1) = -11.4510;
poly18_xy(1, 2) = 31.5760; poly18_xy(2, 2) = -11.4510;
poly18_xy(1, 3) = 31.5760; poly18_xy(2, 3) = -11.9590;
poly18_xy(1, 4) = 29.4170; poly18_xy(2, 4) = -11.9590;
CSX = AddPcbrndPoly(CSX, PCBRND, 1, poly18_xy, 1);
%%% Port(s) on terminals

point3(1, 1) = 10.5306; point3(2, 1) = -10.4350;
[start3, stop3] = CalcPcbrnd2PortV(PCBRND, point3, 1, 3);
%[CSX, port{1}] = AddLumpedPort(CSX, 999, 1, 50.000000, start3, stop3, [0 0 -1]);

point1(1, 1) = 10.5306; point1(2, 1) = -11.7050;
[start1, stop1] = CalcPcbrnd2PortV(PCBRND, point1, 1, 3);
[CSX, port{1}] = AddLumpedPort(CSX, 999, 2, 50.000000, start1, start3, [0 1 0], true);

point4(1, 1) = 29.9250; point4(2, 1) = -10.4350;
[start4, stop4] = CalcPcbrnd2PortV(PCBRND, point4, 1, 3);
%[CSX, port{3}] = AddLumpedPort(CSX, 999, 3, 50.000000, start4, stop4, [0 1 0]);

point2(1, 1) = 29.9250; point2(2, 1) = -11.7050;
[start2, stop2] = CalcPcbrnd2PortV(PCBRND, point2, 1, 3);
[CSX, port{2}] = AddLumpedPort(CSX, 999, 4, 50.000000, start2, start4, [0 1 0]);
