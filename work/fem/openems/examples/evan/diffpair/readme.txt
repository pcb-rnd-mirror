
This is a quick demo of a differential pair done with a hack to make an
h-port.

This isn't really dialed in yet. The mesh isn't right so I had to hack both
the ports because we don't have hports yet and I don't have a mesh that
crosses both ports with out extremely high density which is too
computationally wasteful.

Obviously with out the ports and etc the file clock.m was manually edited so
please don't attempt to regenerate it by exporting clock.lht yet.




