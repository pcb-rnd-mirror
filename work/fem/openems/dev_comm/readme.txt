
There has been some confusion about what these directories are.

matlab
this is the library used by our direct exporter

examples
this is a set of example files using my library for pcb export. i also use
them to test the library. at some point igor2 contributed an antenna to this
which he pulled from some ti data sheet.

ref
this is a set of example files i made for myself showing how openems's
geometry works

koen
this directory has a collection of variations on examples originally written
by Koen De Vleeschauwer.

koen/eagle
this is his original work with the output he got renamed and the output it
produces now for comparison. it uses an eagle file to make a hyp file which is
then run through hyp2mat.

koen/pcbrnd
this is what i got when i modified the example to import the hyp file into
pcb-rnd and then export it again to prove the hyp2mat export and our hyp
output could work.

koen/pcbrnd_direct/hairpinfilter
this is what i got when i took that example fixed the import errors up more
(from hyp-> lht conversion) and redid everything using our direct exporter and
utlimately our mesher as well.

koen/pcbrnd_direct/hairpinfilter_diff/
this is a series of diff's i created to try to document the alterations our
exporter needed to produce working output.

landscape
this was a set of drawings in gschem outlining the ecology of tools currently
existing in the space. it also has a few drawings i did in the start showing
early thoughts about how geometry should be exported. they are not really
relivant now.



