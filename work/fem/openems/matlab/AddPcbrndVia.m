function CSX = AddPcbrndVia(CSX, PCBRND, layer_start, layer_stop, points, dia_id, dia_od, filler, platting)
%
% CSX = CSX Cad data structure as defined by openems
% PCBRND = the pcb-rnd data structure as defined by this library
% layer_start = the starting layer for the via
% layer_stop = the ending layer for the via
% points = the x and y location of the via
% dia_id = the internal diameter of the via
% dia_od = the external diameter of the via
% filler = the matterial name for the stuff used to fill the via hole
%
% Copyright (C) 2020 Evan Foss
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Lesser General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>
%

% the following converts the 
points = CalcPcbrndPointsOffset(PCBRND, points);

current = PCBRND.layer_types(PCBRND.layers(layer_start).number);

% we need the start and stop layer info for the following reasons
% if ether of them is 3D instead of 2D that must be added to the via.
% we need to use the matterial from the start layer to decide the via 
% matterial.
start.layer = PCBRND.layers(layer_start);
stop.layer = PCBRND.layers(layer_stop);


% here is do the math for converting [x, y] to [x, y, z] with z offsets for 
% start and stop layer thickness.
stop_point = [ points(1,1), points(2,1), start.layer.ztop ];
start_point = [ points(1,1), points(2,1), stop.layer.zbottom ];

% unlike the other public functions in pcb2csx this currently doesn't obey any 
% changes on 2D vs 3D copper layers. 
CSX = AddCylinder(CSX, platting.name, PCBRND.prio.copperplane, start_point, stop_point, dia_od);
CSX = AddCylinder(CSX, filler.name, PCBRND.prio.void, start_point, stop_point, dia_id);

endfunction

