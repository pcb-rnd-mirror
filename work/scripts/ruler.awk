# An action to draw a ruler
function ruler(user_input)
{
	setunits("mm")
	grid ("set", "0.5mm")
	setvalue("line","0.180mm")
	selectlayer("silk")
	for (x=0; x < user_input+0 + 1 ; x++){
	       	if (x % 10 == 0){
			ddraft("line from " x "mm,1mm to " x "mm,3.5mm")
			if (x != 0){
				createtext( "#8" , 0, x - 0.6 "mm", "4.0mm", 0 , 100, x "")
			}
		}
			else {
			ddraft("line from " x "mm,1mm to " x "mm,2.0mm") 
		}
	}
	return 0
}

BEGIN {
	fgw_func_reg("ruler")
}
