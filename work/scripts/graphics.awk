# draw ruled graphs, spirals, and regular conv polygons for pcb-rnd

function rules(xcoord,ycoord,size, lines)
{
	# take the list of values.... get the interval from size/lines  
	x = xcoord
	y = ycoord
	for (i=0; i < size+0; i++){
		ddraft("line from " x "," y " to " x  "," y + size )
		ddraft("line from " x "," y " to " x+size  "," y )
		ddraft("line from " x "," y + size " to " x + size "," y + size)
		ddraft("line from " x "," y + size - i " to " x + size - i  "," y + size)
		ddraft("line from " x + size "," y + size - i " to " x + size - i  "," y)
	}
}

# Archimedean spirals
function ar_spiral(xcen ,ycen, radius, inradius, turns, sides, phase)
{
	if (sides+0 < 3){
		message ("must have 3 or more sides")
		return 0
	}
	x = xcen
	y = ycen
	steps = sides * turns
	phi  = deg_to_rad(phase)
	for (i=0 ; i <= steps; i++ ){
		r =  inradius + (i*(radius - inradius))/steps
		angle = 2 * 3.141592654 * i/sides
		xn = xcen + r * cos(angle + phi)
		yn = ycen + r * sin(angle + phi)
		if ( i > 0) {
			ddraft("line from " x "," y " to " xn "," yn )
			spiral_length += sqrt((xn-x)^2 + (yn-y)^2) 
		}
		x = xn
		y = yn
	}
	message("spiral length is: " spiral_length)
	spiral_length = 0
}

# draw regular concave polygons
function poly_trace(xcen,ycen, sides, side_length, circumradius,rot_angle) # TODO:rot_deg, apothem)

{
	pi = 3.141592654
	segment_angle =  2*3.14159264/sides
	message("segment angle is: " segment_angle )
	x = 0
	y = 0
	xsum = x
	ysum = y
	angle = deg_to_rad(rot_angle)
	#validation checks: min. sides, draw location checks, input checks (apo or circum, etc)
	if (sides+0 <= 2) {
		message ("number of sides must be greater than 2")
		return 0
	}
	# check if too many inputs exist, make sure the others don't
	message("circumradius is: " circumradius)
	if (circumradius != ""){
		if (side_length != 0 ){
			message ("give one input total for inputs side_length and circumradius")
			return 0
		}
		radius = circumradius
		message ("radius from circumradius: " radius)
	}
	else if (side_length != ""){
		radius = side_length / (2*sin(3.141592654/ ( 2 * sides) ) )
		message ("radius from side_length: " radius)
	}
	for (i = 0 ; i < sides+0  ; i++){
		angle +=  segment_angle
		xn = x + cos(angle)*radius
		yn = y + sin(angle)*radius
		xsum = xsum + x 
		ysum = ysum + y 
		xvert[i] = x
		yvert[i] = y
		x = xn
		y = yn
	}
	if ( xcen - circumradius < 1 || ycen - circumradius < 1 ){
		message("xcen: " xcen)
		message("xsum/sides: " xsum/sides)
		return 0
	}
	for (i in xvert){ 	
		ddraft("line from " xcen + xvert[i] - (xsum/sides) "," ycen + yvert[i] - (ysum/sides) " to " xcen + xvert[i+1] - (xsum/sides) "," ycen + yvert[i+1] - (ysum/sides) )
	}
	delete xvert
	delete yvert
}

# other types of spiral: fixed width
# other funcition ideas -- oval (coefficients for x or y)
# square with curved corners (can we draw arcs yet?)

function deg_to_rad(angle){
	rad = ( angle *  2 * 3.14159264 ) / 360
	return rad
	}


BEGIN {
	fgw_func_reg("rules")
	fgw_func_reg("ar_spiral")
	fgw_func_reg("poly_trace")
}
