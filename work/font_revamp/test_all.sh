#!/bin/sh

for fn in *.rp
do
	echo "====== $fn ======"
	echo '
		brave(oldfont, on)
		Load(layout, '$fn')
		export(debug, --compact, --stateless, --cam, '$fn'.o.dbgu=top-copper);
		brave(oldfont, off)
		Load(layout, '$fn')
		export(debug, --compact, --stateless, --cam, '$fn'.n.dbgu=top-copper);
	' | pcb-rnd --hid batch 2>&1 | grep -v "Brave setting:"
	sort $fn.o.dbgu > $fn.o.dbg
	sort $fn.n.dbgu > $fn.n.dbg
	rm $fn.o.dbgu $fn.n.dbgu
	diff -u $fn.o.dbg $fn.n.dbg && rm $fn.o.dbg $fn.n.dbg
done

