BEGIN {
	cat("template/page_head.html")
	print "<table class=\"ol\">"
	print "<caption> <h2> Donation Records</h2> <caption>"
	print "<tr><th class=\"shade\" align=\"left\">Date</th><th class=\"shade\" align=\"left\">Name</th><th class=\"shade\" align=\"left\">subproject</th><th class=\"shade\" align=\"right\">Amount</th> <th class=\"shade\" align=\"right\">Accumulated</th></tr>"
	print "<!------ generated ------>"
}

/^#/ { next }

{
	subp=$3
	sum += ($2+0)
	if ((subp == "-") || (subp == ""))
		subp = "&nbsp;"
	print "<tr>"
	print "	<td>" $1
	print "	<td>" $4
	print "	<td>" subp
	print "	<td align=\"right\">" $2
	print "	<td align=\"right\">" sum
	print "</tr>"
}


END {
	print "<!------ static ------>"
	print "</table>"
	cat("template/page_foot.html")
}