ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=GiO8btB+IvBOeXkzbAoAAABx;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAABy; loclib_name=bc817_sot23;
						li:objects {
						}
						ha:attrib {
							footprint=sot23
							li:portmap {
								{B->pcb/pinnum=1}
								{E->pcb/pinnum=2}
								{C->pcb/pinnum=3}
							}
						}
					}
					ha:group.2 {
						uuid=5GqIZZT/0fr+vbU1XlkAAAAw; loclib_name=2n3904_to92;
						li:objects {
						}
						ha:attrib {
							footprint=TO92
							li:portmap {
								{E->pcb/pinnum=1}
								{B->pcb/pinnum=2}
								{C->pcb/pinnum=3}
							}
						}
					}
					ha:group.3 {
						uuid=5GqIZZT/0fr+vbU1XlkAAAAx; loclib_name=led5;
						li:objects {
						}
						ha:attrib {
							footprint=led5
							li:portmap {
								{C->pcb/pinnum=1}
								{A->pcb/pinnum=2}
							}
						}
					}
					ha:group.4 {
						uuid=5GqIZZT/0fr+vbU1XlkAAAAy; loclib_name=pol_rcy;
						li:objects {
						}
						ha:attrib {
							li:portmap {
								{P->pcb/pinnum=1}
								{N->pcb/pinnum=2}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=GiO8btB+IvBOeXkzbAoAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=GiO8btB+IvBOeXkzbAoAAAAJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAQ;
				x=44000; y=144000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAK; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAR;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=C
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAL; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAS;
						x=-16000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=A
							role=terminal
						}
					}
					ha:line.3 { x1=-4000; y1=0; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.4 { x1=-12000; y1=0; x2=-10000; y2=0; stroke=sym-decor; }
					ha:line.5 { x1=-10000; y1=4000; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-6000; y1=0; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.7 { x1=-10000; y1=4000; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.8 { x1=-6000; y1=4000; x2=-6000; y2=-4000; stroke=sym-decor; }
					ha:text.9 { x1=-4000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.10 { x1=-8000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.11 { x1=-8000; y1=8000; x2=-6000; y2=11000; stroke=sym-decor; }
					ha:line.12 { x1=-6000; y1=11000; x2=-7000; y2=10000; stroke=sym-decor; }
					ha:line.13 { x1=-6000; y1=11000; x2=-6517; y2=9545; stroke=sym-decor; }
					ha:line.14 { x1=-10000; y1=7000; x2=-8000; y2=10000; stroke=sym-decor; }
					ha:line.15 { x1=-8000; y1=10000; x2=-8000; y2=8000; stroke=sym-decor; }
					ha:line.16 { x1=-8303; y1=6354; x2=-6303; y2=9354; stroke=sym-decor; }
					ha:line.17 { x1=-6303; y1=9354; x2=-7303; y2=8354; stroke=sym-decor; }
					ha:line.18 { x1=-6303; y1=9354; x2=-6820; y2=7899; stroke=sym-decor; }
					ha:line.19 { x1=-10303; y1=5354; x2=-8303; y2=8354; stroke=sym-decor; }
					ha:line.20 { x1=-8303; y1=8354; x2=-8303; y2=6354; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=led5
					name=LED1
					role=symbol
				}
			}
			ha:group.3 {
				uuid=GiO8btB+IvBOeXkzbAoAAAAM; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAQ;
				x=156000; y=144000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAN; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAR;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=C
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAO; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAS;
						x=-16000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=A
							role=terminal
						}
					}
					ha:line.3 { x1=-4000; y1=0; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.4 { x1=-12000; y1=0; x2=-10000; y2=0; stroke=sym-decor; }
					ha:line.5 { x1=-10000; y1=4000; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-6000; y1=0; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.7 { x1=-10000; y1=4000; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.8 { x1=-6000; y1=4000; x2=-6000; y2=-4000; stroke=sym-decor; }
					ha:text.9 { x1=-4000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.10 { x1=-8000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.11 { x1=-8000; y1=8000; x2=-6000; y2=11000; stroke=sym-decor; }
					ha:line.12 { x1=-6000; y1=11000; x2=-7000; y2=10000; stroke=sym-decor; }
					ha:line.13 { x1=-6000; y1=11000; x2=-6517; y2=9545; stroke=sym-decor; }
					ha:line.14 { x1=-10000; y1=7000; x2=-8000; y2=10000; stroke=sym-decor; }
					ha:line.15 { x1=-8000; y1=10000; x2=-8000; y2=8000; stroke=sym-decor; }
					ha:line.16 { x1=-8303; y1=6354; x2=-6303; y2=9354; stroke=sym-decor; }
					ha:line.17 { x1=-6303; y1=9354; x2=-7303; y2=8354; stroke=sym-decor; }
					ha:line.18 { x1=-6303; y1=9354; x2=-6820; y2=7899; stroke=sym-decor; }
					ha:line.19 { x1=-10303; y1=5354; x2=-8303; y2=8354; stroke=sym-decor; }
					ha:line.20 { x1=-8303; y1=8354; x2=-8303; y2=6354; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=led5
					name=LED2
					role=symbol
				}
			}
			ha:group.4 {
				uuid=GiO8btB+IvBOeXkzbAoAAAAv; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=44000; y=132000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=acy(300)
					name=R1
					role=symbol
					value=470
				}
			}
			ha:group.5 {
				uuid=GiO8btB+IvBOeXkzbAoAAAAy; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=92000; y=132000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=acy(300)
					name=R2
					role=symbol
					value=100k
				}
			}
			ha:group.6 {
				uuid=GiO8btB+IvBOeXkzbAoAAAA1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=112000; y=132000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAA2; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAA3; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=acy(300)
					name=R3
					role=symbol
					value=100k
				}
			}
			ha:group.7 {
				uuid=GiO8btB+IvBOeXkzbAoAAAA4; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=156000; y=132000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAAA5; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAA6; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=acy(300)
					name=R4
					role=symbol
					value=470
				}
			}
			ha:group.8 {
				uuid=GiO8btB+IvBOeXkzbAoAAABf; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAk;
				x=60000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAABg; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAl;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAABh; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAm;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
					ha:arc.8 { cx=34000; cy=0; r=23000; sang=167.500000; dang=25.000000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=-3000; x2=8000; y2=-3000; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-4000; x2=7000; y2=-2000; stroke=sym-decor; }
					ha:text.11 { x1=16000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../A.footprint%; floater=1; }
					ha:text.12 { x1=19000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../A.devmap%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=pol_rcy
					footprint=rcy(100)
					name=C1
					role=symbol
					value=10u
				}
			}
			ha:group.9 {
				uuid=GiO8btB+IvBOeXkzbAoAAABi; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAk;
				x=144000; y=92000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=GiO8btB+IvBOeXkzbAoAAABj; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAl;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAABk; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAm;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
					ha:arc.8 { cx=34000; cy=0; r=23000; sang=167.500000; dang=25.000000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=-3000; x2=8000; y2=-3000; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-4000; x2=7000; y2=-2000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=pol_rcy
					footprint=rcy(100)
					name=C2
					role=symbol
					value=10u
				}
			}
			ha:group.10 {
				uuid=GiO8btB+IvBOeXkzbAoAAABt; src_uuid=iNOQfJpO6hT/HFDFGjoAAACK;
				x=56000; y=68000; mirx=1;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=10266; y1=-1780; x2=9224; y2=-3517; }
							ha:line { x1=9224; y1=-3517; x2=10935; y2=-3368; }
							ha:line { x1=10935; y1=-3368; x2=10266; y2=-1780; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAABu; src_uuid=iNOQfJpO6hT/HFDFGjoAAACL;
						x=12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=C
							role=terminal
						}
					}
					ha:group.3 {
						uuid=GiO8btB+IvBOeXkzbAoAAABv; src_uuid=iNOQfJpO6hT/HFDFGjoAAACM;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=B
							role=terminal
						}
					}
					ha:group.4 {
						uuid=GiO8btB+IvBOeXkzbAoAAABw; src_uuid=iNOQfJpO6hT/HFDFGjoAAACN;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=E
							role=terminal
						}
					}
					ha:text.5 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.6 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.7 { cx=9000; cy=0; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=7000; y1=4000; x2=7000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=4000; y1=0; x2=7000; y2=0; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-1000; x2=12000; y2=-4000; stroke=sym-decor; }
					ha:line.11 { x1=7000; y1=1000; x2=12000; y2=4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=2n3904_to92
					name=Q1
					role=symbol
				}
			}
			ha:group.11 {
				uuid=GiO8btB+IvBOeXkzbAoAAABz; src_uuid=iNOQfJpO6hT/HFDFGjoAAACK;
				x=144000; y=68000;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=10266; y1=-1780; x2=9224; y2=-3517; }
							ha:line { x1=9224; y1=-3517; x2=10935; y2=-3368; }
							ha:line { x1=10935; y1=-3368; x2=10266; y2=-1780; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.2 {
						uuid=GiO8btB+IvBOeXkzbAoAAAB0; src_uuid=iNOQfJpO6hT/HFDFGjoAAACL;
						x=12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=C
							role=terminal
						}
					}
					ha:group.3 {
						uuid=GiO8btB+IvBOeXkzbAoAAAB1; src_uuid=iNOQfJpO6hT/HFDFGjoAAACM;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=B
							role=terminal
						}
					}
					ha:group.4 {
						uuid=GiO8btB+IvBOeXkzbAoAAAB2; src_uuid=iNOQfJpO6hT/HFDFGjoAAACN;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=E
							role=terminal
						}
					}
					ha:text.5 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.6 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.7 { cx=9000; cy=0; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=7000; y1=4000; x2=7000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=4000; y1=0; x2=7000; y2=0; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-1000; x2=12000; y2=-4000; stroke=sym-decor; }
					ha:line.11 { x1=7000; y1=1000; x2=12000; y2=4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=2n3904_to92
					name=Q2
					role=symbol
				}
			}
			ha:group.12 {
				uuid=GiO8btB+IvBOeXkzbAoAAACJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABd;
				x=200000; y=128000; rot=270.000000;
				li:objects {
					ha:line.1 { x1=15000; y1=5000; x2=15000; y2=-5000; stroke=sym-decor; }
					ha:line.2 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=13000; y1=2000; x2=13000; y2=-2000; stroke=sym-decor; }
					ha:group.4 {
						uuid=GiO8btB+IvBOeXkzbAoAAACK; src_uuid=iNOQfJpO6hT/HFDFGjoAAABe;
						x=24000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.5 {
						uuid=GiO8btB+IvBOeXkzbAoAAACL; src_uuid=iNOQfJpO6hT/HFDFGjoAAABf;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:line.6 { x1=4000; y1=0; x2=7000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=17000; y1=2000; x2=17000; y2=-2000; stroke=sym-decor; }
					ha:line.8 { x1=7000; y1=5000; x2=7000; y2=-5000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=2000; x2=9000; y2=-2000; stroke=sym-decor; }
					ha:line.10 { x1=17000; y1=0; x2=20000; y2=0; stroke=sym-decor; }
					ha:text.11 { x1=4000; y1=6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=connector(2,1)
					name=B1
					li:portmap {
						{P->pcb/pinnum=1}
						{N->pcb/pinnum=2}
					}
					role=symbol
				}
			}
			ha:group.17 {
				uuid=GiO8btB+IvBOeXkzbAoAAACN;
				li:objects {
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.18 {
				uuid=GiO8btB+IvBOeXkzbAoAAACO;
				li:objects {
					ha:line.1 { x1=44000; y1=160000; x2=44000; y2=168000; stroke=wire; }
					ha:line.2 { x1=112000; y1=168000; x2=112000; y2=132000; stroke=wire; }
					ha:line.4 { x1=112000; y1=168000; x2=112000; y2=168000; stroke=junction; }
					ha:line.5 { x1=92000; y1=132000; x2=92000; y2=168000; stroke=wire; }
					ha:line.6 { x1=92000; y1=168000; x2=92000; y2=168000; stroke=junction; }
					ha:line.7 { x1=156000; y1=168000; x2=156000; y2=160000; stroke=wire; }
					ha:line.8 { x1=44000; y1=168000; x2=200000; y2=168000; stroke=wire; }
					ha:line.9 { x1=156000; y1=168000; x2=156000; y2=168000; stroke=junction; }
					ha:line.10 { x1=200000; y1=168000; x2=200000; y2=128000; stroke=wire; }
					ha:text.11 { x1=117000; y1=168000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=Vcc
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.19 {
				li:conn {
					/2/18/1
					/2/2/2/1
				}
			}
			ha:connection.20 {
				li:conn {
					/2/18/2
					/2/6/2/1
				}
			}
			ha:connection.21 {
				li:conn {
					/2/18/5
					/2/5/2/1
				}
			}
			ha:connection.22 {
				li:conn {
					/2/18/7
					/2/3/2/1
				}
			}
			ha:group.23 {
				uuid=GiO8btB+IvBOeXkzbAoAAACP;
				li:objects {
					ha:line.2 { x1=44000; y1=92000; x2=60000; y2=92000; stroke=wire; }
					ha:line.3 { x1=44000; y1=76000; x2=44000; y2=112000; stroke=wire; }
					ha:line.4 { x1=44000; y1=92000; x2=44000; y2=92000; stroke=junction; }
					ha:text.5 { x1=48000; y1=92000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=left_e
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.24 {
				li:conn {
					/2/4/1/1
					/2/23/3
				}
			}
			ha:connection.25 {
				li:conn {
					/2/23/2
					/2/8/2/1
				}
			}
			ha:group.32 {
				uuid=GiO8btB+IvBOeXkzbAoAAACS;
				li:objects {
					ha:line.1 { x1=80000; y1=92000; x2=96000; y2=92000; stroke=wire; }
					ha:line.2 { x1=96000; y1=92000; x2=112000; y2=68000; stroke=wire; }
					ha:line.3 { x1=112000; y1=68000; x2=112000; y2=112000; stroke=wire; }
					ha:line.4 { x1=112000; y1=68000; x2=144000; y2=68000; stroke=wire; }
					ha:line.5 { x1=112000; y1=68000; x2=112000; y2=68000; stroke=junction; }
					ha:text.6 { x1=117000; y1=68000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=right_b
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.33 {
				li:conn {
					/2/32/1
					/2/8/1/1
				}
			}
			ha:group.34 {
				uuid=GiO8btB+IvBOeXkzbAoAAACT;
				li:objects {
					ha:line.1 { x1=124000; y1=92000; x2=108000; y2=92000; stroke=wire; }
					ha:line.2 { x1=108000; y1=92000; x2=92000; y2=68000; stroke=wire; }
					ha:line.3 { x1=92000; y1=68000; x2=92000; y2=112000; stroke=wire; }
					ha:line.4 { x1=56000; y1=68000; x2=92000; y2=68000; stroke=wire; }
					ha:line.5 { x1=92000; y1=68000; x2=92000; y2=68000; stroke=junction; }
					ha:text.6 { x1=71000; y1=68000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=left_b
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.35 {
				li:conn {
					/2/34/1
					/2/9/1/1
				}
			}
			ha:connection.36 {
				li:conn {
					/2/34/3
					/2/5/1/1
				}
			}
			ha:connection.37 {
				li:conn {
					/2/34/4
					/2/10/3/1
				}
			}
			ha:connection.38 {
				li:conn {
					/2/32/3
					/2/6/1/1
				}
			}
			ha:connection.39 {
				li:conn {
					/2/32/4
					/2/11/3/1
				}
			}
			ha:group.40 {
				uuid=GiO8btB+IvBOeXkzbAoAAACU;
				li:objects {
					ha:line.1 { x1=156000; y1=144000; x2=156000; y2=132000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.41 {
				li:conn {
					/2/40/1
					/2/3/1/1
				}
			}
			ha:connection.42 {
				li:conn {
					/2/40/1
					/2/7/2/1
				}
			}
			ha:group.43 {
				uuid=GiO8btB+IvBOeXkzbAoAAACV;
				li:objects {
					ha:line.1 { x1=156000; y1=112000; x2=156000; y2=76000; stroke=wire; }
					ha:line.2 { x1=144000; y1=92000; x2=156000; y2=92000; stroke=wire; }
					ha:line.3 { x1=156000; y1=92000; x2=156000; y2=92000; stroke=junction; }
					ha:text.4 { x1=148000; y1=92000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=right_e
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.44 {
				li:conn {
					/2/43/1
					/2/7/1/1
				}
			}
			ha:connection.45 {
				li:conn {
					/2/43/1
					/2/11/2/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/43/2
					/2/9/2/1
				}
			}
			ha:connection.47 {
				li:conn {
					/2/18/10
					/2/12/5/1
				}
			}
			ha:group.48 {
				uuid=GiO8btB+IvBOeXkzbAoAAACW;
				li:objects {
					ha:line.1 { x1=44000; y1=60000; x2=44000; y2=44000; stroke=wire; }
					ha:line.3 { x1=44000; y1=44000; x2=200000; y2=44000; stroke=wire; }
					ha:line.4 { x1=200000; y1=44000; x2=200000; y2=104000; stroke=wire; }
					ha:line.5 { x1=156000; y1=60000; x2=156000; y2=44000; stroke=wire; }
					ha:line.6 { x1=156000; y1=44000; x2=156000; y2=44000; stroke=junction; }
					ha:text.7 { x1=115000; y1=44000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=gnd
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.49 {
				li:conn {
					/2/48/1
					/2/10/4/1
				}
			}
			ha:connection.50 {
				li:conn {
					/2/48/4
					/2/12/4/1
				}
			}
			ha:connection.51 {
				li:conn {
					/2/48/5
					/2/11/4/1
				}
			}
			ha:group.52 {
				uuid=GiO8btB+IvBOeXkzbAoAAACX;
				li:objects {
					ha:line.1 { x1=44000; y1=144000; x2=44000; y2=132000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.53 {
				li:conn {
					/2/52/1
					/2/2/1/1
				}
			}
			ha:connection.54 {
				li:conn {
					/2/52/1
					/2/4/2/1
				}
			}
			ha:connection.55 {
				li:conn {
					/2/23/3
					/2/10/2/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=John Doe
			page=1/1
			print_page=A/4
			title=Blinking LED
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     line_refraction = false
     grids_idx = 0
     grid = 1.0240 mm
    }
   }
  }
}
