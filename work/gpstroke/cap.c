#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "c24lib/image/image.h"
#include "c24lib/image/draw.h"
#include "c24lib/image/pnm.h"

#include "pcb_draw.h"

static inline void pcb_clear(image_t *img)
{
	int n;
	pixel_t *p = img->pixmap;
	for(n = 0; n < img->sx * img->sy; n++,p++) {
		*p = pixel_white;
	}
}


int main(int argc, char *argv[])
{
	image_t *img = image_new(200, 200);
	pcb_clear(img);

	draw_pcb_fillcr(img, 50, 20, 6);

	draw_pcb_line(img, 10, 10, 100, 140, 12);
	draw_pcb_line(img, 100, 10, 10, 140, 12);
	pnm_save(img, "img.pnm");
	return 0;
}
