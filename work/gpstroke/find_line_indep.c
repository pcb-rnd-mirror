#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "c24lib/image/image.h"
#include "c24lib/image/draw.h"
#include "c24lib/image/pnm.h"

#include "pcb_draw.h"
#include "gp.h"

#define DUMPDIR "/tmp/gpstroke/"


/*
#define OPTIMIZE_FROM 5000
#define COOL_PERIOD 2000
*/

#define ITERATIONS 21000
#define STROKE_PENALTY 1000
#define MAX_TH 1
#define MIN_TH 5

#define MAX_STROKES 64
int max_strokes = 16;
image_t *ref, *canvas;

typedef struct {
	int x1, y1, x2, y2, th;
} stroke_t;

typedef struct {
	int len;
	stroke_t str[MAX_STROKES];
} pcb_t;

#include "pcb_common.h"

static inline int pcb_num_strokes(pcb_t *p)
{
	return p->len;
}

static void pcb_dump_str(FILE *f, pcb_t *p)
{
	int n;
	for(n = 0; n < p->len; n++)
		fprintf(f,"stroke %d %d %d %d %d\n", p->str[n].x1, p->str[n].y1, p->str[n].x2, p->str[n].y2, p->str[n].th);
}


static inline void pcb_draw(image_t *img, pcb_t *p)
{
	int n;

	draw_color(pixel_black);
	for(n = 0; n < p->len; n++)
		draw_pcb_line(img, p->str[n].x1, p->str[n].y1, p->str[n].x2, p->str[n].y2, clamp(p->str[n].th, MIN_TH, MAX_TH));
}

static inline void rand_stroke(stroke_t *s)
{
	s->x1 = myrand(0, ref->sx-1);
	s->x2 = myrand(0, ref->sx-1);
	s->y1 = myrand(0, ref->sy-1);
	s->y2 = myrand(0, ref->sy-1);
	s->th = myrand(1, MAX_TH);
}

static pcb_t *load_str(char *fn)
{
	char line[256];
	FILE *f = fopen(fn, "r");
	pcb_t *p;
	if (f == NULL)
		return 0;
	p = calloc(sizeof(pcb_t), 1);
	while(fgets(line, sizeof(line), f) != NULL) {
		char *s = line;
		int x1, y1, x2, y2, th;
		if (strncmp(s, "stroke", 6) == 0)
			s+=6;
			
		if (sscanf(s, "%d %d %d %d %d", &x1, &y1, &x2, &y2, &th) == 5) {
			stroke_t *s = &p->str[p->len];
			s->x1 = x1;
			s->y1 = y1;
			s->x2 = x2;
			s->y2 = y2;
			s->th = th;
			p->len++;
		}
	}
	fclose(f);
	return p;
}

void *pcb_create(void)
{
	pcb_t *p = malloc(sizeof(pcb_t));
	int n;

	if (default_new != NULL) {
		memcpy(p, default_new, sizeof(pcb_t));
		return p;
	}

	p->len = myrand(1, max_strokes/2);
	for(n = 0; n < p->len; n++)
		rand_stroke(&p->str[n]);

	return p;
}

static inline void pcb_mutate_add(pcb_t *p)
{
	int loc, x, y;
	if (p->len >= MAX_STROKES)
		return;

	loc = myrand(0, black_len-1);

	x = image_x(ref, ref->pixmap+black[loc]);
	y = image_y(ref, ref->pixmap+black[loc]);

	p->str[p->len].x1 = x;
	p->str[p->len].y1 = y;
	p->str[p->len].x2 = x + myrand(2, 20) - 10;
	p->str[p->len].y2 = y + myrand(2, 20) - 10;
	p->str[p->len].th = myrand(3, 10);
	p->len++;
}

static inline void pcb_del_stroke(pcb_t *p, int idx)
{
	int rem;
	rem = p->len - idx - 1;
	if (rem > 0)
		memmove(p->str+idx, p->str+idx+1, rem*sizeof(stroke_t));
	p->len--;

}

static inline void pcb_mutate_del(pcb_t *p)
{
	if (p->len < 2)
		return;

	pcb_del_stroke(p, myrand(0, p->len-1));
}

static inline void pcb_mutate_chg(pcb_t *p)
{
	int idx = myrand(0, p->len-1);
	int fld = myrand(0, 5);
	int dirx = (rand() % 3) - 1;
	int diry = (rand() % 3) - 1;
	int dir = (rand() % 2) ? 1 : -1;
	int tmp;

	switch(fld) {
		case 0:
			tune(p->str[idx].x1, 0, ref->sx, dirx);
			tune(p->str[idx].y1, 0, ref->sy, diry);
			break;
		case 1:
			tune(p->str[idx].x2, 0, ref->sx, dirx);
			tune(p->str[idx].y2, 0, ref->sy, diry);
			break;
		case 2: tune(p->str[idx].th, MIN_TH, MAX_TH, dir); break;
		case 3: rand_stroke(&p->str[idx]); break;
		case 4:
			tmp = p->str[idx].x1;
			p->str[idx].x1 = p->str[idx].x2;
			p->str[idx].x2 = tmp;
			break;
		case 5:
			tmp = p->str[idx].y1;
			p->str[idx].y1 = p->str[idx].y2;
			p->str[idx].y2 = tmp;
			break;
	}
}

#define sqr(a) ((a) * (a))

#define dist2(s1, i1, s2, i2) \
	(sqr(s1.x ## i1 - s2.x ## i2) + sqr(s1.y ## i1 - s2.y ## i2))

static inline void pcb_mutate_merge(pcb_t *p)
{
	int idx1 = myrand(0, p->len-1);
	int idx2 = myrand(0, p->len-1);
	int n, best, bestn, dst[4];

	if (idx1 == idx2)
		return;

	/* merge the closest points */
	dst[0] = dist2(p->str[idx1], 1, p->str[idx2], 1);
	dst[1] = dist2(p->str[idx1], 1, p->str[idx2], 2);
	dst[2] = dist2(p->str[idx1], 2, p->str[idx2], 1);
	dst[3] = dist2(p->str[idx1], 2, p->str[idx2], 2);

	best = sqr(ref->sx + ref->sy);
	for(n = 0; n < 4; n++) {
		if (dst[n] < best) {
			best = dst[n];
			bestn = n;
		}
	}

	switch(bestn) {
		case 0:
			p->str[idx1].x2 = p->str[idx2].x1;
			p->str[idx1].y2 = p->str[idx2].y1;
			break;
		case 1:
			p->str[idx1].x2 = p->str[idx2].x2;
			p->str[idx1].y2 = p->str[idx2].y2;
			break;
		case 2:
			p->str[idx1].x1 = p->str[idx2].x1;
			p->str[idx1].y1 = p->str[idx2].y1;
			break;
		case 3:
			p->str[idx1].x1 = p->str[idx2].x2;
			p->str[idx1].y1 = p->str[idx2].y2;
			break;
	}
	pcb_del_stroke(p, idx2);
}

void pcb_mutate(void *data)
{
	int n=myrand(0, 100);
	if (n > 65)
		pcb_mutate_chg(data);
	switch(n%3) {
		case 0: pcb_mutate_add(data); break;
		case 1: pcb_mutate_del(data); break;
		case 2: pcb_mutate_merge(data); break;
	}
}

gp_pop_t pop = {
	.len = 16,
	.weak_kill = 5,
	.clone_times = 2,
	.create = pcb_create,
	.score = pcb_score,
	.mutate = pcb_mutate,
	.clone = pcb_clone,
	.dump = pcb_dump
};

int main(int argc, char *argv[])
{
	int n, last_best = -1;

	common_init(argc, argv);

	gp_setup(&pop);

	for(n = 0; n < ITERATIONS; n++) {
#ifdef OPTIMIZE_FROM
		if ((n >= OPTIMIZE_FROM) && ((n % COOL_PERIOD) == 0)) {
			stroke_penalty *= 1.1;
		}
#endif
		gp_iterate(&pop);
		if (((pop.pop_cnt % 50) == 0) && (pop.indiv[0].fitness != last_best)) {
			gp_dump(stdout, &pop, 1);
			last_best = pop.indiv[0].fitness;
		}
	}
	pnm_save(ref, DUMPDIR "ref.pnm");
	free(black);
}
