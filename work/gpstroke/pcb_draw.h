#include "c24lib/image/image.h"

void draw_pcb_line(image_t *dst, int x1, int y1, int x2, int y2, double th);

void draw_pcb_fillcr(image_t *dst, int xc, int yc, int r);
