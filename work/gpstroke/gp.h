#include <stdio.h>

typedef struct {
	void *data;
	double fitness;
} gp_indiv_t;

typedef struct {
	/* config */
	int len;
	int weak_kill;
	int clone_times;

	void *(*create)(void);
	void (*mutate)(void *data);
	void (*clone)(void *dst, void *src);
	double (*score)(void *data);
	const char *(*dump)(void *data, int population_id, int individual_id);

	/* internal */
	gp_indiv_t *indiv;
	int pop_cnt;
} gp_pop_t;

void gp_setup(gp_pop_t *pop);
void gp_iterate(gp_pop_t *pop);
void gp_dump(FILE *f, gp_pop_t *pop, int max_dump);

