#include <stdlib.h>
#include <math.h>
#include "pcb_draw.h"
#include "c24lib/image/image.h"

static void draw_thick_line(image_t * dst, int x1, int y1, int x2, int y2, double vx, double vy, double nx, double ny, int th)
{
	double x, y, ymin, ymax;

	if (y1 <= y2) {
		ymin = y1;
		ymax = y2;
	}
	else {
		ymin = y2;
		ymax = y1;
	}

	x = x1;
	y = y1;
	draw_pcb_fillcr(dst, x1, y1, th/2);
	for(;;) {
		int n;
		double xx, yy;
		xx = x - nx * th / 2.0;
		yy = y - ny * th / 2.0;
		for(n = 0; n <= th; n++) {
			if ((xx >= 0) && (xx <= dst->sx) && (yy >= 0) && (yy <= dst->sy)) {
				pixel_t *pixel = (dst->pixmap) + (int) xx + dst->sx * (int) yy;
				*pixel = pixel_black;
			}
			xx += nx;
			yy += ny;
		}
		
		x += vx/2.0;
		y += vy/2.0;
		if (x > x2)
			break;
		if ((y > ymax) || (y < ymin))
			break;
	}

	draw_pcb_fillcr(dst, x, y, th/2);

	return;
}


void draw_pcb_line(image_t *dst, int x1, int y1, int x2, int y2, double th)
{
	double nx, ny, vx, vy;
	double l;
	int tmp;

	if (x1 > x2) {
		tmp = x1;
		x1 = x2;
		x2 = tmp;
		tmp = y1;
		y1 = y2;
		y2 = tmp;
	}

	vx = x2 - x1;
	vy = y2 - y1;
	l = hypot(vx, vy);
	vx /= l;
	vy /= l;
	nx = -vy;
	ny = vx;

	if ((x1 != x2) || (y1 != y2))
		draw_thick_line(dst, x1, y1, x2, y2, vx, vy, nx, ny, th);
	else
		draw_pcb_fillcr(dst, x1, y1, th/2);

//	draw_color(pixel_red);
//	draw_line(dst, x1+nx*th/2.0, y1+ny*th, x2+nx*th, y2+ny*th/2.0);
//	draw_line(dst, x1-nx*th/2.0, y1-ny*th, x2-nx*th, y2-ny*th/2.0);
}

int *fc_pat[1000];

static void fc_pat_gen(int r)
{
	int y, *p;
	p = fc_pat[r] = malloc(sizeof(int) * (r+1));
	for(y = 0; y <= r; y++) {
		double x = (double)r * cos(asin((double)y/(double)r));
		p[y] = round(x);
	}
}

void draw_pcb_fillcr(image_t *dst, int xc, int yc, int r)
{
	int y, x, *pa;
	pixel_t *p;

	if (fc_pat[r] == NULL)
		fc_pat_gen(r);

	pa = fc_pat[r];

	for(y = -r; y <= r; y++) {
		int x1, x2, ay;
		if (y+yc < 0)
			continue;
		if (y+yc >= dst->sy)
			break;
		ay = y < 0 ? -y : y;
		x1 = xc - pa[ay];
		x2 = xc + pa[ay];
		if (x1 >= dst->sx)
			continue;
		if (x1 < 0)
			x1 = 0;
		if (x2 >= dst->sx)
			x2 = dst->sx-1;
		p = &image_pix(dst, x1, y+yc);
		for(x = x1; x <= x2; x++,p++) {
			*p = pixel_black;
		}
	}
/*	p = &image_pix(dst, xc, yc);
	*p = pixel_red;*/
}

