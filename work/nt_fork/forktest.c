#include <stdlib.h>
#include <stdio.h>

#ifdef WIN32
# include "forknt.h"
#else
# include <sys/types.h>
# include <unistd.h>
#endif

int global = 1;

int main()
{
	int local = 2;

	printf("pre-fork: global=%d local=%d\n", global, local);
	if (fork() == 0) {
		sleep(1);
		global = 101;
		printf("child1: global=%d local=%d\n", global, local);
		sleep(4);
		printf("child2: global=%d local=%d\n", global, local);
		exit(0);
	}
	sleep(1);
	printf("parent1: global=%d local=%d\n", global, local);
	local = 202;
	sleep(1);
	printf("parent2: global=%d local=%d\n", global, local);
	sleep(5);
	printf("parent3: global=%d local=%d\n", global, local);
	return 0;
}
