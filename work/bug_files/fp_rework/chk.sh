#!/bin/sh

# REQUIRES CANONICAL FILE FORMATTING

# Prints if a subc aux x/y doesn't point in the right direction

awk '

function chk()
{
	if (role == "x") {
		if (x1 == x2)
			print "ERROR role x", x1, x2, FILENAME
	}
	else if (role == "y") {
		if (y1 == y2)
			print "ERROR role y", y1, y2, FILENAME
	}
}

function reset()
{
	x1 = 0
	y1 = 0
	x2 = 0
	y2 = 0
	role = ""

}

/ha:line/ { chk(); reset(); }

/x1 =/ { x1 = $3 }
/x2 =/ { x2 = $3 }
/y1 =/ { y1 = $3 }
/y2 =/ { y2 = $3 }
/subc-role =/ { role = $3 }

ENDFILE { chk(); reset(); }

' "$@"
