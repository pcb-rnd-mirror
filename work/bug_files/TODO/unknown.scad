// Round cap line
module pcb_line_rc(x1, y1, length, angle, width, thick) {
	translate([x1,y1,0]) {
		rotate([0,0,angle]) {
			translate([length/2, 0, 0])
				cube([length, width, thick], center=true);
			cylinder(r=width/2, h=thick, center=true, $fn=30);
			translate([length, 0, 0])
				cylinder(r=width/2, h=thick, center=true, $fn=30);
		}
	}
}
// Square cap line
module pcb_line_sc(x1, y1, length, angle, width, thick) {
	translate([x1,y1,0]) {
		rotate([0,0,angle]) {
			translate([length/2, 0, 0])
				cube([length + width, width, thick], center=true);
		}
	}
}
// filled rectangle
module pcb_fill_rect(x1, y1, x2, y2, angle, thick) {
	translate([(x1+x2)/2,(y1+y2)/2,0])
		rotate([0,0,angle])
			cube([x2-x1, y2-y1, thick], center=true);
}
// filled polygon
module pcb_fill_poly(coords, thick) {
	linear_extrude(height=thick)
		polygon(coords);
}
// filled circle
module pcb_fcirc(x1, y1, radius, thick) {
	translate([x1,y1,0])
		cylinder(r=radius, h=thick, center=true, $fn=30);
}
module pcb_outline() {
	// board outer contour
	polygon([
		[0.0000,127.0000],[127.0000,127.0000],[127.0000,0.0000],[0.0000,0.0000]
	]);
}
module pcb_cutouts() {
	// board inner cutouts (non-padstacks)
}
module pcb_layer_bottom_silk_pos_1() {
	color([0,0,0])
		translate([0,0,-0.833000]) {
		}
}

module pcb_layer_group_bottom_silk() {
	pcb_layer_bottom_silk_pos_1();
}

module pcb_layer_bottom_copper_pos_2() {
	color([1,0.4,0.2])
		translate([0,0,-0.811000]) {
		}
}

module pcb_layer_group_bottom_copper() {
	pcb_layer_bottom_copper_pos_2();
}

module pcb_layer_top_copper_pos_3() {
	color([1,0.4,0.2])
		translate([0,0,0.811000]) {
		}
}

module pcb_layer_top_copper_pos_4() {
	color([1,0.4,0.2])
		translate([0,0,0.811000]) {
		}
}

module pcb_layer_group_top_copper() {
	pcb_layer_top_copper_pos_3();
	pcb_layer_top_copper_pos_4();
}

module pcb_layer_top_mask_pos_5() {
	color([0,0.7,0,0.5])
		translate([0,0,0.822000]) {
			pcb_fill_rect(0.0000, 0.0000, 127.0000, 127.0000, 0.000000, 0.010000);
		}
}

module pcb_layer_top_mask_neg_6() {
	color([0,0.7,0,0.5])
		translate([0,0,0.812000]) {
		}
}

module pcb_layer_top_mask_pos_7() {
	color([0,0.7,0,0.5])
		translate([0,0,0.822000]) {
		}
}

module pcb_layer_group_top_mask() {
	union() {
	difference() {
	pcb_layer_top_mask_pos_5();
	pcb_layer_top_mask_neg_6();
}
	pcb_layer_top_mask_pos_7();
}
}

module pcb_layer_bottom_mask_pos_8() {
	color([0,0.7,0,0.5])
		translate([0,0,-0.822000]) {
			pcb_fill_rect(0.0000, 0.0000, 127.0000, 127.0000, 0.000000, 0.010000);
		}
}

module pcb_layer_bottom_mask_neg_9() {
	color([0,0.7,0,0.5])
		translate([0,0,-0.832000]) {
		}
}

module pcb_layer_bottom_mask_pos_10() {
	color([0,0.7,0,0.5])
		translate([0,0,-0.822000]) {
		}
}

module pcb_layer_group_bottom_mask() {
	union() {
	difference() {
	pcb_layer_bottom_mask_pos_8();
	pcb_layer_bottom_mask_neg_9();
}
	pcb_layer_bottom_mask_pos_10();
}
}

module pcb_layer_top_silk_pos_11() {
	color([0,0,0])
		translate([0,0,0.833000]) {
		}
}

module pcb_layer_top_silk_pos_12() {
	color([0,0,0])
		translate([0,0,0.833000]) {
		}
}

module pcb_layer_group_top_silk() {
	pcb_layer_top_silk_pos_11();
	pcb_layer_top_silk_pos_12();
}

module pcb_drill() {
}
module pcb_board_main() {
	translate ([0, 0, -0.800000])
		linear_extrude(height=1.600000)
			pcb_outline();
	pcb_layer_group_bottom_silk();
	pcb_layer_group_bottom_copper();
	pcb_layer_group_top_copper();
	pcb_layer_group_top_mask();
	pcb_layer_group_bottom_mask();
	pcb_layer_group_top_silk();
}

module pcb_board() {
		intersection() {
			translate ([0, 0, -4])
				linear_extrude(height=8)
					pcb_outline();
			union() {
				difference() {
					pcb_board_main();
					pcb_drill();
					translate([0,0,-4])
						linear_extrude(height=8)
							pcb_cutouts();
				}
			}
		}
	}

pcb_board();
