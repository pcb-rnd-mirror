
The files in this directory are from my work at Eaton Peabody Lab @ Mass. Eye
and Ear. They were developed on an NIH core engineering grant. They are GPL2
licensed for compatibility with the projects license. 

They are for testing the mainline exporter output against the export plugins
of pcb-rnd. I will be adding more example output in time along with a more
detailed write up of what the file names mean.

The mainline's current release 20140316 will crash if you attempt to export 
this layout to post script. I got around this by using the mainline's git 
head (Wed Aug 31 2016) which has a pactch from November 2015 that fixed this
bug. 

-Evan Foss 2016.08.27

I found what I consider to be a bug in the mainline. They might call it a 
feature. Using png export and leaving a part of the layout selected will 
highlight that part in the resulting png file even when it is *not* in 
"as-shown" mode. I feel it should be exclusive to "as-shown" mode. Ask 
them to "fix" it later. We had a high confrontation point day already.

-Evan Foss 2016.09.01

