
## SETUP AFL(-CLANG-FAST) ##

    $ sudo apt-get install clang llvm tmux
    $ curl -O http://lcamtuf.coredump.cx/afl/releases/afl-latest.tgz
    $ tar xvzf afl-latest.tgz
    $ cd afl-*
    # if on non-x86: export AFL_NO_X86=1
    $ make
    $ cd llvm_mode
    $ LLVM_CONFIG="llvm-config-3.8" CC="clang-3.8" CXX="clang++3.8" make
    $ cd ..
    $ sudo make install

if everything went well you should have the afl clang/gcc wrappers in $PATH.
the clang-fast wrappers have the advantage that they are implemented as llvm
plugins and do not mess with the generated assembly (of afl-gcc). thus this is
what we want.

    $ afl-clang-fast --version
    afl-clang-fast 2.52b by <lszekeres@google.com>
    clang version 3.8.0-2ubuntu4 (tags/RELEASE_380/final)
    Target: x86_64-pc-linux-gnu
    Thread model: posix
    InstalledDir: /usr/bin

### BUILD PCB-RND WITH AFL-CLANG-FAST ###

make a clean checkout to not mess with already installed pcb-rnd

    $ svn co svn://repo.hu/pcb-rnd/trunk pcb-rnd

    # only needed if more than one clang version is installed
    # must match the llvm-config-X.X version from above
    $ export AFL_CC="clang-3.8"
    $ export AFL_CXX="clang++-3.8"

    $ make clean
    $ CC="afl-clang-fast" CXX="afl-clang-fast++" ./configure --debug --all=buildin --prefix=/opt/pcb-rnd
    $ make CC="afl-clang-fast" CXX="afl-clang-fast++"

    # check that pcb-rnd is working
    $ src/pcb-rnd --version
    ...

note: `make` might fail being unable to compile `puplug`. if that happens to you
contact the mailing list or visit the irc channel

### RUNNING AFL ###

    $ svn co svn://repo.hu/pcb-rnd/work pcb-rnd-work
    $ cd pcb-rnd-work/bug_files/afl
    # prepare seed files in tests
    $ mkdir tests
    # e.g. cp [pcb-rnd-checkout]/src/default4.lht tests
    $ tmux
    $ afl-fuzz -t 5000 -m none -i tests -o findings -- path/to/pcb-rnd --gui batch @@
    ... let run forever ...
    ... this is a single node instance of afl, for parallel fuzzing see afl doc ...

you can tweak the flags to afl-fuzz to better match your system. the `-t`
parameter is for adjusting the timeout; `-m` is a memory limit that afl
enforces. the above `afl-fuzz` command starts a single afl instance. parallel
mode is a bit more involved, consult afl doc for possible options.

afl will complain about frequency scaling and power setup for your cpu.
adjust the settings as recommended. (the afl docs describe how you can bypass 
them if you really have to)

### TODO: RAMDISK SETUP ###

### TODO: SEED FILES ###

### TODO: FUZZING WITH ASAN ###

this is untested. there are some caveats with alf and the address sanitizer.
more to be found in the afl docs. (in short: do this only on 32bit plattforms)

    $ export AFL_CC="clang-3.8"
    $ export AFL_CXX="clang++-3.8"

    $ make clean
    $ CC="afl-clang-fast" CXX="afl-clang-fast++" CFLAGS="-fsanitize=address" \
        ./configure --debug --all=buildin --prefix=/opt/pcb-rnd
    $ make CC="afl-clang-fast" CXX="afl-clang-fast++" CFLAGS="-fsanitize=address"

### INTERPRETING THE RESULTS ####

TL;DR

## CONTRIBUTING ##

amazon and google offer free cloud computing to everyone. you can run 1 virtual
machine for free. if you have some time left and want to contribute making
pcb-rnd more stable run just one instance of afl forever sending any crash
that might occur to the mailing list or contact the community via irc.

of course, any other means of getting more fuzzing instances running are much
appreciated.

thanks for you help!

## RELATED ##

[0]: http://lcamtuf.coredump.cx/afl/
[1]: http://research.aurainfosec.io/hunting-for-bugs-101/
[2]: https://int21.de/slides/troopers2017-fuzzing/
