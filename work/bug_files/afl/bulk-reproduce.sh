#!/bin/bash

DIR="$1"

reproduce(){
    local f="$1"
    local crash=$(./gdb.sh $f 2>&1)
    echo -n "file=$f "
    case "$crash" in
        *SIGSEGV*)
            echo signal=segv
        ;;
        *SIGABRT*)
            echo signal=abrt
        ;;
        *)
            echo signal=unknown
        ;;
    esac
}

for c in $DIR/*sig:11*; do reproduce $c; done