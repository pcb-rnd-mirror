#!/bin/bash

# simple gdb wrapper for pcb-rnd

set -e
#set -x

GDB=$(which gdb)
CMD=$(which pcb-rnd)
XS="--gui batch $1"

shift

$GDB --batch -q --ex "r $XS" --ex 'bt' --ex 'disass $pc, $pc+16' --ex 'info reg' --ex 'quit' "$CMD" 0</dev/null
