#!/bin/bash

DIR="$1"
OUT=out

if [ "$1" = "" ]; then
    exit
fi

process(){
    local f="$1"
    local crash=$(./gdb.sh $f 2>&1)
    echo -n "file=$f "
    case "$crash" in
        *SIGSEGV*)
            echo signal=segv
            local hash=$(sha256sum $f | cut -d ' ' -f 1)
            echo "    => $OUT/segv-$hash"
            mv $f $OUT/segv-$hash
        ;;
        *SIGABRT*)
            echo signal=abrt
            local hash=$(sha256sum $f | cut -d ' ' -f 1)
            echo "    => $OUT/abrt-$hash"
            cp $f $OUT/abrt-$hash
        ;;
        *)
            echo signal=unknown
        ;;
    esac
}

mkdir -p $OUT
echo "output directory is '$OUT'"
for c in $DIR/*; do process $c; done
