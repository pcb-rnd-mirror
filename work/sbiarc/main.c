#include <stdio.h>
#include <math.h>
#include "sbiarc.h"

static void draw_begin(void)
{
printf("<?xml version='1.0'?>\n");
printf("<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.0'>\n");

/*width="2032.0000" height="2032.0000" viewBox="-2.0000 -2.0000 132.0000 132.0000"*/
}

static void draw_end(void)
{
	printf("</svg>\n");
}

static void draw_line(double x1, double y1, double x2, double y2)
{
	printf("	<line x1='%f' y1='%f' x2='%f' y2='%f' style='stroke:rgb(0,0,0);stroke-width:0.02' />\n", x1, y1, x2, y2);
}

static void draw_sba(sba_t *s)
{
	double a, astep, r, rstep, steps;

	printf("	<circle cx='%f' cy='%f' r='%f' stroke='none' fill='#ff9999'/>\n", s->cx[0], s->cy[0], s->r[0]);
	printf("	<circle cx='%f' cy='%f' r='%f' stroke='none' fill='#99ff99'/>\n", s->cx[1], s->cy[1], s->r[1]);


	astep = (s->start < s->ipa[0]) ? 0.1 : -0.1;

	steps = (s->ipa[0] - s->start) / astep;
	rstep = ((s->r[0] + s->r[1]) / 2 - s->r[0]) / steps;
fprintf(stderr, "astep=%f start=%f ipa=%f rstep=%f (steps=%f)\n", astep, s->start, s->ipa[0], rstep, steps);
	for(a = s->start, r = s->r[0]; astep > 0 ? (a < s->ipa[0]) : (a > s->ipa[0]); a += astep, r += rstep) {
		double x, y;
		x = s->cx[0] + cos(a) * r;
		y = s->cy[0] + sin(a) * r;
		draw_line(s->cx[0], s->cy[0], x, y);
		if (a > 2*M_PI)
			a -= 2*M_PI;
		if (a < -2*M_PI)
			a += 2*M_PI;
fprintf(stderr, "a=%f\n", a);
	}
}

int main()
{
	sba_t s;
	s.cx[0] = 10; s.cy[0] = 1; s.r[0] = 5;
	s.cx[1] = 40; s.cy[1] = 5; s.r[1] = 9;
	s.start = M_PI/2; s.delta = -M_PI * 2;
	s.style = SBA_INFLEX;

	sba_update(&s);

	draw_begin();
	draw_sba(&s);
	draw_end();
}
