#include "sbiarc.h"

int sba_update(sba_t *s)
{
	sba_calc_t vx, vy, vlen; /* vector between centers */
	sba_calc_t vipx, vipy, vipl; /* inflection point on v */
	sba_calc_t ipx, ipy, ipr; /* "inflection" point on the curve, where r is halfway between r[0] and r[1] */

	vx = s->cx[1] - s->cx[0]; vy = s->cy[1] - s->cy[0];
	if ((vx == 0) && (vy == 0)) {
#warning TODO: single arc
		abort();
	}
	vlen = sqrt(vx*vx + vy*vy);
	vx /= vlen; vy /= vlen;

	vipl = s->r[0] / (s->r[0] + s->r[1]);
	vipx = s->cx[0] + vx * vipl; vipy = s->cy[0] + vy * vipl;

	ipr = (s->r[0] + s->r[1])/2;

	if (s->style == SBA_STRAIGHT) {
		ipx = vipx + -vy * ipr; ipy = vipy + vx * ipr;
	}
	else {
		ipx = vipx; ipy = vipy;
	}

	s->ipa[0] = atan2(ipy - s->cy[0], ipx - s->cx[0]);
	s->ipa[1] = atan2(ipy - s->cy[1], ipx - s->cx[1]);

	return 0;
}


