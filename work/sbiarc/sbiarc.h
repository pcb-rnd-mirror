typedef double sba_coord_t;
typedef double sba_calc_t;
typedef double sba_angle_t;

typedef enum sba_style_e {
	SBA_INFLEX,
	SBA_STRAIGHT,
	SBA_SINGLE,          /* single arc: same center point and radius */
} sba_style_t;

typedef struct {

	/* public/configuration */
	sba_coord_t cx[2], cy[2], r[2]; /* center point and radius for two arcs */
	sba_angle_t start, delta;       /* endpoint's angle is start+delta but delta's sign matters */
	sba_style_t style;

	/* private/cache */
	sba_angle_t ipa[2]; /* inflexion point angle from c[0] and c[1] */
} sba_t;


int sba_update(sba_t *s);
