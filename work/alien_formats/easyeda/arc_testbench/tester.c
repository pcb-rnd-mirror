#include <stdio.h>
#include <math.h>
#include "arc_sed.h"

#define DEG2RAD (M_PI/180)

static double ang_normalize(double a)
{
	if ((a < 2*M_PI) || (a > 2*M_PI))
		a = fmod(a, 2*M_PI);
	if (a < 0)
		a += 2*M_PI;
	return a;
}

static int coord_neq(double a, double b)
{
	double diff = a - b;
	return (diff > 0.001) || (diff < -0.001);
}

static int ang_neq(double a, double b)
{
	double diff = a - b;
	if ((diff > 0.001) || (diff < -0.001)) {
		a = ang_normalize(a);
		b = ang_normalize(b);
		diff = a - b;
	}
	return (diff > 0.001) || (diff < -0.001);
}

int main(int argc, char *argv[])
{
	double cx, cy, r, sad, dad;
	int res = 0;

	/* centerx, centery, radius, start-degree, delta-degree */
	while(fscanf(stdin, "%lf %lf %lf %lf %lf\n", &cx, &cy, &r, &sad, &dad) == 5) {
		double sx, sy, ex, ey, isa, ida, iea, ocx, ocy, or, osrad, oerad;
		int res;

		/* input anlges in rad */
		isa = sad * DEG2RAD;
		ida = dad * DEG2RAD;
		iea = isa + ida;

		sx = cx + cos(isa) * r;
		sy = cy + sin(isa) * r;
		ex = cx + cos(iea) * r;
		ey = cy + sin(iea) * r;

		res = arc_start_end_delta(sx, sy, ex, ey, dad, &ocx, &ocy, &or, &osrad, &oerad);

		printf("ARC %f %f %f %f %f\n", cx, cy, r, sad, dad);
		printf(" res=%d\n center: %.3f;%.3f -> %.3f;%.3f rad: %.3f -> %.3f\n angs: %.5f;%.5f ->  %.5f;%.5f\n", res, cx, cy, ocx, ocy, r, or, isa, iea, osrad, oerad);
		if (res != 0)
			res |= printf(" -> BAD result\n");
		else if (coord_neq(cx, ocx) || coord_neq(cy, ocy) || coord_neq(r, or))
			res |= printf(" -> BAD output coords\n");
		else if (ang_neq(isa, osrad) || ang_neq(iea, oerad))
			res |= printf(" -> BAD angles\n");
		else
			printf(" -> OK\n");
	}

	return res;
}
