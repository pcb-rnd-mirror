#!/bin/sh

awk '
	BEGIN {
		angs = "90 180 270 -90 -180 -270"
		nangs = split(angs, ANGS, " ")
		srand()
		for(n = 0; n < 1000; n++) {
			if (rand() > 0.7) {
				a = int(rand()*nangs)+1
				delta = ANGS[a]
			}
			else
				delta = rand()*719-358
			print rand()*1000, rand()*1000, rand()*500+1, rand()*720-360, delta
		}
	}
' | ./tester

if test $? = 0
then
	echo ""
	echo " *** QC PASS ***"
	exit 0
else
	echo ""
	echo " *** FAIL ***"
	exit 1
fi