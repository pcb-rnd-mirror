/* librnd */
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

typedef struct rnd_hidlib_s {
	int dummy;
} rnd_hidlib_t;


#define RND_MSG_ERROR 0
static inline void rnd_message(int level, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

static inline int rnd_strcasecmp(const char *a, const char *b) { return strcasecmp(a, b); }

#define DO_PRAGMA(arg) _Pragma(#arg)
#define TODO(x) DO_PRAGMA(message("TODO: " #x))

/* pcb-rnd */
typedef struct pcb_plug_io_s {
	int dummy;
} pcb_plug_io_t;

typedef enum pcb_plug_iot_e {
	PCB_PLUG_IOT_DUMMY=1
} pcb_plug_iot_t;
