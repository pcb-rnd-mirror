/* pcbdoc_ascii emulation */

#include <librnd/core/config.h>

typedef struct altium_block_s {
	int link;                     /* in tree blocks */
	long size;                    /* allocated size of raw */
	char raw[1];                  /* bytes read from the file */
} altium_block_t;

typedef struct altium_field_s {
	int type; /* derived from ->key */
	const char *key;
	enum { ALTIUM_FT_STR, ALTIUM_FT_CRD, ALTIUM_FT_DBL, ALTIUM_FT_LNG } val_type;
	union {
		const char *str;
		rnd_coord_t crd;
		double dbl;
		long lng;                  /* also used for bool */
	} val;
} altium_field_t;

typedef struct altium_record_s {
	int type; /* derived from ->key */
	const char *type_s;
} altium_record_t;

typedef struct altium_tree_s {
	int dummy;
} altium_tree_t;


#define gdl_append(l, a, b)
#define RND_MIL_TO_COORD(mil) (mil)

static inline altium_record_t *pcbdoc_ascii_new_rec(altium_tree_t *tree, const char *type_s, int kw)
{
	printf("REC: %s/%d\n", type_s, kw);
	return 0;
}

static inline int pcbdoc_ascii_parse_fields(altium_tree_t *tree, altium_record_t *rec, const char *fn, long line, char **fields)
{
	printf(" ascii: %s", *fields);
	(*fields) += strlen(*fields);
	return 0;
}

static inline altium_field_t *pcbdoc_ascii_new_field(altium_tree_t *tree, altium_record_t *rec, const char *key, int kw, const char *val)
{
	static altium_field_t dummy;
	return &dummy;
}

