#include <stdlib.h>
#include <stdio.h>
#include "config.h"
#include "libucdf/ucdf.h"
#include "pcbdoc_ascii.h"
#include "pcbdoc_bin.h"

#undef fopen

int pcbdoc_bin_parse_tracks6(rnd_hidlib_t *hidlib, altium_tree_t *tree, ucdf_file_t *fp, altium_buf_t *tmp);

int main(int argc, char *argv[])
{
	const char *fn = "Data.dat";
	FILE *f;
	int res;
	altium_tree_t tree = {0};
	rnd_hidlib_t hidlib = {0};
	altium_buf_t tmp = {0};
	
	f = fopen(fn, "rb");
	if (f == NULL) {
		fprintf(stderr, "Can't open '%s' for read\n", fn);
		exit(1);
	}

	res = pcbdoc_bin_parse_tracks6(&hidlib, &tree, f, &tmp);

	printf("res=%d @ %ld\n", res, ftell(f));

	fclose(f);

	return res;
}
