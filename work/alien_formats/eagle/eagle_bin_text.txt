Eagle binary version 4 and 5 seem to append a text block immediately
after the board layout (.brd). Same applies to library files (.lbr).

This seems to be a hack to get around the 24 byte block size in v3
which limits text fields to at most 5 chars or so.

In v4 and v5 .brd layout files, there is also DRC block after the text block.

In v4, the text block does not appear to be part of the tree.

good examples of v4 binary are:

rs232.brd
and
tvbgone3.brd

In v5, afaict, the text block may be treated as a node of the binary tree

A simple example of indirect text fields using strings in appended text block,
taken from the single element library file FTSH-105-XX-XX-D.lbr in
work/alien_formats/eagle/OSHW_binary_examples

the following text fields in the file can be labels for elements or
library/device/symbol descriptions

at 001176 + 16
7F 58 C2 EC 0C 00

at 001200 + 16
7F 78 CA EC 0C 00

a pair at 01224 + 13
7F A0 09 27 0D  7F 18 8A EC 0C 00

at 001320 + 16
7F 18 8B EC 0C 00

at 001344 +16
7F 88 CB EC 0C 00

at 001752 + 16:
7F 58 9B EC 0C 00

another pair at 001776 + 13:
7F 80 F1 F1 11 7F C8 C2 07 12 00

then text block starts at:
002328 + 0
begins with block type and block size numbers:
13 12 99 19 F6 00 00 00
followed by the following '\0\ delimited strings:
FTSH-105-XX-XX-D   (~17 long)
FTSH-105-XX-XX-D   (~17 long)
FTSH-105-XX-XX-D   (~17 long)
0.050"X0.050" CL THROUGH HOLE TERMINAL STRIP ASSEMBLY,10 PINS. (~61 long)
FTSH-105-XX-XX-D   (~17 long)
FTSH-105-XX-XX-D   (~17 long)
FTSH-105-XX-XX-D   (~17 long)
FTSH-105-XX-XX-D   (~17 long)
0.050"X0.050" CL THROUGH HOLE TERMINAL STRIP ASSEMBLY, 10PINS.  (~61 long)

each string appears to be used in order of their reference in the code
blocks defining the binary tree.


to use these text block strings with only one read of the file:

one option is:
post processing of the binary tree nodes in read-from-file order would
allow the n-th node with a 127 reference(s) to retrieve and use the n-th
+/- (n+1)th string from the text block
this may require "next" pointers in the nodes to allow a read-from-file
order traversal

another option is:
have the parser identify and number the 1...m  127 references during
load, so that post processing can traverse the tree in any order and
retrieve the m-th string and add it to the relevant node

there may be a better way to do it. The file could be read first to
extract the DRCand text block, if present, and then read again for the
design/library tree, but this may be more inefficient.

Importantly, 
one 24 byte binary block may reference up to two consecutive strings
from the text block, making the mapping from individual strings to
nodes non injective.


with respect to the text block possibly forming part of the tree:

the following

        pcb_trace("E: unknown block ID 0x%02x%02x at offset %ld\n",
block[0], block[1], ftell(f));

at line 1312 or so in eagle_bin.c

generates the following error

./pcb-rnd ../../work/alien_formats/eagle/OSHW_binary_examples/BusPiratev3.5c-SOIC.brd
blocks remaining prior to function call = -1
E: unknown block ID 0x1312 at offset 805920
Problem with remaining blocks... is this a library file?
FAILED TO LOAD: -1

which is what leads me to suspect that v5 boards make the text block
part of the tree, whereas in v4 they were just an addendum.

alternatively, we have mucked up the block/node counts somehow and expect a node when there is not one.

if we have interpreted the node counts properly, the binary tree parser now needs to cope with a text block that is not likely to be 24 bytes like all the other nodes it knows about.

Where to process the text strings:

near the bottom of eagle_bin.c, just after the layout tree has been read, there is a prototype
routine read_notes(ctx, f, fn, &eagle_bin_ctx) that reads in the preamble bytes, reads the text block
length bytes, then readss until the end of the text block, but does nothing with it currently,
merely looking to get to the end of it so that the next routine can find and read the DRC block
 
        /* could test if < v4 as v3.xx seems to have no DRC or Netclass or Free Text end blocks */
        read_notes(ctx, f, fn, &eagle_bin_ctx);

at a minimum the individual '\0' delimited text block strings will need to be read in for use/mapping
somewhere around here 


