#!/bin/sh

# check if a poly dump contains line segments ending at the beginning of
# the next segment, forming a closed loop

awk '
/^-------------- Begin Polygon --------------/ {
	getline first
	next
}

(first != "") {
	curr=$0
	getline second
	if (second ~ "^----") {
		if ($0 != first)
			print "Error in close at", NR, $0, first
		first=""
	}
	else if ($0 != second)
		print "Error in pairing at", NR, $0, second
}


'