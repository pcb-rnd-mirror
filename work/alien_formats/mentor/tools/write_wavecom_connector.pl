#!/usr/bin/perl -w
use Time::localtime;
use Math::Trig;

# Parse and check command line parameters
if ($#ARGV != 1) {
    print "Usage:\n";
    print "  write_cell <name> <output_file>\n";
    exit;
}

$CELL_NAME = $ARGV[0];
$file_out = $ARGV[1];

open(OUT_FILE, ">$file_out")|| die("Can't open output file $file_out: $!");


print OUT_FILE ".FILETYPE CELL_LIBRARY\n";
print OUT_FILE ".VERSION \"02.50\"\n";
print OUT_FILE ".UNITS MM\n";
print OUT_FILE ".PACKAGE_CELL \"$CELL_NAME\"\n";
print OUT_FILE " ..PACKAGE_GROUP GENERAL\n";
print OUT_FILE " ..MOUNT_TYPE SURFACE\n";
print OUT_FILE " ..NUMBER_LAYERS 2\n";
print OUT_FILE " ..ROUTE_GRID 0.1\n";
print OUT_FILE " ..VIA_GRID 0.1\n";
print OUT_FILE " ..PRIMARY_PART_GRID 0.1\n";
print OUT_FILE " ..SECONDARY_PART_GRID 0.1\n";

for ($i=2;$i<=60;$i+=2) {
			$name=$i;
			print OUT_FILE " ..PIN \"$name\"\n";
			print OUT_FILE "  ...XY (".(-29*0.5*0.5+($i-2)*0.25).",-2.5)\n";
			print OUT_FILE "  ...NETNAME \"Net-$name\"\n";
			print OUT_FILE "  ...PADSTACK \"Pad Oblong 0.3x3 MM,\"\n";
			print OUT_FILE "  ...ROTATION 90\n";
			print OUT_FILE "  ...PIN_OPTIONS NONE\n";
}

for ($i=1;$i<=59;$i+=2) {
			$name=$i;
			print OUT_FILE " ..PIN \"$name\"\n";
			print OUT_FILE "  ...XY (".(-29*0.5*0.5+($i-1)*0.25).",2.5)\n";
			print OUT_FILE "  ...NETNAME \"Net-$name\"\n";
			if ($i == 1) {
				print OUT_FILE "  ...PADSTACK \"Pad Rectangle 0.3x3 MM,\"\n";
			}
			else {
				print OUT_FILE "  ...PADSTACK \"Pad Oblong 0.3x3 MM,\"\n";
			}
			print OUT_FILE "  ...ROTATION 90\n";
			print OUT_FILE "  ...PIN_OPTIONS NONE\n";
}
$i=61;
for ($x=-1; $x <=1; $x+=2) {
	for ($y=-1; $y <=1; $y+=2) {
			$name=$i;
			print OUT_FILE " ..PIN \"$name\"\n";
			print OUT_FILE "  ...XY (".($x*(0.5*((30-1)+1.5)+1.25)/2).",".($y*3.4/2).")\n";
			print OUT_FILE "  ...NETNAME \"Net-$name\"\n";
			print OUT_FILE "  ...PADSTACK \"Pad Rectangle 1.25x1 MM,\"\n";
			print OUT_FILE "  ...ROTATION 90\n";
			print OUT_FILE "  ...PIN_OPTIONS NONE\n";
			$i++;	
	}
}

close(OUT_FILE);