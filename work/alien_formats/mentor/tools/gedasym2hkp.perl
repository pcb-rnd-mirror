#!/usr/bin/perl -w
use Time::localtime;
use Math::Trig;

# define the version supported;
$GEDA_FILEFORMAT_VERSION_SUPPORTED=1;

# possible status values
$VERSION_NOT_FOUND=1; # The parser didn't find the version of the file.
$PARSING=2;           # It is parsing pins (toplevel structure)
$PARSING_PARAM=3;     # It is parsing parameters belonging to an object.

# Some colors for HKP output.
$graphics_color = 3;
$text_color = 6;
$default_graphics_width = 0.012;

$include_this_text=1;
# All pins have its number in HKP format (begginning at 1).
$pin_count=1;
# Guess if parsing parameters of a pin
$this_pin=0;

# Store all lines in arrays to be able to postprocess.
@GFX=();
@PINS=();
@TEXTS=();


# Parse and check command line parameters
if ($#ARGV != 1) {
    print "Usage:\n";
    print "  gedasym2hkp <input_file> <output_file>\n";
    exit;
}
$file_in = $ARGV[0];
$file_out = $ARGV[1];

$status = $VERSION_NOT_FOUND;
open(HKP_FILE, $file_in)|| die("Can't open input file $file_in: $!");

while ( <HKP_FILE> )
{
	#remove newline character
	$line=$_;
	chop($line);
	if ($line =~ /^v /) {
	    #print "Found version: $line\n";
	    @elements = split(/\s+/,$line);
	    if ($elements[2] == $GEDA_FILEFORMAT_VERSION_SUPPORTED) {
		$status=$PARSING;
		#print "Status: Parsing file\n";
	    }
	}
	if ( ($status == $PARSING) || ($status == $PARSING_PARAM) ) {
	    if ($line =~ /^\{/) {
		#print "Begin of properties\n";
		$status = $PARSING_PARAM;
	    } 
	    elsif ($line =~ /^L /) {
		# Line
		($command, $x1, $y1, $x2, $y2, $color, $width, $capstyle, 
		 $dashstyle, $dashlength, $dashspace) = split(/\s+/, $line);
		if ($width == 0) {
		    $width = $default_graphics_width;
		}
		push (@GFX, ("*GFX_LINE $dashstyle $width"."in $graphics_color 0 ".
			     "<$x1,$y1> <$x2,$y2>\n"));
		if ($status != $PARSING_PARAM) {
		    $this_pin = 0;
		}
	    }
	    elsif ($line =~ /^B /) {
		# A Box has to be converted to a polyline
		($command, $x, $y, $box_width, $box_height, $color, $width, $capstyle, 
		 $dashstyle, $dashlength, $dashspace, $filltype, $fillwidth, $angle1,
		 $pitch1,$angle2, $pitch2) = split(/\s+/, $line);
		if ($width == 0) {
		    $width = $default_graphics_width;
		}
		if ($filltype > 0) {
		    $filltype = 1;
		}
		push (@GFX, ("*GFX_LINE $dashstyle $width"."in $graphics_color $filltype".
		    " <$x,$y> <$x," . ($y+$box_height) . "> <".
		    ($x+$box_width) . "," . ($y+$box_height) . 
		    "> <" . ($x+$box_width) . "," . $y . "> <$x,$y>\n"));		
		if ($status != $PARSING_PARAM) {
		    $this_pin = 0;
		}
	    }
	    elsif ($line =~ /^V /) {
		# Circle
		($command, $center_x, $center_y, $radius, $color, $width, $capstyle, 
		 $dashstyle, $dashlength, $dashspace, $filltype, $fillwidth, $angle1,
		 $pitch1,$angle2, $pitch2) = split(/\s+/, $line);
		if ($width == 0) {
		    $width = $default_graphics_width;
		}
		if ($filltype > 0) {
		    $filltype = 1;
		}
		push(@GFX, ("*GFX_CIRCLE $dashstyle $width"."in $radius $graphics_color ".
		    "$filltype <$center_x,$center_y>\n"));
		if ($status != $PARSING_PARAM) {
		    $this_pin = 0;
		}
	    }
	    elsif ($line =~ /^A /) {
		# Arc
		($command, $center_x, $center_y, $radius, $start_angle, $sweep_angle, $color, 
		 $width, $capstyle, $dashstyle, $dashlength, $dashspace) = split(/\s+/, $line);
		if ($width == 0) {
		    $width = $default_graphics_width;
		}
		push(@GFX, ("*GFX_ARC $dashstyle $width"."in $radius $graphics_color 0 ".
			    "<$center_x,$center_y> "."<".
			    ($center_x+int($radius*cos(deg2rad($start_angle+$sweep_angle)))).
			    ",".($center_y+int($radius*sin(deg2rad($start_angle+$sweep_angle)))).
			    "> <".
			    ($center_x+int($radius*cos(deg2rad($start_angle)))).
			    ",".($center_y+int($radius*sin(deg2rad($start_angle)))).
			    ">\n"));
		if ($status != $PARSING_PARAM) {
		    $this_pin = 0;
		}
	    }
	    elsif ($line =~ /^T /) {
		# Independent text
		($command, $x, $y, $color, $size, $visibility, $show_name_value, 
		 $angle, $alignment, $numlines) = split(/\s+/, $line);
		# Read the lines of text.
		$text="";
		$overridable=0;
		for ($i=$numlines;$i>0;$i--) {
		    if (defined($line=<HKP_FILE>)) {
			chop($line);
			if ($text eq "") {
			    $text = $line;
			}
			else {
			    $text.="\\r".$line;
			}
		    }
		}
		# Change some parameters
		$line_just = ($alignment - ($alignment % 3))/3-1;
		$paragraph_just = ($alignment - ($alignment % 2))/2-1;

		# Aprox text size 8 points is about 0.13 inch height.
		$size = $size*0.123/8;

		# Fix text position (0.115in is too big for geda symbols)
		if ( ($angle == 0) || ($angle == 180) ) {
			$y-=75;
		}
		elsif ( ($angle == 90) || ($angle == 270) ) {
			# Align the text to the middle.
			$paragraph_just = 0;
			$y-=50;
		}

		$angle = ($angle - ($angle % 90)) / 90;

		$name_vis = $value_vis = 0; # By default, the text is invisible
		if ( ( ($show_name_value == 0) || ($show_name_value == 2) ) &&
		     ($visibility > 0) ) {
		    $name_vis = 1;
		}
		if ( ( ($show_name_value == 0) || ($show_name_value == 1) ) &&
		     ($visibility > 0) ) {
		    $value_vis = 1;
		}
		$name_vis = 0; # Never show the text type.
				
		# Guess text type
		$text_type=7;  # By default it's independent text.
		$text_uppercase=uc $text;
		$file_out_uppercase=uc $file_out;
		if ($file_out_uppercase =~ /$text_uppercase\./) {
			# See if it's an independent text with the part-number.
			$text_type = 10;
			$text="";
		}
		if ($numlines==1) {
		    # Pin attributes are one-line only.
		    
		    # Don't include some texts
		    if ( ($text =~ /^pinseq\s*=/) ||
			 ($text =~ /^footprint\s*=/) ||
			 ($text =~ /^device\s*=/) ||
			 ($text =~ /^numslots\s*=/) ||
			 #($text =~ /^refdes\s*=/) ||
			 ($text =~ /^description\s*=/) ||
			 ($text =~ /^slot.*=/) ||
			 ($text =~ /^net\s*=/)
			 )
		    {
			$include_this_text = 0;			
		    }	      
		    if ($text =~ /^refdes\s*=/) {
			$text = substr($text,index($text,'=')+1);
			$text_type = 15;
			$name_vis = 0;			
			$text = "";
			$overridable=1;
		    }
#		    if ($text =~ /^device\s*=/) {
#			$text = substr($text,index($text,'=')+1);
#			$text_type = 10;
#			$name_vis = 0;
#		    }
		    if ($text =~ /^pinnumber\s*=/) {
			$text_type = 4;
			$text = substr($text,index($text,'=')+1);
			# Don't add pin numbers to a symbol. 
			# They are mapped in the Central library.
			$text = ""; 
			
		    }
		    if ($text =~ /^pinlabel\s*=/) {
			$text_type = 3;
			$text = substr($text,index($text,'=')+1);
			$text =~ s/^\s+//; #remove leading spaces
			$text =~ s/\s+$//; #remove trailing spaces
			
		    }
		    if ($text =~ /^pintype\s*=/) {
			$text_type = 4;
			$pin_string=$PINS[$this_pin-1];
			($command, $pin_number, $pintype, $location)=split(/\s+/, $pin_string);
			$text=substr($text,index($text,'=')+1);
			if ($text =~ /\s*in/) {
			    $pintype=1;
			}
			elsif ($text =~ /\s*out/) {
			    $pintype=2;
			}
			elsif ($text =~ /\s*io/) {
			    $pintype=3;
			}
			elsif ($text =~ /\s*oc/) {
			    $pintype=5;
			}
			elsif ($text =~ /\s*oe/) {
			    $pintype=6;
			}
			elsif ($text =~ /\s*pas/) {
			    $pintype=9;
			}
			elsif ($text =~ /\s*tp/) {
			    $pintype=2;
			}
			elsif ($text =~ /\s*tri/) {
			    $pintype=4;
			}
			elsif ($text =~ /\s*clk/) {
			    $pintype=1; # Asserting clock is input pin.
			}
			elsif ($text =~ /\s*pwr/) {
			    $pintype=7; # 7 if it's power, 8 if it's GND. Can guess this?
			}
			# Change the pintype of this pin
			$PINS[$this_pin-1]="$command $pin_number $pintype $location\n";
			$include_this_text = 0;
		    }
		}

		if ($include_this_text == 1) {
		    # The text should not be included if its the type of the pin.
		    push(@TEXTS, ("*TEXT $size"."in $angle $text_type $line_just $paragraph_just ".
				  "$value_vis $this_pin ".
				  "$text_color \"DEFAULT\" $name_vis <$x,$y> $overridable 0 \"".$text."\"\n"));
		    if ($text_type == 10) {
		    	$overridable = 1;
			# If it's the part-number (device= text), then add part-name and part-value attribs.
			# Add the part-name (non-visible, overridable)
			push(@TEXTS, ("*TEXT $size"."in $angle 2 $line_just $paragraph_just ".
				      "0 $this_pin ".
				      "$text_color \"DEFAULT\" 0 <$x,$y> $overridable 0 \"".$text."\"\n"));
			# Add the part-label (non-visible, overridable)
			push(@TEXTS, ("*TEXT $size"."in $angle 183 $line_just $paragraph_just ".
				      "0 $this_pin ".
				      "$text_color \"DEFAULT\" 0 <$x,$y> $overridable 0 \"".$text."\"\n"));
		    }
			
		}
		$include_this_text = 1;
		if ($status != $PARSING_PARAM) {
		    $this_pin = 0;
		}
	    }
	    elsif ( ($line =~ /^P /) && ($status != $PARSING_PARAM) )  {
		# Pin
		($command, $x1, $y1, $x2, $y2, $color, $pintype, $whichend) = split(/\s+/, $line);
		# Guess which is the active point
		if ($whichend == 0) {
		    $x=$x1;
		    $y=$y1;
		}
		else {
		    $x=$x2;
		    $y=$y2;
		}
		# Pintype in geda format is just normal pin or bus pin.
		# Don't know if it's an input, output, or whatever, so it's input by default.
		# This should be carry out later, when found the text with the pintype
		# parameter.
		$pintype++;
		push(@PINS, ("*PIN $pin_count $pintype <$x,$y>\n"));
		# Draw the line of the pin.
		push (@GFX, ("*GFX_LINE 0 $default_graphics_width"."in $graphics_color 0 ".
			     "<$x1,$y1> <$x2,$y2>\n"));

		$this_pin=$pin_count;
		$pin_count++;
	    }		
	}
	if ($status == $PARSING_PARAM) {
	    if ($line =~ /^\}/) {
		#print "End of properties\n";
		$status = $PARSING;
		$this_pin = 0;
	    } 
	}
#	print "$line\n";
}
close(HKP_FILE);


# Check if there is a power pin. If so, then check the name and change the pintype to PWR or GND.
for $pin (@PINS) {
    ($pin_command, $pin_number, $pintype, $pin_location)=split(/\s+/,$pin);
    if ($pintype == 7) {
	# If it's a power pin, find out if it's GND or supply pin.
	for $text_line (@TEXTS) {
	    @line=split(/\s+/, $text_line);
	    $text_pin_number = $line[7];
	    $text_value = $line[14];
	    if ( ($text_pin_number == $pin_number) && # If it's associated with that pin
		 ($text_type == 3)) {                 # and it's the pinlabel.
		if ( ($text_value =~ /^"[Gg][Nn][Dd].*"/) || # If its name is GND* or VSS*
		     ($text_value =~ /^"[Vv][Ss][Ss].*"/)) {
		    $PINS[$pin_number-1]="$pin_command $pin_number 8 $pin_location\n";
		}
	    }
	}
    }
}


# HKP format doesn't allow "strange" characters, so translate them.
for ($i=0;$i<$#TEXTS+1;$i++) {
    @line1=split(/\s+/, $TEXTS[$i]);
    $text_type1=$line1[3];

    if ($text_type1 == 3) {
	# Get the name of the pin
	$_=$line1[14];
	s/\"//g; # Quit all double quotes.
	$pin_name1=$_;
	
	# Replace strange characters.
	#$_=$pin_name1;
	#tr/A-Za-z0-9/_/c;
	#$line1[14]="\"".$_."\"";

	# Update the name
	$string="";
	for $each (@line1) {
	    $string.=$each." ";
	}
	$string.="\n";
	$TEXTS[$i]=$string;
    }

}

# HKP doesn't allow two pins with the same name, so check and correct this.
# It's case insensitive.
for ($i=0;$i<$#TEXTS+1;$i++) {
    @line1=split(/\s+/, $TEXTS[$i]);
    $text_type1=$line1[3];
    if ($text_type1 == 3) {
	# If it's a pin name
	# Get the name of the pin
	$_=$line1[14];
	s/\"//g; # Quit all double quotes.
	$pin_name1=$_;

	$pin_count=1;
	for ($j=$i+1;$j<$#TEXTS+1;$j++) {
	    # Check only with pin names
	    @line2=split(/\s+/, $TEXTS[$j]);
	    $text_type2=$line2[3];
	    # Get the name of the other pin
	    $_=$line2[14];
	    s/\"//g; # Quit all double quotes.
	    $pin_name2=$_;
	    
	    if ( ($text_type2 == 3) && ($pin_name1 =~ /\s*$pin_name2\s*/i) &&
		 ($pin_name2 =~ /\s*$pin_name1\s*/i) ) {
		# If the name in it's the same, change both, if necessary.
		if ($pin_count == 1) {
		    # Only change the first if it's the first time.
		    $line1[14]="\"$pin_name1"."1\"";
		    $string="";
		    for $each (@line1) {
			$string.=$each." ";
		    }
		    $string.="\n";
		    $TEXTS[$i]=$string;
		}
		$pin_count++;
		$line2[14]="\"$pin_name2$pin_count\"";
		$string="";
		for $each (@line2) {
		    $string.=$each." ";
		}
		$string.="\n";
		$TEXTS[$j]=$string;
	    }
	}
    }   
}

$name = substr($file_in, 0, rindex($file_in,"."));
if ($name =~ /\//) {
    $name = substr($name, rindex($name, "/")+1);
}
if ($name =~ /\\/) {
    $name = substr($name, rindex($name, "\\"));
}

open(OUT_FILE, ">$file_out")|| die("Can't open output file $file_out: $!");
print OUT_FILE "*VERSION 16000000\n";
print OUT_FILE "*UNITS 1000.000000 per_inch\n";
print OUT_FILE "*CELL_OPEN $name 30 ".(localtime()->year+1900)."/".(localtime()->mon+1)."/".localtime->mday.
    "@".localtime()->hour.":".localtime()->min.":".localtime()->sec."\n";
print OUT_FILE @GFX;
print OUT_FILE @PINS;
print OUT_FILE @TEXTS;
print OUT_FILE "*CELL_CLOSE <0,0>\n";
close(OUT_FILE);
