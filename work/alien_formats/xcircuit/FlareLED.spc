*SPICE circuit <FlareLED> from XCircuit v3.8 rev 78

C1 int2 GND 1.0p
C2 int42 GND 1.0p
C3 int11 int29 1.0p
C4 GND int11 1.0p
C5 int16 Vdd 1.0p
R1 int13 int12 1.0k
R2 GND int10 1.0k
R3 int14 int5 1.0k
R4 int10 int4 1.0k
R5 int9 int41 1.0k
R6 int45 int4 1.0k
C6 int13 int31 1.0p

.end
