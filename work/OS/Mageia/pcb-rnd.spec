# Build with --debug (1) or --symbols (0)
%define debug   0
%define plugindir %{_usr}/lib/%{name}/plugins
%define libplugindir %{_usr}/lib/librnd/plugins

%define major   3

# commented out if not svn snapshot
%define svn     36875

%define rel     4

Name:           pcb-rnd
Version:        3.0.5
Release:        %mkrel %{?svn:%rel.%svn}%{?!svn:%rel}
Summary:        Standard installation
License:        GPLv2+ and LGPLv2+ and BSD and MIT
Group:          Sciences/Other
URL:            http://repo.hu/projects/pcb-rnd
Source0:        http://repo.hu/projects/%{name}/releases/%{name}%{?svn:-%{svn}}%{!?svn:-%{version}}.tar.gz
Source1:        mk-tar
Source2:        logo.svg
Source3:        pcb-rnd.desktop
Source4:        mk-spec
Source5:        spec.tpl
Source6:        packaging.README

BuildRequires:  fungw-devel
BuildRequires:  librnd-devel >= 3.1.0
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(gdlib)
BuildRequires:  pkgconfig(glu)
BuildRequires:  pkgconfig(libxml-2.0)

Requires:       pcb-rnd-core = %{version}-%{release}
Recommends:     pcb-rnd-io-standard = %{version}-%{release}
Recommends:     pcb-rnd-io-alien = %{version}-%{release}
Recommends:     pcb-rnd-lib-gui = %{version}-%{release}
Recommends:     %{_lib}rnd3-hid-gtk2-gl >= 3.2.0
Recommends:     %{_lib}rnd3-hid-gtk2-gdk >= 3.2.0
Recommends:     pcb-rnd-export = %{version}-%{release}
Recommends:     pcb-rnd-export-sim = %{version}-%{release}
Recommends:     pcb-rnd-export-extra = %{version}-%{release}
Recommends:     pcb-rnd-auto = %{version}-%{release}
Recommends:     pcb-rnd-extra = %{version}-%{release}
Recommends:     pcb-rnd-cloud = %{version}-%{release}
Recommends:     pcb-rnd-doc = %{version}-%{release}
Recommends:     pcb-rnd-import-net = %{version}-%{release}
Recommends:     %{name}-doc = %{version}-%{release}
Recommends:     geda-gnetlist

Obsoletes:      %{name}-static < 2.2.4-2

%description
pcb-rnd is a highly modular PCB (Printed Circuit Board) layout tool
with a rich set of plugins for communicating with various external
design tools and other EDA/CAD packages.

Feature highlights:
  - subcircuits, pad stacks
  - flexible footprint model; unrestricted pad shapes
  - arbitrary copper, silk, paste and soldermask objects
  - sophisticated, flexible layer model
  - flexible/universal polygon model
  - any object, even polygons, can have a clearance within a polygon
  - advanced mil and mm grid, with support for mixed unit design
  - strong CLI support
  - static footprints and parametric (generated) footprints
  - query language for advanced search & select
  - powerful, user scriptable, modular Design Rule Checker (DRC)
  - layout optimizers such as teardrops and a trace puller
  - footprint library from local file systems, HTTP and board files
  - netlist management: imported and as-built; back annotation

For full details of supported formats etc. please visit:
http://www.repo.hu/projects/pcb-rnd/datasheet.html

%package core
Summary:        Pcb-rnd executable with the core functionality
Requires:       %{_lib}rnd3 >= 3.2.0

%description core
Includes the data model, the most common action commands, the native file
format. Can be used in headless mode or batch/scripted mode for automated
processing or with GUI (if pcb-rnd-lib-gui and librnd GUI HIDs are installed).

%package doc
Summary:        Documentation for pcb-rnd
BuildArch:      noarch

%description doc
User manual (html) and manual pages.

%package auto
Summary:        Autoroute and autoplace
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       %{_lib}rnd3 >= 3.2.0
Requires:       pcb-rnd-lib-io = %{version}-%{release}
Requires:       pcb-rnd-io-standard = %{version}-%{release}

%description auto
Feature plugins for automated component placing and track routing.

%package cloud
Summary:        Networking plugins
Requires:       wget
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       %{_lib}rnd3-cloud >= 3.2.0

%description cloud
'Cloud' footprint access plugin that integrates edakrill and gedasymbols.org.

%package debug
Summary:        Debug and diagnostics
Requires:       pcb-rnd-core = %{version}-%{release}

%description debug
Extra action commands to help in debugging and diagnosing problems and bugs.

%package export-extra
Summary:        Export formats: special/extra
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       pcb-rnd-export = %{version}-%{release}

%description export-extra
Less commonly used export formats: fidocadj, ipc-356-d, stl, old geda
connection list format, direct printing with lpr.

%package export-sim
Summary:        Export plugins to simulators
Requires:       pcb-rnd-core = %{version}-%{release}

%description export-sim
Export the board in formats that can be used for simulation: openems.

%package export
Summary:        Common export plugins
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       %{_lib}rnd3 >= 3.2.0
Requires:       %{_lib}rnd3-pixmap >= 3.2.0

%description export
Export the board in vector graphics (svg, ps, eps), raster graphics (png,
jpeg, etc.), gerber, 3d model in openscad, xy for pick and place, BoM, etc.

%package extra
Summary:        Extra action commands and optional functionality
Requires:       pcb-rnd-core = %{version}-%{release}

%description extra
Align objects in grid, optimize tracks, font editor, combine polygons,
renumber subcircuits, apply vendor drill mapping.

%package import-geo
Summary:        Geometry import
Requires:       pcb-rnd-core = %{version}-%{release}

%description import-geo
Plugins for importing pixmaps, fonts and vector graphics.

%package import-net
Summary:        Netlist/schematics import plugins
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       %{_lib}rnd3 >= 3.2.0

%description import-net
Import netlist and footprint information from edif, ltspice, mentor graphics,
gschem and tinycad.

%package io-alien
Summary:        File format compatibility with other PCB layout designers
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       pcb-rnd-lib-io = %{version}-%{release}
Requires:       pcb-rnd-extra = %{version}-%{release}
Requires:       %{_lib}rnd3 >= 3.2.0

%description io-alien
Load and/or save boards in file formats supported by other EDA tools,
such as KiCAD, Eagle, protel/autotrax, etc.

%package io-standard
Summary:        Commonly used non-native board and footprint file formats
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       pcb-rnd-lib-io = %{version}-%{release}

%description io-standard
Plugins for tEDAx footprint format and the gEDA/PCB file formats (footprint
and board).

%package lib-gui
Summary:        Support library for building the GUI
Requires:       pcb-rnd-core = %{version}-%{release}
Requires:       %{_lib}rnd3-lib-gui >= 3.2.0

%description lib-gui
Provides pcb-rnd specific dialog boxes (e.g. fontsel) and top window GUI
elements. 

%package lib-io
Summary:        Support library for alien file formats
Requires:       pcb-rnd-core = %{version}-%{release}

%description lib-io
Provides generic, low level parsers (such as s-expression parser) for I/O
plugins that need to deal with alien file formats. 

%prep
%autosetup -n %{name}-%{?svn}%{!?svn:%{version}} -p1

%build
./configure \
--libarchdir=%{_lib} \
--all=disable --buildin-fp_fs --buildin-draw_fab --buildin-act_read --buildin-drc_query --buildin-mincut --buildin-ch_onpoint --buildin-report --buildin-rubberband_orig --buildin-exto_std --buildin-fp_board --buildin-propedit --buildin-io_lihata --buildin-autocrop --buildin-lib_polyhelp --buildin-draw_csect --buildin-ddraft --buildin-ch_editpoint --buildin-act_draw --buildin-tool_std --buildin-show_netnames --buildin-query --buildin-lib_compat_help --buildin-shape --buildin-lib_formula --buildin-extedit --plugin-export_excellon --plugin-export_fidocadj --plugin-export_lpr --plugin-export_oldconn --plugin-export_stat --plugin-io_kicad_legacy --plugin-io_eagle --plugin-io_tedax --plugin-import_gnetlist --plugin-io_kicad --plugin-import_mucs --plugin-renumber --plugin-import_calay --plugin-smartdisperse --plugin-draw_fontsel --plugin-polycombine --plugin-export_gcode --plugin-export_bom --plugin-ar_cpcb --plugin-lib_hid_pcbui --plugin-teardrops --plugin-shand_cmd --plugin-io_pads --plugin-import_tinycad --plugin-export_openems --plugin-import_orcad_net --plugin-import_ltspice --plugin-export_dxf --plugin-export_ipcd356 --plugin-import_ttf --plugin-import_mentor_sch --plugin-export_ps --plugin-import_accel_net --plugin-millpath --plugin-djopt --plugin-import_edif --plugin-import_protel_net --plugin-import_sch2 --plugin-diag --plugin-export_stl --plugin-autoplace --plugin-export_svg --plugin-import_net_cmd --plugin-fp_wget --plugin-fontmode --plugin-import_netlist --plugin-polystitch --plugin-import_pads_net --plugin-dialogs --plugin-import_sch_rnd --plugin-io_dsn --plugin-export_xy --plugin-export_png --plugin-import_hpgl --plugin-import_ipcd356 --plugin-lib_netmap --plugin-io_hyp --plugin-cam --plugin-puller --plugin-import_fpcb_nl --plugin-io_pcb --plugin-distalign --plugin-asm --plugin-export_openscad --plugin-jostle --plugin-autoroute --plugin-io_autotrax --plugin-vendordrill --plugin-export_gerber --plugin-io_bxl --plugin-ar_extern --plugin-import_net_action \
%if %{debug} == 1
        prefix=%{_prefix} --debug
%else
        prefix=%{_prefix} --symbols
%endif

make

%install
%make_install

# Install icon
cp %{SOURCE2} .
mkdir -p %{buildroot}%{_iconsdir}/hicolor/scalable/apps
cp logo.svg %{buildroot}%{_iconsdir}/hicolor/scalable/apps/%{name}.svg

# Install Desktop files
mkdir -p %{buildroot}%{_datadir}/applications
cp %{SOURCE3} %{buildroot}%{_datadir}/applications/

%files
# Meta-Package with desktop file
%{_datadir}/applications/%{name}.desktop

%files doc
%doc %{_docdir}/%{name}/

%files core
%{_iconsdir}/hicolor/scalable/apps/pcb-rnd.svg
%{_datadir}/applications/%{name}.desktop
%{_bindir}/pcb-rnd
%{_mandir}/man1/pcb-rnd.1*
%config(noreplace) %{_sysconfdir}/%{name}/conf_core.lht
%config(noreplace) %{_sysconfdir}/%{name}/menu-default.lht
%{_datadir}/pcb-rnd/default2.lht
%{_datadir}/pcb-rnd/default4.lht
%{_datadir}/pcb-rnd/default_font
%{_bindir}/fp2preview
%{_bindir}/fp2subc
%{_bindir}/pcb-prj2lht
%{_mandir}/man1/fp2preview.1*
%{_mandir}/man1/fp2subc.1*
%{_mandir}/man1/pcb-prj2lht.1*
%{_datadir}/pcb-rnd/footprint/
%config(noreplace) %{_sysconfdir}/%{name}/ch_editpoint.conf
%config(noreplace) %{_sysconfdir}/%{name}/drc_query.conf
%config(noreplace) %{_sysconfdir}/%{name}/fp_fs.conf
%config(noreplace) %{_sysconfdir}/%{name}/show_netnames.conf

%files auto
%{plugindir}/ar_cpcb.pup
%{plugindir}/ar_cpcb.so
%{plugindir}/ar_extern.pup
%{plugindir}/ar_extern.so
%{plugindir}/asm.pup
%{plugindir}/asm.so
%{plugindir}/autoplace.pup
%{plugindir}/autoplace.so
%{plugindir}/autoroute.pup
%{plugindir}/autoroute.so
%{plugindir}/import_mucs.pup
%{plugindir}/import_mucs.so
%{plugindir}/smartdisperse.pup
%{plugindir}/smartdisperse.so
%config(noreplace) %{_sysconfdir}/%{name}/ar_extern.conf
%config(noreplace) %{_sysconfdir}/%{name}/asm.conf

%files cloud
%{plugindir}/fp_wget.pup
%{plugindir}/fp_wget.so
%config(noreplace) %{_sysconfdir}/%{name}/fp_wget.conf

%files debug
%{plugindir}/diag.pup
%{plugindir}/diag.so
   
%files export-extra
%{plugindir}/export_fidocadj.pup
%{plugindir}/export_fidocadj.so
%{plugindir}/export_ipcd356.pup
%{plugindir}/export_ipcd356.so
%{plugindir}/export_lpr.pup
%{plugindir}/export_lpr.so
%{plugindir}/export_oldconn.pup
%{plugindir}/export_oldconn.so
%{plugindir}/export_stl.pup
%{plugindir}/export_stl.so

%files export-sim
%{plugindir}/export_openems.pup
%{plugindir}/export_openems.so

%files export
%{plugindir}/cam.pup
%{plugindir}/cam.so
%{plugindir}/export_bom.pup
%{plugindir}/export_bom.so
%{plugindir}/export_dxf.pup
%{plugindir}/export_dxf.so
%{plugindir}/export_excellon.pup
%{plugindir}/export_excellon.so
%{plugindir}/export_gcode.pup
%{plugindir}/export_gcode.so
%{plugindir}/export_gerber.pup
%{plugindir}/export_gerber.so
%{plugindir}/export_openscad.pup
%{plugindir}/export_openscad.so
%{plugindir}/export_png.pup
%{plugindir}/export_png.so
%{plugindir}/export_ps.pup
%{plugindir}/export_ps.so
%{plugindir}/export_stat.pup
%{plugindir}/export_stat.so
%{plugindir}/export_svg.pup
%{plugindir}/export_svg.so
%{plugindir}/export_xy.pup
%{plugindir}/export_xy.so
%{plugindir}/millpath.pup
%{plugindir}/millpath.so
%{_bindir}/pcb-rnd-svg
%{_mandir}/man1/pcb-rnd-svg.1*
%config(noreplace) %{_sysconfdir}/%{name}/cam.conf
%config(noreplace) %{_sysconfdir}/%{name}/export_xy.conf

%files extra
%{plugindir}/distalign.pup
%{plugindir}/distalign.so
%{plugindir}/djopt.pup
%{plugindir}/djopt.so
%{plugindir}/fontmode.pup
%{plugindir}/fontmode.so
%{plugindir}/jostle.pup
%{plugindir}/jostle.so
%{plugindir}/polycombine.pup
%{plugindir}/polycombine.so
%{plugindir}/polystitch.pup
%{plugindir}/polystitch.so
%{plugindir}/puller.pup
%{plugindir}/puller.so
%{plugindir}/renumber.pup
%{plugindir}/renumber.so
%{plugindir}/shand_cmd.pup
%{plugindir}/shand_cmd.so
%{plugindir}/teardrops.pup
%{plugindir}/teardrops.so
%{plugindir}/vendordrill.pup
%{plugindir}/vendordrill.so

%files import-geo
%{plugindir}/import_hpgl.pup
%{plugindir}/import_hpgl.so
%{plugindir}/import_ttf.pup
%{plugindir}/import_ttf.so

%files import-net
%{plugindir}/import_accel_net.pup
%{plugindir}/import_accel_net.so
%{plugindir}/import_calay.pup
%{plugindir}/import_calay.so
%{plugindir}/import_edif.pup
%{plugindir}/import_edif.so
%{plugindir}/import_fpcb_nl.pup
%{plugindir}/import_fpcb_nl.so
%{plugindir}/import_gnetlist.pup
%{plugindir}/import_gnetlist.so
%{plugindir}/import_ipcd356.pup
%{plugindir}/import_ipcd356.so
%{plugindir}/import_ltspice.pup
%{plugindir}/import_ltspice.so
%{plugindir}/import_mentor_sch.pup
%{plugindir}/import_mentor_sch.so
%{plugindir}/import_net_action.pup
%{plugindir}/import_net_action.so
%{plugindir}/import_net_cmd.pup
%{plugindir}/import_net_cmd.so
%{plugindir}/import_netlist.pup
%{plugindir}/import_netlist.so
%{plugindir}/import_orcad_net.pup
%{plugindir}/import_orcad_net.so
%{plugindir}/import_pads_net.pup
%{plugindir}/import_pads_net.so
%{plugindir}/import_protel_net.pup
%{plugindir}/import_protel_net.so
%{plugindir}/import_sch2.pup
%{plugindir}/import_sch2.so
%{plugindir}/import_sch_rnd.pup
%{plugindir}/import_sch_rnd.so
%{plugindir}/import_tinycad.pup
%{plugindir}/import_tinycad.so
%{_bindir}/gsch2pcb-rnd
%{_usr}/lib/pcb-rnd/g*.scm
%{_mandir}/man1/gsch2pcb-rnd.1*
%config(noreplace) %{_sysconfdir}/%{name}/import_gnetlist.conf
%config(noreplace) %{_sysconfdir}/%{name}/import_sch_rnd.conf

%files io-alien
%{plugindir}/io_autotrax.pup
%{plugindir}/io_autotrax.so
%{plugindir}/io_bxl.pup
%{plugindir}/io_bxl.so
%{plugindir}/io_dsn.pup
%{plugindir}/io_dsn.so
%{plugindir}/io_eagle.pup
%{plugindir}/io_eagle.so
%{plugindir}/io_hyp.pup
%{plugindir}/io_hyp.so
%{plugindir}/io_kicad.pup
%{plugindir}/io_kicad.so
%{plugindir}/io_kicad_legacy.pup
%{plugindir}/io_kicad_legacy.so
%{plugindir}/io_pads.pup
%{plugindir}/io_pads.so
%{_mandir}/man1/txt2bxl.1*
%{_mandir}/man1/bxl2txt.1*
%{_bindir}/txt2bxl
%{_bindir}/bxl2txt
%config(noreplace) %{_sysconfdir}/%{name}/io_pads.conf

%files io-standard
%{plugindir}/io_pcb.pup
%{plugindir}/io_pcb.so
%{plugindir}/io_tedax.pup
%{plugindir}/io_tedax.so

%files lib-gui
%{plugindir}/dialogs.pup
%{plugindir}/dialogs.so
%{plugindir}/draw_fontsel.pup
%{plugindir}/draw_fontsel.so
%{plugindir}/lib_hid_pcbui.pup
%{plugindir}/lib_hid_pcbui.so
%config(noreplace) %{_sysconfdir}/%{name}/adialogs.conf

%files lib-io
%{plugindir}/lib_netmap.pup
%{plugindir}/lib_netmap.so

