#!/bin/bash

declare -A SUGGESTS
declare -A DLONG
declare -A ARCH
declare -A CONTROL_BONUS

### config part ###

# debian/ to generate/patch
debian=../debian

# generated packaging auto (should be trunk/doc/developer/packaging/auto
# after running packages.sh)
auto=/tmp/pcb-rnd-deb/trunk/doc/developer/packaging/auto

PREFIX=/usr
PLUGDIR=$PREFIX/lib/pcb-rnd/plugins
CONFDIR=/etc/pcb-rnd
LIBPLUGDIR=$PREFIX/lib/librnd/plugins
LIBCONFDIR=/etc/pcb-rnd

# data not coming from $auto
SUGGESTS["pcb-rnd"]="sch-rnd, camv-rnd"
DLONG["pcb-rnd"]="`cat $auto/../description.txt`"
ARCH["pcb-rnd-doc"]="all"

# Bdale's patches for smooth upgrade:
# 'Replaces:' make sure the modular packaging replaces the old, monolithic
# package without file collision
CONTROL_BONUS["pcb-rnd-import-net"]="Replaces: pcb-rnd (<< 2.2.0-2)
Breaks: pcb-rnd (<< 2.2.0-2)"
CONTROL_BONUS["pcb-rnd-doc"]="Replaces: pcb-rnd (<< 2.2.0-2)
Breaks: pcb-rnd (<< 2.2.0-2)"
CONTROL_BONUS["pcb-rnd-io-standard"]="Replaces: pcb-rnd (<< 2.2.0-2)
Breaks: pcb-rnd (<< 2.2.0-2)"
CONTROL_BONUS["pcb-rnd-core"]="Replaces: pcb-rnd (<< 2.2.0-2)
Breaks: pcb-rnd (<< 2.2.0-2)"

### implementation ###

librnd_min_ver=`cat $auto/librnd_min_ver`

cat_desc()
{
	local fn="$1"
	sed "s/[ \t\r\n][ \t\r\n]*/ /g" < $fn | awk -v w=70 '
	{
		line=$0
		while(line != "") {
			if (length(line) <= w) {
				print line
				break
			}
			for(n = w; n > 0; n--) {
				if (substr(line, n, 1) == " ")
					break;
			}
			if (n < 1) n = w
			pre=substr(line, 1, n-1)
			print pre
			line = substr(line, n+1)
		}
	}
	'
}

# get a list of package deps from auto and insert ${binary:Version}
# for pcb-rnd packages and minimum librnd version for librnd packages
binary_version()
{
#	sed 's/ / (= \${binary:Version}), /g;s/$/ (= \${binary:Version})/'
	echo "---" >> WTF1
	echo "---" >> WTF2
	tr " \t" "\n\n" | tee -a WTF1 | awk -v "librnd_min_ver=$librnd_min_ver" '
		function comma() {
			if (not_first) printf(", ");
			not_first = 1
		}
	/^librnd/   { comma(); printf("%s (>= %s)", $1, librnd_min_ver) }
	/^pcb-rnd/  { comma(); printf("%s (= ${binary:Version})", $1, librnd_min_ver) }
	' | tee -a WTF2
}

gen_control_pkg()
{
	local pkg="$1"
	local suggests="${SUGGESTS[$pkg]}" dshort dlong="${DLONG[$pkg]}"
	local bonus="${CONTROL_BONUS[$pkg]}"
	local deps arch="${ARCH[$pkg]}"

	dshort=`cat $auto/$pkg.short`
	if test -z "$dlong"
	then
		dlong=`cat_desc $auto/$pkg.long`
	fi
	deps="`cat $auto/$pkg.deps`"

	if test ! -z "$deps"
	then
		deps="`echo \"$deps\" | binary_version`"
		deps=", $deps"
	fi
	if test -z "$arch"
	then
		arch="any"
	fi

echo "
Package: $pkg
Architecture: $arch
Depends: \${misc:Depends}, \${shlibs:Depends}$deps"
if test ! -z "$suggests"
then
	echo "Suggests: $suggests"
fi
if test ! -z "$bonus"
then
	echo "$bonus"
fi
echo "Description: $dshort"
echo "$dlong" | sed 's/^$/./;s/^/ /'
}


gen_control()
{
	local p list=`grep -v "^pcb-rnd$"< $auto/List | sort`

	sed "s/@@@librnd_min_ver@@@/$librnd_min_ver/g" < control.in
	gen_control_pkg pcb-rnd
	for p in $list
	do
		gen_control_pkg $p
	done
}

gen_install_pkg()
{
	local pkg="$1"

	sed '
		s@[$]PREFIX@'$PREFIX'@g;
		s@[$]C@'$CONFDIR'@g;
		s@[$]P@'$PLUGDIR'@g;
		s@[$]LC@'$LIBCONFDIR'@g;
		s@[$]LP@'$LIBPLUGDIR'@g;
		s@ @\n@g;
	' < $auto/$pkg.files
}

gen_installs()
{
	local p
	for p in `cat $auto/List`
	do
		gen_install_pkg $p > $debian/$p.install
	done
}

gen_rules()
{
	(
		cat rules.in
		sed 's/^/\t\t/;s/$/ \\/;' < $auto/Configure.args
	) > $debian/rules
}

### main ###

gen_control > $debian/control
gen_installs
gen_rules

for n in `ls patches/*.patch 2>/dev/null`
do
	cat $n | (cd $debian && patch -p0)
done
