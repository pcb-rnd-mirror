- with polybool2:
	+ BUG: Bdale's disappearing poly bugreport, hosted in librnd/work/poly_broken
		+ BUG: pcb-rnd/work/test_poly/bdale4: broken clipping
	+ BUG: pcb-rnd/work/test_poly/bdale3: dicer bug: disappearing clearance: place a signal via right to the rectangular pin with no clarance overlap, rectangular pin's clearance disappears [report: Igor2]
	+ BUG: pcb-rnd/work/test_poly/out-extract*.rf, on which is not autocropped, triggers assert with scrolling, but the autocropped version does not. Polygon vertex coords are well under size limit for 32 bit coords in both cases. out-minimal.rf is a more minimal polygon version of the subc which triggers assert in footprint edit mode with {ze} and scroll bar use. [Report: Erich]
	+ BUG: thermal tool poly assert. pcb-rnd/work/test_poly/thermal-assert.rp and use thermal tool and shift click to start cycling through thermal types on padstack. Assert eventually occurs. [report: Erich]
	+ BUG: pcb-rnd/work/test_poly/intersect.* (thin hair gerber bug) [report: cuvoodoo]
	+ BUG: pcb-rnd/work/test_poly/line-arc-del-assert.rp load layout, delete right most vertical, line, poly validity assert ensues [report: Erich]
	+ BUG: pcb-rnd/work/test_poly/line-arc-del-assert.rp load layout, delete bottom most horizontal line, note strange clipping that shorts across adjacent arc [report: Erich]
	+ BUG: pcb-rnd/work/test_poly/line-arc-del-assert.rp load layout, delete bottom most arc, note strange clipping that across where arc used to be [report: Erich]
	+ BUG: pcb-rnd/work/test_poly/derive.rp, edit pstk proto, add mask shape, derive -> derived polygon is broken (invalid coords) [report: Scott]
	- BUG: pcb-rnd/work/test_poly/phatch.lht: see TODO
	/ BUG: pcb-rnd/work/test_poly/clipping-2154-1676.* see TODO

