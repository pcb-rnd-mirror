	if (plug_is_enabled("toporouter")) {
		put("/local/gts/enable", strue);
		want_glib = 1;
	}
	else
		put("/local/gts/enable", sfalse);

	if (want_glib) {
		require("libs/sul/glib", 0, 0);
		if (!istrue(get("libs/sul/glib/presents"))) {
			if (plug_is_enabled("toporouter")) {
				report_repeat("WARNING: Since GLIB is not found, disabling the toporouter...\n");
				hook_custom_arg("Disable-toporouter", NULL);
			}
		}
	}

	printf("Generating gts/Makefile (%d)\n", generr |= tmpasm("../src_3rd/gts", "Makefile.in", "Makefile"));
