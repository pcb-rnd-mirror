Bug #846789 reported by KaiMartin on 2011-09-11

Bug Description

In current git-head the toporouter struggles indefinitely even for very simple
boards.
To reproduce:

1) prepare a simple schematic: Two connected resistors are enough

2) save as foo.sch

3) pcb foo.pcb

4) import schematic --> The resistors appear in the middle of the canvas

5) execute the command "toporouter()"
pcb claims a whole core indefinitely with no results whatsoever.

---<)kaimartin(>---

Tags: toporouter Edit Tag help
Bert Timmerman (bert-timmerman) on 2011-09-11

Changed in pcb:
status: New ? Confirmed

Andrew Poelstra (asp11) wrote on 2011-09-11: #1

Hitting Ctrl+C in gdb after the lockup, and doing a backtrace, gives nested
calls to swap_if_in_circle() roughly 10 000 deep. The offending line is gts/
cdt.c:506.

Andrew Poelstra (asp11) wrote on 2011-09-11: #2

git bisect says I did it. Offending commit:

commit 97b3260ecf977dcaa959e51fbd39c6b0d7b414b7
Author: Andrew Poelstra <email address hidden>
Date: Fri Aug 5 23:39:37 2011 -0700

    *** CONVERT PCB'S BASE UNITS TO NANOMETERS ***

    Convert base units to nm, change Coord from int to long,
    change LARGE_VALUE from a magic number to (LONG_MAX / 2 - 1).

    Fixes-bug: lp-772027

Bert Timmerman (bert-timmerman) on 2011-11-19

tags: added: toporouter

DJ Delorie (djdelorie) wrote on 2011-11-19: #3

could someone attach a known-to-hang .pcb for this? And instructions about
which layers need to be visible during toporouting?

Traumflug (mah-jump-ing) wrote on 2015-09-27: #4

Marking "Low", because the Toporouter is generally considered to be redundant
(and abandoned).

Changed in geda-project:
importance: Undecided ? Low

