#!/bin/sh

pcblib=../../trunk/pcblib

echo "<html><body><table border=1 cellspacing=0>"
echo "<tr><th>footprint<th>openscad"
for fn in `find $pcblib -name '*.fp' | sort`
do
	dn=`dirname $fn`
	prefix=`basename $dn`
	bn=`basename $fn`
	model=`grep "openscad[^=]*[ \t]*=" < $fn | tr '\n' '|'`
	echo "<tr><th>$prefix/$bn <td>$model"
done
echo "</table></body></html>"

