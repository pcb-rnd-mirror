use <P.scad>

// Round cap line
module pcb_line_rc(x1, y1, length, angle, width, thick) {
	translate([x1,y1,0]) {
union() {

		rotate([0,0,angle]) {
			translate([length/2+2, 0, 0])
				cube([length-8, width, thick], center=true);
//			cylinder(r=width/2, h=thick, center=true, $fn=3);
//			translate([length, 0, 0])
//				cylinder(r=width/2, h=thick, center=true, $fn=3);
		}
	}
	}
}


if (0)
for(y=[1:8000]) {
	pcb_line_rc(1, y, 20, 0, 0.5, 0.1);
	pcb_line_rc(41, y, 20, 0, 0.5, 0.1);
//	pcb_line_rc(21, y, 20, 0, 0.5, 0.1);
}


//linear_extrude(height=1)
	drawing();

