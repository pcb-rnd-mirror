#!/bin/sh
awk '
BEGIN {
	sx = 90;
	sy = 90;
	pt = "[-1,-1], ["sx+1", -1], ["sx+1", "sy+1"], [-1, "sy+1"]"
	pts = 4;
	paths = "[0,1,2,3]";

	for(y = 0; y < sx; y++) {
		for(x = 0; x < sy; x++) {
			paths = paths ", ["
			for(a = 0; a < 2*3.1415; a+=0.4) {
				pt = pt ", [" x + cos(a)*0.6 "," y + sin(a)*0.6 "]"
				paths = paths ((a == 0) ? "":",") pts
				pts++
			}
			paths = paths "]"
		}
	}

	print "module drawing() {"
	print "	polygon(points=[" pt "], paths=[" paths "], convexity=10);"
	print "}"
}
' > P.scad

