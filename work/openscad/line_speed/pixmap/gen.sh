#!/bin/sh

awk '
	BEGIN {
	for(y = 0; y < 190; y++) {
		for(x = 0; x < 190; x++)
			printf("%.0f ", sin((x+y)/10)*2)
		print ""
	}
	}
' > A.dat
