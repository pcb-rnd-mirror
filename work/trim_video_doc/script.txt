video should between 40-45 seconds, sample video is 43 seconds

source: trim-construct-2n3904.ogv at http://scififaster.net/rnd-doc-work/

title: example using pcb-rnd's ddraft plugin trim action
description: using trim action to create a silk drawing suitable for a TO-92 type part

[during the first setence of the narration, a still image of
https://upload.wikimedia.org/wikipedia/commons/e/ea/2n3904.jpg
should be shown, captioned with the url as "source: "]

script:
This video shows how to use pcb-rnd's trim action with the selected argument to easily create the silk graphics of a TO-92 footprint.

First, select the arc tool to draw an arc, and modify the arc object properties
to turn it into a complete circle (the value for arc/angle/delta should be 360)

Second, select the line tool, and make sure that you are in orthogonal mode
(toggle the mode using the / key) Ortho mode is shown in the operational
readout below the main window, as an (X).

Draw the vertical line bisecting the circle and extending beyond it

Switch to the selection tool and select the circle, use the trim(selected)
action, and click on the parts of the vertical line that extend outside the
circle to remove them.

No select the line segment and again use the trim(selected) action to remove
the arc segment of the circle to create the outline of the TO-92 package

