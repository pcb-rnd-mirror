﻿
var GC_MAX_COUNT = 32;
var NM_PER_PIXEL = 250000000;

/**
 * Constructs a graphic engine that processes graphic commands.
 *
 * @param options.comm - communication interface
 * @param options.canvas - HTML canvas rendering context
 * @param options.colors - color chart for symbolic color names
 * @param options.layerGroups - options based on layer group id
 * @param options.onNewLayerGroup - called when a new layer group is defined
 */
function GraphicEngine(options) {
	this.options = Object.assign({}, options);
	this.comm = options.comm;
	this.mainCc = options.mainCc;
	this.maskCc = options.maskCc;
	this.cc = this.mainCc;
	this.colors = options.colors;
	this.unit = options.unit || "mm";
	this.layerGroupOptions = options.layerGroups && typeof options.layerGroups === "object" ?
		options.layerGroups : {};

	this.cc.imageSmoothingEnabled = false;
	this.mainCc.setTransform(1, 0, 0, 1, 0, 0);
	this.maskCc.setTransform(1, 0, 0, 1, 0, 0);
	this.cc.clearRect(0, 0, this.cc.canvas.width, this.cc.canvas.height);
	//this.cc.globalAlpha = 0.5;

	this.board = {
		width: 0,
		height: 0
	};
	this.GCs = new Array(GC_MAX_COUNT);
	this.gc = undefined;
	this.mask = "off";
	this.layers = [];
	this.layerGroups = [];
	this.currentLayerGroup = undefined;
	this.enableDrawing = true;
}
GraphicEngine.getLayerList = function () {
	return this.layers;
}
GraphicEngine.enableLayerGroup = function (id, enable) {
	var group = this.layerGroups[id];
	if (!group) {
		group = {};
		this.layerGroups
	}
	group.enable = enable;
}
GraphicEngine.prototype.onMessage = function (command, args) {
	try {
		switch (command) {
			case "line": this.line(Number(args[0]),
				Number(args[1]), Number(args[2]),
				Number(args[3]), Number(args[4]));
				return;
			case "rect": this.rect(Number(args[0]),
				Number(args[1]), Number(args[2]), Number(args[3]), Number(args[4]),
				Number(args[5]));
				return;
			case "arc": this.arc(Number(args[0]),
				Number(args[1]), Number(args[2]), Number(args[3]), Number(args[4]),
				Number(args[5]), Number(args[6]));
				return;
			case "fcirc": this.fillCircle(Number(args[0]),
				Number(args[1]), Number(args[2]), Number(args[3]));
				return;
			case "poly": this.fillPoly(Number(args[0]),
				Number(args[1]), args[2]);
				return;
			case "clr": this.setColor(Number(args[0]), args[1]);
				return;
			case "linwid": this.setLineWidth(Number(args[0]), Number(args[1]));
				return;
			case "setxor": this.setXor(Number(args[0]), Number(args[1]));
				return;
			case "cap": this.setLineCap(Number(args[0]), args[1]);
				return;
			case "umask": // don't handle this command
				return;
			case "newlg": this.newLayerGroup(args[0], Number(args[1]),
				args[2][0], args[2][1], args[2][2]);
				return;
			case "newly": this.newLayer(args[0], Number(args[1]), Number(args[2]));
				return;
			case "setlg": this.setLayerGroup(Number(args[0]), Number(args[1]) === 1);
				return;
			case "makeGC": this.makeGC();
				return;
			case "delGC": this.deleteGC(Number(args[0]));
				return;
			case "ready": this.ready();
				return;
			case "unit": this.setUnit(args[0]);
				return;
			case "brddim": this.boardDimensions(Number(args[0]), Number(args[1]));
				return;
			case "ver": this.version(Number(args[0]));
				return;
			default: this.comm.error("unknown command: " + command);
		}
	} catch (e) {
		console.log("exception occured: ", e);
	}
}
GraphicEngine.prototype.line = function (gci, x1, y1, x2, y2) {
	if (!this.enableDrawing) {
		return;
	}
	this.selectGC(gci);
	this.cc.beginPath();
	this.cc.moveTo(x1, y1);
	this.cc.lineTo(x2, y2);
	this.cc.stroke();
}
GraphicEngine.prototype.rect = function (gci, x1, y1, x2, y2, filled) {
	if (!this.enableDrawing) {
		return;
	}
	this.selectGC(gci);
	if (filled == 1) {
		this.cc.fillRect(x1, y1, x2 - x1, y2 - y1);
	} else {
		var px = NM_PER_PIXEL / nanometerPerUnit(this.unit);
		this.cc.save();
		this.cc.lineWidth = px;
		this.cc.strokeRect(x1, y1, x2 - x1, y2 - y1);
		this.cc.restore();
	}
}
GraphicEngine.prototype.arc = function (gci, x1, y1, xr, yr, startAngle, deltaAngle) {
	if (!this.enableDrawing) {
		return;
	}
	this.selectGC(gci);
	this.cc.beginPath();
	if (deltaAngle > 360 || deltaAngle < -360) {
		startAngle = 0;
		deltaAngle = 360;
	}
	startAngle = (-startAngle + 180) * Math.PI / 180;
	deltaAngle = -deltaAngle * Math.PI / 180;
	if (deltaAngle !== 0) {
		if (this.cc.ellipse) {
			this.cc.ellipse(x1, y1, xr, yr, 0, startAngle, startAngle + deltaAngle, deltaAngle <= 0);
			this.cc.stroke();
		} else {
			// note: expect xr and yr being equal
			this.cc.arc(x1, y1, xr, startAngle, startAngle + deltaAngle, deltaAngle <= 0);
			this.cc.stroke();
		}
	} else {
		// this is a workaround, see http://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas
		x1 += xr * Math.cos(startAngle);
		y1 += xr * Math.sin(startAngle);
		var saveFill = this.cc.fillStyle;
		this.cc.fillStyle = this.cc.strokeStyle;
		this.cc.arc(x1, y1, this.cc.lineWidth / 2, 0, 2 * Math.PI);
		this.cc.fill();
		this.cc.fillStyle = saveFill;
	}
}
GraphicEngine.prototype.fillCircle = function (gci, x1, y1, r) {
	if (!this.enableDrawing) {
		return;
	}
	this.selectGC(gci);
	this.cc.beginPath();
	this.cc.arc(x1, y1, r, 0, 2 * Math.PI);
	this.cc.fill();
}
GraphicEngine.prototype.fillPoly = function (gci, n, points) {
	if (!this.enableDrawing) {
		return;
	}
	if (!Array.isArray(points) || points.length === 0) {
		return;
	}
	this.selectGC(gci);
	this.cc.beginPath();
	this.cc.moveTo(points[0][0], points[0][1]);
	for (var i = 1; i < points.length; ++i) {
		this.cc.lineTo(points[i][0], points[i][1]);
	}
	this.cc.fill();
}
GraphicEngine.prototype.setColor = function (gci, color) {
	gc = this.GCs[gci];
	if (!gc) {
		this.comm.error("invalid GC");
		return;
	}
	gc.color = color;


	if (this.gc === gc) {
		if (color === "drill" || color === "erase") {
			this.cc.globalCompositeOperation = "destination-out";
			this.cc.fillStyle = "black";
			this.cc.strokeStyle = "black";
		} else {
			this.cc.globalCompositeOperation = "source-over";
			this.cc.fillStyle = color;
			this.cc.strokeStyle = color;
		}
	}
}
GraphicEngine.prototype.setLineWidth = function (gci, width) {
	gc = this.GCs[gci];
	if (!gc) {
		this.comm.error("invalid GC");
		return;
	}
	width = width;
	gc.lineWidth = width;
	if (this.gc === gc) {
		this.cc.lineWidth = width;
	}
}
//GraphicEngine.prototype.useMask = function (mode) {
//	this.mask = mode;
//	// hack - more info needed about this
//	if (this.gc) {
//		if (mode === "clear") {
//			this.cc.fillStyle = "white";
//			this.cc.strokeStyle = "white";
//		} else {
//			this.cc.fillStyle = this.gc.color;
//			this.cc.strokeStyle = this.gc.color;
//		}
//	}
//}
GraphicEngine.prototype.setXor = function (gci, state) {
	gc = this.GCs[gci];
	if (!gc) {
		this.comm.error("invalid GC");
		return;
	}
	gc.xor = state != 0;
	if (this.gc === gc) {
	}
}
GraphicEngine.prototype.setLineCap = function (gci, cap) {
	gc = this.GCs[gci];
	if (!gc) {
		this.comm.error("invalid GC");
		return;
	}
	var cap = capToLineCap(cap);
	if (!cap) {
		this.comm.error("invalid cap: " + cap);
		return;
	}
	gc.lineCap = cap;
	if (this.gc === gc) {
		this.cc.lineCap = gc.lineCap;
	}
}
GraphicEngine.prototype.newLayerGroup = function (name, id, location, purpose, properties) {
	if (this.layerGroups[id]) {
		return;
	}
	var locstr = "";
	if (Array.isArray(location) && location.length > 0) {
		locstr = location[0];
	}
	var purposes = [];
	var isMask = false;
	if (Array.isArray(purpose)) {
		for (var i = 0; i < purpose.length; i++) {
			if (typeof purpose[i] === "string") {
				purposes.push(purpose[i]);
			}
			if (purpose[i] === "mask") {
				isMask = true;
			}
		}
	}
	var props = {};
	if (Array.isArray(properties)) {
		for (var i = 0; i < properties.length; i++) {
			if (typeof properties[i] === "string") {
				props[properties[i]] = true;
			}
		}
	}
	var lg = {
		name: name,
		id: id,
		location: locstr,
		purposes: purposes,
		props: props,
		isMask: isMask
	};
	this.layerGroups[id] = lg;
	if (this.options.onNewLayerGroup) {
		this.options.onNewLayerGroup(lg);
	}
}
GraphicEngine.prototype.newLayer = function (name, id, groupId) {
	this.layers[name] = {
		name: name,
		id: id,
		group: groupId,
	};
}
GraphicEngine.prototype.setLayerGroup = function (id, empty) {
	if (this.enableDrawing && this.currentLayerGroup && this.currentLayerGroup.isMask) {
		this.maskCc.setTransform(1, 0, 0, 1, 0, 0);
		this.mainCc.setTransform(1, 0, 0, 1, 0, 0);
		this.mainCc.drawImage(this.maskCc.canvas, 0, 0);
		this.setMatrix();
		this.cc = this.mainCc;
	}
	this.currentLayerGroup = this.layerGroups[id];
	this.enableDrawing = !this.layerGroupOptions[id] || this.layerGroupOptions[id].visible;
	if (this.enableDrawing && this.currentLayerGroup && this.currentLayerGroup.isMask) {
		this.cc = this.maskCc;
		this.mainCc.setTransform(1, 0, 0, 1, 0, 0);
		this.cc.clearRect(0, 0, this.cc.canvas.width, this.cc.canvas.height);
		this.setMatrix();
		this.cc.save();
		this.cc.fillStyle = this.colors.mask;
		this.cc.fillRect(0, 0, this.board.width, this.board.height);
		this.cc.restore();
	}
}
GraphicEngine.prototype.makeGC = function () {
	for (var i = 1; i < GC_MAX_COUNT; ++i) {
		if (!this.GCs[i]) {
			this.GCs[i] = {
				gci: i,
				color: 0,
				lineWidth: 1,
				lineCap: "round"
			};
			this.comm.send("MadeGC", [i]);
			return;
		}
	}
	this.comm.error("too many GC");
}
GraphicEngine.prototype.deleteGC = function (gci) {
	if (!this.GCs[gci]) {
		this.comm.error("undefined GC");
		return;
	}
	this.GCs[gci] = undefined;
}
GraphicEngine.prototype.selectGC = function (gci) {
	if (this.gc && this.gc.gci === gci)
		return;

	var gc = this.GCs[gci];
	if (!gc) {
		this.comm.error("invalid GC index");
		throw new Error("invalid GC index");
	}
	this.gc = gc;

	// set all drawing properties in advance
	// it can be costly so optimization may be necessary
	this.cc.lineWidth = gc.lineWidth;
	this.cc.lineCap = gc.lineCap;

	if (gc.color === "drill" || gc.color === "erase") {
		this.cc.globalCompositeOperation = "destination-out";
		this.cc.fillStyle = "black";
		this.cc.strokeStyle = "black";
	} else {
		this.cc.globalCompositeOperation = "source-over";
		this.cc.fillStyle = gc.color;
		this.cc.strokeStyle = gc.color;
	}
}
GraphicEngine.prototype.setMatrix = function() {
	var scale = nanometerPerUnit(this.unit) / NM_PER_PIXEL * 10;
	this.mainCc.setTransform(1, 0, 0, 1, 0, 0);
	this.mainCc.scale(scale, scale);
	this.maskCc.setTransform(1, 0, 0, 1, 0, 0);
	this.maskCc.scale(scale, scale);
}
GraphicEngine.prototype.ready = function () {
	this.setMatrix();
	this.comm.send("Ready", []);
}
GraphicEngine.prototype.version = function (ver) {
	if (Number(ver) !== 1) {
		this.comm.error("unsupported protocol version: " + ver);
		this.comm.disconnect();
	}
}
GraphicEngine.prototype.setUnit = function (unit) {
	if (!nanometerPerUnit(unit)) {
		this.comm.error("unknown unit: " + unit);
		return;
	}
	this.unit = unit;
}
GraphicEngine.prototype.boardDimensions = function (w, h) {
	this.board.width = w;
	this.board.height = h;
}


function nanometerPerUnit(unit) {
	switch (unit) {
		case "nm": return 1;
		case "mm": return 1000000000;
		case "mil": return 25400000;
		default: return undefined;
	}
}


function capToLineCap(cap) {
	switch (cap) {
		case "r": return "round";
		case "b": return "round"; // octagon cap is not supported yet
		case "s": return "square";
		default: return undefined;
	}
}

exports.GraphicEngine = GraphicEngine;