var readline = require('readline');
var express = require('express');
var morgan = require('morgan');

var app = express();
var expressWs = require('express-ws')(app); // registers app.ws

var serverecho;
var clientecho;


var rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});


rl.on("line", function (input) {
	if (clientecho) {
		clientecho.send(input);
	}
});

// this will serve static files
app.use("/", express.static("../client"));
app.use("/test", express.static("../testclient"));
app.use("/testdata", express.static("../testdata"));
app.use("/ref", gzipDirectory("../../../../trunk/tests/RTT/ref"));
app.use("/protocol", express.static("../protocol"));

// this will log http requests
app.use(morgan("common"));

app.ws('/serverecho', function (ws, req) {
	console.log("open server echo socket");
	serverecho = ws;
	ws.on("message", function (msg) {
		console.log("server echoes", msg);
		ws.send(msg);
	});
	ws.on("close", function () {
		console.log("close server echo socket");
		serverecho = null;
	});
});

app.ws('/clientecho', function (ws, req) {
	console.log("open client echo socket");
	clientecho = ws;
	ws.on("message", function (msg) {
		console.log("client echoes", msg);
	});
	ws.on("close", function () {
		console.log("close client echo socket");
		clientecho = null;
	});
});

var server = app.listen(process.env.port || 3000, function () {
	console.log('testserver listening on port', server.address().port);
});

// middleware for serving gzipped files
// note: this would be too slow in production code but it will do it for test
function gzipDirectory(localPath) {
	return function (req, res) {
		var path = req.path + ".gz";
		if (/\.gz$/.test(req.path)) {
			if (req.acceptsEncodings("gzip")) {
				// automatic gunzip on client side
				res.set('Content-Encoding', 'gzip');
				res.sendFile(req.path, { root: localPath });
			} else {
				res.status(415).send("gzip support required in the browser");
			}
		} else {
			// send as normal static file
			res.sendFile(req.path, { root: localPath });
		}
	};
}
