"use strict";


const test = require("tape");
const ProtocolParser = require("..").ProtocolParser;


test("cmd", (t) => {
	t.deepEqual(parse("a()\n"), { cmd: "a", args: [] });
	t.deepEqual(parse("abc{}\n"), { cmd: "abc", args: [] });
	t.deepEqual(parse("azAZ09_+-.#()\n"), { cmd: "azAZ09_+-.#", args: [] });
	t.deepEqual(parse("abcdefghijklmnop{}\n"), { cmd: "abcdefghijklmnop", args: [] });

	t.end();
});


test("badcmd", (t) => {
	t.notOk(parse("()\n"));
	t.notOk(parse("  ()\n"));
	t.notOk(parse("abcdefghijklmnopq()\n"));
	t.notOk(parse(":()\n"));
	t.notOk(parse("ö()\n"));
	t.notOk(parse("abc>()\n"));

	t.end();
});


test("badargs", (t) => {
	t.notOk(parse("a\n"));
	t.notOk(parse("a  \n"));
	t.notOk(parse("a(\n"));
	t.notOk(parse("a{\n"));
	t.notOk(parse("a(}\n"));
	t.notOk(parse("a{)\n"));
	t.notOk(parse("a(((\n"));
	t.notOk(parse("a{{{\n"));

	t.end();
});


test("generic", (t) => {
	t.deepEqual(parse("a()\n"), {
		cmd: "a",
		args: []
	});
	t.deepEqual(parse("a(b)\n"), {
		cmd: "a",
		args: ["b"]
	});
	t.deepEqual(parse("a(abc)\n"), {
		cmd: "a",
		args: ["abc"]
	});
	t.deepEqual(parse("a(azAZ09_+-.#)\n"), {
		cmd: "a",
		args: ["azAZ09_+-.#"]
	});
	t.deepEqual(parse("a(abcdefghijklmnop)\n"), {
		cmd: "a",
		args: ["abcdefghijklmnop"]
	});

	t.notOk(parse("a(abcdefghijklmnopq)\n"));

	t.end();
});


test("generic-multiple", (t) => {
	t.deepEqual(parse("a(abc def ghi)\n"), {
		cmd: "a",
		args: ["abc", "def", "ghi"]
	});

	t.end();
});


test("binary", (t) => {
	t.deepEqual(parse("a{}\n"), {
		cmd: "a",
		args: []
	});
	t.deepEqual(parse("a{A=}\n"), {
		cmd: "a",
		args: [""]
	});
	t.deepEqual(parse("a{B=b}\n"), {
		cmd: "a",
		args: ["b"]
	});
	t.deepEqual(parse("a{E= \n})}\n"), {
		cmd: "a",
		args: [" \n})"]
	});
	t.deepEqual(parse("a{a=" + " ".repeat(26) + "}\n"), {
		cmd: "a",
		args: [" ".repeat(26)]
	});
	t.deepEqual(parse("a{0=" + " ".repeat(52) + "}\n"), {
		cmd: "a",
		args: [" ".repeat(52)]
	});
	t.deepEqual(parse("a{+=" + " ".repeat(62) + "}\n"), {
		cmd: "a",
		args: [" ".repeat(62)]
	});
	t.deepEqual(parse("a{/=" + " ".repeat(63) + "}\n"), {
		cmd: "a",
		args: [" ".repeat(63)]
	});
	t.deepEqual(parse("a{BA=" + " ".repeat(64) + "}\n"), {
		cmd: "a",
		args: [" ".repeat(64)]
	});

	t.end();
});


test("binary-multiple", (t) => {
	t.deepEqual(parse("a{D=abc D=def D=ghi}\n"), {
		cmd: "a",
		args: ["abc", "def", "ghi"]
	});

	t.end();
});


test("gen-gen", (t) => {
	t.deepEqual(parse("a((()))\n"), {
		cmd: "a",
		args: [[[]]]
	});
	t.deepEqual(parse("a(b1(c11)(c21 c22)b3)\n"), {
		cmd: "a",
		args: ["b1", ["c11"], ["c21", "c22"], "b3"]
	});

	t.end();
});


test("gen-bin", (t) => {
	t.deepEqual(parse("a({{}})\n"), {
		cmd: "a",
		args: [[[]]]
	});
	t.deepEqual(parse("a(b1(c11){D=c21 D=c22}b3)\n"), {
		cmd: "a",
		args: ["b1", ["c11"], ["c21", "c22"], "b3"]
	});

	t.end();
});


test("bin-bin", (t) => {
	t.deepEqual(parse("a{{{}}}\n"), {
		cmd: "a",
		args: [[[]]]
	});
	t.deepEqual(parse("a{C=b1{C=c1}{D=c21 D=c22}C=b3}\n"), {
		cmd: "a",
		args: ["b1", ["c1"], ["c21", "c22"], "b3"]
	});

	t.end();
});


test("bin-gen", (t) => {
	t.notOk(parse("a{(())}\n"));
	t.notOk(parse("a{C=b1(c1)C=b3}\n"));

	t.end();
});


test("whitespace", (t) => {
	t.deepEqual(parse(" \t b \t {   \t } \t \n"), {
		cmd: "b",
		args: []
	});

	t.end();
});


test("comment", (t) => {
	t.equal(parse("#a()\n"), "pending");
	t.equal(parse("# \t b \t {   \t } \t \n"), "pending");
	t.equal(parse("#\n"), "pending");

	t.end();
});


test("after-comment", (t) => {
	t.deepEqual(parse("#\na()\n"), {
		cmd: "a",
		args: []
	});
	t.deepEqual(parse("#\n \t a()\n"), {
		cmd: "a",
		args: []
	});

	t.end();
});


test("unfinished", (t) => {
	t.equal(parse(""), "pending");
	t.equal(parse("\n"), "pending");
	t.equal(parse("\n\t \n"), "pending");
	t.equal(parse("a()"), "pending");
	t.equal(parse(" \t b \t {   "), "pending");
	t.equal(parse("fdfds"), "pending");

	t.end();
});


function parse(str) {
	var cmd;
	var args;
	var err;
	let buffer = Buffer.from(str);
	let parser = new ProtocolParser();

	parser.onCommand = function (_cmd, _args) {
		cmd = _cmd;
		args = _args;
	};
	parser.onError = function (pos, _err) {
		console.log("#    msg[%d]: ", pos, _err);
		err = _err;
	};
	parser.parse(buffer);

	if (err) {
		return false;
	} else if (cmd) {
		return { cmd: cmd, args: args };
	} else {
			return "pending";
	}
}
