"use strict";


// parser states
const MSG = 0;
const COMMAND = 1;
const ARGUMENT = 2;
const BINARY_LENGTH = 3;
const BINARY_STRING = 4;
const TEXT_STRING = 5;
const COMMENT = 8;
const EOL = 9;


// list types
const GENERIC = 0;
const BINARY = 1;


/**
 * Constructs a HID protocol parser.
 */
function ProtocolParser() {
	this.state = MSG;
}
/**
 * Parses an array of octets.
 *
 * @param buffer - An array-like indexable object whose elements are octets.
 */
ProtocolParser.prototype.parse = function (buffer) {
	for (var i = 0; i < buffer.length; ++i) {
		var b = buffer[i];
			switch (this.state) {

				case MSG:
					if (b === 9 || b === 10 || b === 13 || b === 32) {
						// empty message or leading space
					} else if (b === 35) { // '#'
						this.state = COMMENT;
					} else {
						this.state = COMMAND;
						this.command = '';
						--i; // pass the same character to COMMAND
					}
					break;

				case COMMAND:
					if (b >= 65 && b <= 90 || b >= 97 && b <= 122 ||
						b >= 48 && b <= 57 ||
						b === 95 || b === 43 || b === 45 || b === 46 || b === 35) {
						this.command += String.fromCharCode(b);
						if (this.command.length > 16) {
							this.onError(i, "command too long");
							this.state = COMMENT;
							--i; // process the same character again so we won't miss EOL
						}
					} else {
						if (this.command.length > 0) {
							this.state = ARGUMENT;
							this.list = null;
							this.args = null;
							--i;
						} else {
							this.onError(i, "missing command");
							this.state = COMMENT;
							--i; // process the same character again so we won't miss EOL
						}
					}
					break;

				case ARGUMENT:
					if (b === 9 || b === 32) {
						// skip whitespace
					} else if (b === 10 || b === 13) {
						this.onError(i, "missing or unfinished arguments");
						this.state = EOL;
						--i;
					} else if (b === 40) { // '('
						if (this.list && this.list.type === BINARY) {
							this.onError(i, "generic list inside binary list");
							this.state = COMMENT;
							--i;
						} else {
							var list = {
								elements: [],
								type: GENERIC,
								parent: this.list
							};
							if (this.list) {
								this.list.elements.push(list);
							}
							this.list = list;
						}
					} else if (b === 123) { // '{'
						var list = {
							elements: [],
							type: BINARY,
							parent: this.list
						};
						if (this.list) {
							this.list.elements.push(list);
						}
						this.list = list;
					} else if (b === 41 || b === 125) { // ')' || '}'
						if (!this.list) {
							this.onError(i, "list terminator outside of list");
							this.state = COMMENT;
							--i;
						} else if (b === 41 && this.list.type === BINARY) {
							this.onError(i, "generic list terminator in binary list");
							this.state = COMMENT;
							--i;
						} else if (b === 125 && this.list.type === GENERIC) {
							this.onError(i, "binary list terminator in generic list");
							this.state = COMMENT;
							--i;
						} else {
							if (this.list.parent) {
								this.list = this.list.parent;
							} else {
								// the root list has been closed
								this.args = ProtocolParser.compactTree(this.list);
								this.list = null;
								this.state = EOL;
							}
						}
					} else { // string
						if (!this.list) {
							this.onError(i, "string outside of list");
							this.state = COMMENT;
							--i;
						} else if (this.list.type === BINARY) {
							this.state = BINARY_LENGTH;
							this.strLen = 0;
							this.strLenSize = 0;
							this.str = '';
							--i;
						} else {
							this.state = TEXT_STRING;
							this.str = '';
							--i;
						}
					}
					break;

				case TEXT_STRING:
					if (b >= 65 && b <= 90 || b >= 97 && b <= 122 ||
						b >= 48 && b <= 57 ||
						b === 95 || b === 43 || b === 45 || b === 46 || b === 35) {
						this.str += String.fromCharCode(b);
						if (this.str.length > 16) {
							this.onError(i, "text string too long");
							this.state = COMMENT;
							--i;
						}
					} else {
						this.list.elements.push(this.str);
						this.state = ARGUMENT;
						--i;
					}
					break;

				case BINARY_LENGTH:
					if (this.strLenSize > 5) {
						this.onError(i, "binary string length is too long");
						this.state = COMMENT;
						--i;
					} else if (b >= 65 && b <= 90) { // A-Z
						this.strLenSize++;
						var code = b - 65;
						this.strLen = (this.strLen << 6) + code;
					} else if (b >= 97 && b <= 122) { // a-z
						this.strLenSize++;
						var code = b - 97 + 26;
						this.strLen = (this.strLen << 6) + code;
					} else if (b >= 48 && b <= 57) { // 0-9
						this.strLenSize++;
						var code = b - 48 + 52;
						this.strLen = (this.strLen << 6) + code;
					} else if (b === 43) { // '+'
						this.strLenSize++;
						this.strLen = (this.strLen << 6) + 62;
					} else if (b === 47) { // '/'
						this.strLenSize++;
						this.strLen = (this.strLen << 6) + 63;
					} else if (b === 61) { // '='
						this.state = BINARY_STRING;
					} else {
						this.onError(i, "invalid character in binary string length");
						this.state = COMMENT;
						--i;
					}
					break;

				case BINARY_STRING:
					if (this.strLen <= 0) {
						this.list.elements.push(this.str);
						this.state = ARGUMENT;
						--i;
					} else {
						this.str += String.fromCharCode(b);
						this.strLen--;
					}
					break;

				case COMMENT:
					if (b === 10 || b === 13) {
						this.state = MSG;
					}
					break;

				case EOL:
					if (b === 9 || b === 32) {
						// skip whitespace
					} else if (b === 10 || b === 13) {
						this.onCommand(this.command, this.args);
						this.state = MSG;
					} else {
						this.onError(i, "extra characters after argument list");
						this.state = COMMENT;
						--i;
					}
					break;

			} // end switch
	} // end for
};
/**
 * Override this to execute a command.
 */
ProtocolParser.prototype.onCommand = function (command, args) {
};
/**
 * Override this to respond to errors.
 */
ProtocolParser.prototype.onError = function () {
};
/**
 * Converts the argument tree to array of arrays by removing object levels.
 */
ProtocolParser.compactTree = function (tree) {
	var list = tree.elements;
	for (var i = 0; i < list.length; ++i) {
		var elem = list[i];
		if (typeof elem === "object") {
			list[i] = ProtocolParser.compactTree(elem);
		}
	}
	return list;
};

exports.ProtocolParser = ProtocolParser;
