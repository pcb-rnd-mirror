var echoText = document.getElementById("echo-text");
var echoResponse = document.getElementById("echo-response");


function sendToEcho() {
	console.log("client calls ", echoText.value);
	serverEchoSocket.send(echoText.value);
	echoText.value = "";
}


function getWebSocketUrl(relpath) {
	var loc = window.location, url;
	if (loc.protocol === "https:") {
		url = "wss:";
	} else {
		url = "ws:";
	}
	url += "//" + loc.host + "/" + relpath;
	return url;
}


var serverEchoSocket =
	new WebSocket(getWebSocketUrl("serverecho"), "remote-hid-protocol");
serverEchoSocket.onopen = function (event) {
	console.log("server echo socket is connected to", serverEchoSocket.url);
};
serverEchoSocket.onclose = function (event) {
	console.log("server echo socket is closed");
};
serverEchoSocket.onerror = function (event) {
	console.log("server echo socket error", event);
};
serverEchoSocket.onmessage = function (event) {
	console.log("server echoes", event.data);
	echoResponse.value = event.data;
};


var clientEchoSocket =
	new WebSocket(getWebSocketUrl("clientecho"), "remote-hid-protocol");
clientEchoSocket.onopen = function (event) {
	console.log("client echo socket is connected to", clientEchoSocket.url);
};
clientEchoSocket.onclose = function (event) {
	console.log("client echo socket is closed");
};
clientEchoSocket.onerror = function (event) {
	console.log("client echo socket error", event);
};
clientEchoSocket.onmessage = function (event) {
	console.log("client echoes", event.data);
	var div = document.createElement("div");
	div.appendChild(document.createTextNode(event.data));
	document.body.appendChild(div);
	clientEchoSocket.send(event.data);
};


