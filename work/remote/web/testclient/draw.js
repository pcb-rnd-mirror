﻿"use strict"

removeElementById("loadjs");

var protocol = require("../protocol/protocol");
var draw = require("../client/scripts/draw");
var currentBuffer;
var layerGroups = {};
var defaultDisabledPurposes = ["mask", "paste", "fab"];


buildFileList();

function buildFileList() {
	var req = new XMLHttpRequest();
	req.onreadystatechange = function () {
		if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
			console.log("filelist:", req.response);
			var lines = req.response.match(/[^\r\n]+/g);
			var html = "";
			for (var i = 0; i < lines.length; ++i) {
				html += "<option value='" + lines[i] + "'>" + lines[i] + "</option>";
			}
			var filelist = document.getElementById("filelist");
			filelist.innerHTML = html;
			filelist.onchange = function () {
				var filename = filelist.options[filelist.selectedIndex].value;
				drawRemoteFile("../ref/" + filename);
			}
		}
	};
	req.open("GET", "../testdata/testlist.txt");
	req.send();
}


function drawRemoteFile(url) {
	var req = new XMLHttpRequest();
	req.onreadystatechange = function () {
		if (req.readyState === XMLHttpRequest.DONE && req.status === 200) {
			console.log("drawing", url);
			currentBuffer = new Uint8Array(req.response);
			layerGroups = {};
			document.getElementById("layerGroupList").innerHTML = "";
			drawBuffer();
			drawBuffer();
		}
	};
	req.responseType = 'arraybuffer';
	req.open("GET", url);
	req.send();
}


function drawBuffer() {
	var mainCc = document.getElementById("main-canvas").getContext("2d");
	var maskCc = document.getElementById("mask-canvas").getContext("2d");

	var lgMap = {};
	for (var id in layerGroups) {
		lgMap[id] = { visible: layerGroups[id].visible };
	}

	var ge = new draw.GraphicEngine({
		comm: new TestComm(),
		mainCc: mainCc,
		maskCc: maskCc,
		colors: {
			mask: "#274f29"
		},
		layerGroups: lgMap,
		onNewLayerGroup: onNewLayerGroup
	});

	var parser = new protocol.ProtocolParser();
	parser.onCommand = function (cmd, args) {
		ge.onMessage(cmd, args);
	};
	parser.onError = function (pos, err) {
		console.log("parser error: msg[%d]: ", pos, err);
	};

	parser.parse(currentBuffer);
}


function onNewLayerGroup(lg) {
	if (layerGroups[lg.id]) {
		return;
	}

	// determine initial visibility
	var visible = true;
	for (var i = 0; i < lg.purposes.length; ++i) {
		for (var j = 0; j < defaultDisabledPurposes.length; ++j) {
			if (lg.purposes[i] === defaultDisabledPurposes[j])
				visible = false;
		}
	}

	layerGroups[lg.id] = {
		id: lg.id,
		purposes: lg.purposes,
		visible: visible
	};

	// list the group
	var purposes = " (";
	for (var i = 0; i < lg.purposes.length; ++i) {
		if (i) {
			purposes += ", ";
		}
		purposes += lg.purposes[i];
	}
	purposes += ")";
	var list = document.getElementById("layerGroupList");
	var li = document.createElement("li");
	var cbox = document.createElement("input");
	cbox.type = "checkbox";
	cbox.id = "checkbox" + lg.id;
	cbox.checked = visible;
	cbox.onchange = function () {
		layerGroups[lg.id].visible = cbox.checked;
		drawBuffer();
	}
	li.appendChild(cbox);
	li.appendChild(document.createTextNode(lg.id + ": " + lg.name + purposes));
	list.appendChild(li);
}


function TestComm() {
	this.send = function(cmd, args) {
		console.log("send", cmd, args);
	}
	this.error = function (msg) {
		console.log("error:", msg);
	}
	this.disconnect = function () {
		console.log("client disconnected");
	}
}


function removeElementById(id) {
	var e = document.getElementById(id);
	if (e && e.parentNode) {
		e.parentNode.removeChild(e);
	}
}
