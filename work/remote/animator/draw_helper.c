#include <stdio.h>
#include <math.h>
#include <libporty/net/network.h>

/* draw a line with rounded ends
   do not draw end circles if omit_ends is non-zero
   extend both ends by length_bloat */
void draw_rline(P_net_socket sk, double x1, double y1, double x2, double y2, double width, int round_cap)
{
	double nx = y2-y1;
	double ny = -1.0 * (x2-x1);
	double len=sqrt(nx*nx+ny*ny);
	double length_bloat = 0;

	if (len != 0) {
		double vx, vy;
		nx /= len;
		ny /= len;
		vx = -ny;
		vy = nx;

		if (length_bloat != 0) {
			x1 -= length_bloat * vx;
			x2 += length_bloat * vx;
			y1 -= length_bloat * vy;
			y2 += length_bloat * vy;
		}

/* if there are no end circles, we are square and should extend by extra width/2 */
		if (!round_cap) {
			x1 -= (width/2) * vx;
			x2 += (width/2) * vx;
			y1 -= (width/2) * vy;
			y2 += (width/2) * vy;
		}

		P_net_printf(sk, "poly %f %f %f %f %f %f %f %f\n", 
			x1+nx*width/2, y1+ny*width/2, x2+nx*width/2, y2+ny*width/2,
			x2-nx*width/2, y2-ny*width/2, x1-nx*width/2, y1-ny*width/2);
	}

	if (round_cap) {
		int segs = width/10;
		if (segs < 10)
			segs = 10;
		P_net_printf(sk, "fillcircle %f %f %f %d\n", x1, y1, width/2, segs);
		if ((x1 != x2) || (y1 != y2))
			P_net_printf(sk, "fillcircle %f %f %f %d\n", x2, y2, width/2, segs);
	}
}
