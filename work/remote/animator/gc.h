typedef struct {
	unsigned in_use:1;
	unsigned xor:1;
	char cap;
	char color[16];
	double width;
} gc_t;

#define NUM_GC 32
gc_t gc[NUM_GC];
gc_t gc_active;

static void set_color(const char *color)
{
	if (strcmp(color, "erase") == 0)
		color = "#ffffff";
	if (strcmp(color, "hole") == 0)
		color = "#000000";
	if (strcasecmp(gc_active.color, color) == 0)
		return;
	strcpy(gc_active.color, color);
	P_net_printf(s_anim_out, "color %s\n", color);
}


static gc_t *use_gc(int idx)
{
	gc_t *g;

	if ((idx < 0) || (idx >= NUM_GC))
		return NULL;
	if (!gc[idx].in_use)
		return NULL;
	g = gc+idx;
	set_color(g->color);
	return g;
}

static void cmd_makeGC(proto_node_t *args)
{
	int n, found = 0;

	send_begin(&pctx, "MakeGC");
	send_open(&pctx, 0, 1);
	for(n = 0; n < NUM_GC; n++) {
		if (!gc[n].in_use) {
			found = 1;
			memset(gc+n, 0, sizeof(gc_t));
			gc[n].in_use = 1;
			sendf(&pctx, "%d", n);
			break;
		}
	}

	if (!found)
		sends(&pctx, "-1");

	send_close(&pctx);
	send_end(&pctx);
}


static void cmd_delGC(proto_node_t *args)
{
	char *gs = astr(child1(args));
	if (gs != NULL) {
		int idx = atoi(gs);
		if ((idx >= 0) && (idx < NUM_GC))
			gc[idx].in_use = 0;
	}
}

#define get_gc(idx, idx_str) \
do { \
	char *__end__; \
	idx = strtol(gs, &__end__, 10); \
	if ((*__end__ != '\0') || (idx < 0) || (idx >= NUM_GC) || !gc[idx].in_use) \
		return; \
} while(0)

static void cmd_clr(proto_node_t *args)
{
	int idx;
	proto_node_t *ch1 = child1(args);
	char *gs = astr(ch1), *cs = astr(next(ch1));

	if ((cs == NULL) || (gs == NULL))
		return;
	get_gc(idx, gs);
	if ((strlen(cs) > sizeof(gc[idx].color)-1))
		return;
	strcpy(gc[idx].color, cs);
}

static void cmd_cap(proto_node_t *args)
{
	int idx;
	proto_node_t *ch1 = child1(args);
	char *gs = astr(ch1), *cs = astr(next(ch1));

	if ((cs == NULL) || (gs == NULL))
		return;
	get_gc(idx, gs);
	if ((*cs == 'r') || (*cs == 'b') || (*cs == 's'))
		gc[idx].cap = *cs;
}

static void cmd_linwid(proto_node_t *args)
{
	int idx;
	char *end;
	double d;
	proto_node_t *ch1 = child1(args);
	char *gs = astr(ch1), *ws = astr(next(ch1));

	if ((ws == NULL) || (gs == NULL))
		return;
	get_gc(idx, gs);
	d = strtod(ws, &end);
	if (*end == '\0')
		gc[idx].width = d;
}

static void cmd_setxor(proto_node_t *args)
{
	int idx;
	proto_node_t *ch1 = child1(args);
	char *gs = astr(ch1), *xs = astr(next(ch1));

	if ((xs == NULL) || (gs == NULL))
		return;
	get_gc(idx, gs);
	if ((*xs == '0') || (*xs == '1'))
		gc[idx].xor = !!(xs - '0');
}
