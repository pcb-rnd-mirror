#define _DEFAULT_SOURCE
#define _BSD_SOURCE
#include <stdio.h>
#include <libporty/net/network.h>
#include <libporty/net_async/abgproc.h>
#include <libporty/net_async/astdio.h>
#define proto_vsnprintf vsnprintf
#include "src_plugins/hid_remote/proto_lowcommon.h"
#include "src_plugins/hid_remote/proto_lowsend.h"
#include "src_plugins/hid_remote/proto_lowparse.h"
#include "cmds_hash.h"
#include "compat.h"
#include "draw_helper.h"

static proto_ctx_t pctx;
P_net_socket s_netin, s_netout, s_anim_out, s_anim_in;

static char *current_unit = NULL;

#include "gc.h"


static void cmd_ver(proto_node_t *args)
{
	char *vs = astr(child1(args));
	if (vs != NULL) {
		int ver = atoi(vs);
		if (ver != 1) {
			fprintf(stderr, "protocol version mismatc: ver=%d (expected 1)\n", ver);
			exit(1);
		}
	}
}

static void cmd_unit(proto_node_t *args)
{
	char *us = astr(child1(args));
	if (us != NULL) {
		if (current_unit != NULL)
			free(current_unit);
		current_unit = pcb_strdup(us);
	}
}

#define BADVAL -1024
static void cmd_line(proto_node_t *args)
{
	proto_node_t *ch1 = child1(args);
	int gci = aint(ch1, -1);
	double x1, y1, x2, y2;
	gc_t *gc;

	x1 = adbl(next(ch1), BADVAL);
	y1 = adbl(next(next(ch1)), BADVAL);
	x2 = adbl(next(next(next(ch1))), BADVAL);
	y2 = adbl(next(next(next(next(ch1)))), BADVAL);
	if ((gci == BADVAL) || (x1 == BADVAL) || (y1 == BADVAL) || (x2 == BADVAL) || (y2 == BADVAL))
		abort();
	gc = use_gc(gci);
	if (gc == NULL)
		abort();
	draw_rline(s_anim_out, x1, y1, x2, y2, gc->width, gc->cap == 'r');
}

static void cmd_brddim(proto_node_t *args)
{
	proto_node_t *ch1 = child1(args);
	double w, h;

	w = adbl(ch1, BADVAL);
	h = adbl(next(ch1), BADVAL);
	if ((w == BADVAL) || (h == BADVAL))
		abort();
	P_net_printf(s_anim_out, "scale 1 -1\nviewport %f %f - %f %f\nframe\n", -20.0, -20.0, w+20.0, h+20.0);
}


static void cmd_ready(proto_node_t *args)
{
	send_begin(&pctx, "ready");
	send_open(&pctx, 0, 1);
	send_close(&pctx);
	send_end(&pctx);
}


static void read_netin(void)
{
	P_size_t len, n;
	char buff[1024], *s;
	len = P_net_read(s_netin, buff, sizeof(buff));
	for(n = 0, s = buff; n < len; s++,n++) {
		switch(parse_char(&pctx, *s)) {
			case PRES_ERR:
				fprintf(stderr, "Protocol error.\n");
				exit(1);
			case PRES_PROCEED:
				break;
			case PRES_GOT_MSG:
				switch(cmds_sphash(pctx.pcmd)) {
					case cmds_ver: cmd_ver(pctx.targ); break;
					case cmds_unit: cmd_unit(pctx.targ); break;
					case cmds_ready: cmd_ready(pctx.targ); break;
					case cmds_makeGC: cmd_makeGC(pctx.targ); break;
					case cmds_delGC: cmd_delGC(pctx.targ); break;
					case cmds_clr: cmd_clr(pctx.targ); break;
					case cmds_cap: cmd_cap(pctx.targ); break;
					case cmds_linwid: cmd_linwid(pctx.targ); break;
					case cmds_setxor: cmd_setxor(pctx.targ); break;
					case cmds_line: cmd_line(pctx.targ); break;
					case cmds_brddim: cmd_brddim(pctx.targ); break;
				
					case cmds_exit: exit(0);
					/* ignore */
					case cmds_inval:
					case cmds_umask:
					case cmds_setly:
					break;
				}
				proto_node_free(pctx.targ);
				break;
		}
	}

}

int main()
{
	struct P_pollfd pfds[2];

	if (P_net_async_stdio(&s_netin, &s_netout) != 0) {
		fprintf(stderr, "Can't open stdio\n");
		return 1;
	}
	
	pfds[0].fd = s_netin;
	pfds[0].events = P_POLLIN;

	s_anim_out = 2;

	for(;;) {
		P_poll(pfds, 1, 100);
		if ((pfds[0].revents & P_POLLIN))
			read_netin();
	}
}
