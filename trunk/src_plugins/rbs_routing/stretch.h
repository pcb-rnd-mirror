#include "seq.h"

typedef struct rbsr_stretch_s {
	rbsr_map_t map;
	grbs_addr_t from, to;
	rnd_coord_t fromx, fromy, tox, toy;
/*	pcb_arc_t *from_arc, *to_arc;*/
	double from_arc_sa, to_arc_ea;
	grbs_line_t *gline; /* line being stretched */
	int acceptable;

	grbs_point_t *via;

	grbs_point_t *jump_over_pt; /* point we are jumping over in jump-over mode */
	grbs_arc_t *jump_over_consider; /* arc being considered in jump-over mode */

	rbsr_seq_t seq; /* the actual route */

	grbs_snapshot_t *snap;

	htpp_t removes;        /* key: pcb_any_obj_t *; value: NULL; list of pcb-rnd objects to remove when the grbs route is copied back */

} rbsr_stretch_t;


int rbsr_stretch_any_begin(rbsr_stretch_t *rbss, pcb_board_t *pcb, rnd_coord_t x, rnd_coord_t y);

/* Start stretching a routing line; returns 0 on success */
int rbsr_stretch_line_begin(rbsr_stretch_t *rbss, pcb_board_t *pcb, pcb_line_t *line, rnd_coord_t tx, rnd_coord_t ty);
int rbsr_stretch_accept(rbsr_stretch_t *rbss);
int rbsr_stretch_cancel(rbsr_stretch_t *rbss);

/* This is being accept and cancel; no need to call directly */
void rbsr_stretch_line_end(rbsr_stretch_t *rbss, rnd_bool apply);


/* Stretch the current line so it goes around tx;ty */
int rbsr_stretch_line_to_coords(rbsr_stretch_t *rbss, rnd_coord_t tx, rnd_coord_t ty);

/* Update stretch to crosshair; returns 1 if redraw is needed */
int rbsr_stretch_to_coords(rbsr_stretch_t *rbss);


