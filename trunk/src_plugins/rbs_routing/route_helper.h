
/* Return point that is close enough to tx;ty to route around; returns NULL if
   not found. If allow_incident is 1, also consider incident (into-the-point)
   routing. If allow_incident is 1 and result is routing incident, set
   refuse_incident to 0 (if it is not NULL). Uses rnd_pixel_slop since
   tx;ty is typically the crosshair. */
grbs_point_t *rbsr_crosshair_get_pt(rbsr_map_t *rbs, rnd_coord_t tx, rnd_coord_t ty, int allow_incident, int *refuse_incident);

/* given an existing endpoint the route is coming from (fromx;fromy) and a
   target point (tx;ty) and the point to route around (pt), compute the
   direction the route will go around pt. May return GRBS_ADIR_INC if
   tx;ty is within the copper of the point */
grbs_arc_dir_t rbsr_crosshair_get_pt_dir(rbsr_map_t *rbs, rnd_coord_t fromx, rnd_coord_t fromy, rnd_coord_t tx, rnd_coord_t ty, grbs_point_t *pt);
