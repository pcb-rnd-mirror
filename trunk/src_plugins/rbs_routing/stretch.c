/*
 *                            COPYRIGHT
 *
 *  pcb-rnd, interactive printed circuit board design - Rubber Band Stretch Router
 *  Copyright (C) 2024,2025 Tibor 'Igor2' Palinkas
 *  (Supported by NLnet NGI0 Entrust in 2024)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <libgrbs/route.h>
#include "search.h"
#include "remove.h"
#include "install.h"
#include "route_helper.h"

static int rbsr_stretch_install_2net(rbsr_stretch_t *rbss, grbs_2net_t *tn)
{
	pcb_layer_t *ly = pcb_get_layer(rbss->map.pcb->Data, rbss->map.lid);
	return rbsr_install_2net(ly, tn);
}


static int coll_ingore_tn_line(grbs_t *grbs, grbs_2net_t *tn, grbs_line_t *l)
{
	rnd_trace("ign coll line\n");
	return 1;
}

static int coll_ingore_tn_arc(grbs_t *grbs, grbs_2net_t *tn, grbs_arc_t *a)
{
	rnd_trace("ign coll arc\n");
	return 1;
}

static int coll_ingore_tn_point(grbs_t *grbs, grbs_2net_t *tn, grbs_point_t *p)
{
	rnd_trace("ign coll pt\n");
	return 1;
}

RND_INLINE void schedule_remove(rbsr_stretch_t *rbss, void *user_data)
{
	pcb_any_obj_t *obj = user_data;

	if (obj != NULL)
		htpp_set(&rbss->removes, obj, NULL);
}

RND_INLINE void stretch_get_addr(rbsr_stretch_t *rbss, grbs_addr_t *dst, grbs_arc_t *arc, grbs_arc_t **rem, int is_from)
{
	if (arc->r == 0) {
		/* incident */
		dst->type = ADDR_POINT;
		dst->obj.pt = arc->parent_pt;
	}
	else {
		dst->type = (is_from ? ADDR_ARC_END : ADDR_ARC_START) | ADDR_ARC_CONVEX;
		dst->obj.arc = arc;
		*rem = arc;
	}
	dst->last_real = NULL;
}

int rbsr_stretch_line_begin(rbsr_stretch_t *rbss, pcb_board_t *pcb, pcb_line_t *line, rnd_coord_t tx, rnd_coord_t ty)
{
	grbs_line_t *gl;
	grbs_2net_t *orig_tn;
	rnd_layer_id_t lid = pcb_layer_id(pcb->Data, line->parent.layer);
	grbs_arc_t *rem1 = NULL, *rem2 = NULL;

	htpp_init(&rbss->removes, ptrhash, ptrkeyeq);

	rbsr_map_pcb(&rbss->map, pcb, lid);
	rbsr_map_debug_draw(&rbss->map, "rbss1.svg");
	rbsr_map_debug_dump(&rbss->map, "rbss1.dump");

	rbss->map.grbs.user_data = rbss;
	rbss->map.grbs.coll_ingore_tn_line = coll_ingore_tn_line;
	rbss->map.grbs.coll_ingore_tn_arc = coll_ingore_tn_arc;
	rbss->map.grbs.coll_ingore_tn_point = coll_ingore_tn_point;


	gl = htpp_get(&rbss->map.robj2grbs, line);
	if (gl == NULL) {
		rnd_message(RND_MSG_ERROR, "rbsr_stretch_line_begin(): can't stretch this line (not in the grbs map)\n");
		return -1;
	}

	/* for jump-over preliminary drawing when there's no grbs realization */
	if (gl->RBSR_REVERSED) {
		rbss->fromx = line->Point2.X; rbss->fromy = line->Point2.Y;
		rbss->tox = line->Point1.X; rbss->toy = line->Point1.Y;
	}
	else {
		rbss->fromx = line->Point1.X; rbss->fromy = line->Point1.Y;
		rbss->tox = line->Point2.X; rbss->toy = line->Point2.Y;
	}
	rbss->gline = gl;
	gl->RBSR_WIREFRAME_FLAG = 1;

	orig_tn = grbs_arc_parent_2net(gl->a1);
	stretch_get_addr(rbss, &rbss->from, gl->a1, &rem1, 1);
	stretch_get_addr(rbss, &rbss->to, gl->a2, &rem2, 0);

	/* new grbs arcs will be created at the from and to side of the line; remember
	   the original pcb arc object for these so the new grbs arc objects'
	   user_data can be set to them so on install they are accepted as the
	   same arcs and copied back */
	rbss->from_arc_sa = gl->a1->sa;
	rbss->to_arc_ea = gl->a2->sa + gl->a2->da;

	rbss->snap = grbs_snapshot_save(&rbss->map.grbs);

	rbsr_map_debug_draw(&rbss->map, "rbss2.svg");
	rbsr_map_debug_dump(&rbss->map, "rbss2.dump");

	return 0;
}

void rbsr_stretch_line_end(rbsr_stretch_t *rbss, rnd_bool apply)
{
	if (apply) {
		htpp_entry_t *e;
		grbs_2net_t *tn;

		/* apply object removals */
		for(e = htpp_first(&rbss->removes); e != NULL; e = htpp_next(&rbss->removes, e)) {
			pcb_any_obj_t *obj = e->key;
			pcb_remove_object(obj->type, obj->parent.layer, obj, obj);
		}

		/* tune existing objects and install new objects */
		tn = grbs_arc_parent_2net(rbss->gline->a1);
		rbsr_stretch_install_2net(rbss, tn);
	}


	/* No need to free rbss->via separately: it's part of the grbs map */
	htpp_uninit(&rbss->removes);

	rbsr_map_uninit(&rbss->map);
}

int rbsr_stretch_any_begin(rbsr_stretch_t *rbss, pcb_board_t *pcb, rnd_coord_t x, rnd_coord_t y)
{
	void *ptr1, *ptr2, *ptr3;
	int type;

	rbss->jump_over_pt = NULL;

	type = pcb_search_obj_by_location(PCB_OBJ_LINE, &ptr1, &ptr2, &ptr3, x, y, 0);
	if (type == 0)
		type = pcb_search_obj_by_location(PCB_OBJ_LINE, &ptr1, &ptr2, &ptr3, x, y, rnd_pixel_slop);
	if (type == 0)
		type = pcb_search_obj_by_location(PCB_OBJ_LINE, &ptr1, &ptr2, &ptr3, x, y, rnd_pixel_slop*5);

	if (type == PCB_OBJ_LINE) {
		pcb_line_t *l = ptr2;
		return rbsr_stretch_line_begin(rbss, pcb, l, x, y);
	}

	if (type == PCB_OBJ_ARC) {
		rnd_message(RND_MSG_ERROR, "stretching arc is not yet supported\n");
		return -1;
	}

	rnd_message(RND_MSG_ERROR, "Failed to find a line or arc at that location\n");
	return -1;
}


int rbsr_stretch_to_coords(rbsr_stretch_t *rbss)
{
	grbs_point_t *pt;
	int changed = 0;

	rbss->acceptable = 0;

	/* jump over */
	pt = rbsr_crosshair_get_pt(&rbss->map, pcb_crosshair.X, pcb_crosshair.Y, 0, NULL);
	changed = (pt != rbss->jump_over_pt);


	rbss->jump_over_pt = pt;
	rbss->jump_over_consider = NULL;

	grbs_snapshot_restore(rbss->snap);


	if (pt != NULL) {
		grbs_addr_t *a1, *a2;
		grbs_arc_t *na, *toa, *fra;
		grbs_arc_dir_t dir;
		
		dir = rbsr_crosshair_get_pt_dir(&rbss->map, rbss->fromx, rbss->fromy, pcb_crosshair.X, pcb_crosshair.Y, pt);
		if (dir == GRBS_ADIR_INC)
			return 1;
		rnd_trace("jump-over: %p %d from: %$mm;%$mm\n", pt, dir, rbss->fromx, rbss->fromy);

		if (grbs_mod_split_line(&rbss->map.grbs, rbss->gline, pt, (dir & GRBS_ADIR_CONVEX_CW) ? -1 : +1) == 0)
			rbss->acceptable = 1;
	}
	else
		rnd_trace("jump-over: NULL\n");

	return 1;
}

int rbsr_stretch_accept(rbsr_stretch_t *rbss)
{
	if (!rbss->acceptable)
		return 0;

	rnd_trace("IMPLEMENT!\n");
	rbsr_stretch_line_end(rbss, 1);
	return 1;
}


int rbsr_stretch_cancel(rbsr_stretch_t *rbss)
{
	rbsr_stretch_line_end(rbss, 0);
	return 0;
}
